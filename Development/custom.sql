INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
('user_profile_photo', 'user', 'Edit My Photo', 'User_Plugin_Menus', '', 'user_profile', '', '1', '0', '2');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
('user_profile_style', 'user', 'Edit Profile Style', 'User_Plugin_Menus', '', 'user_profile', '', '1', '0', '3');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
('user_profile_portfolio', 'user', 'Edit Portfolio', 'User_Plugin_Menus', '', 'user_profile', '', '1', '0', '4');

ALTER TABLE `engine4_users` ADD `portfolio` TEXT DEFAULT NULL;

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `enabled`, `custom`, `order`) VALUES
('user_edit_portfolio', 'user', 'Edit My Portfolio', 'User_Plugin_Menus', '{"route":"user_extended","module":"user","controller":"edit","action":"portfolio"}', 'user_edit', '', '1', '0', '5');