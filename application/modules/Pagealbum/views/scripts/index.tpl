<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Pagealbum
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-09-06 17:53 idris $
 * @author     Idris
 */
?>

<?php if( $this->albums->getTotalItemCount() > 0 ): ?>
  <ul class="thumbs">
    <?php foreach( $this->albums as $album ): ?>
      <li>
        <a class="thumbs_photo" href="<?php echo $album->getHref(); ?>" onclick="page_album.view(<?php echo $album->getIdentity(); ?>); return false;" >
          <span style="background-image: url(<?php echo $album->getPhotoUrl('thumb.normal'); ?>);"></span>
        </a>
        <p class="thumbs_info">
          <span class="thumbs_title">
            <?php echo $this->htmlLink($album->getHref(),
                             $this->string()->chunk(Engine_String::substr($album->getTitle(), 0, 45), 10),
                             array('onclick' => "page_album.view({$album->getIdentity()}); return false;"))
            ?>
          </span>
          <?php echo $this->translate('By');?>
          <?php if (Engine_Api::_()->getDbTable('settings', 'core')->getSetting('page.show.owner', 0) == 1 || Engine_Api::_()->getItem('page', $album->page_id)->getOwner() != $album->getOwner()):?>
            <?php echo $this->htmlLink($album->getOwner()->getHref(), $album->getOwner()->getTitle(), array('class' => 'thumbs_author')) ?>
          <?php else:?>
            <?php echo $this->htmlLink(Engine_Api::_()->getItem('page', $album->page_id)->getHref(), Engine_Api::_()->getItem('page', $album->page_id)->getTitle())?>
          <?php endif;?>
          <br />
          <?php echo $this->translate(array('%s photo', '%s photos', $album->count()),$this->locale()->toNumber($album->count())) ?>
        </p>
      </li>
    <?php endforeach; ?>
  </ul>

  <?php if( $this->albums->count() > 1 ) : ?>
    <br />
    <?php echo $this->paginationControl($this->albums, null, array("pagination.tpl","pagealbum"), array(
      'page' => $this->pageObject
    ));?>
  <?php endif; ?>
<?php else: ?>
  <div class="tip">
    <span>
      <?php echo $this->translate('Nobody has created an album yet.');?>
      <?php if ($this->isAllowedPost): // @todo check if user is allowed to create an album ?>
        <?php echo $this->translate('Be the first to %1$screate%2$s one!', '<a href="javascript:void(0)" onClick="page_album.create();">', '</a>'); ?>
      <?php endif; ?>
    </span>
  </div>
<?php endif; ?>