<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Pagealbum
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2010-09-06 17:53 kirill $
 * @author     Kirill
 */
?>

<div class="clr"></div>
<div id="page_random_container">
  <div class="random-main">
    <ul class="random-list">
      <?php foreach($this->photos as $counter => $photo) : ?>
        <li>
          <a href="javascript:void(0);" class="thumbs_photo hePhotoThumb"
            onclick='page_album.photo_id=<?php echo $photo->getIdentity();?>;page_album.album=<?php echo $photo->collection_id;?>;page_album.init_album();page_album.view(<?php echo $photo->collection_id;?>);'>
            <i id="image_<?php echo $photo->getIdentity(); ?>" style="background-image: url('<?php echo $photo->getPhotoUrl('thumb.normal'); ?>')" ></i>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<div class="clr"></div>