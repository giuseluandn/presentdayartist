<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Style.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Form_Edit_Portfolio extends Engine_Form
{
  public function init()
  {
      $this->addElement('TinyMce', 'portfolio', array(
          'disableLoadDefaultDecorators' => true,
          'required' => true,
          'allowEmpty' => false,
          'decorators' => array(
              'ViewHelper'
          ),
          'filters' => array(
              new Engine_Filter_Censor(),
          )
      ));

      $this->addElement('Button', 'done', array(
      'label' => 'Save',
      'type' => 'submit',
    ));
  }
}