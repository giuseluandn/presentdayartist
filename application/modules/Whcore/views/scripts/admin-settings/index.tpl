<script type="text/javascript">
//<![CDATA[
function updateFields() {
  $$('input[name=wh_facebook_appid]').set('disabled', true);
  var new_value = $$('input[name=wh_facebook_type]:checked')[0].get('value');
  if ('0' == new_value)
    $$('input[name=wh_facebook_appid]')[0].set('disabled', false);
  else if ('1' == new_value)
    $$('input[name=wh_facebook_appid]').set('disabled', true);
}
window.addEvent('load', function(){
  updateFields();
});
//]]>
</script>

<?php if (count($this->navigation)): ?>
    <div class='tabs'>
        <?php
        echo $this->navigation()->menu()->setContainer($this->navigation)->render()
        ?>
    </div>
<?php endif; ?>

<div class='clear'>
    <div class='settings'>
		<div style="float:right; width:350px; padding-left:25px;">
		            <a class="twitter-timeline"  href="https://twitter.com/WebHiveTeam" data-widget-id="537898141614604288">Tweets by @WebHiveTeam</a>
		            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div>    
        <?php echo $this->form->render($this); ?>

</div>	   
</div>

