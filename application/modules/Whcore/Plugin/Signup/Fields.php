<?php

class Whcore_Plugin_Signup_Fields extends User_Plugin_Signup_Fields {

    protected $_adminFormClass = 'Whcore_Form_Admin_Signup_Fields';

    public function getForm() {
        $this->_form = parent::getForm();
        $accountSession = new Zend_Session_Namespace('User_Plugin_Signup_Account');
        $profileTypeValue = @$accountSession->data['profile_type'];

        if (!empty($_SESSION['wh_signup']) && Engine_Api::_()->hasModuleBootstrap($_SESSION['wh_signup']['type'])) {
            try {
                $data = $_SESSION['wh_signup']['data'];
                unset($data['email'], $data['username']);

                // populate fields
                $struct = $this->_form->getFieldStructure();
                foreach ($struct as $fskey => $map) {
                    $field = $map->getChild();
                    if ($field->isHeading())
                        continue;

                    if (isset($field->type) && in_array($field->type, array_keys($data))) {
                        $el_key = $map->getKey();
                        $el_val = $data[$field->type];
                        $el_obj = $this->_form->getElement($el_key);
                        if ($el_obj instanceof Zend_Form_Element &&
                                !$el_obj->getValue()) {
                            $el_obj->setValue($el_val);
                        }
                    }
                }
            } catch (Exception $e) {
                // Silence?
            }
        }

        return $this->_form;
    }

}
