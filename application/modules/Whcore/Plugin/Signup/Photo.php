<?php

class Whcore_Plugin_Signup_Photo extends User_Plugin_Signup_Photo {

    protected $_adminFormClass = 'Whcore_Form_Admin_Signup_Photo';

    public function __construct() {
        $this->_formClass = 'User_Form_Signup_Photo';

        $enabledModuleNames = Engine_Api::_()->getDbtable('modules', 'core')->getEnabledModuleNames();
        if (in_array('avatar', $enabledModuleNames)) {
            $this->_script = array('signup/form/photo.tpl', 'avatar');
            //$this->_formClass = 'Avatar_Form_Admin_Signup_Photo';
        }
    }

    public function onSubmit(Zend_Controller_Request_Abstract $request) {
        if ($request->getParam("uploadPhoto") == 'webcam' or $request->getParam("uploadPhoto") == 'avatar') {
            $this->getSession()->data['Filedata'] = $_SESSION['TemporaryProfileImg'];
            $this->getSession()->data['File_check'] = $_SESSION['TemporaryProfileImg'];
            $this->getSession()->active = true;
            $this->onSubmitNotIsValid();
            return false;
        } else {
            parent::onSubmit($request);
        }
    }

    public function onView() {
        parent::onView();

        if (!empty($_SESSION['wh_signup']) && Engine_Api::_()->hasModuleBootstrap($_SESSION['wh_signup']['type'])) {
            try {
                $this->_fetchImage($_SESSION['wh_signup']['data']['photo']);
            } catch (Exception $e) {
                // Silence?
            }
        }
    }

    public function onAdminProcess($form) {
        $step_table = Engine_Api::_()->getDbtable('signup', 'user');
        $step_row = $step_table->fetchRow($step_table->select()->where('class = ?', 'Whcore_Plugin_Signup_Photo'));
        $step_row->enable = $form->getValue('enable');
        $step_row->save();

        $settings = Engine_Api::_()->getApi('settings', 'core');
        $values = $form->getValues();
        $settings->user_signup_photo = $values['require_photo'];

        $form->addNotice('Your changes have been saved.');
    }

}
