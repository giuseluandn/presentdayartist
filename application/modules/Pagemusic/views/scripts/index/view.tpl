<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Pagemusic
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: view.tpl 2010-10-21 17:53 idris $
 * @author     Idris
 */
?>
<?php if (count($this->songs) > 0): ?>
<script type="text/javascript">
	AudioPlayer.setup("<?php echo $this->baseUrl(); ?>/application/modules/Pagemusic/externals/standalone/player.swf", {
		width: 290,
		initialvolume: 100,
		transparentpagebg: "yes",
    leftbg: "339BCB",
		lefticon: "FFFFFF",
    righticon: "FFFFFF",
    righticonhover: "000000",
    voltrack: "FFFFFF",
    volslider: "000000",
    rightbg: "339BCB",
    rightbghover: "339BCB",
    loader: "339BCB"
	});
</script>
<?php endif; ?>
<div class="page-music-view">

	<div class="page-music-view-item">
    <div class="page-music-view-item-cover playlist-cover">
      <a href="<?php echo $this->playlist->getHref(); ?>" onclick="page_music.view(<?php echo $this->playlist->getIdentity(); ?>); return false;">
        <?php
          $photo_url = $this->playlist->photo_id ? $this->playlist->getPhotoUrl() : "application/modules/Pagemusic/externals/images/nophoto.jpg";
        ?>
        <img src="<?php echo $photo_url; ?>" class="pagemusic_art" width="174" alt="" />
        <span class="jewelcase"></span>
      </a>
    </div>
    <div class="page-music-view-item-info">
      <a class="title" href="<?php echo $this->playlist->getHref(); ?>" onclick="page_music.view(<?php echo $this->playlist->getIdentity(); ?>); return false;"><?php echo $this->playlist->getTitle(); ?></a>
      <p class="stats">
				<?php echo $this->translate(array('pagemusic_%s play', 'pagemusic_%s plays', $this->playlist->play_count), ($this->playlist->play_count)); ?> 
				(<?php echo $this->translate(array('pagemusic_%s listener', 'pagemusic_%s listeners', $this->playlist->listener_count), ($this->playlist->listener_count)); ?>)
			</p>
      <p class="label">
        <?php echo $this->playlist->getDescription(); ?><br /><br />
				<?php echo $this->translate(array('pagemusic_%s track', 'pagemusic_%s tracks', $this->playlist->track_count), ($this->playlist->track_count)); ?>
      </p>
			<?php if ($this->tagString): ?>
			<div class="page-tag">
				<div class="tags">
					<?php echo $this->tagString; ?>
				</div>
				<div class="clr"></div>
			</div>
			<?php endif; ?>

			<div class="page-misc">
				<div class="page-misc-date">
					<?php echo $this->translate("pagemusic_Posted %s", $this->timestamp($this->playlist->creation_date)); ?>
				</div>
				<?php if (count($this->musicTags)):?>
				<div class="page-tag">
					<div class="tags">
            <?php foreach ($this->musicTags as $tag): ?>
              <a href='javascript:void(0);' onclick="page_search.search_by_tag(<?php echo $tag->getTag()->tag_id; ?>);">#<?php echo $tag->getTag()->text ?></a>&nbsp;
            <?php endforeach; ?>
					</div>
					<div class="clr"></div>
				</div>
				<?php endif; ?>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>

			<div class="playlist_share_wrapper">
				<?php echo $this->htmlLink( $this->url(array('module' => 'activity', 'controller' => 'index', 'action' => 'share', 'type' => $this->playlist->getType(), 'id' => $this->playlist->getIdentity(), 'format' => 'smoothbox'), 'default', true), $this->translate('Share Playlist'), array('class' => 'smoothbox')); ?>
			</div>
			<div class="clr"></div>
			<?php if ($this->pageObject->isTeamMember()): ?>
				<p class="options">
					<a class="edit" href="javascript:page_music.edit(<?php echo $this->playlist->getIdentity(); ?>)"></a>
					<a class="delete" href="javascript:page_music.confirm_delete(<?php echo $this->playlist->getIdentity(); ?>)"></a>
				</p>
			<?php endif; ?>
    </div>
		<?php if (!$this->isAllowedPost): ?>
			<div class="backlink_wrapper">
				<a class="backlink" href="javascript:page_music.index()"><?php echo $this->translate('pagemusic_Back To Playlists'); ?></a>
			</div>
		<?php endif; ?>
		<div class="clr"></div>
  </div>

	<div class="clr"></div>

	<?php if (count($this->songs) > 0): ?>
	<div class="page-music-view-songs">
		<table cellpadding="0" cellspacing="0" class="tracklist">
			<thead>
				<tr class="thead">
					<td class="number" width="1%">#</td>
					<td class="title"><?php echo $this->translate("pagemusic_Track"); ?></td>
					<?php if ($this->viewer->getIdentity()): ?>
						<td class="share" width="5%"></td>
					<?php endif; ?>
					<td class="listen" width="290px"></td>
					<td class="listeners" width="1%"><?php echo $this->translate("pagemusic_Listeners"); ?></td>
				</tr>
			</thead>
			<tbody>
				<?php $number = 0; ?>
				<?php foreach($this->songs as $song) :?>
				<?php $number++; ?>
				<tr class="song">
					<td class="number" width="1%"><span class="misc_info"><?php echo $number; ?>.</span></td>
					<td class="title"><?php echo $song->getTitle(); ?></td>
					<?php if ($this->viewer->getIdentity()): ?>
						<td class="share" width="5%">
							<div class="share_wrapper">
								<?php echo $this->htmlLink( $this->url(array('module' => 'activity', 'controller' => 'index', 'action' => 'share', 'type' => $song->getType(), 'id' => $song->getIdentity(), 'format' => 'smoothbox'), 'default', true), $this->translate('Share'), array('class' => 'smoothbox')); ?>
							</div>
						</td>
					<?php endif; ?>
					<td class="listen" width="290px">
						<div id="song_wrapper_<?php echo $song->getIdentity(); ?>" style="margin-bottom: -2px">
							<div id="song_<?php echo $song->getIdentity(); ?>"></div>
						</div>

						<script type="text/javascript">
							AudioPlayer.embed("song_<?php echo $song->getIdentity(); ?>", {soundFile: "<?php echo $this->storage->get($song->file_id)->map(); ?>", titles: "<?php echo $song->getTitle(); ?>"});
						</script>
					</td>
					<td class="listeners" width="1%"><span class="misc_info"><?php echo $song->listener_count; ?></span></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php else: ?>
		<br />
		<div class="tip"><span><?php echo $this->translate('pagemusic_NO_SONGS_TEXT'); ?></span></div>
	<?php endif; ?>



  <?php if (Engine_Api::_()->getDbTable('modules' ,'hecore')->isModuleEnabled('wall')): ?>
    <?php echo $this->wallComments($this->playlist, $this->viewer(), array('class' => 'page-music-view-comments')); ?>
  <?php else: ?>
	<div class="comments page-music-view-comments" id="playlist_comments"></div>
  <?php endif;?>

</div>