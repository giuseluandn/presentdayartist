<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Pagemusic
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2012-02-20 15:35 Ulan T $
 * @author     Ulan T
 */
?>
<?php
$this->headScript()
  ->appendFile($this->layout()->staticBaseUrl . 'externals/swfobject/swfobject.js')
  ->appendFile($this->layout()->staticBaseUrl . 'externals/flowplayer/flashembed-1.0.1.pack.js')
  ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Pagemusic/externals/scripts/music.js')
  ->appendFile($this->layout()->staticBaseUrl . 'application/modules/Pagemusic/externals/standalone/audio-player.js');
?>
<script type="text/javascript">
  AudioPlayer.setup("<?php echo $this->layout()->staticBaseUrl; ?>application/modules/Pagemusic/externals/standalone/player.swf", {
    width: 290,
    initialvolume: 100,
    transparentpagebg: "yes",
    leftbg: "339BCB",
    lefticon: "FFFFFF",
    righticon: "FFFFFF",
    righticonhover: "000000",
    voltrack: "FFFFFF",
    volslider: "000000",
    rightbg: "339BCB",
    rightbghover: "339BCB",
    loader: "339BCB"
  });
</script>

<script type="text/javascript">
  en4.core.runonce.add(function(){

  <?php if( !$this->renderOne ): ?>
    var anchor = $('profile_pagemusic').getParent();
    $('profile_pagemusic_previous').style.display = '<?php echo ( $this->paginator->getCurrentPageNumber() == 1 ? 'none' : '' ) ?>';
    $('profile_pagemusic_next').style.display = '<?php echo ( $this->paginator->count() == $this->paginator->getCurrentPageNumber() ? 'none' : '' ) ?>';

    $('profile_pagemusic_previous').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() - 1) ?>
        }
      }), {
        'element' : anchor
      })
    });

    $('profile_pagemusic_next').removeEvents('click').addEvent('click', function(){
      en4.core.request.send(new Request.HTML({
        url : en4.core.baseUrl + 'widget/index/content_id/' + <?php echo sprintf('%d', $this->identity) ?>,
        data : {
          format : 'html',
          subject : en4.core.subject.guid,
          page : <?php echo sprintf('%d', $this->paginator->getCurrentPageNumber() + 1) ?>
        }
      }), {
        'element' : anchor
      })
    });
    <?php endif; ?>
  });
</script>

<div id="profile_pagemusic">
  <?php foreach($this->paginator as $item) : ?>
  <?php
    if( $item['type'] == 'music' )
      $playlist = Engine_Api::_()->getItem('music_playlist', $item['playlist_id']);
    else
      $playlist = Engine_Api::_()->getItem('playlist', $item['playlist_id']);
  ?>
  <div class="page-music-view-songs">
    <table cellpadding="0" cellspacing="0" class="tracklist">
      <thead>
      <tr class="thead" id="my_playlist_<?php echo $item['type'].'_'.$item['playlist_id'] ;?>">
        <td class="number" width="1%">#</td>
        <td class="title">
          <?php echo $this->htmlLink($playlist->getHref(), $playlist->getTitle()); ?>
        </td>
        <td width="300px">Player</td>
      </tr>
      </thead>
      <tbody >
      <?php $number = 0; ?>
      <?php foreach($playlist->getSongs() as $song) :?>
        <?php $number++; ?>
        <tr class="song">
          <td class="number" width="1%"><span class="misc_info"><?php echo $number; ?>.</span></td>
          <td class="title"><?php echo $song->getTitle(); ?></td>
          <td class="listen" width="300px" align="right">
            <div id="song_wrapper_<?php echo $item['type'].'_'.$song->getIdentity(); ?>" style="margin-bottom: -2px">
              <div id="song_<?php echo $item['type'].'_'.$song->getIdentity(); ?>"></div>
            </div>

          <script type="text/javascript">
            en4.core.runonce.add(function(){
            AudioPlayer.embed("song_<?php echo $item['type'].'_'.$song->getIdentity(); ?>", {soundFile: "<?php echo $this->storage->get($song->file_id)->map(); ?>", titles: "<?php echo $song->getTitle(); ?>"});
            });
          </script>
          </td>
        </tr>
      <?php endforeach; ?>
      </tbody>

    </table>
  </div>
  <?php endforeach; ?>
</div>

<div>
  <div id="profile_pagemusic_previous" class="paginator_previous">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Previous'), array(
    'onclick' => '',
    'class' => 'buttonlink icon_previous'
  )); ?>
  </div>
  <div id="profile_pagemusic_next" class="paginator_next">
    <?php echo $this->htmlLink('javascript:void(0);', $this->translate('Next'), array(
    'onclick' => '',
    'class' => 'buttonlink_right icon_next'
  )); ?>
  </div>
</div>