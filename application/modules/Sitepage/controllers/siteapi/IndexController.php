<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Siteapi
 * @copyright  Copyright 2015-2016 BigStep Technologies Pvt. Ltd.
 * @license    http://www.socialengineaddons.com/license/
 * @version    IndexController.php 2015-09-17 00:00:00Z SocialEngineAddOns $
 * @author     SocialEngineAddOns
 */

class Sitepage_IndexController extends Siteapi_Controller_Action_Standard {

    /**
     * Auth checkup and creating the subject.
     * 
     */
    public function init() {
        //AUTHORIZATION CHECK
        // if (!$this->_helper->requireAuth()->setAuthParams('sitepage_page', null, "view")->isValid())
        // 	$this->respondWithError('unauthorized');
        $viewer = Engine_Api::_()->user()->getViewer();

        if (!Engine_Api::_()->authorization()->isAllowed('sitepage_page', $viewer, 'view'))
            $this->respondWithError('unauthorized');

        if ($this->getRequestParam("page_id") && (0 !== ($page_id = (int) $this->getRequestParam("page_id")) &&
                null !== ($page = Engine_Api::_()->getItem('sitepage_page', $page_id)))) {
            Engine_Api::_()->core()->setSubject($page);
        }
    }
    
    /**
     * Returns the Directory page listings matching the get parameters with pagination.
     * 
     * @return pagination of Directory Page listings
     */
    public function browseAction() {
        // Validate request methods
        $this->validateRequestMethod();

        $getRequest = $this->getRequestAllParams;
        $getRequest['draft'] = 0;
        $getRequest['visible'] = 1;
        $getRequest['page'] = (isset($getRequest['page'])) ? $getRequest['page'] : 1;
        $getRequest['limit'] = !isset($getRequest['limit']) ? $getRequest['limit'] : 20;

        try {
            $response = $this->_getDirectoryPages($getRequest);

            $this->respondWithSuccess($response, true);
        } catch (Exception $ex) {
            $this->respondWithValidationError('internal_server_error', $ex->getMessage());
        }
    }
    
    /**
     * Returns the Users Directory page listings matching the get parameters with pagination.
     * 
     * @return pagination of Directory Page listings
     */
    public function manageAction() {
        // Validate request methods
        $this->validateRequestMethod();

    	$viewer = $viewer = Engine_Api::_()->user()->getViewer();
    	if(!$viewer->getIdentity())
    		$this->respondWithError('unauthorized');

        $getRequest = $this->getRequestAllParams;
        $getRequest['draft'] = 0;
        $getRequest['manage'] = $getRequest['visible'] = 1;
        $getRequest['user_id'] = Engine_Api::_()->user()->getViewer()->getIdentity();
        $getRequest['page'] = (isset($getRequest['page'])) ? $getRequest['page'] : 1;
        $getRequest['limit'] = !isset($getRequest['limit']) ? $getRequest['limit'] : 20;
        
        try {
            $response = $this->_getDirectoryPages($getRequest);
            $this->respondWithSuccess($response, true);
        } catch (Exception $ex) {
            $this->respondWithValidationError('internal_server_error', $ex->getMessage());
        }
    }

    /**
     * Returns the create DirectoryPage form or Stores data and creates a Directory Page.
     * 
     */
    public function createAction() {
        $viewer = Engine_Api::_()->user()->getViewer();
        $viewer_id = $viewer->getIdentity();
        if (!$viewer_id)
            $this->respondWithError('unauthorized');

        if (!Engine_Api::_()->authorization()->isAllowed('sitepage_page', $viewer, 'create'))
            $this->respondWithError('unauthorized');

        $quota = Engine_Api::_()->authorization()->getPermission($viewer->level_id, 'sitepage_page', "max");
        $currentCount = 10;

        if ($currentCount >= $quota)
                $this->respondWithError('page_creation_quota_exceed');
        
        // Get directory page form 
        if($this->getRequest()->isGet()){
            $form_fields = Engine_Api::_()->getApi('Siteapi_Core', 'sitepage')->getForm();
            $this->respondWithSuccess($form_fields);
        }
        
        // If method not Post or form not valid , Return
        if ($this->getRequest()->isPost()) {

            $sitepageUrlEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepageurl');
            $table = Engine_Api::_()->getItemTable('sitepage_page');
            $db = $table->getAdapter();
            $db->beginTransaction();

            try {
                // Create sitepage
                $values = $this->getAllParams();

                // Start form validation
                $validators = Engine_Api::_()->getApi('Siteapi_FormValidators', 'sitepage')->getFormValidators();
                $values['validators'] = $validators;
                $values['package_id'] = 1;
                $validationMessage = $this->isValid($values);

                // Response validation error
                if (!empty($validationMessage) && @is_array($validationMessage)) {
                    $this->respondWithValidationError('validation_fail', $validationMessage);
                }
                
                // Custom field work
                $categoryIds = array();
                $categoryIds[] = $values['category_id'];
                $categoryIds[] = $values['subcategory_id'];
                $categoryIds[] = $values['subsubcategory_id'];

                $values['profile_type'] = Engine_Api::_()->getDbTable('categories', 'sitepage')->getProfileType($categoryIds, 0, 'profile_type');
                if (isset($values['profile_type']) && !empty($values['profile_type'])) {
                    // START FORM VALIDATION
                    $profileFieldsValidators = Engine_Api::_()->getApi('Siteapi_FormValidators', 'sitepage')->getFieldsFormValidations($values);
                    $values['validators'] = $profileFieldsValidators;
                    $profileFieldsValidationMessage = $this->isValid($values);
                }

                if (is_array($eventValidationMessage) && is_array($profileFieldsValidationMessage))
                    $validationMessage = array_merge($eventValidationMessage, $profileFieldsValidationMessage);
                else if (is_array($eventValidationMessage))
                    $validationMessage = $eventValidationMessage;
                else if (is_array($profileFieldsValidationMessage))
                    $validationMessage = $profileFieldsValidationMessage;
                else
                    $validationMessage = 1;

                if (!empty($validationMessage) && @is_array($validationMessage)) {
                    $this->respondWithValidationError('validation_fail', $validationMessage);
                }
                
                // End form validation

                $values['owner_id'] = $viewer->getIdentity();
                $values['subcategory_id'] = (isset($values['subcategory_id']) && !empty($values['subcategory_id'])) ? $values['subcategory_id'] : 0;
                $values['subsubcategory_id'] = (isset($values['subsubcategory_id']) && !empty($values['subsubcategory_id'])) ? $values['subsubcategory_id'] : 0;
                $sitepage = $table->createRow();
                if (Engine_Api::_()->getApi('subCore', 'sitepage')->pageBaseNetworkEnable()) {
                    if (isset($values['networks_privacy']) && !empty($values['networks_privacy'])) {
                        if (in_array(0, $values['networks_privacy'])) {
                            $values['networks_privacy'] = new Zend_Db_Expr('NULL');
                        } else if(is_array($values['networks_privacy'])) {
                            $values['networks_privacy'] = (string) join(",", $values['networks_privacy']);
                        }
                    }
                }
                $sitepage->setFromArray($values);
                $package = Engine_Api::_()->getItem('sitepage_package', $sitepage->package_id);

                 if (!Engine_Api::_()->sitepage()->hasPackageEnable()) {
                   $sitepage->featured = Engine_Api::_()->authorization()->getPermission($user_level, 'sitepage_page', 'featured');
                   $sitepage->sponsored = Engine_Api::_()->authorization()->getPermission($user_level, 'sitepage_page', 'sponsored');
                   $sitepage->approved = Engine_Api::_()->authorization()->getPermission($user_level, 'sitepage_page', 'approved');
                 } else {
                   $sitepage->featured = $package->featured;
                   $sitepage->sponsored = $package->sponsored;
                   if ($package->isFree() && !empty($sitepage_is_approved) && !empty($getPackageAuth)) {
                     $sitepage->approved = $package->approved;
                   } else {
                     $sitepage->approved = 0;
                   }
                 }

                if (!empty($sitepage->approved)) {
                    $sitepage->pending = 0;
                    $sitepage->aprrove_date = date('Y-m-d H:i:s');

                    if (Engine_Api::_()->sitepage()->hasPackageEnable()) {
                        $expirationDate = $package->getExpirationDate();
                        if (!empty($expirationDate))
                            $sitepage->expiration_date = date('Y-m-d H:i:s', $expirationDate);
                        else
                            $sitepage->expiration_date = '2250-01-01 00:00:00';
                    }
                    else {
                        $sitepage->expiration_date = '2250-01-01 00:00:00';
                    }
                }

                $sitepage->save();
                
                if (!empty($sitepage->approved)) {
                    Engine_Api::_()->sitepage()->sendMail("ACTIVE", $sitepage->page_id);
                } else {
                    Engine_Api::_()->sitepage()->sendMail("APPROVAL_PENDING", $sitepage->page_id);
                }

                $manageadminsTable = Engine_Api::_()->getDbtable('manageadmins', 'sitepage');
                $row = $manageadminsTable->createRow();
                $row->user_id = $sitepage->owner_id;
                $row->page_id = $sitepage->page_id;
                $row->save();

                // Start profile maps work
                Engine_Api::_()->getDbtable('profilemaps', 'sitepage')->profileMapping($sitepage);
                $page_id = $sitepage->page_id;
                
                if (!empty($sitepageUrlEnabled)) {
                    $values['page_url'] = trim(preg_replace('/-+/', '-', preg_replace('/[^a-z0-9-]+/i', '-', strtolower($values['title']))), '-');
                    if (!empty($count_index) || !empty($count_index_url)) {
                        $values['page_url'] = $values['page_url'] . '-' . $page_id;
                        $table->update(array('page_url' => $values['page_url']), array('page_id = ?' => $page_id));
                    } else {
                        $values['page_url'] = $values['page_url'];
                        $table->update(array('page_url' => $values['page_url']), array('page_id = ?' => $page_id));
                    }
                }

                $sitepageFormEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepageform');
                if ($sitepageFormEnabled) {
                    $tablecontent = Engine_Api::_()->getDbtable('content', 'core');
                    $params = $tablecontent->select()
                                    ->from($tablecontent->info('name'), 'params')
                                    ->where('name = ?', 'sitepageform.sitepage-viewform')
                                    ->query()->fetchColumn();
                    $decodedParam = Zend_Json::decode($params);
                    $tabName = $decodedParam['title'];
                    if (empty($tabName))
                        $tabName = 'Form';
                    
                    $sitepageformtable = Engine_Api::_()->getDbtable('sitepageforms', 'sitepageform');
                    $optionid = Engine_Api::_()->getDbtable('pagequetions', 'sitepageform');
                    $table_option = Engine_Api::_()->fields()->getTable('sitepageform', 'options');
                    $sitepageform = $table_option->createRow();
                    $sitepageform->setFromArray($values);
                    $sitepageform->label = $values['title'];
                    $sitepageform->field_id = 1;
                    $option_id = $sitepageform->save();
                    $optionids = $optionid->createRow();
                    $optionids->option_id = $option_id;
                    $optionids->page_id = $page_id;
                    $optionids->save();
                    $sitepageforms = $sitepageformtable->createRow();
                    if (isset($sitepageforms->offer_tab_name))
                        $sitepageforms->offer_tab_name = $tabName;
                    $sitepageforms->description = 'Please leave your feedback below and enter your contact details.';
                    $sitepageforms->page_id = $page_id;
                    $sitepageforms->save();
                }

                // Set photo
                if (!empty($_FILES)) {
                    Engine_Api::_()->getApi('Siteapi_Core', 'sitepage')->setPhoto($_FILES['photo'], $sitepage);
                    $albumTable = Engine_Api::_()->getDbtable('albums', 'sitepage');
                    $album_id = $albumTable->update(array('photo_id' => $sitepage->photo_id), array('page_id = ?' => $sitepage->page_id));
                }

                // Add tags
                $tags = preg_split('/[,]+/', $values['tags']);
                $tags = array_filter(array_map("trim", $tags));
                $sitepage->tags()->addTagMaps($viewer, $tags);

                if (!empty($page_id)) {
                    $sitepage->setLocation();
                }

                // Set privacy
                $auth = Engine_Api::_()->authorization()->context;

                // Get the page admin list.
                $ownerList = $sitepage->getPageOwnerList();

                $sitepagememberEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagemember');
                if (!empty($sitepagememberEnabled)) {
                    $roles = array('owner', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                } else {
                    $roles = array('owner', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                }

                $values['auth_view'] = (isset($values['auth_view']) && !empty($values['auth_view'])) ? $values['auth_view'] : "everyone";
                $values['auth_comment'] = (isset($values['auth_comment']) && !empty($values['auth_comment'])) ? $values['auth_comment'] : "everyone";
                $viewMax = array_search($values['auth_view'], $roles);
                $commentMax = array_search($values['auth_comment'], $roles);

                foreach ($roles as $i => $role) {
                    $auth->setAllowed($sitepage, $role, 'view', ($i <= $viewMax));
                    $auth->setAllowed($sitepage, $role, 'comment', ($i <= $commentMax));
                    $auth->setAllowed($sitepage, $role, 'print', 1);
                    $auth->setAllowed($sitepage, $role, 'tfriend', 1);
                    $auth->setAllowed($sitepage, $role, 'overview', 1);
                    $auth->setAllowed($sitepage, $role, 'map', 1);
                    $auth->setAllowed($sitepage, $role, 'insight', 1);
                    $auth->setAllowed($sitepage, $role, 'layout', 1);
                    $auth->setAllowed($sitepage, $role, 'contact', 1);
                    $auth->setAllowed($sitepage, $role, 'form', 1);
                    $auth->setAllowed($sitepage, $role, 'offer', 1);
                    $auth->setAllowed($sitepage, $role, 'invite', 1);
                }

                $sitepagememberEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagemember');
                if (!empty($sitepagememberEnabled)) {
                    $roles = array('owner', 'like_member', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                } else {
                    $roles = array('owner', 'like_member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                }

                // Start work for sub page.
                $values['auth_sspcreate'] = (isset($values['auth_sspcreate']) && !empty($values['auth_sspcreate'])) ? $values['auth_sspcreate'] : "owner";

                $createMax = array_search($values['auth_sspcreate'], $roles);
                foreach ($roles as $i => $role) {
                    if ($role === 'like_member') {
                        $role = $ownerList;
                    }
                    $auth->setAllowed($sitepage, $role, 'sspcreate', ($i <= $createMax));
                }
                // End work for subpage
                
                // Start sitepagediscussion plugin work      
                $sitepagediscussionEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagediscussion');
                if ($sitepagediscussionEnabled) {
                    
                    // Start discussion privacy work
                    if (empty($values['sdicreate'])) {
                        $values['sdicreate'] = "registered";
                    }

                    $createMax = array_search($values['sdicreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'sdicreate', ($i <= $createMax));
                    }
                    // End discussion privacy work
                }
                
                // End sitepagediscussion plugin work
               
                
                
                // Start sitepagealbum plugin work
                $sitepagealbumEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagealbum');
                if ($sitepagealbumEnabled) {
                    // Start photo privacy work
                    $values['spcreate'] = (isset($values['spcreate']) && !empty($values['spcreate'])) ? $values['spcreate'] : "registered";
                    $createMax = array_search($values['spcreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'spcreate', ($i <= $createMax));
                    }
                    // End photo privacy work
                }

                // End sitepagealbum privacy work


                // Start sitepagesocument privacy work
                $sitepageDocumentEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagedocument');
                if ($sitepageDocumentEnabled) {
                    $sitepagememberEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagemember');
                    if (!empty($sitepagememberEnabled)) {
                        $roles = array('owner', 'like_member', 'member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                    } else {
                        $roles = array('owner', 'like_member', 'owner_member', 'owner_member_member', 'owner_network', 'registered', 'everyone');
                    }

                    $values['sdcreate'] = (isset($values['sdcreate']) && !empty($values['sdcreate'])) ? $values['sdcreate'] : "registered";

                    $createMax = array_search($values['sdcreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'sdcreate', ($i <= $createMax));
                    }
                }
                // End sitepagedocument privacy work


                // Start sitepagevideo privacy work
                $sitepageVideoEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagevideo');
                if ($sitepageVideoEnabled || (Engine_Api::_()->hasModuleBootstrap('sitevideo') && Engine_Api::_()->getDbtable('modules', 'sitevideo')->getIntegratedModules(array('enabled' => 1, 'item_type' => 'sitepage_page', 'item_module' => 'sitepage')))) {
                    $values['svcreate'] = (isset($values['svcreate']) && !empty($values['svcreate'])) ? $values['svcreate'] : "registered";

                    $createMax = array_search($values['svcreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'svcreate', ($i <= $createMax));
                    }
                }
                // End sitepagevideo privacy work


                // Start sitepagepoll privacy work
                $sitepagePollEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagepoll');
                if ($sitepagePollEnabled) {
                    $values['splcreate'] = (isset($values['splcreate']) && !empty($values['splcreate'])) ? $values['splcreate'] : "registered";

                    $createMax = array_search($values['splcreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'splcreate', ($i <= $createMax));
                    }
                }
                // End sitepagepoll privacy work

                // Start sitepagenote privacy work
                $sitepageNoteEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagenote');
                if ($sitepageNoteEnabled) {
                    $values['sncreate'] = (isset($values['sncreate']) && !empty($values['sncreate'])) ? $values['sncreate'] : "registered";

                    $createMax = array_search($values['sncreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'sncreate', ($i <= $createMax));
                    }
                }


                // End sitepagenote privacy work

                // Start sitepagemusic privacy work
                $sitepageMusicEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagemusic');
                if ($sitepageMusicEnabled) {
                    $values['smcreate'] = (isset($values['smcreate']) && !empty($values['smcreate'])) ? $values['smcreate'] : "registered";

                    $createMax = array_search($values['smcreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'smcreate', ($i <= $createMax));
                    }
                }
                // End sitepagemusic privacy work


                // Start sitepageevent privacy work
                $sitepageeventEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepageevent');
                if ($sitepageeventEnabled || (Engine_Api::_()->hasModuleBootstrap('siteevent') && Engine_Api::_()->getDbtable('modules', 'siteevent')->getIntegratedModules(array('enabled' => 1, 'item_type' => 'sitepage_page', 'item_module' => 'sitepage')))) {
                    $values['secreate'] = (isset($values['secreate']) && !empty($values['secreate'])) ? $values['secreate'] : "registered";

                    $createMax = array_search($values['secreate'], $roles);
                    foreach ($roles as $i => $role) {
                        if ($role === 'like_member') {
                            $role = $ownerList;
                        }
                        $auth->setAllowed($sitepage, $role, 'secreate', ($i <= $createMax));
                    }
                }
                // End sitepageevent privacy work

                // Start sitepagemember privacy work
                $sitepageMemberEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagemember');
                if ($sitepageMemberEnabled) {
                    $membersTable = Engine_Api::_()->getDbtable('membership', 'sitepage');
                    $row = $membersTable->createRow();
                    $row->resource_id = $sitepage->page_id;
                    $row->page_id = $sitepage->page_id;
                    $row->user_id = $sitepage->owner_id;
                    $row->notification = '0';
                    //$row->action_notification = '["posted","created"]';
                    $row->save();
                    Engine_Api::_()->sitepage()->updateMemberCount($sitepage);
                    $sitepage->save();
                }
                $memberInvite = Engine_Api::_()->getApi('settings', 'core')->getSetting('pagemember.invite.option', 1);
                $member_approval = Engine_Api::_()->getApi('settings', 'core')->getSetting('pagemember.member.approval.option', 1);
                if (empty($memberInvite)) {
                    $memberInviteOption = Engine_Api::_()->getApi('settings', 'core')->getSetting('pagemember.invite.automatically', 1);
                    $sitepage->member_invite = $memberInviteOption;
                    $sitepage->save();
                }
                if (empty($member_approval)) {
                    $member_approvalOption = Engine_Api::_()->getApi('settings', 'core')->getSetting('pagemember.member.approval.automatically', 1);
                    $sitepage->member_approval = $member_approvalOption;
                    $sitepage->save();
                }
                // End sitepagemember privacy work

                // Start business integration work
                $business_id = $this->_getParam('business_id');
                if (!empty($business_id)) {
                    $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
                    $moduleEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitebusinessintegration');
                    if (!empty($moduleEnabled)) {
                        $contentsTable = Engine_Api::_()->getDbtable('contents', 'sitebusinessintegration');
                        $row = $contentsTable->createRow();
                        $row->owner_id = $viewer_id;
                        $row->resource_owner_id = $sitepage->owner_id;
                        $row->business_id = $business_id;
                        $row->resource_type = 'sitepage_page';
                        $row->resource_id = $sitepage->page_id;
                        $row->save();
                    }
                }
                $group_id = $this->_getParam('group_id');
                if (!empty($group_id)) {
                    $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
                    $moduleEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitegroupintegration');
                    if (!empty($moduleEnabled)) {
                        $contentsTable = Engine_Api::_()->getDbtable('contents', 'sitegroupintegration');
                        $row = $contentsTable->createRow();
                        $row->owner_id = $viewer_id;
                        $row->resource_owner_id = $sitepage->owner_id;
                        $row->group_id = $group_id;
                        $row->resource_type = 'sitepage_page';
                        $row->resource_id = $sitepage->page_id;
                        $row->save();
                    }
                }
                // End business integration work

                // Start store integration work
                $store_id = $this->_getParam('store_id');
                if (!empty($store_id)) {
                    $viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
                    $moduleEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitestoreintegration');
                    $sitestoreEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitestore');
                    if (!empty($moduleEnabled) && !empty($sitestoreEnabled)) {
                        $contentsTable = Engine_Api::_()->getDbtable('contents', 'sitestoreintegration');
                        $row = $contentsTable->createRow();
                        $row->owner_id = $viewer_id;
                        $row->resource_owner_id = $sitepage->owner_id;
                        $row->store_id = $store_id;
                        $row->resource_type = 'sitepage_page';
                        $row->resource_id = $sitepage->page_id;
                        $row->save();
                    }
                }
                // End store integration work

                // Start subpage work
                $parent_id = $this->_getParam('parent_id');
                if (!empty($parent_id)) {
                    $sitepage->subpage = 1;
                    $sitepage->parent_id = $parent_id;
                    $sitepage->save();
                }

                // Custom field work
                if (Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepage.profile.fields', 1)) {

                    $mapData = Engine_Api::_()->getApi('core', 'fields')->getFieldsMaps('sitepage_page');

                    // Getting profile fields
                    $getRowsMatching = $mapData->getRowsMatching('option_id', $values['profile_type']);
                    $fieldValues = Engine_Api::_()->fields()->getFieldsValues($sitepage);

                    // Looking for data in form post and inserting in field values
                    if (!empty($getRowsMatching)) {
                        foreach ($getRowsMatching as $field) {
                            $key = $field->field_id . '_' . $field->option_id . '_' . $field->child_id . '_field_' . $field->child_id;
                            if (isset($values[$key]) && !empty($values[$key])) {
                                $fieldvalue = $fieldValues->getRowsMatching(array(
                                    'field_id' => $field->child_id,
                                    'item_id' => $sitepage->page_id,
                                ));

                                if (!empty($fieldvalue)) {
                                    $fieldvalue[0]->value = $values[$key];
                                    $fieldvalue[0]->save();
                                } else {
                                    $valuesRow = $fieldValues->createRow();
                                    $valuesRow->field_id = $field->child_id;
                                    $valuesRow->item_id = $sitepage->page_id;
                                    $valuesRow->index = 0;
                                    $valuesRow->value = $values[$key];
                                    $valuesRow->save();
                                }
                            }
                        }
                    }
                }

                // Start default email to superadmin when anyone create pages.
                $emails = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepage.defaultpagecreate.email', Engine_API::_()->seaocore()->getSuperAdminEmailAddress());
                if (!empty($emails)) {
                    $emails = explode(",", $emails);
                    $host = $_SERVER['HTTP_HOST'];
                    $newVar = _ENGINE_SSL ? 'https://' : 'http://';
                    $object_link = $newVar . $host . $sitepage->getHref();
                    $viewerGetTitle = $viewer->getTitle();
                    $sender_link = '<a href=' . $newVar . $host . $viewer->getHref() . ">$viewerGetTitle</a>";
                    foreach ($emails as $email) {
                        $email = trim($email);
                        Engine_Api::_()->getApi('mail', 'core')->sendSystem($email, 'SITEPAGE_PAGE_CREATION', array(
                            'sender' => $sender_link,
                            'object_link' => $object_link,
                            'object_title' => $sitepage->getTitle(),
                            'object_description' => $sitepage->getDescription(),
                            'queue' => true
                        ));
                    }
                }
                // End default email to superadmin when anyone create pages.
                
                if (!empty($sitepage) && !empty($sitepage->draft) && empty($sitepage->pending)) {
                    Engine_Api::_()->sitepage()->attachPageActivity($sitepage);


                    // Start AUTOMATICALLY LIKE THE PAGE WHEN MEMBER CREATE A PAGE.
                    $autoLike = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepage.automatically.like', 1);
                    if (!empty($autoLike)) {
                        Engine_Api::_()->sitepage()->autoLike($sitepage->page_id, 'sitepage_page');
                    }
                    //END automatically like the page when member create a page.

                    // Sending activity feed to facebook.
                    $enable_Facebooksefeed = $enable_fboldversion = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('facebooksefeed');
                    if (!empty($enable_Facebooksefeed)) {
                        $sitepage_array = array();
                        $sitepage_array['type'] = 'sitepage_new';
                        $sitepage_array['object'] = $sitepage;
                        Engine_Api::_()->facebooksefeed()->sendFacebookFeed($sitepage_array);
                    }
                }
                // Commit
                $db->commit();
                $this->successResponseNoContent('no_content');
            } catch (Exception $e) {
                $db->rollBack();
                $this->respondWithError('internal_server_error', $e->getMessage());
            }
            
        }
        
    }

    /**
     * Returns the Directory page search form 
     * 
     */
    public function searchFormAction() {
        
        // Validate request method
        $this->validateRequestMethod();
        $this->respondWithSuccess(Engine_Api::_()->getApi('Siteapi_Core', 'sitepage')->getBrowseSearchForm());
    }

    /**
     * Returns Categories , Sub-Categories, SubSub-Categories and pages array
     * 
     * 
     */
    public function categoryAction() {
        
        // Validate request method
        $this->validateRequestMethod();

        // Get viewer
        $viewer = Engine_Api::_()->user()->getViewer();

        // Prepare response
        $values = $response = array();
        $category_id = $this->getRequestParam('category_id', null);
        $subCategory_id = $this->getRequestParam('subCategory_id', null);
        $subsubcategory_id = $this->getRequestParam('subsubcategory_id', null);
        $showAllCategories = $this->getRequestParam('showAllCategories', 1);
        $showCategories = $this->getRequestParam('showCategories', 1);
        $showPages = $this->getRequestParam('showPages', 1);

        if ($this->getRequestParam('showCount')) {
            $showCount = 1;
        } else {
            $showCount = $this->getRequestParam('showCount', 0);
        }
        $orderBy = $this->getRequestParam('orderBy', 'category_name');

        $tableCategory = Engine_Api::_()->getDbtable('categories', 'sitepage');
        Engine_Api::_()->getApi('Core', 'siteapi')->setView();

        $categories = array();

        // Get pages table
        $tableSitepage = Engine_Api::_()->getDbtable('pages', 'sitepage');
        $sitepageShowAllCategories = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepage.categorywithslug', 1);
        $showAllCategories = !empty($sitepageShowAllCategories) ? $showAllCategories : 0;

        if ($showCategories) {

            if ($showAllCategories) {

                $category_info = $tableCategory->getCategories(array('category_id', 'category_name', 'cat_order'), null, 0, 0, 1, 0, $orderBy, 1);
                $categoriesCount = count($category_info);
                foreach ($category_info as $value) {

                    $sub_cat_array = array();

                    if ($showCount) {
                        $categories[] = $category_array = array('category_id' => $value->category_id,
                            'category_name' => $this->translate($value->category_name),
                            'order' => $value->cat_order,
                            'count' => $tableSitepage->getPagesCount($value->category_id, 'category_id', 1),
                            'images' => Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($value),
                        );
                    } else {
                        $categories[] = $category_array = array('category_id' => $value->category_id,
                            'category_name' => $this->translate($value->category_name),
                            'order' => $value->cat_order,
                            'images' => Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($value),
                        );
                    }
                }
            } else {
                $category_info = $tableCategory->getCategorieshaspages(0, 'category_id', null, array(), array('category_id', 'category_name', 'cat_order'));
                $categoriesCount = count($category_info);
                foreach ($category_info as $value) {
                    if ($showCount) {
                        $categories[] = $category_array = array('category_id' => $value->category_id,
                            'category_name' => $value->category_name,
                            'order' => $value->cat_order,
                            'count' => $tableSitepage->getPagesCount($value->category_id, 'category_id', 1),
                            'images' => Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($value),
                        );
                    } else {
                        $categories[] = $category_array = array('category_id' => $value->category_id,
                            'category_name' => $this->translate($value->category_name),
                            'order' => $value->cat_order,
                            'images' => Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($value),
                        );
                    }
                }
            }

            $response['categories'] = $categories;

            if (!empty($category_id)) {

                if ($showAllCategories) {
                    $category_info2 = $tableCategory->getSubcategories($category_id);

                    foreach ($category_info2 as $subresults) {
                        if ($showCount) {
                            $sub_cat_array[] = $tmp_array = array('sub_cat_id' => $subresults->category_id,
                                'sub_cat_name' => $this->translate($subresults->category_name),
                                'count' => $tableSitepage->getPagesCount($subresults->category_id, 'subcategory_id', 1),
                                'order' => $subresults->cat_order);
                        } else {
                            $sub_cat_array[] = $tmp_array = array('sub_cat_id' => $subresults->category_id,
                                'sub_cat_name' => $this->translate($subresults->category_name),
                                'order' => $subresults->cat_order);
                        }
                    }
                } else {
                    $category_info2 = $tableCategory->getCategorieshaspages($category_id, 'subcategory_id', null, array(), array('category_id', 'category_name', 'cat_order'));
                    foreach ($category_info2 as $subresults) {
                        if ($showCount) {
                            $sub_cat_array[] = $tmp_array = array('sub_cat_id' => $subresults->category_id,
                                'sub_cat_name' => $this->translate($subresults->category_name),
                                'count' => $tableSitepage->getPagesCount($subresults->category_id, 'subcategory_id', 1),
                                'order' => $subresults->cat_order);
                        } else {
                            $sub_cat_array[] = $tmp_array = array('sub_cat_id' => $subresults->category_id,
                                'sub_cat_name' => $this->translate($subresults->category_name),
                                'order' => $subresults->cat_order);
                        }
                    }
                }

                $response['subCategories'] = $sub_cat_array;
            }

            if (!empty($subCategory_id)) {

                if ($showAllCategories) {
                    $subcategory_info2 = $tableCategory->getSubcategories($subCategory_id, array('category_id', 'category_name', 'cat_order'));
                    $treesubarrays = array();
                    foreach ($subcategory_info2 as $subvalues) {
                        if ($showCount) {
                            $treesubarrays[] = $treesubarray = array('tree_sub_cat_id' => $subvalues->category_id,
                                'tree_sub_cat_name' => $this->translate($subvalues->category_name),
                                'count' => $tableSitepage->getPagesCount($subvalues->category_id, 'subsubcategory_id', 1),
                                'order' => $subvalues->cat_order,
                            );
                        } else {
                            $treesubarrays[] = $treesubarray = array('tree_sub_cat_id' => $subvalues->category_id,
                                'tree_sub_cat_name' => $this->translate($subvalues->category_name),
                                'order' => $subvalues->cat_order,
                            );
                        }
                    }
                } else {
                    $subcategory_info2 = $tableCategory->getCategorieshaspages($subCategory_id, 'subsubcategory_id', null, array(), array('category_id', 'category_name', 'cat_order'));
                    $treesubarrays = array();
                    foreach ($subcategory_info2 as $subvalues) {
                        if ($showCount) {
                            $treesubarrays[] = $treesubarray = array('tree_sub_cat_id' => $subvalues->category_id,
                                'tree_sub_cat_name' => $this->translate($subvalues->category_name),
                                'order' => $subvalues->cat_order,
                                'count' => $tableSitepage->getPagesCount($subvalues->category_id, 'subsubcategory_id', 1),
                            );
                        } else {
                            $treesubarrays[] = $treesubarray = array('tree_sub_cat_id' => $subvalues->category_id,
                                'tree_sub_cat_name' => $this->translate($subvalues->category_name),
                                'order' => $subvalues->cat_order
                            );
                        }
                    }
                }
                $response['subsubCategories'] = $treesubarrays;
            }
        }

        if ($showPages && isset($category_id) && !empty($category_id)) {
            $params = array();
            $itemCount = $params['itemCount'] = $this->_getParam('itemCount', 0);

            // Get categories
            $categories = array();

            $category_info = Engine_Api::_()->getDbtable('categories', 'sitepage')->getCategorieshaspages($category_id, 'category_id', $itemCount, $params, array('category_id', 'category_name', 'cat_order'));
            $category_pages_array = array();

            $params = $this->_getAllParams();


            // Get page results
            $category_pages_info = $category_pages_info = Engine_Api::_()->getDbtable('pages', 'sitepage')->pagesBySettings($params);

            foreach ($category_pages_info as $result_info) {
                $tmp_array = array('page_id' => $result_info->page_id,
                    'imageSrc' => Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($result_info),
                    'page_title' => $result_info->title,
                    'owner_id' => $result_info->owner_id,
                    'viewCount' => $result_info->view_count,
                    'slug' => $result_info->getSlug(),
                );
                $category_pages_array[] = $tmp_array;
            }

            $response['pages'] = $category_pages_array;
        }
        if (isset($categoriesCount) && !empty($categoriesCount))
            $response['totalItemCount'] = $categoriesCount;
        $response['canCreate'] = Engine_Api::_()->authorization()->isAllowed('sitepage_page', $viewer, 'create');

        $this->respondWithSuccess($response, true);
    }

    /**
     * Returns the tabs menu of the Directory Page
     * 
     * @return array
     */
    private function _tabsMenus() {
        if (!Engine_Api::_()->core()->hasSubject('page'))
            $sitepage = $subject = Engine_Api::_()->core()->getSubject('sitepage_page');

        // RETURN IF NO SUBJECT AVAILABLE.
        if (empty($subject))
            $this->respondWithError('no_record');

        $tabsMenu = array();

        // Prepare updated count
        $streamTable = Engine_Api::_()->getDbtable('stream', 'activity');
        $updates_count = $streamTable->select()
                        ->from($streamTable->info('name'), 'count(*) as count')
                        ->where('object_id = ?', $subject->page_id)
                        ->where('object_type = ?', "sitepage_page")
                        ->where('target_type = ?', "sitepage_page")
                        ->where('type like ?', "%post%")
                        ->query()->fetchColumn();

        $tabsMenu['updates'] = array(
            'count' => $updates_count,
            'name' => 'updates',
            'label' => $this->translate('Updates'),
            'url' => 'sitepage/updates/' . $subject->getIdentity(),
        );

        $tabsMenu['info'] = array(
            'name' => 'info',
            'label' => $this->translate('Info'),
            'url' => 'sitepage/info/' . $subject->getIdentity()
        );


        if (Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagealbum')) {
            $photos_count = Engine_Api::_()->getDbtable('photos', 'sitepage')->countTotalPhotos(array('page_id' => $sitepage->page_id));
            $tabsMenu['photos'] = array(
                'count' => $photos_count,
                'name' => 'photos',
                'label' => $this->translate('Photos'),
                'url' => 'sitepage/photos/' . $subject->getIdentity(),
            );
        }

        if (Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagereview')) {

            $review_count = Engine_Api::_()->getDbtable('reviews', 'sitepagereview')->totalReviews($sitepage->page_id);
            $tabsMenu['reviews'] = array(
                'count' => $review_count,
                'name' => 'reviews',
                'label' => $this->translate('Reviews'),
                'url' => 'sitepage/Reviews/' . $subject->getIdentity(),
            );
        }

        if (Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagevideo')) {
            $videos_count = Engine_Api::_()->getDbtable('videos', 'sitepagevideo')->getPageVideoCount($sitepage->page_id);
            $tabsMenu['videos'] = array(
                'count' => $videos_count,
                'name' => 'videos',
                'label' => $this->translate('Videos'),
                'url' => 'sitepage/Videos/' . $subject->getIdentity(),
            );
        }

        if (Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepagediscussion')) {
            $discussion_count = Engine_Api::_()->getDbtable('topics', 'sitepage')->countTotalTopics($sitepage->page_id);
            $tabsMenu['discussions'] = array(
                'count' => $discussion_count,
                'name' => 'topics',
                'label' => $this->translate('Discussions'),
                'url' => 'sitepage/topics/' . $subject->getIdentity(),
            );
        }

        return $tabsMenu;
    }

    /**
     * Returns the paginated Directory pages listings after filtering from search papameters if sent
     * 
     * 
     */
    private function _getDirectoryPages($params) {
        $response = $tempParams = $data = $tempResponse = array();
        $imageType = 'thumb.icon';
        $viewer = Engine_Api::_()->user()->getViewer();
        $tableObj = Engine_Api::_()->getDbtable('pages', 'sitepage');

        $siteapipageBrowse = Zend_Registry::isRegistered('sitepage_browse') ? Zend_Registry::get('sitepage_browse') : null;
        $response['canCreate'] = $this->_helper->requireAuth()->setAuthParams('sitepage_page', null, 'create')->checkRequire();


        if (empty($params['manage']) && $params['show'] == 2) {
            // Get an array of friend ids
            $table = Engine_Api::_()->getItemTable('user');
            $select = $viewer->membership()->getMembersSelect('user_id');
            $friends = $table->fetchAll($select);
            // Get stuff
            $ids = array();
            foreach ($friends as $friend) {
                $ids[] = $friend->user_id;
            }
            $params['users'] = $ids;
        }

        if (isset($params['image_type']) && !empty($params['image_type']))
            $imageType = $params['image_type'];


        $pagesObj = Engine_Api::_()->sitepage()->getSitepagesPaginator($params);
        $items_count = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepage.page', $params['limit']);
        $pagesObj->clearPageItemCache();
        $response['totalItemCount'] = $getTempPagesCount = $pagesObj->getTotalItemCount();
        
        if ($getTempPagesCount) {
            foreach ($pagesObj as $pageObj) {
                $data = $pageObj->toArray();

                if (!empty($params['manage']) && $pageObj->isOwner($viewer))
                    $data["menu"] = Engine_Api::_()->getApi('Siteapi_Core', 'sitepage')->gutterMenus($pageObj, 'manage');

                $categoryObj = Engine_Api::_()->getItem('sitepage_category', $data['category_id']);
                if (isset($categoryObj) && !empty($categoryObj))
                    $data['category_title'] = $categoryObj->getTitle();

                // Add images
                $getContentImages = Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($pageObj);
                $data = array_merge($data, $getContentImages);

                // Add owner images
                $getContentOwnerImages = Engine_Api::_()->getApi('Core', 'siteapi')->getContentImage($pageObj, true);
                $data = array_merge($data, $getContentOwnerImages);

                $data["owner_title"] = $pageObj->getOwner()->getTitle();
                $ownerUrl = Engine_Api::_()->getApi('Core', 'siteapi')->getContentURL($pageObj->getOwner(), "owner_url");
                $data = array_merge($data, $ownerUrl);

                $contentUrl = Engine_Api::_()->getApi('Core', 'siteapi')->getContentURL($pageObj);
                $data = array_merge($data, $contentUrl);

                $isAllowedView = $pageObj->authorization()->isAllowed($viewer, 'view');
                $data["allow_to_view"] = empty($isAllowedView) ? 0 : 1;

                $isAllowedEdit = $pageObj->authorization()->isAllowed($viewer, 'edit');
                $data["edit"] = empty($isAllowedEdit) ? 0 : 1;
                $isAllowedDelete = $pageObj->authorization()->isAllowed($viewer, 'delete');
                $data["delete"] = empty($isAllowedDelete) ? 0 : 1;

                $tempResponse[] = $data;
            }

            if (!empty($tempResponse))
                $response['response'] = $tempResponse;
        }
        return $response;
    }

    /*
    * Page url Validation
    *
    */
  public function pageurlvalidationAction() {

    $this->validateRequestMethod();

    $page_url = $this->_getParam('page_url');
    if(!$page_url)
        $this->respondWithValidationError('parameter_missing',"parameter named page_url missing");

    $sitepageUrlEnabled = Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sitepageurl');
    if (!empty($sitepageUrlEnabled)) {
      $urlArray = Engine_Api::_()->sitepage()->getBannedUrls();
    }
    if (empty($page_url)) {
      $this->respondWithValidationError('urlNotvalid',"Url not valid");
    }

    $url_lenght = strlen($page_url);
    if ($url_lenght < 3) {
        $this->respondWithValidationError('urlNotvalid',"Url should be atleast 3 characters long");
    } elseif ($url_lenght > 255) {
        $this->respondWithValidationError('urlNotvalid',"url should be atmost 255 characters long");
    }

    $change_url = Engine_Api::_()->getApi('settings', 'core')->getSetting('sitepage.change.url', 1);
    $check_url = $this->_getParam('check_url');
    if (!empty($check_url)) {
      $pageId = $this->_getParam('page_id');
      $page_id = Engine_Api::_()->sitepage()->getPageId($page_url, $pageId);
    } else {
      $page_id = Engine_Api::_()->sitepage()->getPageId($page_url);
    }
    if (!empty($sitepageUrlEnabled)) {
      if (!empty($page_id) || (in_array(strtolower($page_url), $urlArray))) {
         $this->respondWithValidationError('urlNotvalid',"Url not available");
      }
    } else {
      if (!empty($page_id)) {
            $this->respondWithValidationError('urlNotvalid',"Url not available");        
      }
    }



    if (!preg_match("/^[a-zA-Z0-9-_]+$/", $page_url)) {
        $this->respondWithValidationError('urlNotvalid',"URL component can contain alphabets, numbers, underscores & dashes only");
    } else {
        $this->successResponseNoContent('no_content');        
    }
  }

}
