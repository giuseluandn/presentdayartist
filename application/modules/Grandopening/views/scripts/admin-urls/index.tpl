<script type="text/javascript">
    
    function addUrl(){
        var addurl = $('urls-form-input').getChildren('input')[0].value;
        
        var url = '<?php echo $this->url(); ?>';
        new Request({
            url: url,
            onSuccess: function(data) {
                window.location.reload();
            }
        }).post('url='+encodeURIComponent(addurl));
    }
    
</script>


<h2><?php echo $this->translate('Grand Opening Plugin'); ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='tabs'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
  </div>
<?php endif; ?>
<p class="urls-tips">
    <?php echo $this->translate("Manage all page URLs that Grand Opening don\'t block.") ?>
</p>


<div class="urls-list">
    
    <?php foreach( $this->paginator as $data ) : ?>
        <div class="urls-list-text"><?php echo $data['url']; ?></div><div class="urls-list-options">
            <?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'grandopening', 'controller' => 'urls', 'action' => 'delete', 'id' => $data['url_id']), 'delete', array('class' => 'smoothbox')) ?>
        </div>
    <?php endforeach; ?>
    
</div>

<br/><br/>

<p class="urls-tips-add">
    <?php echo $this->translate("Add pages open for non-logged visitors") ?>
</p>

<div class="urls-add">
    <div class="url-input-holder">
        <span class="urls-form-label"><?php echo $this->domain . $this->baseUrl() . '/'; ?></span>
        <span id="urls-form-input"><input type="text" value name="url" /></span>
    </div>
</div>
 <button id="submit" type="submit" onclick="addUrl(); return false;">Add URL</button>



<br/>
<?php if( $this->paginator->count() >= 1): ?>
    <div>
      <?php echo $this->paginationControl($this->paginator); ?>
    </div>
<?php endif; ?>