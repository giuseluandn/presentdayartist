<?php

class Grandopening_Plugin_Core extends Zend_Controller_Plugin_Abstract {

    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        $pathinfo = $request->getPathInfo();
        //die(Zend_Debug::dump($pathinfo));

        $cookie = $request->getCookie('whGOadmin');
        if ($cookie == 1 && $pathinfo != '/pages/grandopening')
            return;

        $settings = Engine_Api::_()->getApi('settings', 'core');
        if (!$settings->getSetting('grandopening_enable', 0) && $pathinfo != '/pages/grandopening')
            return;

        if ($settings->getSetting('use_date', 0)) {
            $time = Engine_Api::_()->grandopening()->getEndTime();
            if (trim($settings->getSetting('grandopening_endtime', 0), '0-') && $time < time() && $pathinfo != '/pages/grandopening')
                return;
        }

        if (Engine_Api::_()->user()->getViewer()->getIdentity() && $pathinfo != '/pages/grandopening')
            return;


        $table = Engine_Api::_()->getDbTable('urls', 'grandopening');
        $select = $table->select()->from($table->info('name', array('url')));
        $urls = $table->fetchAll($select);
        $urls = $urls->toArray();

        //$path_ok = array('admin', 'login', 'grandopening/email', 'getslide', 'utility/tasks', 'signup', 'user/auth/forgot', 'auth/reset/code', 'payment/subscription');
        foreach ($urls as $value_path) {
            if (strpos($pathinfo, $value_path['url']))
                return;
        }

        if ($pathinfo != '' && $pathinfo != '/' && $pathinfo != '/pages/grandopening') {
            $frontController = Zend_Controller_Front::getInstance();
            $response = $frontController->getResponse();
            return $response->setRedirect(Zend_Registry::get('StaticBaseUrl'));
        }

        $bg = $request->getParam('bg', false);

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        if (null === $viewRenderer->view) {
            $viewRenderer->initView();
        }
        $view = $viewRenderer->view;

        $request->setModuleName('core')
                ->setControllerName('pages')
                ->setActionName('grandopening')
                ->setParam('action', 'grandopening');
        if (!$bg) {
            $coversTable = Engine_Api::_()->getItemTable('cover');
            $cover = $coversTable->getCover();
            $bg = $cover['title'];
        }

        $ext = pathinfo(APPLICATION_PATH . '/public/opening_cover/' . $bg, PATHINFO_EXTENSION);
        if ('mp4' != $ext) {
            $styles = 'html#smoothbox_window {background-image: url(public/opening_cover/' . $bg . ')}';

            $view->headStyle()->appendStyle($styles);
        } else {
            $bgSession = new Zend_Session_Namespace('grandopening_background');
            $bgSession->background = '//' . $_SERVER['HTTP_HOST'] . Zend_Registry::get('Zend_View')->baseUrl('/') . 'public/opening_cover/' . $bg;
        }
        $view->headMeta()->appendName('viewport', 'width=device-width, initial-scale=1.0');
    }

}
