<?php

class Grandopening_AdminUrlsController extends Core_Controller_Action_Admin
{
  public function indexAction()
  {
    $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                                                           ->getNavigation('grandopening_admin_main', array(), 'grandopening_admin_main_urls');

    
    $table = Engine_Api::_()->getDbtable('urls', 'grandopening');
    
    $url = $this->_getParam('url');
    
    if( $this->getRequest()->isPost() && !empty($url) ){
        
        $db = $table->getAdapter();
        
        try {
            
            $db->insert($table->info('name'),array('url' => $this->_getParam('url')));

            $db->commit();
        } catch (Exception $ex) {
            $db->rollBack();
            throw($ex);
        }
        
    }
    
    $this->view->domain = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost();
    $this->view->form = $form = new Grandopening_Form_Admin_Url();
    
    
    $select = $table->select()->from($table->info('name'))->order('url_id ASC');
    
    $urls = $table->fetchAll($select);
    $urls = $urls->toArray();
                                                                                                                 
    $this->view->paginator = $paginator =  Zend_Paginator::factory($select);
    $items_per_page = 40;
    $paginator->setCurrentPageNumber( $this->_getParam('page'));
    $paginator->setItemCountPerPage($items_per_page);
  }

  public function deleteAction()
  {
    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');
    $this->view->delete_title = 'Delete URL?';
    $this->view->delete_description = 'Are you sure that you want to delete this URL?';

    // Check post
    if( $this->getRequest()->isPost())
    {
      $db = Engine_Db_Table::getDefaultAdapter();
      $db->beginTransaction();

      try
      {
        $this->_helper->api()->getDbtable('urls', 'grandopening')->findRow($this->_getParam('id'))->delete();
        
        $db->commit();
      }

      catch( Exception $e )
      {
        $db->rollBack();
        throw $e;
      }

      $this->_forward('success', 'utility', 'core', array(
          'smoothboxClose' => 1000,
          'parentRefresh'=> 1000,
          'messages' => array('Url successfully delated')
      ));
    } else {
        // Output
        $this->renderScript('etc/delete.tpl');
    }

    
  }
  
}
