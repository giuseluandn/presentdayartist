CREATE TABLE `engine4_grandopening_urls` (
  `url_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255),
  PRIMARY KEY (`url_id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE INTO engine4_grandopening_urls (`url`)
                                   VALUES ('help/terms'),
                                          ('payment/subscription'),
                                          ('auth/reset/code'),
                                          ('admin'),
                                          ('login'),
                                          ('signup'),
                                          ('getslide'),
                                          ('utility/tasks'),
                                          ('user/auth/forgot'),
                                          ('user/auth/forgot'),
                                          ('help/privacy');

INSERT IGNORE INTO engine4_core_menuitems (`name`, `module`, label, plugin, params, menu, submenu, enabled, custom, `order`)
                                   VALUES ('grandopening_admin_main_urls', 'grandopening', 'Manage Urls', '', '{"route":"admin_default","module":"grandopening","controller":"urls"}', 'grandopening_admin_main', '', 1, 0, 4);
