<?php

class Grandopening_Form_Admin_Url extends Engine_Form {

    public function init() {
        $this->setTitle('Add Url')
                ->setAttrib('class', 'global_form_popup');

        $this->addElement('Date', 'start_date', array(
            'label' => 'Start Date',
            'yearMin' => date('Y') - 1,
            'yearMax' => date('Y') + 5
        ));

        $this->addElement('Button', 'submit', array(
            'label' => 'Save Changes',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
        ));

        $this->addDisplayGroup(array('submit'), 'buttons', array());
    }

}