<?php

class Grandopening_Widget_VideoBackgroundController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        $bgSession = new Zend_Session_Namespace('grandopening_background');
        if (!isset($bgSession->background))
            return $this->setNoRender();

        $this->view->mp4 = $bgSession->background;
        Zend_Session::namespaceUnset('grandopening_background');
    }

}
