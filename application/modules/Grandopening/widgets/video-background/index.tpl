<div class="go-video-wrapper">
    <video id="grandopening_video" autoplay loop="true" <?php echo Engine_Api::_()->getApi('settings', 'core')->getSetting('grandopening.videomute', 1) == 1 ? 'muted' : '' ?>>
        <source type="video/mp4" src="<?php echo $this->mp4 ?>"></source>
    </video>
</div>