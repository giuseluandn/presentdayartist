<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
 
class Pdaportfolio_Plugin_Constants {
	const FOLDER_CODE = 'Portfolio';
	const KILOBYTE = 1024;
	const DEFAULT_LIMIT = 20;
}