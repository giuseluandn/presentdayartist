<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
 
class Pdaportfolio_Plugin_Core {
	public function onUserCreateAfter($event) {
		$item = $event->getPayload();
		if( $item->getType() == 'user' ) {
			$values = array(
				'title' => 'My Portfolio',
				'auth_view' => 'everyone',
				'auth_comment' => 'everyone',
				'auth_create' => 'owner',
				'auth_edit' => 'owner',
				'auth_delete' => 'owner',
				'auth_edit_perm' => 'owner',
				'parent_type' => 'user',
				'parent_id' => $item->getIdentity(),
				'user_id' => $item->getIdentity(),
				'parent_folder_id' => 0,
			);
			$folderTbl = new Pdaportfolio_Model_DbTable_Folders();
			$folder = $folderTbl->createRow();
			$folder->setFromArray($values);
			$folder->save();
		}
	}
	
	public function onUserDeleteBefore($event) {
		$item = $event->getPayload();
		if( $item->getType() == 'user' ) {
			//Remove folders and files in each folders
			$folders = Engine_Api::_()->pdaportfolio()->getSubFolders(NULL, $item);
			if ( count($folders) > 0 ){
				foreach ($folders as $folder) {
					$folder->delete();
				}	
			}
			
			//Remove all files in other users folder
			$files = Engine_Api::_()->pdaportfolio()->getFilesByParent($item);
			if ( count($files) > 0 ){
				foreach ($files as $file) {
					$file->delete();
				}	
			}
			
		}
	}
}