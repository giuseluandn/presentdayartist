<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */

return array (
		array (
				'title' => 'Portfolio Browse Menu',
				'description' => 'Displays a menu for the module portfolio.',
				'category' => 'Portfolio',
				'type' => 'widget',
				'name' => 'pdaportfolio.browse-menu'
		),
		array (
				'title' => 'Portfolio Search',
				'description' => 'Displays portfolio search box on browse page.',
				'category' => 'Portfolio',
				'type' => 'widget',
				'name' => 'pdaportfolio.portfolio-search',
				'defaultParams' => array(
					'title' => 'Portfolio Search',
				),
		),
		array (
				'title' => 'Profile Folders',
				'description' => 'Displays folders of an object.',
				'category' => 'Portfolio',
				'type' => 'widget',
				'name' => 'pdaportfolio.profile-folders',
				'requirements' => array('subject'),
				'defaultParams' => array(
					'title' => 'Folders',
				),
		),
);