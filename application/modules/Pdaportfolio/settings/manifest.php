<?php defined("_ENGINE") or die("access denied"); return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'pdaportfolio',
    'version' => '4.01',
    'path' => 'application/modules/Pdaportfolio',
    'title' => 'PDA - Portfolio',
    'description' => 'Portfolio',
    'author' => '<a href="https://www.upwork.com/freelancers/~0127a28095128bc77c" title="John" target="_blank">John</a>',
    'callback' => 
    array (
      'path' => 'application/modules/Pdaportfolio/settings/install.php',
      'class' => 'Pdaportfolio_Installer',
    ),
    'actions' =>
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Pdaportfolio',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/pdaportfolio.csv',
    ),
  ),
  'hooks' => 
  array (
    0 => 
    array (
      'event' => 'onUserCreateAfter',
      'resource' => 'Pdaportfolio_Plugin_Core',
    ),
    1 => 
    array(
      'event' => 'onUserDeleteBefore',
      'resource' => 'Pdaportfolio_Plugin_Core',
    ),
  ),
  'routes' => 
  array (
    'pdaportfolio_general' => 
    array (
      'route' => 'portfolio/:controller/:action/*',
      'defaults' => 
      array (
        'module' => 'pdaportfolio',
        'controller' => 'index',
        'action' => 'index',
      ),
      'reqs' => 
      array (
        'action' => '(index|listing|manage|create|browse-by-tree|browse|move|share|delete|view)',
      ),
    ),
    'pdaportfolio_folder_specific' => 
    array (
      'route' => 'portfolio/folder/:action/:folder_id/:slug/*',
      'defaults' => 
      array (
        'module' => 'pdaportfolio',
        'controller' => 'folder',
        'action' => 'view',
        'slug' => '-',
      ),
      'reqs' => 
      array (
        'folder_id' => '\\d+',
        'action' => '(view|delete|upload|move|edit|edit-perm)',
      ),
    ),
    'pdaportfolio_file_specific' => 
    array (
      'route' => 'portfolio/file/:action/:file_id/:slug/*',
      'defaults' => 
      array (
        'module' => 'pdaportfolio',
        'controller' => 'file',
        'action' => 'view',
        'slug' => '-',
      ),
      'reqs' => 
      array (
        'file_id' => '\\d+',
        'action' => '(view|delete|edit|download)',
      ),
    ),
    'pdaportfolio_share_view' => 
    array (
      'route' => 'portfolio/sh/:object_type/:object_id/:code/:slug/*',
      'defaults' => 
      array (
        'module' => 'pdaportfolio',
        'controller' => 'index',
        'action' => 'shareview',
        'slug' => '-',
      ),
      'reqs' => 
      array (
        'object_type' => '(folder|file)',
        'object_id' => '\\d+',
      ),
    ),
  ),
  'items' => 
  array (
    0 => 'pdaportfolio_folder',
    1 => 'pdaportfolio_file',
    2 => 'folder',
    3 => 'pdaportfolio_document',
    4 => 'file',
  ),
);?>