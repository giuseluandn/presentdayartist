<?php defined("_ENGINE") or die("access denied"); return array (
  'menus' => 
  array (
  ),
  'menuitems' => 
  array (
    0 => 
    array (
      'id' => 131,
      'name' => 'core_main_pdaportfolio',
      'module' => 'pdaportfolio',
      'label' => 'Portfolio',
      'plugin' => '',
      'params' => '{"route":"pdaportfolio_general"}',
      'menu' => 'core_main',
      'submenu' => '',
      'enabled' => 1,
      'custom' => 0,
      'order' => 999,
    ),
    1 => 
    array (
      'id' => 132,
      'name' => 'pdaportfolio_main_browse',
      'module' => 'pdaportfolio',
      'label' => 'Browse Folders',
      'plugin' => '',
      'params' => '{"route":"pdaportfolio_general"}',
      'menu' => 'pdaportfolio_main',
      'submenu' => '',
      'enabled' => 1,
      'custom' => 0,
      'order' => 1,
    ),
    2 => 
    array (
      'id' => 133,
      'name' => 'pdaportfolio_main_manage',
      'module' => 'pdaportfolio',
      'label' => 'Manage My Folders',
      'plugin' => 'Pdaportfolio_Plugin_Menus',
      'params' => '{"route":"pdaportfolio_general","action":"manage"}',
      'menu' => 'pdaportfolio_main',
      'submenu' => '',
      'enabled' => 1,
      'custom' => 0,
      'order' => 2,
    ),
    3 => 
    array (
      'id' => 134,
      'name' => 'pdaportfolio_admin_main_level',
      'module' => 'pdaportfolio',
      'label' => 'Member Level Settings',
      'plugin' => NULL,
      'params' => '{"route":"admin_default","module":"pdaportfolio","controller":"level"}',
      'menu' => 'pdaportfolio_admin_main',
      'submenu' => NULL,
      'enabled' => 1,
      'custom' => 0,
      'order' => 2,
    ),
    4 => 
    array (
      'id' => 135,
      'name' => 'pdaportfolio_admin_main_settings',
      'module' => 'pdaportfolio',
      'label' => 'Global Settings',
      'plugin' => NULL,
      'params' => '{"route":"admin_default","module":"pdaportfolio","controller":"settings"}',
      'menu' => 'pdaportfolio_admin_main',
      'submenu' => NULL,
      'enabled' => 1,
      'custom' => 0,
      'order' => 2,
    ),
    5 => 
    array (
      'id' => 136,
      'name' => 'core_admin_plugins_pdaportfolio',
      'module' => 'pdaportfolio',
      'label' => 'PDA - Portfolio',
      'plugin' => '',
      'params' => '{"route":"admin_default","module":"pdaportfolio","controller":"settings","action":"index"}',
      'menu' => 'core_admin_main_plugins',
      'submenu' => '',
      'enabled' => 1,
      'custom' => 0,
      'order' => 1,
    ),
    6 => 
    array (
      'id' => 137,
      'name' => 'pdaportfolio_admin_main_scribd',
      'module' => 'pdaportfolio',
      'label' => 'Scribd Settings',
      'plugin' => NULL,
      'params' => '{"route":"admin_default","module":"pdaportfolio","controller":"scribd"}',
      'menu' => 'pdaportfolio_admin_main',
      'submenu' => NULL,
      'enabled' => 1,
      'custom' => 0,
      'order' => 3,
    ),
    7 => 
    array (
      'id' => 179,
      'name' => 'pdaportfolio_main_link',
      'module' => 'pdaportfolio',
      'label' => 'Manage Shared Links',
      'plugin' => 'Pdaportfolio_Plugin_Menus',
      'params' => '{"route":"pdaportfolio_general","controller":"link","action":"browse"}',
      'menu' => 'pdaportfolio_main',
      'submenu' => NULL,
      'enabled' => 1,
      'custom' => 0,
      'order' => 3,
    ),
  ),
  'mails' => 
  array (
  ),
  'jobtypes' => 
  array (
  ),
  'notificationtypes' => 
  array (
  ),
  'actiontypes' => 
  array (
  ),
  'permissions' => 
  array (
    0 => 
    array (
      0 => 'admin',
      1 => 'file',
      2 => 'comment',
      3 => 2,
      4 => NULL,
    ),
    1 => 
    array (
      0 => 'admin',
      1 => 'folder',
      2 => 'comment',
      3 => 1,
      4 => NULL,
    ),
    2 => 
    array (
      0 => 'admin',
      1 => 'folder',
      2 => 'create',
      3 => 1,
      4 => NULL,
    ),
    3 => 
    array (
      0 => 'admin',
      1 => 'folder',
      2 => 'delete',
      3 => 1,
      4 => NULL,
    ),
    4 => 
    array (
      0 => 'admin',
      1 => 'folder',
      2 => 'edit',
      3 => 1,
      4 => NULL,
    ),
    5 => 
    array (
      0 => 'admin',
      1 => 'folder',
      2 => 'edit_perm',
      3 => 1,
      4 => NULL,
    ),
    6 => 
    array (
      0 => 'admin',
      1 => 'folder',
      2 => 'view',
      3 => 1,
      4 => NULL,
    ),
    7 => 
    array (
      0 => 'moderator',
      1 => 'file',
      2 => 'comment',
      3 => 2,
      4 => NULL,
    ),
    8 => 
    array (
      0 => 'moderator',
      1 => 'folder',
      2 => 'comment',
      3 => 1,
      4 => NULL,
    ),
    9 => 
    array (
      0 => 'moderator',
      1 => 'folder',
      2 => 'create',
      3 => 1,
      4 => NULL,
    ),
    10 => 
    array (
      0 => 'moderator',
      1 => 'folder',
      2 => 'delete',
      3 => 1,
      4 => NULL,
    ),
    11 => 
    array (
      0 => 'moderator',
      1 => 'folder',
      2 => 'edit',
      3 => 1,
      4 => NULL,
    ),
    12 => 
    array (
      0 => 'moderator',
      1 => 'folder',
      2 => 'edit_perm',
      3 => 1,
      4 => NULL,
    ),
    13 => 
    array (
      0 => 'moderator',
      1 => 'folder',
      2 => 'view',
      3 => 1,
      4 => NULL,
    ),
    14 => 
    array (
      0 => 'user',
      1 => 'file',
      2 => 'comment',
      3 => 2,
      4 => NULL,
    ),
    15 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'auth_ext',
      3 => 3,
      4 => '',
    ),
    16 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'comment',
      3 => 1,
      4 => NULL,
    ),
    17 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'create',
      3 => 1,
      4 => NULL,
    ),
    18 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'delete',
      3 => 1,
      4 => NULL,
    ),
    19 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'edit',
      3 => 1,
      4 => NULL,
    ),
    20 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'edit_perm',
      3 => 1,
      4 => NULL,
    ),
    21 => 
    array (
      0 => 'user',
      1 => 'folder',
      2 => 'view',
      3 => 1,
      4 => NULL,
    ),
    22 => 
    array (
      0 => 'public',
      1 => 'folder',
      2 => 'view',
      3 => 1,
      4 => NULL,
    ),
  ),
  'pages' => 
  array (
    'pdaportfolio_file_view' => 
    array (
      'page_id' => 25,
      'name' => 'pdaportfolio_file_view',
      'displayname' => 'Portfolio Preview Page',
      'url' => NULL,
      'title' => 'Portfolio Preview Page',
      'description' => 'This is the portfolio preview page.',
      'keywords' => '',
      'custom' => 0,
      'fragment' => 0,
      'layout' => '',
      'levels' => NULL,
      'provides' => '',
      'view_count' => 0,
      'ynchildren' => 
      array (
        0 => 
        array (
          'content_id' => 673,
          'page_id' => 25,
          'type' => 'container',
          'name' => 'top',
          'parent_content_id' => NULL,
          'order' => 1,
          'params' => '["[]"]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 674,
              'page_id' => 25,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 673,
              'order' => 6,
              'params' => '["[]"]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 675,
                  'page_id' => 25,
                  'type' => 'widget',
                  'name' => 'pdaportfolio.browse-menu',
                  'parent_content_id' => 674,
                  'order' => 3,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
        1 => 
        array (
          'content_id' => 676,
          'page_id' => 25,
          'type' => 'container',
          'name' => 'main',
          'parent_content_id' => NULL,
          'order' => 2,
          'params' => '[]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 677,
              'page_id' => 25,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 676,
              'order' => 6,
              'params' => '[]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 678,
                  'page_id' => 25,
                  'type' => 'widget',
                  'name' => 'core.content',
                  'parent_content_id' => 677,
                  'order' => 6,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
                1 => 
                array (
                  'content_id' => 679,
                  'page_id' => 25,
                  'type' => 'widget',
                  'name' => 'core.comments',
                  'parent_content_id' => 677,
                  'order' => 7,
                  'params' => '{"title":"Comments"}',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'pdaportfolio_folder_view' => 
    array (
      'page_id' => 26,
      'name' => 'pdaportfolio_folder_view',
      'displayname' => 'Portfolio Folder View Page',
      'url' => NULL,
      'title' => 'Portfolio Folder View Page',
      'description' => 'This is the portfolio folder view page.',
      'keywords' => '',
      'custom' => 0,
      'fragment' => 0,
      'layout' => '',
      'levels' => NULL,
      'provides' => '',
      'view_count' => 0,
      'ynchildren' => 
      array (
        0 => 
        array (
          'content_id' => 680,
          'page_id' => 26,
          'type' => 'container',
          'name' => 'top',
          'parent_content_id' => NULL,
          'order' => 1,
          'params' => '["[]"]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 681,
              'page_id' => 26,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 680,
              'order' => 6,
              'params' => '["[]"]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 682,
                  'page_id' => 26,
                  'type' => 'widget',
                  'name' => 'pdaportfolio.browse-menu',
                  'parent_content_id' => 681,
                  'order' => 3,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
        1 => 
        array (
          'content_id' => 683,
          'page_id' => 26,
          'type' => 'container',
          'name' => 'main',
          'parent_content_id' => NULL,
          'order' => 2,
          'params' => '[""]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 684,
              'page_id' => 26,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 683,
              'order' => 6,
              'params' => '[""]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 685,
                  'page_id' => 26,
                  'type' => 'widget',
                  'name' => 'core.content',
                  'parent_content_id' => 684,
                  'order' => 6,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
                1 => 
                array (
                  'content_id' => 686,
                  'page_id' => 26,
                  'type' => 'widget',
                  'name' => 'core.comments',
                  'parent_content_id' => 684,
                  'order' => 7,
                  'params' => '{"title":"Comments"}',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'pdaportfolio_index_index' => 
    array (
      'page_id' => 27,
      'name' => 'pdaportfolio_index_index',
      'displayname' => 'Portfolio Home Page',
      'url' => NULL,
      'title' => 'Portfolio Home Page',
      'description' => 'This is the portfolio home page.',
      'keywords' => '',
      'custom' => 0,
      'fragment' => 0,
      'layout' => '',
      'levels' => NULL,
      'provides' => 'no-viewer;no-subject',
      'view_count' => 0,
      'ynchildren' => 
      array (
        0 => 
        array (
          'content_id' => 687,
          'page_id' => 27,
          'type' => 'container',
          'name' => 'top',
          'parent_content_id' => NULL,
          'order' => 1,
          'params' => '["[]"]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 688,
              'page_id' => 27,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 687,
              'order' => 6,
              'params' => '["[]"]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 689,
                  'page_id' => 27,
                  'type' => 'widget',
                  'name' => 'pdaportfolio.browse-menu',
                  'parent_content_id' => 688,
                  'order' => 3,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
        1 => 
        array (
          'content_id' => 690,
          'page_id' => 27,
          'type' => 'container',
          'name' => 'main',
          'parent_content_id' => NULL,
          'order' => 2,
          'params' => '[""]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 691,
              'page_id' => 27,
              'type' => 'container',
              'name' => 'right',
              'parent_content_id' => 690,
              'order' => 5,
              'params' => '["[]"]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 692,
                  'page_id' => 27,
                  'type' => 'widget',
                  'name' => 'pdaportfolio.portfolio-search',
                  'parent_content_id' => 691,
                  'order' => 8,
                  'params' => '{"title":"","name":"pdaportfolio.portfolio-search"}',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
            1 => 
            array (
              'content_id' => 693,
              'page_id' => 27,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 690,
              'order' => 6,
              'params' => '[""]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 694,
                  'page_id' => 27,
                  'type' => 'widget',
                  'name' => 'core.content',
                  'parent_content_id' => 693,
                  'order' => 6,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'pdaportfolio_index_manage' => 
    array (
      'page_id' => 28,
      'name' => 'pdaportfolio_index_manage',
      'displayname' => 'Portfolio Manage Page',
      'url' => NULL,
      'title' => 'Portfolio Manage Page',
      'description' => 'This is the portfolio manage page.',
      'keywords' => '',
      'custom' => 0,
      'fragment' => 0,
      'layout' => '',
      'levels' => NULL,
      'provides' => '',
      'view_count' => 0,
      'ynchildren' => 
      array (
        0 => 
        array (
          'content_id' => 695,
          'page_id' => 28,
          'type' => 'container',
          'name' => 'top',
          'parent_content_id' => NULL,
          'order' => 1,
          'params' => '["[]"]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 696,
              'page_id' => 28,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 695,
              'order' => 6,
              'params' => '["[]"]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 697,
                  'page_id' => 28,
                  'type' => 'widget',
                  'name' => 'pdaportfolio.browse-menu',
                  'parent_content_id' => 696,
                  'order' => 3,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
        1 => 
        array (
          'content_id' => 698,
          'page_id' => 28,
          'type' => 'container',
          'name' => 'main',
          'parent_content_id' => NULL,
          'order' => 2,
          'params' => '[""]',
          'attribs' => NULL,
          'ynchildren' => 
          array (
            0 => 
            array (
              'content_id' => 699,
              'page_id' => 28,
              'type' => 'container',
              'name' => 'right',
              'parent_content_id' => 698,
              'order' => 5,
              'params' => '["[]"]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 700,
                  'page_id' => 28,
                  'type' => 'widget',
                  'name' => 'pdaportfolio.portfolio-search',
                  'parent_content_id' => 699,
                  'order' => 8,
                  'params' => '{"title":"","name":"pdaportfolio.portfolio-search"}',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
            1 => 
            array (
              'content_id' => 701,
              'page_id' => 28,
              'type' => 'container',
              'name' => 'middle',
              'parent_content_id' => 698,
              'order' => 6,
              'params' => '[""]',
              'attribs' => NULL,
              'ynchildren' => 
              array (
                0 => 
                array (
                  'content_id' => 702,
                  'page_id' => 28,
                  'type' => 'widget',
                  'name' => 'core.content',
                  'parent_content_id' => 701,
                  'order' => 6,
                  'params' => '["[]"]',
                  'attribs' => NULL,
                  'ynchildren' => 
                  array (
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);?>