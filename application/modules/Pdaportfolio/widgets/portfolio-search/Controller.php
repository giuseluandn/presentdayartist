<?php

/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
class Pdaportfolio_Widget_PortfolioSearchController extends Engine_Content_Widget_Abstract {
	public function indexAction() {
		// Data preload
		$viewer = Engine_Api::_ ()->user ()->getViewer ();
		$params = array ();

		// Get search form
		$this->view->form = $form = new Pdaportfolio_Form_Search ();

		//$form->setAction ( $this->view->url ( array (), 'default' ) . "portfolio/" );

		$request = Zend_Controller_Front::getInstance ()->getRequest ();
		$params = $request->getParams ();
		$form->populate ( $params );
	}
}