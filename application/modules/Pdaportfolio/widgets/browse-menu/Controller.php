<?php

/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
class Pdaportfolio_Widget_BrowseMenuController extends Engine_Content_Widget_Abstract
{

	public function indexAction()
	{
		return $this->setNoRender();
		// Get navigation
		$this -> view -> navigation = Engine_Api::_() -> getApi('menus', 'core') -> getNavigation('pdaportfolio_main');
	}

}
