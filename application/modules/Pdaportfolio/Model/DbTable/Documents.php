<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */

class Pdaportfolio_Model_DbTable_Documents extends Engine_Db_Table {
	protected $_rowClass = "Pdaportfolio_Model_Document";

	public function checkFileUploaded($file_id){
		$file_row = Engine_Api::_ ()->getItem ( 'pdaportfolio_document', $file_id);
		if (is_object ( $file_row )) {
			return TRUE;
		}
		return FALSE;
	}
}
