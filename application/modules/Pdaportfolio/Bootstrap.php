<?php

class Pdaportfolio_Bootstrap extends Engine_Application_Bootstrap_Abstract
{
    public function _initLayout()
    {

        $view = Zend_Registry::get('Zend_View');
        $view->headScript()
            ->appendFile($view->layout()->staticBaseUrl . 'application/modules/Pdaportfolio/externals/scripts/core.js');

        $view->headTranslate(
            array(
                'Please drop your files here',
                'No file to upload',
                'Edit',
                'Move',
                'Delete',
                'Share',
                ' MB',
                ' KB'
            )
        );
    }
}

?>