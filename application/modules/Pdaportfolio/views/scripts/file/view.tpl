<div class="ynfs_folder_navigation ynfs_block">
	<a class='buttonlink icon_back' href='javascript:void(0);' onClick='history.go(-1);'>
		<?php echo $this->translate('Go Back') ?>
	</a>
</div>
<div class="ynfs_filepreview">
	<div class="ynfs_filepreview_filename">
		<?php
		$file_img_url = $this->baseUrl() . "/application/modules/Pdaportfolio/externals/images/file_types/" . $this->file->getFileIcon();
		$file_path = $this->layout()->staticBaseUrl . "portfolio/file/download/" . $this->file->getIdentity();
		?>
		<div class="ynfs_icon ynfs_file_default" style="background-image: url(<?php echo $file_img_url ?>);float: left;">
		</div>
		<div><?php echo $this->htmlLink($this->file->getHref(),$this->file->name)?>
			<?php echo $this->translate('posted by');?> <?php echo $this->htmlLink($this->file->getOwner()->getHref(), $this->file->getOwner()->getTitle()) ?>
		</div>

		<button onclick="location.href='<?php echo $file_path  ?>';" style="float:right;" name="filepreview_download" id="filepreview_download" type="button">Download</button>
	</div>
</div>
<br>
<?php if(in_array($this->file->ext, array('mp3'))):
	$this->headScript()
		->appendFile($this->layout()->staticBaseUrl . 'application/modules/Pdaportfolio/externals/scripts/aplayer/APlayer.min.js');
	$this->headLink()
		->prependStylesheet($this->layout()->staticBaseUrl . 'application/modules/Pdaportfolio/externals/scripts/aplayer/APlayer.min.css');
	$user = Engine_Api::_()->getItem('user', $this->file->user_id);
	$user_photo = ($user->getPhotoUrl('thumb.profile')) ? $user->getPhotoUrl('thumb.profile') : "application/modules/User/externals/images/nophoto_user_thumb_profile.png";
	?>
	<div id="player1" class="aplayer"></div>
	<script type="text/javascript">
		var ap1 = new APlayer({
			element: document.getElementById('player1'),
			narrow: false,
			autoplay: false,
			showlrc: false,
			mutex: true,
			theme: '#e6d0b2',
			music: {
				title: '<?php echo $this->file->name?>',
				author: "<?php echo $user->getTitle(); ?>",
				url: '<?php echo $this->layout()->staticBaseUrl.$this->folder -> path . $this->file -> name?>',
				pic: '<?php echo $user_photo?>'
			}
		});
		ap1.on('play', function () {
			console.log('play');
		});
		ap1.on('play', function () {
			console.log('play play');
		});
		ap1.on('pause', function () {
			console.log('pause');
		});
		ap1.on('canplay', function () {
			console.log('canplay');
		});
		ap1.on('playing', function () {
			console.log('playing');
		});
		ap1.on('ended', function () {
			console.log('ended');
		});
		ap1.on('error', function () {
			console.log('error');
		});
		ap1.init();
	</script>

<?php elseif(in_array($this->file->ext, array('mp4'))):?>

	<video id="video1" style="width:1100px;max-width:100%;" controls="">
		<source src="<?php echo $this->layout()->staticBaseUrl.$this->folder -> path . $this->file -> name?>" type="video/mp4">
		Your browser does not support HTML5 video.
	</video>


<?php else:?>

<?php if($this -> api_viewer):?>
	<div id='embedded_doc'></div>
<?php elseif($this->is_support && !$this->is_image):?>
	<iframe src="https://docs.google.com/viewer?url=<?php echo $this -> file_path?>&embedded=true" class = "ynfs_googleviewer_info" frameborder="0"></iframe>
<?php endif;?>
<?php
if (!$this->is_embed):
	echo "<div class='tip'><span >" . $this->translate('The API information has not been configured.') . "</span></div>";
elseif (!$this->is_support && $this->is_image) : ?>
	<a style="cursor: default;">
		<img  class= "ynfs_filepreview_info" src="<?php echo $this->image ?>" />
	</a>
<?php elseif (!$this->is_support || Engine_Api::_() -> pdaportfolio()-> isMobile()) :
	echo "<div class='tip'><span >" . $this->translate('This document is not supported to be previewed.') . "</span></div>";
elseif (!$this->is_success) :
	echo "<div class='tip'><span >" . $this->translate('Sorry, there is something wrong with uploading document to preview') . "</span></div>";

elseif ($this->status != 'DONE') : ?>
	<div class="ynfs_embeds_unavailable">
		<div class="ynfs_embeds_error">
			<div class="ynfs_lightbox">
				<div class="ynfs_wrapper">
					<div class="ynfs_content">
						<h1><?php echo $this->translate('Temporary: This document is still converting'); ?></h1><hr>
						<h2><?php echo $this->translate('Sorry, we can not display this document.'); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php endif;?>
