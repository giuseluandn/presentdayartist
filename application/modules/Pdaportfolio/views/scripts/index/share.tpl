<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
?>


<div class="global_form_popup">
	<?php echo $this -> translate("Link:"); ?>
	<input type="text" onclick="select()" name="pdaportfolio_link" class="ynfs_share_link_textbox"
		value="<?php echo $this->base_url . 
					$this->url(array(
						'object_type' => $this->object_type,
						'object_id' => $this->object_id,
						'code' => $this->code
					), 
					'pdaportfolio_share_view', 
					true) ?>" />
	<div class="ynfs_share_link_note"><i><?php echo $this->translate("Copy to clipboard: Ctrl+C")?></i></div>
</div>

