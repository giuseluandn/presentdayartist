<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
?>
<div class="ynfs_block ynfs_controls">
	<a href="javascript:void(0);" class="buttonlink ynfs_file_add_icon" onclick="$('uploadForm').show();">
		<?php echo $this->translate('Upload File') ?>
	</a>
</div>

<form class="global_form" method="post" 
	action="<?php echo $this->url(array('action'=>'upload', 'folder_id' => $this->folder->getIdentity(), 'parent_type' => $this->parentType, 'parent_id' => $this->parentId), 'pdaportfolio_folder_specific', true); ?>" 
	enctype="multipart/form-data" id="uploadForm" style="display: none;">
	<div>
		<div>
			<ul class="form-errors" style="display: none;">
				<li></li>
			</ul>
			
			<div class="formRow">
				<div id="file-wrapper" class="form-wrapper">
					<div id="file-label" class="form-label" style="width: 104px;">
						<label for="file" class="required"><?php echo $this->translate('Upload file(s)') ?></label>
					</div>
					<div id="file-element" class="form-element">
						<input type="file" id="file" name="file[]" multiple />
						<br />
						<p style="font-style: italic; font-size: smaller;">
							<?php echo $this->translate("Use this control if your browser doesn't support Drap and Drop") ?>
						</p>
					</div>
				</div>
			</div>
			
			<div id="ynfs_upload">
			</div>
			
			<input type="hidden" name="file_total" id="file_total" value="<?php echo $this->fileTotal ?>" />
			<input type="hidden" name="max_file_total" id="max_file_total" value="<?php echo $this->maxFileTotal ?>" />
			
			<input type="hidden" name="total_size_per_user" id="total_size_per_user" value="<?php echo $this->totalSizePerUser ?>" />
			<input type="hidden" name="max_total_size_per_user" id="max_total_size_per_user" value="<?php echo $this->maxTotalSizePerUser ?>" />
			
			<div id="submit-wrapper" class="form-wrapper">
				<div id="submit-label" class="form-label" style="width: 104px;">&nbsp;</div>
				<div id="submit-element" class="form-element">
					<button name="upload" id="upload" type="submit">
						<?php echo $this->translate('Start Upload') ?>
					</button>
					<button name="cancel" id="cancel" type="button" onclick="en4.pdaportfolio.cancelUploadFile()">
						<?php echo $this->translate('Cancel') ?>
					</button>
				</div>
			</div>
		</div>
	</div>
</form>

<?php
	echo $this->partial('_browse_folders.tpl', 'pdaportfolio',
		array(
			'subFolders' => $this->subFolders,
			'foldersPermissions' => $this->foldersPermissions,
			'files' => $this->files,
			'currentFolder' => $this->folder,
			'parentType' => $this->parentType,
			'parentId' => $this->parentId,
			'canCreate' => $this->canCreate
		)
	);

	$this->headScript()
		->appendFile($this->baseUrl() . '/application/modules/Pdaportfolio/externals/scripts/Request.File.js')
		->appendFile($this->baseUrl() . '/application/modules/Pdaportfolio/externals/scripts/Form.MultipleFileInput.js')
		->appendFile($this->baseUrl() . '/application/modules/Pdaportfolio/externals/scripts/Form.Upload.js');
?>

<script>
	var upload;
	window.addEvent('domready', function(){
		// Create the file uploader
		upload = new Form.Upload('file', {
		    dropMsg: "Drop files here",
		    onComplete: function(response){
				result = JSON.decode(response);
				msg = "";
				if (result.error == "0" && result.message != "")
					msg = result.message + "\r\n";
				
				if ((typeof result.wrong_format !== "undefined") && (result.wrong_format != "")) {
					files_array = result.wrong_format.split("|");
					for (i=0; i<files_array.length-1; i++) {
						msg += files_array[i] + " - wrong file extension\n";
				  	}
				}
				if ((typeof result.wrong_size !== "undefined") && (result.wrong_size != "")) {
					files_array = result.wrong_size.split("|");
					for (i=0; i<files_array.length-1; i++) {
						msg += files_array[i] + " - wrong file size\n";
				  	}
				}
				alert(msg);
				window.location = window.location;
		    },
		    dropZone: $("global_wrapper")
		});
	});
</script>