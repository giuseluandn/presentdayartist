<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Pdaportfolio
 * @author     John
 */
?>
<?php
	echo $this->partial('_script_tags.tpl', 'pdaportfolio'); 
?>
<div class="headline">
    <h2>
        <?php echo $this->translate('Portfolio'); ?>
    </h2>
    <div class="tabs">
        <?php
        // Render the menu
        echo $this->navigation()
                ->menu()
                ->setContainer($this->navigation)
                ->render();
        ?>
    </div>
</div>

<?php echo $this->form->render($this); ?>