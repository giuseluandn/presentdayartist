<?php
/**
 * John
 *
 * @category   Application_Extensions
 * @package    Yndonation
 * @author     John
 */

class Pdaportfolio_AdminScribdController extends Core_Controller_Action_Admin {
	public function indexAction()
	{
		$this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
			->getNavigation('pdaportfolio_admin_main', array(), 'pdaportfolio_admin_main_scribd');

		// Make form
		$this->view->form = $form = new Pdaportfolio_Form_Admin_Scribd();
		// get settings
		$settings = Engine_Api::_()->getApi('settings', 'core');
		if( $this->getRequest()->isPost() && $form->isValid($this->_getAllParams()) )
		{
			$values = $form->getValues();
			foreach ($values as $key => $value){
				Engine_Api::_()->getApi('settings', 'core')->setSetting($key, $value);
			}
			$form->addNotice('Your changes have been saved.');
		}
	}
}