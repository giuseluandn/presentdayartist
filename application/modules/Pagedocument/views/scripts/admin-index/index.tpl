<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page Documents
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: index.tpl 2011-09-01 13:17:53 kirill $
 * @author     Kirill
 */

?>
<h2><?php echo $this->translate("pagedocument_Admin title") ?></h2>

<?php if( count($this->navigation) ): ?>
  <div class='page_admin_tabs'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render(); ?>
  </div>
<?php endif; ?>
<h3><?php echo $this->translate("Document Settings") ?></h3>
<?php echo $this->content()->renderWidget('page.admin-settings-menu',array('active_item'=>'page_admin_main_documents')); ?>
<div class="settings admin_home_middle" style="clear: none;">
  <div class="settings">
    <?php $this->form->getDecorator('Description')->setOption('escape', false); ?>
    <?php echo $this->form->render($this); ?>
  </div>
</div>