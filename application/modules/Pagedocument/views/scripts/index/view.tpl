<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page Documents
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: view.tpl 2011-09-01 13:17:53 kirill $
 * @author     Kirill
 */

?>
<div class='pagedocument' id="page_document_container">
  <h3>
    <?php echo $this->document->getTitle(); ?>
  </h3>

  <?php if (!$this->isAllowedPost): ?>
<div class="backlink_wrapper">
  <a class="backlink" href="javascript:page_document.list()"><?php echo $this->translate('Back To Documents'); ?></a>
</div>
<?php endif; ?>

  <div class="pagedocument-info">
    <?php echo $this->translate("pagedocument_Posted", $this->timestamp($this->document->creation_date)); ?><br />
    <?php if($this->document->getCategory()): ?>
      <?php echo $this->translate('pagedocument_Category', $this->document->getCategory()); ?>
    <?php else: ?>
      <?php echo $this->translate('pagedocument_Uncategorized'); ?>
    <?php endif;?><br />
    <?php echo $this->translate("pagedocument_Views", $this->document->view_count); ?>
  </div>

  <div class="pagedocument-description">
      <?php echo $this->document->document_description; ?>
  </div>


  <div class="pagedocument-options">
    <?php
      if($this->document->download_allow != '' && $this->document->download_allow != 'view-only'){
        $link = $this->scribd->getDonloadLink($this->document->doc_id, 'original', $this->viewer->getIdentity());
        if($link && $link['download_link']){
          echo $this->htmlLink($link['download_link'], $this->translate("Download"), array('target' => '_blank'));
        }
      }
    ?>
  </div>
  <?php if($this->document->status=='DONE'): ?>
    <div id='embedded_doc'></div>
    <script type="text/javascript">
    en4.core.runonce.add(function(){
          var doc = scribd.Document.getDoc('<?php echo $this->document->doc_id; ?>', '<?php echo $this->document->access_key; ?>');
//					var oniPaperReady = function(e){}

        var onDocReady = function(e){
          // scribd_doc.api.setPage(3);
        }

        doc.addParam('jsapi_version', 2);
        doc.addEventListener('docReady', onDocReady);
			  doc.addParam( 'width', <?php echo $this->width; ?> );
        doc.addParam( 'height', <?php echo $this->height; ?> );
//			  doc.addParam("full_screen_type", 'flash');
//				doc.addParam("hide_disabled_buttons", 'true');
          <?php if (!empty($this->document->secure_allow)): ?>
  					doc.addParam("use_ssl", 'true');
	  				doc.grantAccess('<?php echo $this->user_id; ?>', '<?php echo $this->session_id; ?>', '<?php echo $this->signature; ?>');
          <?php else : ?>
            doc.addParam('public', true);
          <?php endif; ?>
//					doc.addEventListener( 'iPaperReady', oniPaperReady );
//					doc.write( 'embedded_flash' );
        doc.write('embedded_doc');
      });
		</script>
  <?php elseif($this->document->status=='PROCESSING'): ?>
    <div class="pagedocument-view-conversion"><?php echo $this->translate('pagedocument_Document in processing'); ?></div>
  <?php else: ?>
    <div class="pagedocument-view-error"><?php echo $this->translate('pagedocument_Document error');?></div>
  <?php endif; ?>

</div>



<?php if (Engine_Api::_()->getDbTable('modules' ,'hecore')->isModuleEnabled('wall')): ?>
  <?php echo $this->wallComments($this->document, $this->viewer()); ?>
<?php else: ?>
<div class="comments" id="pagedocument_comments"></div>
<?php endif;?>