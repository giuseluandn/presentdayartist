<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Page Documents
 * @copyright  Copyright Hire-Experts LLC
 * @license    http://www.hire-experts.com
 * @version    $Id: getcreateform.tpl 2011-09-01 13:17:53 kirill $
 * @author     Kirill
 */

?>
<script type="text/javascript">
  en4.core.runonce.add(function (){
tinyMCE.init({
mode: "exact",
plugins: "emotions,table,fullscreen,media,preview,paste",
theme: "advanced",
theme_advanced_buttons1: "undo,redo,cleanup,removeformat,pasteword,|,code,media,image,fullscreen,preview",
theme_advanced_buttons2: "",
theme_advanced_buttons3: "",
theme_advanced_toolbar_align: "left",
theme_advanced_toolbar_location: "top",
element_format: "html",
height: "225px",
convert_urls: false,
media_strict: false,
elements: "document_description",
language: "en",
directionality: "ltr"
});    
  });

</script>
 <?php if(!$this->isAllowedPost){?>
  <div class="page_document_error">
    <ul class="form-errors"><li><?php echo $this->translate('pagedocument_Scribd_credentials_need'); ?></li></ul>
  </div>
<?php } elseif($this->isCreationAllowed){ ?>
  <?php echo $this->createForm->render($this); ?>
<?php } else{ ?>
  <div class="page_document_error">
    <ul class="form-errors"><li><?php echo $this->translate('pagedocument_Limit_is_exeeded'); ?></li></ul>
  </div>
<?php } ?>
