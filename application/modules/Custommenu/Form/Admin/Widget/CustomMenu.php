<?php

class Custommenu_Form_Admin_Widget_CustomMenu extends Core_Form_Admin_Widget_Standard {
    public function init() {
        parent::init();


        // Set form attributes
        $this
            ->setTitle('Custom Menu')
            ->setDescription('Please choose a menu.');

        // Element: name
        $this->addElement('Select', 'menu', array(
            'label' => 'Menu',
        ));

        foreach( Engine_Api::_()->getDbtable('menus', 'core')->fetchAll() as $menu ) {
            $this->menu->addMultiOption($menu->name, $menu->title);
        }

        // Element: ulClass
        $this->addElement('Text', 'ulClass', array(
            'label' => 'List CSS Class',
        ));

        // Element: sticky
        $this->addElement('Radio', 'stickyWidget', array(
            'label' => 'Make widget sticky?',
            'multioptions' => array(
                'no'  => 'No',
                'yes' => 'Yes'
            ),
            'value' => 'no',
            //'disabled' => true
        ));
                                        
    }
}