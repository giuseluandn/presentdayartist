<form method="post" class="global_form_popup">
    <div>
      <h3><?php echo $this->translate("Delete Menu Item") ?></h3>
      <p>        
          <?php echo $this->translate("Are you sure that you want to delete menu item?"); ?>
      </p>
      <br />
      <p>
        <button type='submit'><?php echo $this->translate("Delete") ?></button>
        or <a href='javascript:void(0);' onclick='javascript:parent.Smoothbox.close()'>cancel</a>
      </p>
    </div>
  </form>   