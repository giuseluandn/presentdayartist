<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Custommenu/externals/scripts/nested-sortable.js') ?>

<?php
    function itemTypeToName($type) {
        switch ($type) {
            case 1 :
                return 'Avatar';
                break;
            case 2 :
                return 'Avatar & Profile Link';
                break;
            case 3 :
                return 'Profile Link';
                break;
            case 4 :
                return 'Updates';
                break;
            case 5 :
                return 'Search';
                break;
            case 6 :
                return 'Horizontal Divider';
                break;
        }
    }
?>

<script type="text/javascript">  
    
    window.addEvent('domready', function() {

        new Nested('menu_list', {
            onStart: function(el) {
                el.addClass('drag');
            },
            onComplete: function(el) {
                el.removeClass('drag');                                
                reorder(el);                
            }
        }); 

        var reorder = function(e) {
            var menuitems = e.parentNode.childNodes;            
            var parent    = e.getParent('li');                        
            var ordering  = {};
            
            var menu = '<?php echo $this->selectedMenu->name; ?>';
            ordering['format'] = 'json';
            
            var i = 1;
            
            for (var menuitem in menuitems) {
                var child_id = menuitems[menuitem].id;                        
                
                if ((child_id != undefined) && (child_id.substr(0, 5) == 'admin')) {
                    child_id = child_id.replace('admin_menus_item_', '');
                    if (parent != null) {
                        var parent_id = parent.id.replace('admin_menus_item_', '');
                        var submenu = parent.id.replace('admin_menus_item_', '') + '_submenu';
                        ordering[child_id] = {
                            menu    : menu,
                            submenu : submenu,
                            parent  : parent_id,
                            order   : i
                        };                    
                    } else {
                        ordering[child_id] = {
                            menu    : menu,                            
                            order   : i
                        };
                    }
                    
                                                        
                    i++;
                }            
            }
            
            // Send request
            var url     = '<?php echo $this->url(array('action' => 'order')) ?>';
            var request = new Request.JSON({
                'url'     : url,
                'method'  : 'POST',
                'data'    : ordering,
                onSuccess : function(responseJSON) {},                                    
                onError   : function(text) {
                    console.log(text);
                }                                    
            });        
            request.send();
        }
               
    });    
</script>

<h2>
  <?php echo $this->translate('Menu Editor') ?>
</h2>

<div class="tip">
    <span>
        <?php echo $this->translate('If you want to use sub-menu with drop down option go to Layout editor and set Custom Menu widget');?>
    </span>
</div>

<br />
<p>
  <?php echo $this->translate('CORE_VIEWS_SCRIPTS_ADMINMENU_INDEX_DESCRIPTION') ?>
</p>
<?php
$settings = Engine_Api::_()->getApi('settings', 'core');
if( $settings->getSetting('user.support.links', 0) == 1 ) {
	echo 'More info: <a href="http://support.socialengine.com/questions/217/Admin-Panel-Layout-Menu-Editor" target="_blank">See KB article</a>.';	
} 
?>	
<br />
<br />

<div class="admin_menus_filter">
  <form action="<?php echo $this->url() ?>" method="get">
    <h3><?php echo $this->translate("Editing:") ?></h3>
    <?php echo $this->formSelect('name', $this->selectedMenu->name, array('onchange' => '$(this).getParent(\'form\').submit();'), $this->menuList) ?>
  </form>
</div>

<br />

<div class="admin_menus_options">
  <?php echo $this->htmlLink(array('reset' => false, 'action' => 'create', 'name' => $this->selectedMenu->name), $this->translate('Add Item'), array('class' => 'buttonlink admin_menus_additem smoothbox')) ?>
  <?php echo $this->htmlLink(
          array(
              'reset' => false, 
              'module' => 'custommenu',
              'controller' => 'menus', 
              'action' => 'add-special-item', 
              'name' => $this->selectedMenu->name
          ), 
          $this->translate('Add Special Item'), 
          array(
              'class' => 'buttonlink admin_menus_additem smoothbox'
          )
  );
  ?>
  <?php echo $this->htmlLink(array('controller' => 'menus', 'action' => 'create-menu'), $this->translate('Add Menu'), array('class' => 'buttonlink admin_menus_addmenu smoothbox')) ?>
  <?php if( $this->selectedMenu->type == 'custom' ): ?>
    <?php echo $this->htmlLink(array('reset' => false, 'action' => 'delete-menu', 'name' => $this->selectedMenu->name), $this->translate('Delete Menu'), array('class' => 'buttonlink admin_menus_deletemenu smoothbox')) ?>
  <?php endif ?>
</div>

<br />

<ul class="admin_menus_items" id='menu_list'>
  <?php foreach( $this->menuItems as $menuItem ): ?>
   <li class="admin_menus_item<?php if(isset($menuItem->enabled) && !$menuItem->enabled) echo ' disabled'; if(isset($menuItem->params['itemtype'])) echo ' special-item-admin'; ?>" id="admin_menus_item_<?php echo $menuItem->name ?>">       
       <span class="item_wrapper">
        <span class="item_options">                   
          <?php echo $this->htmlLink(array('reset' => false, 'action' => 'edit', 'name' => $menuItem->name), $this->translate('edit'), array('class' => 'smoothbox')) ?>
          <?php if( $menuItem->custom ): ?>
            | <?php echo $this->htmlLink(array('reset' => false, 'action' => 'delete', 'name' => $menuItem->name), $this->translate('delete'), array('class' => 'smoothbox')) ?>
          <?php endif; ?>
        </span>
        <span class="item_label">           
          <?php if (!empty($menuItem->params['icon'])) : ?>                         
              <img class="item-icon" src="<?php echo $menuItem->params['icon']; ?>" />
          <?php endif; ?>

          <?php if(isset($menuItem->params['itemtype'])) : ?>
            <?php echo itemTypeToName($menuItem->params['itemtype']); ?>
          <?php else : ?>
            <?php echo $this->translate($menuItem->label); ?>
          <?php endif; ?>

        </span>
        <span class="item_url">
          <?php
            $href = '';
            if( isset($menuItem->params['uri']) ) {
              echo $this->htmlLink($menuItem->params['uri'], $menuItem->params['uri']);
            } else if( !empty($menuItem->plugin) ) {
              echo '<a>(' . $this->translate('variable') . ')</a>';
            } else {
              //echo $this->htmlLink($this->htmlLink()->url($menuItem->params), $this->htmlLink()->url($menuItem->params));
            }
          ?>
        </span>                                    
      </span>                            
              
      <?php if (count($this->childItems[$menuItem->name])) : ?>
      <!-- MENU SUBITEMS --> 
      <ul class="subitems">
        <?php foreach ($this->childItems[$menuItem->name] as $item) : ?>
        <li class="admin_menus_item<?php if(isset($item->enabled) && !$item->enabled) echo ' disabled'; if(isset($item->params['itemtype'])) echo ' special-item-admin'; ?> custom_menu_subitem nCollapse" id="admin_menus_item_<?php echo $item->name ?>">
            <span class="item_wrapper">
                <span class="item_options">
                <?php echo $this->htmlLink(array('reset' => false, 'action' => 'edit', 'name' => $item->name), $this->translate('edit'), array('class' => 'smoothbox')) ?>
                <?php if( $item->custom ): ?>
                    | <?php echo $this->htmlLink(array('reset' => false, 'action' => 'delete', 'name' => $item->name), $this->translate('delete'), array('class' => 'smoothbox')) ?>
                <?php endif; ?>
                </span>
                <span class="item_label">
                    <?php if (!empty($item->params['icon'])) : ?>                         
                        <img class="item-icon" src="<?php echo $item->params['icon']; ?>" />
                    <?php endif; ?>

                    <?php if(isset($item->params['itemtype'])) : ?>
                    <?php echo itemTypeToName($item->params['itemtype']); ?>
                    <?php else : ?>
                    <?php echo $this->translate($item->label); ?>
                    <?php endif; ?>

                </span>  
                <span class="item_url">
                <?php
                    $href = '';
                    if( isset($item->params['uri']) && !empty($item->params['uri']) ) {
                        echo $this->htmlLink($item->params['uri'], $item->params['uri']);
                    } else if( !empty($item->plugin) ) {
                        echo '<a>(' . $this->translate('variable') . ')</a>';
                    } else {
                        //echo $this->htmlLink($this->htmlLink()->url($item->params['uri']), $this->htmlLink()->url($item->params['uri']));
                    }
                ?>
                </span>    
            </span>    
        </li>
        <?php endforeach; ?>
      </ul>
      <!-- END OF MENU SUBITEMS --> 
      <?php endif ?>
            
    </li>               
  <?php endforeach; ?>
</ul>
    