<form method="post" class="global_form_popup">
    <div>
      <h3><?php echo $this->translate("Notice") ?></h3>
      <p>        
          <?php echo $this->translate("The item you want to delete has a sub-menu with default items. After item deleting all sub-menu elements will be moved to main navigation"); ?>
      </p>
      <br />
      <p>
        <button type='submit'><?php echo $this->translate("Delete") ?></button>
        or <a href='javascript:void(0);' onclick='javascript:parent.Smoothbox.close()'>cancel</a>
        <input type="hidden" name="confirm" value="1" /> 
      </p>
    </div>
  </form> 