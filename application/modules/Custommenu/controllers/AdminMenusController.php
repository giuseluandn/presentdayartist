<?php

Engine_Loader::loadClass('application_modules_Core_controllers_AdminMenusController');

class Custommenu_AdminMenusController extends Core_AdminMenusController {

    const CHECK_ACCESS_METHOD = 'Custommenu_Plugin_Menus::checkAccess';

    public function init() {
        parent::init();
    }

    public function indexAction() {
        parent::indexAction();

        // Get subitems
        $childItems = array();

        if (!empty($this->view->menuItems)) {

            foreach ($this->view->menuItems as $item) {

                if (!is_null($item->submenu)) {
                    $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
                    $children = $menuItemsTable
                            ->select()
                            ->where('menu = ?', $item->submenu)
                            ->order('order');

                    $childItems[$item->name] = $menuItemsTable->fetchAll($children);
                }
            }
        }

        $this->view->childItems = $childItems;
    }

    /**
     * Create menu item
     * @return type
     * @throws Core_Model_Exception
     */
    public function createAction() {
        $this->view->name = $name = $this->_getParam('name', 'core_main');

        // Get list of menus
        $menus = $this->_menus;

        // Check if selected menu is in list
        $selectedMenu = $menus->getRowMatching('name', $name);

        if (null === $selectedMenu) {
            throw new Core_Model_Exception('Invalid menu name');
        }
        $this->view->selectedMenu = $selectedMenu;

        // Get form
        $this->view->form = $form = new Custommenu_Form_Admin_Item();

        // Rename default form title 
        $form->setTitle('Create Menu Item');
        $form->submit->setLabel('Create Menu Item');

        // Check stuff
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Save
        $values = $form->getValues();
        $label = $values['label'];
        unset($values['label']);

        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

        $menuItem = $menuItemsTable->createRow();
        $menuItem->label = $label;
        $menuItem->menu = $name;
        $menuItem->module = 'core'; // Need to do this to prevent it from being hidden
        $menuItem->plugin = self::CHECK_ACCESS_METHOD;
        $menuItem->submenu = '';
        $menuItem->custom = 1;
        $menuItem->enabled = $values['enabled'];
        unset($values['enabled']);
        $menuItem->params = $values;

        $menuItem->name = 'custommenu_' . sprintf('%s', uniqid());
        $menuItem->save();

        $this->view->status = true;
        $this->view->form = null;
    }

    /**
     * Edit menu item
     * @return type
     * @throws Core_Model_Exception
     */
    public function editAction() {
        $this->view->name = $name = $this->_getParam('name');

        // Get menu item
        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
        $menuItemsSelect = $menuItemsTable->select()->where('name = ?', $name);

        if (!empty($this->_enabledModuleNames)) {
            $menuItemsSelect->where('module IN(?)', $this->_enabledModuleNames);
        }

        $this->view->menuItem = $menuItem = $menuItemsTable->fetchRow($menuItemsSelect);

        $this->view->form = $form = new Custommenu_Form_Admin_Item();

        if (!empty($menuItem->params['itemtype'])) {
            $form->setSpecials();
            $form->removeElement('description');
        }

        if (!$menuItem) {
            return $this->_forward('success', 'utility', 'core', array(
                        'parentRefresh' => true,
                        'messages' => $this->view->translate('Menu item doesn\'t exist. Enable plugin.')
            ));
        }

        // Make safe
        $menuItemData = $menuItem->toArray();

        if (isset($menuItemData['params']) && is_array($menuItemData['params'])) {
            $menuItemData = array_merge($menuItemData, $menuItemData['params']);
        }
        if (!$menuItem->custom) {
            $form->removeElement('uri');
        }
        unset($menuItemData['params']);

        // Check stuff
        if (!$this->getRequest()->isPost()) {
            $form->populate($menuItemData);
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $values = $form->getValues();

        // Save
        $menuItem->label = $values['label'];
        $menuItem->enabled = !empty($values['enabled']);

        unset($values['label']);
        unset($values['enabled']);

        if (!$menuItem->params) {
            $menuItem->params = array();
        }

        if (empty($menuItem->params)) {
            $menuItem->params = array_merge($menuItem->params, $values);
        } else {
            $tempParams = array();
            foreach ($values as $key => $val) {
                $tempParams[$key] = $val;
            }
            $menuItem->params = array_merge($menuItem->params, $tempParams);
        }

        if (!empty($values['target'])) {
            $menuItem->params = array_merge($menuItem->params, array('target' => $values['target']));
        } else if (isset($menuItem->params['target'])) {
            // Remove the target
            $tempParams = array();
            foreach ($menuItem->params as $key => $item) {
                if ($key != 'target') {
                    $tempParams[$key] = $item;
                }
            }
            $menuItem->params = $tempParams;
        }

        if (!empty($values['icon'])) {
            $menuItem->params = array_merge($menuItem->params, array('icon' => $values['icon']));
        } else if (isset($menuItem->params['icon'])) {
            // Remove the target
            $tempParams = array();
            foreach ($menuItem->params as $key => $item) {
                if ($key != 'icon') {
                    $tempParams[$key] = $item;
                }
            }
            $menuItem->params = $tempParams;
        }

        unset($values['uri']);
        unset($values['icon']);
        unset($values['target']);

        $menuItem->save();

        $this->view->status = true;
        $this->view->form = null;

        return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'messages' => $this->view->translate('Menu item successfully updated')
        ));
    }

    /**
     * Reordering 
     * @return void
     */
    public function orderAction() {
        if (!$this->getRequest()->isPost()) {
            return;
        } else {
            $menu = $this->getRequest()->getPost();
        }

        unset($menu['format']);

        $itemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');
        $db = $itemsTable->getAdapter();

        $db->beginTransaction();

        try {
            foreach ($menu as $itemId => $itemData) {
                $menuItem = $itemsTable->fetchRow($itemsTable->select()->where('name = ?', $itemId));
                if ($itemId == $menuItem->name) {
                    if (!empty($itemData['parent'])) {
                        $parent = $itemsTable->fetchRow($itemsTable->select()
                                        ->where('menu = ?', $itemData['menu'])
                                        ->where('name = ?', $itemData['parent'])
                        );

                        $parent->submenu = $itemData['submenu'];
                        $parent->save();

                        $menuItem->menu = $itemData['submenu'];
                    } else {
                        $menuItem->menu = $itemData['menu'];
                    }
                    $menuItem->order = $itemData['order'];
                    $menuItem->save();
                }
            }
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return;
    }

    /*
     * Delete item and subitems
     */

    public function deleteAction() {

        $this->view->name = $name = $this->_getParam('name');

        if ($this->getRequest()->isPost()) {
            $confirm = $this->_getParam('confirm', null);

            $table = Engine_Api::_()->getDbtable('menuItems', 'core');
            $db = $table->getAdapter();

            $itemParent = $table->fetchRow(
                    $table->select()
                            ->where('name = ?', $name)
            );

            if (is_null($itemParent)) {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRefresh' => true,
                            'messages' => $this->view->translate('Menu item doesn\'t exist')
                ));
            }

            $menuName = "{$name}_submenu";

            $itemsChildren = $table->fetchAll(
                    $table->select()
                            ->where('menu = ?', $menuName)
            );

            $db->beginTransaction();

            try {
                $parentMenu = $itemParent->menu;
                // if exist a submenu of current item                
                if ($itemParent->submenu) {
                    foreach ($itemsChildren as $child) {
                        if ($child->custom == 0) {
                            if ($confirm == 1) {
                                $child->menu = $parentMenu;
                                $child->order = 999;
                                $child->save();
                                $itemParent->delete();
                            } else {
                                return $this->render('notice');
                            }
                        } else {
                            $child->delete();
                        }
                    }
                }
                $itemParent->delete();

                $db->commit();

                return $this->_forward('success', 'utility', 'core', array(
                            'parentRefresh' => true,
                            'messages' => $this->view->translate('Menu item successfully deleted')
                ));
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }
        }
    }

    public function deleteMenuAction() {
        $name = $this->_getParam('name');

        if ($this->getRequest()->isPost()) {
            $menuTable = Engine_Api::_()->getDbtable('menus', 'core');
            $db = $menuTable->getAdapter();

            $menuRow = $menuTable->fetchRow(
                    $menuTable->select()
                            ->where('name = ?', $name)
            );

            if (is_null($menuRow)) {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRefresh' => true,
                            'messages' => $this->view->translate('Menu doesn\'t exist')
                ));
            }

            $subMenu = "{$name}_submenu";

            $itemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

            $itemParent = $itemsTable->fetchRow(
                    $itemsTable->select()
                            ->where('menu = ?', $name)
            );

            $itemsChildren = $itemsTable->fetchAll(
                    $itemsTable->select()
                            ->where('submenu = ?', $subMenu)
            );

            $db->beginTransaction();

            try {
                $menuRow->delete();

                if (!is_null($itemParent)) {
                    $itemParent->delete();
                }

                if (!is_null($itemsChildren)) {
                    foreach ($itemsChildren as $child) {
                        $child->delete();
                    }
                }

                $db->commit();

                return $this->_forward('success', 'utility', 'core', array(
                            'parentRedirect' => Zend_Controller_Front::getInstance()->getBaseUrl() . '/admin/custommenu/menus/index',
                            'messages' => $this->view->translate('Menu successfully deleted'),
                            'name' => 'core-main'
                ));
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }
        }
    }

    /**
     * Add special menu items like avatar, profile, updates etc...
     */
    public function addSpecialItemAction() {
        $this->view->name = $name = $this->_getParam('name', 'core_main');

        // Get list of menus
        $menus = $this->_menus;

        // Check if selected menu is in list
        $selectedMenu = $menus->getRowMatching('name', $name);

        if (null === $selectedMenu) {
            throw new Core_Model_Exception('Invalid menu name');
        }
        $this->view->selectedMenu = $selectedMenu;

        // Get form
        $this->view->form = $form = new Custommenu_Form_Admin_Item();

        // Changing default form params
        $form->removeElement('description');
        $form->setSpecials();
        $form->submit->setLabel('Create Menu Item');

        // Check stuff
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Save
        $values = $form->getValues();
        //$label = $values['label'];
        //unset($values['label']);

        $menuItemsTable = Engine_Api::_()->getDbtable('menuItems', 'core');

        // Check if Search || Updates items are present in current menu
        $menuItems = $menuItemsTable->fetchAll(
                $menuItemsTable->select()
                        ->where('menu = ?', $name)
                        ->where('custom = ?', 1)
                        ->order('order')
        );

        foreach ($menuItems as $item) {
            if (is_null($item->params['itemtype'])) {
                continue;
            }

            if (($values['itemtype'] == 4 && $item->params['itemtype'] == $values['itemtype']) || ($values['itemtype'] == 5 && $item->params['itemtype'] == $values['itemtype'])) {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRefresh' => true,
                            'messages' => $this->view->translate('There is already such item in menu')
                ));
            }
        }
        // End of check

        $menuItem = $menuItemsTable->createRow();
        $menuItem->label = '';
        $menuItem->menu = $name;
        $menuItem->module = 'core'; // Need to do this to prevent it from being hidden
        $menuItem->plugin = self::CHECK_ACCESS_METHOD;
        $menuItem->submenu = '';
        $menuItem->custom = 1;
        $menuItem->enabled = $values['enabled'];
        unset($values['enabled']);
        $menuItem->params = $values;
        $menuItem->save();

        $menuItem->name = 'custommenu_' . sprintf('%s', uniqid());
        $menuItem->save();

        $this->view->status = true;
        $this->view->form = null;
    }

}