<?php

class Custommenu_Widget_CustomMenuController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        $this->view->name = $name = $this->_getParam('menu');

        if (!$name) {
            return $this->setNoRender();
        }

        $this->view->navigation = $navigation = Engine_Api::_()
                ->getApi('menus', 'custommenu')
                ->getNavigation($name);

        if (count($navigation) <= 0) {
            return $this->setNoRender();
        }

        $this->view->viewer   = $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->title    = $this->_getParam('title', null);                                               
        $this->view->isMobile = $this->_getParam('nomobile', 1);        
        
        $table  = Engine_Api::_()->getDbtable('content', 'core');
        $selectResult = $table->fetchRow($table->select()->where('content_id = ?', $this->view->identity));
        
        if (!empty($selectResult->params['stickyWidget'])) {
            if ($selectResult->params['stickyWidget'] == 'yes') {
                $this->view->stickyWidget = true;   
            }
        }
                                
        if ($name != 'core_mini' || $name != 'core_main' || $name != 'core_footer' || $name != 'user_home' || $name != 'user_profile') {
            $this->getElement()->removeDecorator('Title');
        }

        if ($viewer->getIdentity()) {
            $this->view->notificationCount = Engine_Api::_()->getDbtable('notifications', 'activity')->hasNotifications($viewer);
        }

        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->view->notificationOnly = $request->getParam('notificationOnly', false);
        $this->view->updateSettings = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.notificationupdate');
        $this->view->ulClass = $this->_getParam('ulClass', null);
        
        //if ($this->view->isMobile) {
            //$this->view->headScript()->appendFile($this->view->baseUrl() . '/application/modules/Custommenu/externals/scripts/desktop.js');
        //}    
        echo $this->renderMenu($name);        
    }

    public function renderTpl($menutype) {        
        $path = $this->getScriptPath();
        $path = str_replace(APPLICATION_PATH . DIRECTORY_SEPARATOR, '', $path);
        $path .= DIRECTORY_SEPARATOR . $menutype . '.tpl';

        return $this->getView()->render($path);	
    }
        
    public function renderMenu($name) {
        if ($name == 'core_mini') {
            return $this->renderTpl($name);
        } elseif ($name == 'core_main') {
            return $this->renderTpl($name);
        } elseif ($name == 'core_footer') {
            return $this->renderTpl($name);
        } elseif ($name == 'user_home') {
            return $this->renderTpl($name);
        } elseif ($name == 'user_profile') {
            return $this->renderTpl($name);
        } elseif (strstr($name, 'custom_')) {
            return $this->renderTpl('custom');
        } elseif ($name == 'mobi_main') {
            return $this->renderTpl('mobi_main');
        } elseif ($name == 'mobi_footer') {
            return $this->renderTpl('mobi_footer');
        } elseif ($name == 'mobi_profile') {
            return $this->renderTpl('mobi_profile');
        } else {
            return $this->renderTpl('tab_menu');
        }
    }

}
