<?php

class Custommenu_Bootstrap extends Engine_Application_Bootstrap_Abstract {

    protected function _initPlugins() {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Custommenu_Plugin_Core());
    }

    protected function _initView() {
        $view = new Zend_View();
        $view->headScript()->appendFile($view->baseUrl() . '/application/modules/Custommenu/externals/scripts/custommenu.js')
                ->appendFile($view->baseUrl() . '/application/modules/Custommenu/externals/scripts/scroller.js')
                ->appendFile($view->baseUrl() . '/application/modules/Custommenu/externals/scripts/desktop.js');
    }

}