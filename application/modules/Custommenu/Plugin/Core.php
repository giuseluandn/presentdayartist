<?php

class Custommenu_Plugin_Core extends Zend_Controller_Plugin_Abstract {

    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'admin-menus' && $request->getActionName() == 'index')
            $request->setModuleName('custommenu');

        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'admin-menus' && $request->getActionName() == 'edit')
            $request->setModuleName('custommenu');

        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'admin-menus' && $request->getActionName() == 'create')
            $request->setModuleName('custommenu');

        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'admin-menus' && $request->getActionName() == 'order')
            $request->setModuleName('custommenu');

        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'admin-menus' && $request->getActionName() == 'delete')
            $request->setModuleName('custommenu');

        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'admin-menus' && $request->getActionName() == 'delete-menu')
            $request->setModuleName('custommenu');
    }

}