<?php

class Custommenu_Plugin_Menus extends Zend_Controller_Plugin_Abstract {

    private function viewer() {
        return Engine_Api::_()->user()->getViewer();
    }
        
    /**
     * Check if menu-item is available to current user 
     * @param Engine_Db_Table_Row $item
     * @return boolean
     */
    public function checkAccess($item) {
        $params = $item->params;
                
        $roleTable = Engine_Api::_()->getDbtable('levels', 'authorization');

        $user = $this->viewer();

        if ($user->getIdentity() == 0) {
            $userRoleId = $roleTable->getPublicLevel();
            $userRoleId = $userRoleId->level_id;
        } else {
            $userRoleId = $user->level_id;
        }

        $display = false;   
        
        if (isset($params['roletypes'])) {
            foreach ($params['roletypes'] as $role) {
                if ($userRoleId == (int) $role) {                                                                
                    $display = true;                                   
                }
            }
        } else { 
            // Fix to display items without roletypes. 
            // Ex: when some module was installed after Custommenu
            $display = true;   
        }                      
        
        if ($display) {            
            return $this->checkItem($item);                                             
        }
                                
    }        
    
    /**
     * Check special item type
     * @param int itemType
     * @return boolean|array
     */
    private function checkItem($item) {
        $itemType = isset($item->params['itemtype']) ? (int) $item->params['itemtype'] : 0;                       
        $iconPath = isset($item->params['icon']) ? $item->params['icon'] : '';
        $itemUri  = isset($item->params['uri']) ? $item->params['uri'] : '';
        $target   = isset($item->params['target']) ? $item->params['target'] : '';

        $noAvatarPath = Zend_Controller_Front::getInstance()->getBaseUrl();
        $noAvatarPath .= '/application/modules/User/externals/images/nophoto_user_thumb_icon.png';

        $avatarUrl = !is_null($this->viewer()->getPhotoUrl('thumb.icon')) ? $this->viewer()->getPhotoUrl('thumb.icon') : $noAvatarPath;

        switch ($itemType) {
            case 1 : // Avatar
                return array(
                    'label' => '&nbsp;',
                    'icon'  => $avatarUrl,
                    'uri'   => $this->viewer()->getHref(),
                    'class' => 'special-item-avatar',
                    'itemtype' => 'avatar'
                );
                break;
            case 2 : // Avatar & Profile Link
                return array(
                    'label' => $this->viewer()->getTitle(),
                    'icon'  => $avatarUrl,
                    'uri'   => $this->viewer()->getHref(),
                    'class' => 'special-item-avatar-profile',
                    'itemtype' => 'avatar-profile'
                );
                break;
            case 3 : // Profile Link                
                return array(
                    'label' => $this->viewer()->getTitle(),
                    'icon'  => $iconPath,
                    'uri'   => $this->viewer()->getHref(),
                    'class' => 'special-item-profile',
                    'itemtype' => 'profile'
                );
                break;
            case 4 : // Updates
                return array(
                    'label'    => '',
                    'icon'     => '',
                    'uri'      => '',
                    'class'    => 'special-item-updates',
                    'itemtype' => 'updates'
                );
                break;
            case 5 : // Search Form
                return array(
                    'label'    => '',
                    'icon'     => '',
                    'uri'      => '',
                    'class'    => 'special-item-search',
                    'itemtype' => 'search',
                );
                break;
            case 6 : // Horizontal Divider 
                return array(
                    'label'    => '',
                    'icon'     => '',
                    'uri'      => '',
                    'class'    => 'divider',
                    'itemtype' => 'divider',
                );
                break;
            default :
                return array(
                    'label'    => $item->label,
                    'icon'     => $iconPath,
                    'uri'      => $itemUri,  
                    'target'   => $target,
                );
        }
    }
    
}
