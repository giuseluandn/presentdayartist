<?php

class Custommenu_Installer extends Engine_Package_Installer_Module {

    public function onInstall() {
        parent::onInstall();
        $this->updateItems();
    }

    protected function updateItems() {
        $db = $this->getDb();

        $table = 'engine4_core_menuitems';

        $queryData = $db->query("SELECT * FROM `{$table}` WHERE `custom` = 0")->fetchAll();

        if (count($queryData) > 0) {

            foreach ($queryData as $item) {

                if (isset($item['params'])) {
                    $item['params'] = (array) json_decode($item['params']);
                    $item['params'] = array_merge($item['params'], $this->getUserRoles());
                    $item['params'] = json_encode($item['params']);
                } else {
                    $item['params'] = json_encode($this->getUserRoles());
                }

                $db->update($table, $item, array('id = ?' => $item['id']));
            }
        }
    }

    private function getUserRoles() {
        $db = $this->getDb();
        $table = 'engine4_authorization_levels';

        $roles = $db->select()
                ->from($table, 'level_id')
                ->query()
                ->fetchAll(Zend_Db::FETCH_ASSOC);

        $rolesStr = '';

        foreach ($roles as $roleId) {
            $rolesStr .= '' . $roleId['level_id'] . ',';
        }

        $rolesStr = rtrim($rolesStr, ',');
        $rolesStr = explode(',', $rolesStr);

        return array('roletypes' => $rolesStr);
    }

}
