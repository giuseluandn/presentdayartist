<?php

return array(
    array(
        'title' => 'Custom Menu',
        'description' => 'Displays a custom user menu.',
        'category' => 'Custom Menu',
        'type' => 'widget',
        'name' => 'custommenu.custom-menu',
        'adminForm' => 'Custommenu_Form_Admin_Widget_CustomMenu',
    ),
);