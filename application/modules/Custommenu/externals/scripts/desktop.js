window.addEvent('domready', function() {                           
    (function() {
        var index = 0;
        Element.implement({
            toggle: function() {
                var args = Array.slice(arguments, 0), count = args.length - 1;
                return this.addEvent('click', function(){
                    args[index].apply(this, arguments);
                    index = (count > index) ? index + 1 : 0;
                });
            }
        });
    })();
    
    var check = 0;
    
    $$('a.menu_open').toggle(function(e) {
        var parentList = $(this).getNext('ul');            

        if (parentList.getElements('ul') != null) {                                                                    
            
            if (check != 0) {
                parentList.setStyle('display', 'none');
                check = 0;
            } else {
                check = 1;
                parentList.setStyle('display', 'block');
            }
                        
            var childList = parentList.getElements('ul.custommenu-nav-subitem');     
                                           
            if (childList.getStyle('display') == 'block') {
                childList.setStyle('display', 'none');
            }

            var controls = parentList.getElements('span.submenu_open');
            controls.setStyle('display', 'block');
        }
    }, function() {                                        
        var parentList = $(this).getNext('ul');                                                                      
        
        if (check != 0) {
            parentList.setStyle('display', 'none');            
            check = 0;
        } else {
            check = 1;
            parentList.setStyle('display', 'block');
        }          
                
    });          
         
    $$('span.submenu_open').toggle(function() {         
        var childList = $(this).getNext('ul.custommenu-nav-subitem');         
        childList.setStyle('display', 'none');
        $(this).set('html', '&nbsp;');
        $(this).removeClass('down'); 
        $(this).addClass('up');
    }, function() {          
        var childList = $(this).getNext('ul.custommenu-nav-subitem');         
        childList.setStyle('display', 'block');
        $(this).set('html', '&nbsp;');
        $(this).removeClass('up'); 
        $(this).addClass('down');
    });    
});

