window.addEvent('domready', function() {
    var container = $$('div.generic_layout_container.layout_core_menu_main ');    
    var submenus  = container.getElement('ul').getElements('ul');

    if (submenus.length > 0) {                
        Array.each(submenus, function(el, index) {
            if (el != null) {
                el.dispose();
            }    
        });                
    }
});