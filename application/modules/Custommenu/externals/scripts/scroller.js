var Scroller = new Class({
    initialize: function(menu) {
        this.menu = $('custommenu-' + menu).getParent();
        this.menu_position = this.menu.getPosition().y;
        this.startListeners();
        this.sticky_class = 'float-widget';
        this.yOffset = this.menu.getSize().y;
    },
    startListeners: function() {
        var action = function() {
            this.setPosition($(document.body).getScroll()[this.move]);
        }.bind(this);
        window.addEvent('scroll', action);
        window.addEvent('load', action);
    },
    setPosition: function(move) {
        if (this.calulateScroll()) {
            this.menu.addClass(this.sticky_class);
            this.menu.setStyles({'position': 'fixed', 'top': 0});
            //main&mini sticky
            if ($$('.sticky-mini').length > 0 && $$('.sticky-container').length > 0) {
                //main menu
                if (this.menu.hasClass('sticky-container'))
                    this.menu.addClass('sticky-main-both')
                            .setStyle('top', $$('.sticky-mini')[0].getSize().y);
                //sidebars
                else if (this.menu.hasClass('sticky-sidebar'))
                    this.menu.setStyle('top', $$('.sticky-mini')[0].getSize().y + $$('.sticky-container')[0].getSize().y + 15);
            }
            //only main
            else if ($$('.sticky-mini').length <= 0 && $$('.sticky-container').length > 0 && this.menu.hasClass('sticky-sidebar'))
                this.menu.setStyle('top', $$('.sticky-container')[0].getSize().y + 15);
            //only mini
            else if ($$('.sticky-mini').length > 0 && $$('.sticky-container').length <= 0 && this.menu.hasClass('sticky-sidebar'))
                this.menu.setStyle('top', $$('.sticky-mini')[0].getSize().y + 15);
            //only sidebars
            else
            if (this.menu.hasClass('sticky-sidebar'))
                this.menu.setStyle('top', 15);
        }
        else {
            if (this.menu.hasClass(this.sticky_class))
                this.menu.removeClass(this.sticky_class);
            this.menu.setStyles({'position': 'relative', 'top': 0});
        }
        return this;
    },
    calulateScroll: function() {
        //for sidebars
        if (this.menu.hasClass('sticky-sidebar')) {
            var sidebar = this.menu.getParent('.layout_right') || this.menu.getParent('.layout_left');
            return window.getScroll().y > (sidebar.getPosition().y+sidebar.getSize().y);
        }
        //for main or mini menu
        return window.getScroll().y > this.menu_position;
    }
});