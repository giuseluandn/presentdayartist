window.addEvent('domready', function() {
    var parents = $$('a.menu_open').getNext('ul');
    // hide all <ul>'s                
    Array.each(parents, function(el, index) {
        el.setStyle('display', 'none');
        var children = el.getElements('ul.custommenu-nav-subitem');
        if (children.length > 0) {
            Array.each(children, function(child, index) {
                child.setStyle('display', 'none');
            });
        }
    });
                    
    $$('a.menu_open').addEvent('click', function(e) {                
        var parentList = $(this).getNext('ul');  
                        
        if (parentList.getStyle('display') == 'none') {
            parentList.setStyle('display', 'block');
        } else {
            parentList.setStyle('display', 'none');
        }  
                        
        if (parentList.getElements('ul') != null) {
            var children = parentList.getElements('ul');
                            
            Array.each(children, function(el, index) {                       
                if (el.getPrevious('span.submenu_open') == null) {
                    new Element('span', {
                        'class' : 'submenu_open',
                        'html'  : 'arrow down'                        
                    }).inject(el, 'before').addEvent('click', function(e) {
                        var childList = $(this).getNext('ul.custommenu-nav-subitem');                  
                        if (childList.getStyle('display') == 'none') {
                            childList.setStyle('display', 'block');
                            $(this).set('html', 'arrow up');
                        } else {
                            childList.setStyle('display', 'none');
                            $(this).set('html', 'arrow down');
                        }
                    });
                }
            });                                        
        }
    });
                    
});

