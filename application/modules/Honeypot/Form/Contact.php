<?php

class Honeypot_Form_Contact extends Engine_Form {

    public $fields;

    public function init() {
        $this->setTitle('Contact Us')
                ->setDescription('_CORE_CONTACT_DESCRIPTION')
                ->setAction($_SERVER['REQUEST_URI'])
        ;

        $tabIndex = 1;

        $this->setAttrib('class', 'contact_form');

        $this->addElement('Text', 'name', array(
            'label' => 'Name',
            'required' => true,
            'notEmpty' => true,
            'tabindex' => $tabIndex++
        ));

        $this->addElement('Text', 'email', array());

        $this->email->getDecorator('htmlTag2')->setOption('class', 'fieldset-1');


        // honeypot

        $honeypot_fields = array('first', 'fname', 'firstname', 'middleinitial', 'middle name', 'middlename', 'middle', 'last',
            'lname', 'lastname', 'surname', 'birthday', 'born', 'jobtitle', 'street', 'streetaddress', 'address1', 'address', 'city', 'state', 'zip', 'zipcode',
            'postalcode', 'country', 'homephone', 'eveningphone', 'evening phone', 'homeareacode', 'eveningareacode', 'workphone', 'work phone', 'dayphone',
            'daytimephone', 'companyphone', 'businessphone', 'workareacode', 'dayareacode', 'companyareacode', 'businessareacode', 'mobilephone', 'cellphone',
            'mobileareacode', 'cellareacode', 'pagerphone', 'pagerareacode', 'areacode', 'phone', 'fax', 'organization', 'company'); // 50 items

        $i = rand(10, 15);
        $fieldset = array();

        for ($i; $i >= 0; $i--) {
            $rand = rand(0, 49);
            $hide = rand(0, 3);

            if (in_array($honeypot_fields[$rand], $fieldset)) {
                $i++;
                continue;
            }

            $this->addElement('Text', $honeypot_fields[$rand], array(
                'label' => $honeypot_fields[$rand],
                'autocomplete' => 'off'
            ));

            switch ($hide) {
                case 0:
                    $fieldset[] = preg_replace("/[\s-]/", "", $honeypot_fields[$rand]);
                    $this->$honeypot_fields[$rand]->getDecorator('htmlTag2')->setOption('style', 'position:absolute;top:-8000px;height:0px;');
                    break;
                case 1:
                    $this->$honeypot_fields[$rand]->getDecorator('htmlTag2')->setOption('class', 'fieldset-1');
                    break;
                case 2:
                    $this->$honeypot_fields[$rand]->getDecorator('htmlTag2')->setOption('class', 'fieldset-2');
                    break;
                case 3:
                    $this->$honeypot_fields[$rand]->getDecorator('htmlTag2')->setOption('class', 'fieldset-3');
                    break;

                default:
                    $this->$honeypot_fields[$rand]->getDecorator('htmlTag2')->setOption('class', 'fieldset-1');
                    break;
            }
        }

        $this->fields = $fieldset;

        // hp end


        if (!isset($_SESSION['hpcem'])) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $length = rand(10, 30);
            $honeypot = '';
            for ($i = 0; $i < $length; $i++) {
                $honeypot .= $characters[rand(0, $charactersLength - 1)];
            }
            $_SESSION['hpcem'] = $honeypot;
        } else {
            $honeypot = $_SESSION['hpcem'];
        }

        $this->addElement('Text', $honeypot, array(
            'label' => 'Email Address',
            'required' => true,
            'notEmpty' => true,
            'tabindex' => $tabIndex++,
            'validators' => array(
                'EmailAddress'
            )
        ));

        $this->addElement('Textarea', 'body', array(
            'label' => 'Message',
            'required' => true,
            'notEmpty' => true,
            'tabindex' => $tabIndex++
        ));

        $show_captcha = Engine_Api::_()->getApi('settings', 'core')->core_spam_contact;
        if ($show_captcha && ($show_captcha > 1 || !Engine_Api::_()->user()->getViewer()->getIdentity() )) {
            $this->addElement('captcha', 'captcha', Engine_Api::_()->core()->getCaptchaOptions());
        }

        $this->addElement('Button', 'submit', array(
            'label' => 'Send Message',
            'type' => 'submit',
            'ignore' => true,
            'tabindex' => $tabIndex++
        ));
    }

}
