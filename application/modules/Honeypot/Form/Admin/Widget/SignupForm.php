<?php

class Honeypot_Form_Admin_Widget_SignupForm extends Core_Form_Admin_Widget_Standard {

    public function init() {
        // Element: timezone
        $this->addElement('Radio', 'timezonefield', array(
            'label' => 'Enable timezone?',
            'multioptions' => array(
                'no' => 'No',
                'yes' => 'Yes'
            ),
            'value' => 'no'
        ));
        // Element: password confirmation
        $this->addElement('Radio', 'epassconffield', array(
            'label' => 'Enable password confirmation?',
            'multioptions' => array(
                'no' => 'No',
                'yes' => 'Yes'
            ),
            'value' => 'no'
        ));
        // Element: language
        $this->addElement('Radio', 'langfield', array(
            'label' => 'Enable language selection?',
            'multioptions' => array(
                'no' => 'No',
                'yes' => 'Yes'
            ),
            'value' => 'no'
        ));
    }

}
