function honeypot(form) {
    var fields = fieldset.split(',');
    var i = fields.length - 1;
    for (i; i >= 0; i--) {
        var elmnt = form.getElement('div#' + fields[i] + '-wrapper');
        if (elmnt) {
            if (elmnt.hasClass('fieldset-1') || elmnt.hasClass('fieldset-2') || elmnt.hasClass('fieldset-3')) {
                continue;
            }
            elmnt.setStyles({position: 'absolute', top: -8000, height: 0});
        }
    }
}

function checkEmail(element) {
    var email = element.get('value');

    if (email.length < 6) {
        return;
    }

    (new Request.JSON({
        'format': 'json',
        'url': en4.core.baseUrl + 'honeypot/index/validate-email/',
        'data': {
            'format': 'json',
            'email': email
        },
        'onSuccess': function (responseJSON, responseText)
        {
            if (responseJSON.count > 0) {
                element.getParent().getChildren('p').set('html', responseJSON.mess);
                $(element).getParent().addClass('hp-email-isset');
                $(element).store('isValid', false);
            } else {
                element.getParent().getChildren('p').set('html', '');
                $(element).getParent().removeClass('hp-email-isset');
                $(element).store('isValid', true);
            }
            submit_status(element);
        }
    })).send();

}

function checkUsername(element) {
    console.log(123);
    var un = element.get('value');

    (new Request.JSON({
        'format': 'json',
        'url': en4.core.baseUrl + 'honeypot/index/validate-username/',
        'data': {
            'format': 'json',
            'username': un
        },
        'onSuccess': function (responseJSON, responseText)
        {
            if (responseJSON.count > 0) {
                element.getParent().getChildren('p').set('html', responseJSON.mess);
                element.getParent().addClass('hp-username-isset');
                element.store('isValid', false);
            } else {
                element.getParent().getChildren('p').set('html', '');
                element.getParent().removeClass('hp-username-isset');
                element.store('isValid', true);
            }
            submit_status(element);
        }
    })).send();
}

function submit_status(element) {
    var form = element.getParent('form');
    var isValid = false;

    $$(form.getElements('input.emailEl')).each(function (element) {
        if (element.value != '')
            isValid = element.retrieve('isValid');
    });

    if (form.getElement('input[name="username"]'))
        isValid = isValid && form.getElement('input[name="username"]').retrieve('isValid');


    if (!isValid)
        form.getElement('button[type="submit"]').set('disabled', true);
    else
        form.getElement('button[type="submit"]').set('disabled', false);
}

en4.core.runonce.add(function () {
    $$('.registration_form').each(function (form) {
        honeypot(form);

        form.getElements('input.emailEl, input[name="username"]').each(function (element) {
            if (element.hasClass('emailEl') && element.get('value').length >= 6)
                checkEmail(element);
            else if (element.get('name') == 'username' && element.get('value').length >= 4)
                checkUsername(element);
        });

    });

});

window.addEvent('domready', function () {
    $(document.body).addEvent("keyup:relay(input.emailEl)", function (event, element) {
        checkEmail(this);
    });

    $(document.body).addEvent("blur:relay(input.emailEl)", function (event, element) {
        checkEmail(this);
    });

    $(document.body).addEvent("keyup:relay(input[name='username'])", function (event, element) {
        checkUsername(this);
    });

    $(document.body).addEvent("blur:relay(input[name='username'])", function (event, element) {
        checkUsername(this);
    });

});