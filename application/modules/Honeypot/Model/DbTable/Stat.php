<?php

class Honeypot_Model_DbTable_Stat extends Engine_Db_Table {

    public function getTotal($start = null, $end = null, $type = "signup") {
        $select = new Zend_Db_Select($this->getAdapter());
        $select->from($this->info('name'), 'COUNT(bot_id) as sum');
        $select->where('type = ?', $type);

        // Can pass "today" into start
        $date = new Zend_Date;
        $date->setTimezone(Engine_Api::_()->getApi('settings', 'core')->getSetting('core_locale_timezone', 'GMT'));
        switch ($start) {
            case 'day':
                $start = $date->setTime('00:00:00')->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j"), gmdate("Y"));
                $end = $date->setTime('23:59:59')->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j") + 1, gmdate("Y"));
                break;
            case 'week':
                $start = $date->setTime('00:00:00')->setWeekday(0)->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j") - gmdate('N') + 1, gmdate("Y"));
                $end = $date->setTime('23:59:59')->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j") - gmdate('N') + 1 + 7, gmdate("Y"));
                break;
            case 'month':
                $start = $date->setTime('00:00:00')->setDate(1)->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j"), gmdate("Y"));
                $end = $date->setTime('23:59:59')->toValue(); // mktime(0, 0, 0, gmdate("n") + 1, gmdate("j"), gmdate("Y"));
                break;
            case 'year':
                $start = $date->setTime('00:00:00')->setDayOfYear(1)->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j"), gmdate("Y"));
                $end = $date->setTime('23:59:59')->toValue(); // mktime(0, 0, 0, gmdate("n"), gmdate("j"), gmdate("Y") + 1);
                break;
        }

        if (null !== $start) {
            $select->where('date >= ?', gmdate('Y-m-d', $start));
        }

        if (null !== $end) {
            $select->where('date <= ?', gmdate('Y-m-d', $end));
        }

        $data = $select->query()->fetch();

        if (!isset($data['sum'])) {
            return 0;
        }

        return $data['sum'];
    }

}
