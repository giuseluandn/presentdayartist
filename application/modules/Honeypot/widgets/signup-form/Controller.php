<?php

class Honeypot_Widget_SignupFormController extends Engine_Content_Widget_Abstract {

    public function indexAction() {

        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer->getIdentity()) {
            $this->setNoRender(true);
        }

        $timezone_field = $this->_getParam('timezonefield', 'no');
        $password_conf_field = $this->_getParam('epassconffield', 'no');
        $lang_field = $this->_getParam('langfield', 'no');

        $this->view->form = new Honeypot_Form_Signup_Account(array('tz_field' => $timezone_field,
            'pc_field' => $password_conf_field,
            'lg_field' => $lang_field));
    }

}
