<?php

class Honeypot_Widget_ContactFormController extends Engine_Content_Widget_Abstract {

    public function indexAction() {

        $translate = Zend_Registry::get('Zend_Translate');
        $this->view->form = $form = new Honeypot_Form_Contact();

        $post = Zend_Controller_Front::getInstance()->getRequest();

        if ($post->isPost()) {

            if ($form->isValid($post->getPost())) {

                $values = Zend_Controller_Front::getInstance()->getRequest()->getPost();

                $honeypot_fields = array('email', 'first', 'fname', 'firstname', 'middleinitial', 'middle name', 'middlename', 'middle', 'last',
                    'lname', 'lastname', 'surname', 'birthday', 'born', 'jobtitle', 'street', 'streetaddress', 'address1', 'address', 'city', 'state', 'zip', 'zipcode',
                    'postalcode', 'country', 'homephone', 'eveningphone', 'evening phone', 'homeareacode', 'eveningareacode', 'workphone', 'work phone', 'dayphone',
                    'daytimephone', 'companyphone', 'businessphone', 'workareacode', 'dayareacode', 'companyareacode', 'businessareacode', 'mobilephone', 'cellphone',
                    'mobileareacode', 'cellareacode', 'pagerphone', 'pagerareacode', 'areacode', 'phone', 'fax', 'organization', 'company'); // 50 items

                $table = Engine_Api::_()->getDbTable('stat', 'honeypot');
                $db = $table->getAdapter();

                $ipObj = new Engine_IP();
                $ip = $ipObj->toIPv4();

                foreach ($honeypot_fields as $value) {
                    if (isset($values[$value]) && !empty($values[$value])) {
                        $fields[$value] = $values[$value];
                    }
                }

                if (!empty($fields)) {

                    $db->beginTransaction();

                    try {
                        $row = $table->createRow();
                        $row->fields = serialize($fields);
                        $row->ip = $ip;
                        $row->date = date('Y-m-d H:00:00');
                        $row->type = 'contactus';
                        $row->save();
                        $db->commit();
                    } catch (Exception $e) {

                    }

                    die(Zend_Registry::get('Zend_Log')->log('Bot contact form sending was prevented by Honeypot plugin (' . serialize($fields) . ')', Zend_Log::INFO));
                }


                $values['email'] = $values[$_SESSION['hpcem']];

                // Success! Process
                // Mail gets logged into database, so perform try/catch in this Controller
                $db = Engine_Db_Table::getDefaultAdapter();
                $db->beginTransaction();
                try {
                    // the contact form is emailed to the first SuperAdmin by default
                    $users_table = Engine_Api::_()->getDbtable('users', 'user');
                    $users_select = $users_table->select()
                            ->where('level_id = ?', 1)
                            ->where('enabled >= ?', 1);
                    $super_admin = $users_table->fetchRow($users_select);
                    $adminEmail = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.mail.contact');
                    if (!$adminEmail) {
                        $adminEmail = $super_admin->email;
                    }

                    $viewer = Engine_Api::_()->user()->getViewer();

                    // Check for error report
                    $error_report = '';
                    $name = $this->_getParam('name');
                    $loc = $this->_getParam('loc');
                    $time = $this->_getParam('time');
                    if ($name && $loc && $time) {
                        $error_report .= "\r\n";
                        $error_report .= "\r\n";
                        $error_report .= "-------------------------------------";
                        $error_report .= "\r\n";
                        $error_report .= $this->view->translate('The following information about an error was included with this message:');
                        $error_report .= "\r\n";
                        $error_report .= $this->view->translate('Exception: ') . base64_decode(urldecode($name));
                        $error_report .= "\r\n";
                        $error_report .= $this->view->translate('Location: ') . base64_decode(urldecode($loc));
                        $error_report .= "\r\n";
                        $error_report .= $this->view->translate('Time: ') . date('c', base64_decode(urldecode($time)));
                        $error_report .= "\r\n";
                    }

                    // Make params
                    $mail_settings = array(
                        'host' => $_SERVER['HTTP_HOST'],
                        'email' => $adminEmail,
                        'date' => time(),
                        'recipient_title' => $super_admin->getTitle(),
                        'recipient_link' => $super_admin->getHref(),
                        'recipient_photo' => $super_admin->getPhotoUrl('thumb.icon'),
                        'sender_title' => $values['name'],
                        'sender_email' => $values['email'],
                        'message' => $values['body'],
                        'error_report' => $error_report,
                    );

                    if ($viewer && $viewer->getIdentity()) {
                        $mail_settings['sender_title'] .= ' (' . $viewer->getTitle() . ')';
                        $mail_settings['sender_email'] .= ' (' . $viewer->email . ')';
                        $mail_settings['sender_link'] = $viewer->getHref();
                    }

                    // send email
                    Engine_Api::_()->getApi('mail', 'core')->sendSystem(
                            $adminEmail, 'core_contact', $mail_settings
                    );

                    // if the above did not throw an exception, it succeeded
                    $db->commit();
                    $this->view->status = true;
                    $this->view->message = $translate->_('Thank you for contacting us!');
                } catch (Zend_Mail_Transport_Exception $e) {
                    $db->rollBack();
                    throw $e;
                }
            }
        }
    }

}
