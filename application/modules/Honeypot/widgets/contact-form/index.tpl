<script>
    window.addEvent('domready', function () {
        $('email-wrapper').setStyle('position', 'absolute');
        $('email-wrapper').setStyle('top', '-8000px');
        $('email-wrapper').setStyle('left', '-8000px');
        $('email-wrapper').setStyle('height', '0px');
    });
</script>

<?php if ($this->status): ?>
    <?php echo $this->message; ?>
<?php else: ?>
    <?php echo $this->form->render($this) ?>
    <script type="text/javascript">
        var fieldset = "<?php echo implode(',', $this->form->fields); ?>";
    </script>
<?php endif; ?>