<?php

return array(
    'package' =>
    array(
        'type'         => 'module',
        'name'         => 'honeypot',
        'version'      => '4.8.8p2',
        'path'         => 'application/modules/Honeypot',
        'title'        => 'Honeypot',
        'description'  => '',
        'author'       => 'WebHive Team',
        'callback'     => array(
            'path'  => 'application/modules/Honeypot/settings/install.php',
            'class' => 'Honeypot_Installer',
        ),
        'actions'      =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'dependencies' => array(
            array(
                'type'       => 'module',
                'name'       => 'whcore',
                'minVersion' => '4.8.8',
            ),
        ),
        'directories'  =>
        array(
            0 => 'application/modules/Honeypot',
        ),
        'files'        =>
        array(
            0 => 'application/languages/en/honeypot.csv',
        ),
    ),
    'hooks'   => array(
        array(
            'event'    => 'onAdminStatistics',
            'resource' => 'Honeypot_Plugin_Core',
        ),
    ),
    //ROUTES
    'routes'  =>
    array(
        // Public
        'honeypot_home' => array(
            'route'    => 'honeypot/index/:action/*',
            'defaults' => array(
                'module'     => 'honeypot',
                'controller' => 'index',
                'action'     => 'index'
            ),
            'reqs'     => array(
                'action' => '(index|validate-email|validate-username)'
            )
        ),
    ),
);
?>