<?php

class Honeypot_Installer extends Engine_Package_Installer_Module {

    public function onInstall() {
        $db = $this->getDb();

        $this->_addGenericPage('honeypot_help_contact', 'Honeypot Contact Page', 'Contact Us', 'This is the contact page');

        $db->update('engine4_user_signup', array('class' => 'Whcore_Plugin_Signup_Account'), array('class = "Honeypot_Plugin_Signup_Account" OR class = "User_Plugin_Signup_Account" OR class = "Linkedin_Plugin_Signup_Account" OR class = "Vkconnect_Plugin_Signup_Account"'));

        parent::onInstall();
    }

}

?>