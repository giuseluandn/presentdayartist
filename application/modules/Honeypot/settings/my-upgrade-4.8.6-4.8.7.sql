ALTER TABLE  `engine4_honeypot_stat` ADD  `type` ENUM(  'signup',  'contactus' ) NOT NULL;

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('honeypot_admin_main_statistics_contact', 'honeypot', 'Contact Us Statistics', '', '{"route":"admin_default","module":"honeypot","controller":"statistics","action":"contact"}', 'honeypot_admin_main', '', 1);

UPDATE `engine4_core_menuitems` SET `label` = 'Sign-Up Statistics', `order` = 0,  `params` =  '{"route":"admin_default","module":"honeypot","controller":"statistics","action":"index"}' WHERE `engine4_core_menuitems`.`name` ='honeypot_admin_main_statistics';
