<?php

return array(
    array(
        'title' => 'Signup Form',
        'description' => 'Signup form on any page',
        'category' => 'Honeypot',
        'type' => 'widget',
        'name' => 'honeypot.signup-form',
        'adminForm' => 'Honeypot_Form_Admin_Widget_SignupForm',
        'defaultParams' => array(
            'timezone' => 0,
            'epassconf' => 0,
            'lang' => 0,
        )
    ),
    array(
        'title' => 'Honeypot Contact Form',
        'description' => 'Contact Form With Anti-Spam',
        'category' => 'Honeypot',
        'type' => 'widget',
        'name' => 'honeypot.contact-form'
    )
        )
?>
