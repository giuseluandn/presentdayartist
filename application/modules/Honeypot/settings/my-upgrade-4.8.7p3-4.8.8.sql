DELETE FROM `engine4_core_menuitems` WHERE `name` = 'honeypot_admin_main_settings' LIMIT 1;
UPDATE `engine4_core_menuitems` SET `params` = '{"route":"admin_default","module":"honeypot","controller":"statistics","action":"index"}' WHERE `name` = 'core_admin_main_plugins_honeypot' LIMIT 1;
DELETE FROM `engine4_core_settings` WHERE `name` = 'honeypot.email' LIMIT 1;