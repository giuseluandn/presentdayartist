INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('core_admin_main_plugins_honeypot', 'honeypot', 'Honeypot', '', '{"route":"admin_default","module":"honeypot","controller":"statistics","action":"index"}', 'core_admin_main_plugins', '', 999),
('honeypot_admin_main_statistics', 'honeypot', 'Sign-Up Statistics', '', '{"route":"admin_default","module":"honeypot","controller":"statistics","action":"index"}', 'honeypot_admin_main', '', 0),
('honeypot_admin_main_statistics_contact', 'honeypot', 'Contact Us Statistics', '', '{"route":"admin_default","module":"honeypot","controller":"statistics","action":"contact"}', 'honeypot_admin_main', '', 1);

CREATE TABLE `engine4_honeypot_stat` (
  `bot_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fields` text NOT NULL,
  `ip` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `type` ENUM(  'signup',  'contactus' ) NOT NULL,
  UNIQUE KEY `bot_id` (`bot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;