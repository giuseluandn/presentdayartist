<?php

class Honeypot_Plugin_Hooks extends Zend_Controller_Plugin_Abstract {

    public function routeShutdown(Zend_Controller_Request_Abstract $request) {

        if ($request->getModuleName() == 'core' && $request->getControllerName() == 'help' && $request->getActionName() == 'contact') {
            $request->setModuleName('honeypot')
                    ->setControllerName('help')
                    ->setActionName('contact');
        }
    }

}
