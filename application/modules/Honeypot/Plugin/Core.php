<?php

class Honeypot_Plugin_Core {

    public function onAdminStatistics($event) {
        $statistics = array(
            'label' => 'Bot Signups',
            'today' => Engine_Api::_()->getDbtable('stat', 'honeypot')->getTotal('day'),
            'total' => Engine_Api::_()->getDbtable('stat', 'honeypot')->getTotal(),
        );

        $event->addResponse($statistics);
    }

}
