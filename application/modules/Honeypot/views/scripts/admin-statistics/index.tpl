<?php
$localeObject = Zend_Registry::get('Locale');

$months = Zend_Locale::getTranslationList('months', $localeObject);
if ($months['default'] == NULL) {
    $months['default'] = "wide";
}
$months = $months['format'][$months['default']];

$days = Zend_Locale::getTranslationList('days', $localeObject);
if ($days['default'] == NULL) {
    $days['default'] = "wide";
}
$days = $days['format'][$days['default']];
?>

<?php
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'externals/calendar/calendar.compat.js');
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Honeypot/externals/scripts/Chart.js');
$this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'externals/calendar/styles.css');
?>


<?php $this->headScript()->captureStart(); ?>
en4.core.runonce.add(function(){
neCalendar=new Class({
Implements: [Calendar],


clicked: function(td, day, cal) {
cal.val = (this.value(cal) == day) ? null : new Date(cal.year, cal.month, day);
this.write(cal);
var value = this.format(cal.val, 'Y-m-d');
this.toggle(cal);
//$('date_form').submit();
}
});
});
<?php $this->headScript()->captureEnd(); ?>


<script type="text/javascript">
    en4.core.runonce.add(function () {

        new neCalendar({'start-date': 'd/m/Y', 'end-date': 'd/m/Y'}, {
            classes: ['event_calendar'],
            pad: 0,
            direction: 0,
            months: <?php echo Zend_Json::encode(array_values($months)) ?>,
            days: <?php echo Zend_Json::encode(array_values($days)) ?>,
            day_suffixes: ['', '', '', ''],
            onHideStart: function () {
                if (typeof cal_date_onHideStart == 'function')
                    cal_date_onHideStart();
            },
            onHideComplete: function () {
                if (typeof cal_date_onHideComplete == 'function')
                    cal_date_onHideComplete();
            },
            onShowStart: function () {
                if (typeof cal_date_onShowStart == 'function')
                    cal_date_onShowStart();
            },
            onShowComplete: function () {
                if (typeof cal_date_onShowComplete == 'function')
                    cal_date_onShowComplete();
            }
        })
    });</script>

<h2>
    <?php echo $this->translate('Honeypot Plugin') ?>
</h2>


<?php if (count($this->navigation)): ?>
    <div class='tabs'>
        <?php
        echo $this->navigation()->menu()->setContainer($this->navigation)->render();
        ?>
    </div>
<?php endif; ?>

<div class="hp-date-filter">
    <form method="post" action="<?php echo $this->url() ?>" enctype="application/x-www-form-urlencoded" id="date_form">
        <div class="date-filter">

            <div id="date-wrapper">
                <div class="form-label" id="date-label">
                    <label class="optional" for="start-date"><?php echo $this->translate('Start Date') ?></label>
                </div>
                <div class="form-element" id="date-element">
                    <div class="event_calendar_container" style="display:inline">
                        <?php echo $this->formHidden('start-date', $this->start_date, array_merge(array('class' => 'calendar', 'id' => 'start-date'), array())) ?>
                        <span class="calendar_output_span" id="calendar_output_span_start-date"><?php echo $this->start_date ?></span>
                    </div>
                </div>
            </div>
            <div id="date-wrapper">
                <div class="form-label" id="date-label">
                    <label class="optional" for="end-date"><?php echo $this->translate('End Date') ?></label>
                </div>
                <div class="form-element" id="date-element">
                    <div class="event_calendar_container" style="display:inline">
                        <?php echo $this->formHidden('end-date', $this->end_date, array_merge(array('class' => 'calendar', 'id' => 'end-date'), array())) ?>
                        <span class="calendar_output_span" id="calendar_output_span_end-date"><?php echo $this->end_date ?></span>
                    </div>
                </div>
            </div>
            <button name="done" id="done" type="submit">Filter</button>
        </div>
    </form>
</div>


<div class='clear'>
    <div class='settings'>

        <div class="honeypot_plugin_middle">
            <div id="honeypot_stats_container" style="width:640px; height:300px;"><canvas id="canvas" height="300" width="640"></canvas></div>
            <div id="chart_legend"></div>
        </div>
        <script type="text/javascript">

            var values = JSON && JSON.parse('<?php echo $this->values; ?>') || $.parseJSON('<?php echo $this->values; ?>');
            var track_dates = JSON && JSON.parse('<?php echo $this->values; ?>') || $.parseJSON('<?php echo $this->values; ?>');
            var randomScalingFactor = function () {
                return Math.round(Math.random() * 100)
            };
            var lineChartData = {
                labels: values.dates,
                datasets: [
                    {
                        label: "<?php echo $this->translate('Bots'); ?>",
                        fillColor: "rgba(255,0,0,0.2)",
                        strokeColor: "rgba(255,0,0,1)",
                        pointColor: "rgba(255,0,0,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: values.bots
                    },
                    {
                        label: "<?php echo $this->translate('Non-Bots'); ?>",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: values.users
                    }
                ]

            }

            var ctx = document.getElementById("canvas").getContext("2d");
            var lineChart = new Chart(ctx).Line(lineChartData, {
                responsive: true,
                scaleShowLabels: true
            });
            var legend = lineChart.generateLegend();
            document.getElementById("chart_legend").innerHTML = legend;
            window.addEvent('domready', function () {

            });

        </script>


        <div class="honeypot_plugin_right">
            <div class="sign-ups-statistics-container">
                <div class="statistics-row">
                    <div class="statistics-raw-field"></div>
                    <div class="statistics-raw-field"><h3><?php echo $this->translate('today'); ?></h3></div>
                    <div class="statistics-raw-field"><h3><?php echo $this->translate('total'); ?></h3></div>
                </div>
                <div class="statistics-row">
                    <div class="statistics-raw-field"><h3><?php echo $this->translate('Bots'); ?></h3></div>
                    <div class="statistics-raw-field"><?php echo $this->stats['bots']['today']; ?></div>
                    <div class="statistics-raw-field"><?php echo $this->stats['bots']['total']; ?></div>
                </div>
                <div class="statistics-row">
                    <div class="statistics-raw-field"><h3><?php echo $this->translate('Non-Bots'); ?></h3></div>
                    <div class="statistics-raw-field"><?php echo $this->stats['users']['today']; ?></div>
                    <div class="statistics-raw-field"><?php echo $this->stats['users']['total']; ?></div>
                </div>
            </div>

        </div>

        <div class="sign-ups-statistics-container" id="most_of_all">

            <div class="statistics-row">
                <div class="statistics-raw-field"><h3><?php echo $this->translate('Most effective honeypot trap fields'); ?></h3></div>
                <div class="statistics-raw-field"><h3><?php echo $this->translate('Bots stopped'); ?></h3></div>
            </div>

            <?php foreach ($this->fields as $name => $count) : ?>
                <div class="statistics-row">
                    <div class="statistics-raw-field"><?php echo $name; ?></div>
                    <div class="statistics-raw-field"><?php echo $count; ?></div>
                    <div class="statistics-raw-field"></div>
                </div>
            <?php endforeach; ?>
        </div>


    </div>
</div>
