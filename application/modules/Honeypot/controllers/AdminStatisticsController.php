<?php

class Honeypot_AdminStatisticsController extends Core_Controller_Action_Admin {

    public function indexAction() {

        $viewer = Engine_Api::_()->user()->getViewer();

        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('honeypot_admin_main', array(), 'honeypot_admin_main_statistics');

        $this->view->start_date = $start_date = $this->_getParam('start-date', '');
        $this->view->end_date = $end_date = $this->_getParam('end-date', '');

        if ($end_date == '' || $start_date == '') {

            $oldTz = date_default_timezone_get();
            date_default_timezone_set($viewer->timezone);
            $this->view->date = $date = date('d/m/Y');
            date_default_timezone_set($oldTz);

            if ($start_date == '')
                $this->view->start_date = $start_date = date('d/m/Y', (strtotime('-1 week', strtotime(date('Y-m-d')))));

            if ($end_date == '')
                $this->view->end_date = $end_date = $date;
        } else {
            if (!checkdate(substr($start_date, 3, 2), substr($start_date, 0, 2), substr($start_date, 6, 4)) || !checkdate(substr($end_date, 3, 2), substr($end_date, 0, 2), substr($end_date, 6, 4)))
                return;
        }


        $DateTime = DateTime::createFromFormat('d/m/Y', $start_date);
        $db_start_date = $DateTime->format('Y-m-d');

        $DateTime = DateTime::createFromFormat('d/m/Y', $end_date);
        $db_end_date = $DateTime->format('Y-m-d');

        $db_end_date = date('Y-m-d', strtotime($db_end_date . ' +1 day'));

        // GET PERIOD
        $begin = new DateTime($db_start_date);
        $end = new DateTime($db_end_date);
        $end = $end->modify('+1 day');

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);
        $dates = array();

        foreach ($daterange as $date) {
            $dates[] = $date->format("d/m/Y");
        }

        // GET BOT SIGNUPS
        $table = Engine_Api::_()->getDbtable('stat', 'honeypot');
        $name = $table->info('name');

        $sql1 = $table->select();
        $sql1->setIntegrityCheck(false);
        $sql1->from($name, array("bot_id", "fields", "DATE_FORMAT(date,'%d/%m/%Y') as date", "type"));
        $sql1->where(new Zend_Db_Expr('(`date` >= \'' . $db_start_date . '\' AND `date` < \'' . $db_end_date . '\')'));
        $sql1->order('date ASC');

        $tmp = $table->fetchAll($sql1);
        $bot_signup_dates = array();
        $bots = array();
        $fields = array();
        $num = 0;

        $tmp2 = array();

        foreach ($tmp as $ind => $day) {
            $day = $day->toArray();

            if (key_exists($day['date'], $tmp2)) {
                if ($day['type'] == 'signup') {
                    $tmp2[$day['date']]['count'] += 1;
                }
            } else {
                if ($day['type'] == 'signup') {
                    $tmp2[$day['date']] = $day;
                    $tmp2[$day['date']]['count'] = 1;
                }
            }
            $f = unserialize($tmp2[$day['date']]['fields']);
            foreach ($f as $name => $val) {
                if ($day['type'] == 'signup') {
                    if (key_exists($name, $fields)) {
                        $fields[$name] += 1;
                    } else {
                        $fields[$name] = 1;
                    }
                }
            }
        }

        foreach ($dates as $value) {
            $day = $value;
            if (key_exists($day, $tmp2)) {
                $bot_signup_dates[$day] = $tmp2[$day]['count'];
            } else {
                $bot_signup_dates[$day] = 0;
            }
        }

        array_multisort($fields, SORT_DESC);

        // GET REAL USERS SIGNUPS
        $utable = Engine_Api::_()->getDbtable('statistics', 'core');
        $uname = $utable->info('name');

        $sql = $utable->select();
        $sql->setIntegrityCheck(false);
        $sql->from($uname, array("DATE_FORMAT(date,'%d/%m/%Y') as date", "value"));
        $sql->where("type = 'user.creations'");
        $sql->where(new Zend_Db_Expr('(`date` >= \'' . $db_start_date . '\' AND `date` < \'' . $db_end_date . '\')'));
        $sql->order('date ASC');

        $tmp = $utable->fetchAll($sql);

        $users_signups = array();
        $users = array();
        $num = 0;

        foreach ($tmp as $value) {
            $day = $value->date;
            $v = $value->value;
            if (key_exists($day, $users_signups)) {
                $users_signups[$day] += 1;
            } else {
                $users_signups[$day] = 1;
            }
        }


        // PROCEED
        $tmp = array_merge($bot_signup_dates, $users_signups);
        array_unique($tmp);
        $dates2 = array();

        foreach ($tmp as $date => $count) {
            $DateTime = DateTime::createFromFormat('d/m/Y', $date);
            $date = $DateTime->format('Y-m-d');
            $dates2[] = $date;
        }

        sort($dates2);

        foreach ($dates2 as $ind => $date) {
            $DateTime = DateTime::createFromFormat('Y-m-d', $date);
            $date = $DateTime->format('d/m/Y');
            $dates2[$ind] = $date;
        }

        foreach ($dates2 as $d) {
            if (key_exists($d, $users_signups)) {
                $users[] = $users_signups[$d];
            } else {
                $users[] = 0;
            }

            if (key_exists($d, $bot_signup_dates)) {
                $bots[] = $bot_signup_dates[$d];
            } else {
                $bots[] = 0;
            }
        }

        if (empty($dates2)) {
            $dates2 = array($start_date, $end_date);
            $bots = array(0, 0);
            $users = array(0, 0);
        }

        if (count($dates2) == 1) {
            $dates2 = array_merge(array("00/00/0000"), $dates2);
        }
        if (count($users) == 1) {
            $users = array_merge(array(0), $users);
        }
        if (count($bots) == 1) {
            $bots = array_merge(array(0), $bots);
        }

        $this->view->values = json_encode(array('dates' => $dates2, 'bots' => $bots, 'users' => $users));


        $statistics['bots'] = array(
            'today' => Engine_Api::_()->getDbtable('stat', 'honeypot')->getTotal('day', null, 'signup'),
            'total' => Engine_Api::_()->getDbtable('stat', 'honeypot')->getTotal(null, null, 'signup'),
        );


        $db = Engine_Api::_()->getDbtable('statistics', 'core')->getAdapter();

        $select = $db->select();
        $select->from('engine4_core_statistics', 'SUM(value) as sum')
                ->where('type = ?', 'user.creations');
        $data = $db->fetchAll($select);

        $select2 = $db->select();
        $select2->from('engine4_core_statistics', 'SUM(value) as sum')
                ->where('type = ?', 'user.creations')
                ->where('date > ?', date('Y-m-d', strtotime(' -1 day')))
                ->where('date < ?', date('Y-m-d', strtotime(' +1 day')));
        $data2 = $db->fetchAll($select2);

        $statistics['users'] = array(
            'today' => $data2[0]['sum'] == '' ? 0 : $data2[0]['sum'],
            'total' => $data[0]['sum']
        );

        $this->view->stats = $statistics;
        $this->view->fields = $fields;
    }

    public function contactAction() {
        $viewer = Engine_Api::_()->user()->getViewer();

        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('honeypot_admin_main', array(), 'honeypot_admin_main_statistics_contact');

        $this->view->start_date = $start_date = $this->_getParam('start-date', '');
        $this->view->end_date = $end_date = $this->_getParam('end-date', '');

        if ($end_date == '' || $start_date == '') {

            $oldTz = date_default_timezone_get();
            date_default_timezone_set($viewer->timezone);
            $this->view->date = $date = date('d/m/Y');
            date_default_timezone_set($oldTz);

            if ($start_date == '')
                $this->view->start_date = $start_date = date('d/m/Y', (strtotime('-1 week', strtotime(date('Y-m-d')))));

            if ($end_date == '')
                $this->view->end_date = $end_date = $date;
        } else {
            if (!checkdate(substr($start_date, 3, 2), substr($start_date, 0, 2), substr($start_date, 6, 4)) || !checkdate(substr($end_date, 3, 2), substr($end_date, 0, 2), substr($end_date, 6, 4)))
                return;
        }


        $DateTime = DateTime::createFromFormat('d/m/Y', $start_date);
        $db_start_date = $DateTime->format('Y-m-d');

        $DateTime = DateTime::createFromFormat('d/m/Y', $end_date);
        $db_end_date = $DateTime->format('Y-m-d');

        $db_end_date = date('Y-m-d', strtotime($db_end_date . ' +1 day'));

        // GET PERIOD
        $begin = new DateTime($db_start_date);
        $end = new DateTime($db_end_date);
        $end = $end->modify('+1 day');

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);
        $dates = array();

        foreach ($daterange as $date) {
            $dates[] = $date->format("d/m/Y");
        }

        // GET BOT SIGNUPS
        $table = Engine_Api::_()->getDbtable('stat', 'honeypot');
        $name = $table->info('name');

        $sql1 = $table->select();
        $sql1->setIntegrityCheck(false);
        $sql1->from($name, array("bot_id", "fields", "DATE_FORMAT(date,'%d/%m/%Y') as date", "type"));
        $sql1->where(new Zend_Db_Expr('(`date` >= \'' . $db_start_date . '\' AND `date` < \'' . $db_end_date . '\')'));
        $sql1->order('date ASC');

        $tmp = $table->fetchAll($sql1);
        $bot_dates = array();
        $bots = array();
        $bot_contact = array();
        $fields = array();
        $num = 0;

        foreach ($tmp as $value) {
            $day = $value->date;
            $f = unserialize($value->fields);
            if (key_exists($day, $bot_signup_dates)) {
                if ($value['type'] != 'signup') {
                    $bot_dates[$day] += 1;
                }
            } else {
                if ($value['type'] != 'signup') {
                    $bot_dates[$day] = 1;
                }
            }
            foreach ($f as $name => $val) {
                if ($value['type'] != 'signup') {
                    if (key_exists($name, $fields)) {
                        $fields[$name] += 1;
                    } else {
                        $fields[$name] = 1;
                    }
                }
            }
        }

        $tmp2 = array();

        foreach ($dates as $value) {
            $day = $value;
            if (key_exists($day, $bot_dates)) {
                //$bot_signup_dates[$day] = $tmp2[$day]['count'];
            } else {
                $bot_dates[$day] = 0;
            }
        }


        // PROCEED
        array_multisort($fields, SORT_DESC);


        $tmp = $bot_dates;
        array_unique($tmp);
        $dates2 = array();

        foreach ($tmp as $date => $count) {
            $DateTime = DateTime::createFromFormat('d/m/Y', $date);
            $date = $DateTime->format('Y-m-d');
            $dates2[] = $date;
        }

        sort($dates2);

        foreach ($dates2 as $ind => $date) {
            $DateTime = DateTime::createFromFormat('Y-m-d', $date);
            $date = $DateTime->format('d/m/Y');
            $dates2[$ind] = $date;
        }

        foreach ($dates2 as $d) {

            if (key_exists($d, $bot_dates)) {
                $bot_contact[] = $bot_dates[$d];
            } else {
                $bot_contact[] = 0;
            }
        }

        if (empty($dates2)) {
            $dates2 = array($start_date, $end_date);
            $bots = array(0, 0);
        }

        if (count($dates2) == 1) {
            $dates2 = array_merge(array("00/00/0000"), $dates2);
        }
        if (count($bot_contact) == 1) {
            $bot_contact = array_merge(array(0), $bot_contact);
        }

        $this->view->values = json_encode(array('dates' => $dates2, 'bots' => $bot_contact));


        $statistics['bots'] = array(
            'today' => Engine_Api::_()->getDbtable('stat', 'honeypot')->getTotal('day', null, 'contactus'),
            'total' => Engine_Api::_()->getDbtable('stat', 'honeypot')->getTotal(null, null, 'contactus'),
        );

        $this->view->stats = $statistics;
        $this->view->fields = $fields;
    }

}
