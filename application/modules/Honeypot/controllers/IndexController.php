<?php

class Honeypot_IndexController extends Core_Controller_Action_Standard {

    public function indexAction() {
        die(var_dump('index'));
    }

    public function validateEmailAction() {

        $email = $this->_getParam('email', '');

        $validatorChain = new Zend_Validate();
        $validatorChain->addValidator(new Zend_Validate_NotEmpty())
                ->addValidator(new Zend_Validate_EmailAddress())
                ->addValidator(new Zend_Validate_Db_NoRecordExists(Engine_Db_Table::getTablePrefix() . 'users', 'email'));

        if (!$validatorChain->isValid($email)) {
            $m = $validatorChain->getMessages();

            $mess = '';
            foreach ($m as $key => $value) {
                $mess .= $value . '. ';
            }

            $this->_helper->json(array('count' => 1, 'mess' => $mess));
            return;
        }

        $this->_helper->json(array('count' => 0));
    }

    public function validateUsernameAction() {

        $un = $this->_getParam('username', '');

        $regex = new Zend_Validate_Regex('/^[a-z][a-z0-9]*$/i');

        $regex->setMessages(array(
            Zend_Validate_Regex::NOT_MATCH => 'Please enter a valid username',
            Zend_Validate_Regex::INVALID => 'Please enter a valid username'
        ));

        $validatorChain = new Zend_Validate();
        $validatorChain->addValidator(new Zend_Validate_StringLength(4, 64))
                ->addValidator($regex)
                ->addValidator(new Zend_Validate_NotEmpty())
                ->addValidator(new Zend_Validate_Db_NoRecordExists(Engine_Db_Table::getTablePrefix() . 'users', 'username'))
        ;

        if (!$validatorChain->isValid($un)) {
            $m = $validatorChain->getMessages();

            $mess = '';
            foreach ($m as $key => $value) {
                $mess .= $value . '. ';
            }

            $this->_helper->json(array('count' => 1, 'mess' => $mess));
            return;
        }

        $this->_helper->json(array('count' => 0, 'mess' => ''));
    }

}
