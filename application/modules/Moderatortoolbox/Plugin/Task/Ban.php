<?php

class Moderatortoolbox_Plugin_Task_Ban extends Core_Plugin_Task_Abstract
{
    public function execute()
    {

        $table = Engine_Api::_()->getDbTable('ban','moderatortoolbox');
        
        $select = $table->select();
        $select->setIntegrityCheck(false);
        $select->from($table->info('name'),array('*'));
        $select->where('date = ?',date('Y-m-d'));
        $select->joinLeft('engine4_users','engine4_users.user_id = '.$table->info('name').'.user_id' ,array('email'));
        $bans = $table->fetchAll($select);
        $bans = $bans->toArray();

        $users   = array();
        $ban_ids = array();

        foreach ($bans as $key => $ban) {
            
            $users[] = $user = Engine_Api::_()->user()->getUser($ban['user_id']);
            
            $user->approved = 1;
            $user->verified = 1;
            $user->enabled  = 1;
            $user->save();
            
            $ban_ids[] = $ban['ban_id'];
        }

        $db = $table->getAdapter();
        $db->beginTransaction;            

        try {
            
            $db->delete($table->info('name'),new Zend_Db_Expr('ban_id IN('.implode(',',$ban_ids).')'));
            
            Engine_Api::_()->getApi('mail', 'core')->sendSystem($users, 'moderatortoolbox_unban', array());
            
            $db->commit();
        } catch (Exception $ex) {
            $db->rollBack();
        }

    }
}