<?php

class Moderatortoolbox_Plugin_Menus {

    // core_mini

    public function onMenuInitialize_CoreMiniModeratortoolbox($row) {
        if (Engine_Api::_()->getApi('core', 'authorization')->isAllowed('moderatortoolbox', null, 'moderate')) {
            return array(
                'label' => $row->label,
                'route' => 'moderatortoolbox_home'
            );
        }

        return false;
    }

    // navigation
    
    public function onMenuInitialize_ModerateLogs($row) {
        if (Engine_Api::_()->getApi('core', 'authorization')->isAllowed('moderatortoolbox', null, 'view_logs')) {
            return array(
                'label' => $row->label,
                'route' => 'moderatortoolbox_logs'
            );
        }

        return false;
    }

}