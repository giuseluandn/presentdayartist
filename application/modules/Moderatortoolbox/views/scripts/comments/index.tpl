<?php
$localeObject = Zend_Registry::get('Locale');

$months = Zend_Locale::getTranslationList('months', $localeObject);
if ($months['default'] == NULL) {
    $months['default'] = "wide";
}
$months = $months['format'][$months['default']];

$days = Zend_Locale::getTranslationList('days', $localeObject);
if ($days['default'] == NULL) {
    $days['default'] = "wide";
}
$days = $days['format'][$days['default']];
?>

<?php
$this->headScript()
        ->appendFile($this->layout()->staticBaseUrl . 'externals/calendar/calendar.compat.js');
$this->headLink()
        ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/calendar/styles.css');
?>

<?php $this->headScript()->captureStart(); ?>
en4.core.runonce.add(function(){
neCalendar=new Class({
Implements: [Calendar],

clicked: function(td, day, cal) {
cal.val = (this.value(cal) == day) ? null : new Date(cal.year, cal.month, day);
this.write(cal);
var value = this.format(cal.val, 'Y-m-d');
this.toggle(cal);
//$('date_form').submit();
}
});
});
<?php $this->headScript()->captureEnd(); ?>

<script type="text/javascript">
    en4.core.runonce.add(function() {

        new neCalendar({'start-date': 'd/m/Y', 'end-date': 'd/m/Y'}, {
            classes: ['event_calendar'],
            pad: 0,
            direction: 0,
            months: <?php echo Zend_Json::encode(array_values($months)) ?>,
            days: <?php echo Zend_Json::encode(array_values($days)) ?>,
            day_suffixes: ['', '', '', ''],
            onHideStart: function() {
                if (typeof cal_date_onHideStart == 'function')
                    cal_date_onHideStart();
            },
            onHideComplete: function() {
                if (typeof cal_date_onHideComplete == 'function')
                    cal_date_onHideComplete();
            },
            onShowStart: function() {
                if (typeof cal_date_onShowStart == 'function')
                    cal_date_onShowStart();
            },
            onShowComplete: function() {
                if (typeof cal_date_onShowComplete == 'function')
                    cal_date_onShowComplete();
            }
        })
    });
</script>

<script type="text/javascript">

    Element.implement({
        fancyDestroy: function() {
            this.set('tween', {
                onComplete: function(el) {
                    el.destroy();
                }
            });
            this.fade('out');
        }
    });


    function selectAll(item) {
        var i;
        var multimodify_form = $('multimodify_form');
        var inputs = multimodify_form.elements;
        for (i = 0; i < inputs.length; i++) {
            if (!inputs[i].disabled && inputs[i].type == 'checkbox') {
                inputs[i].checked = item.checked;
            }
        }
    }

    function deleteComment(cid, rid, uid, act) {
        var res = $$('input[name=modify_' + cid + ']')[0].getProperty('res');
        Smoothbox.open("<div class='settings'><div class='global_form_popup'><div><h3><?php echo $this->translate('Delete comment?'); ?></h3><br /><p><button onclick='javascript:dComment(" + cid + "," + rid + "," + uid + ",\"" + act + "\",\"" + res + "\")'><?php echo $this->translate('Delete'); ?></button> or <a onclick='javascript:parent.Smoothbox.close()'><?php echo $this->translate('Cancel'); ?></a></p></div></div></div>");
    }

    function editComment(cid, rid, text, resource) {
        Smoothbox.open("<div class='settings'><div class='global_form_popup'><div><div><textarea id='edit_comment_input'>" + text + "</textarea></div><button onclick='javascript:saveComment(" + cid + "," + rid + ",\"" + resource + "\")'><?php echo $this->translate('Save'); ?></button> or <a onclick='javascript:parent.Smoothbox.close()'><?php echo $this->translate('Cancel'); ?></a></div></div></div>");
    }

    function deleteComments() {

        var i;
        var cids = [];
        var rids = [];
        var res = [];
        var multimodify_form = $('multimodify_form');
        var inputs = multimodify_form.elements;
        for (i = 1; i < inputs.length; i++) {
            if (!inputs[i].disabled && inputs[i].checked && inputs[i].value != 'on') {
                cids.push(inputs[i].value);
                rids.push(inputs[i].getProperty('rid'));
                res.push($$('input[name=modify_' + inputs[i].value + ']')[0].getProperty('res'));
            }
        }
        cids = cids.join('|');
        rids = rids.join('|');
        res = res.join('|');

        Smoothbox.open("<div class='settings'><div class='global_form_popup'><div><h3><?php echo $this->translate('Delete selected comments?'); ?></h3><br /><p><button onclick='javascript:dComment(\"" + cids + "\",\"" + rids + "\",0,\"delete_all\",\"" + res + "\")'><?php echo $this->translate('Delete'); ?></button> or <a onclick='javascript:parent.Smoothbox.close()'><?php echo $this->translate('Cancel'); ?></a></p></div></div></div>");
    }

    function saveComment(cid, rid, resource) {
        var commentBody = $('edit_comment_input').value;

        var url = '<?php echo $this->url(); ?>/update-comment';
        new Request({
            url: url,
            onSuccess: function(data) {
                $$('input[name=modify_' + cid + ']')[0].getParent().getChildren('.membermoderation_results_info')[0].getChildren('span')[0].set('text', commentBody);
                Smoothbox.close();
            }
        }).post('cid=' + cid + '&rid=' + rid + '&text=' + encodeURIComponent(commentBody) + '&resource=' + resource);
    }

    function dComment(cid, rid, uid, act, res) {

        var url = '<?php echo $this->url(); ?>/delete-comment';
        new Request({
            url: url,
            onSuccess: function(data) {
                if (!isNaN(parseFloat(cid)) && isFinite(cid)) {
                    var el = $$('input[name=modify_' + cid + ']')[0].getParent();
                    el.fancyDestroy();
                } else {
                    cid = cid.split("|");
                    var i;
                    for (i = 0; i < cid.length; i++) {
                        var el = $$('input[name=modify_' + cid[i] + ']')[0].getParent();
                        el.fancyDestroy();
                    }
                }
                Smoothbox.close();
            }
        }).post('cid=' + cid + '&rid=' + rid + '&uid=' + uid + '&act=' + act + '&res=' + res);
    }

</script>

<div class="layout_page_moderatortoolbox-index-index">
    <div class="generic_layout_container layout_top">
        <div class="generic_layout_container layout_middle">
            <div class="generic_layout_container layout_moderatortoolbox_browse_menu">

                <div class="headline">
                    <h2><?php echo $this->translate('Moderation') ?></h2>
                    <div class="tabs">
                        <?php
// Render the menu
                        echo $this->navigation()
                                ->menu()
                                ->setContainer($this->navigation)
                                ->render();
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class='generic_layout_container layout_right'>
    <div class="generic_layout_container layout_moderatortoolbox_browse_search">  
        <form method="post" action="<?php echo $this->url() ?>" enctype="application/x-www-form-urlencoded" id="date_form">

            <div class="browsemembers_criteria">

                <div class="mcount-wrapper" id="mcount-wrapper">
                    <div class="form-label" id="mcount-label">
                        <label class="optional" for="mcount"><?php echo $this->translate('Username') ?></label>
                    </div>
                    <div class="form-element" id="mcount-element">
                        <input type="text" name="uname" value="<?php echo $this->username; ?>" />
                    </div>
                </div>

                <div class="mcount-wrapper" id="mcount-wrapper">
                    <div class="form-label" id="mcount-label">
                        <label class="optional" for="mcount"><?php echo $this->translate('Email') ?></label>
                    </div>
                    <div class="form-element" id="mcount-element">
                        <input type="text" name="email" value="<?php echo $this->email; ?>" />
                    </div>
                </div>

                <div class="mcount-wrapper" id="date-wrapper">
                    <div class="form-label" id="date-label">
                        <label class="optional" for="start-date"><?php echo $this->translate('Start Date') ?></label>
                    </div>
                    <div class="form-element" id="date-element">
                        <div class="event_calendar_container" style="display:inline">
                            <?php echo $this->formHidden('start-date', $this->start_date, array_merge(array('class' => 'calendar', 'id' => 'start-date'), array())) ?>
                            <span class="calendar_output_span" id="calendar_output_span_start-date"><?php echo $this->start_date ?></span>
                        </div>
                    </div>
                </div>

                <div class="mcount-wrapper" id="date-wrapper">
                    <div class="form-label" id="date-label">
                        <label class="optional" for="end-date"><?php echo $this->translate('End Date') ?></label>
                    </div>
                    <div class="form-element" id="date-element">
                        <div class="event_calendar_container" style="display:inline">
                            <?php echo $this->formHidden('end-date', $this->end_date, array_merge(array('class' => 'calendar', 'id' => 'end-date'), array())) ?>
                            <span class="calendar_output_span" id="calendar_output_span_end-date"><?php echo $this->end_date ?></span>
                        </div>
                    </div>
                </div>
                <br />

                <button name="done" id="done" type="submit">Search</button>
            </div>
        </form>
    </div>
</div>
<div class="layout_middle" id="browsemembers_results">
    <div class="commentsmoderation_results">
        <form id="multimodify_form" method="post">
            <ul id="membermoderation_ul">
                <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                    <li><input onclick="selectAll(this)" type='checkbox' class='checkbox'></li>
                <?php else : ?>
                    <h3><?php echo $this->translate('No comments found'); ?></h3>
                <?php endif; ?>
                <?php foreach ($this->paginator as $comment): ?>


                    <?php
                    $sender = Engine_Api::_()->user()->getUser($comment->poster_id);
                    $allow_message = false;

                    $permission = Engine_Api::_()->authorization()->getPermission($this->viewer->level_id, 'messages', 'create');
                    if (Authorization_Api_Core::LEVEL_DISALLOW === $permission) {
                        
                    } else {
                        $messageAuth = Engine_Api::_()->authorization()->getPermission($this->viewer->level_id, 'messages', 'auth');
                        if ($messageAuth == 'none') {
                            
                        } else if ($messageAuth == 'friends') {
                            if (isset($this->viewer->membership()->getRow($sender)->active) &&
                                    $this->viewer->membership()->getRow($sender)->active == 1) {
                                $allow_message = true;
                            }
                        }
                    }
                    ?>


                    <li>

                        <input style="margin-top: 20px;" <?php echo $sender->isAdmin() ? 'disabled' : '' ?> name='modify_<?php echo $comment->getIdentity(); ?>' value=<?php echo $comment->getIdentity(); ?> rid=<?php echo $comment->resource_id; ?> res=<?php echo $comment->resource_type; ?> type='checkbox' class='checkbox' />
                        <?php echo $this->htmlLink($sender->getHref(), $this->itemPhoto($sender, 'thumb.icon')); ?>
                        <div class="membermoderation_results_links">
                            <ul>
                                <li class="feed_item_option_comment"><?php echo $this->htmlLink($comment->getHref(), $this->translate('Permalink'), array('class' => 'buttonlink permalink')); ?></li>
                                <?php if (!$sender->isAdmin()) : ?>
                                    <li class="feed_item_option_comment"><?php echo $this->htmlLink($this->url(), $this->translate('Edit'), array('class' => 'buttonlink edit-comments', 'onclick' => 'editComment(' . $comment->comment_id . ',' . $comment->resource_id . ',"' . trim($comment->body) . '","' . $comment->resource_type . '"); return false;')); ?></li>
                                    <li class="feed_item_option_comment"><?php echo $this->htmlLink($this->url(), $this->translate('Delete'), array('class' => 'buttonlink delete-comment', 'onclick' => 'deleteComment(' . $comment->comment_id . ',' . $comment->resource_id . ',' . (isset($sender->user_id) ? $sender->user_id : 0) . ',"delete"); return false;')); ?></li>
                                    <?php if ($sender->getIdentity() > 0) : ?>
                                        <li class="feed_item_option_comment"><?php echo $this->htmlLink($this->url(), $this->translate('Delete Comment & Ban User'), array('class' => 'user_disable buttonlink', 'onclick' => 'deleteComment(' . $comment->comment_id . ',' . $comment->resource_id . ',' . $sender->user_id . ',"disable"); return false;')); ?></li>
                                    <?php endif ?>
                                <?php endif; ?>
                                <li class="feed_item_option_comment"><?php if ($allow_message) echo $this->htmlLink($this->url(array('action' => 'compose', 'to' => $sender->user_id), 'messages_general', true), $this->translate('Send Message'), array('class' => 'moderation_link buttonlink')); ?></li>
                            </ul>
                        </div>
                        <div class="membermoderation_results_info">
                            <?php echo $this->htmlLink($sender->getHref(), isset($sender->username) ? $sender->username : $sender->getTitle()); ?>
                            <span><?php echo $comment->body; ?></span>

                            <div class="membermoderation_info">
                                <b><?php echo $this->translate('Creation Date:'); ?></b>
                                <?php echo $comment->creation_date; ?>
                            </div>

                        </div>
                    </li>
                <?php endforeach; ?>

            </ul>
            <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                <div class='buttons'>
                    <button type='submit' name="submit_button" value="delete" onClick="deleteComments('delete');
                            return false;"><?php echo $this->translate("Delete Selected") ?></button>
                </div>
            <?php endif ?>

        </form>


        <div class="paginator">
            <?php
            echo $this->paginationControl($this->paginator, null, null, array('query' => $this->values));
            ?>
        </div>
    </div>
</div>
</div>