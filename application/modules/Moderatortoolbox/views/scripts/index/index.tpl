<script type="text/javascript">

    function selectAll(item) {
        var i;
        var multimodify_form = $('multimodify_form');
        var inputs = multimodify_form.elements;
        for (i = 0; i < inputs.length; i++) {
            if (!inputs[i].disabled && inputs[i].type == 'checkbox') {
                inputs[i].checked = item.checked;
            }
        }
    }

    function multiModify(type) {
        var i;
        var ids = [];
        var multimodify_form = $('multimodify_form');
        var inputs = multimodify_form.elements;
        for (i = 1; i < inputs.length; i++) {
            if (!inputs[i].disabled && inputs[i].checked && inputs[i].value != 'on') {
                ids.include(inputs[i].value);
            }
        }
        ids_string = ids.join('|');
        Smoothbox.open('<?php echo $this->url(array(), 'moderatortoolbox_multimodify', true); ?>/key/' + type + '/ids/' + ids_string);
    }

    function loginAsUser(id) {
        if (!confirm('<?php echo $this->translate('Note that you will be logged out of your current account if you click ok.') ?>')) {
            return;
        }
        var url = '<?php echo $this->url(array('module' => 'user', 'controller' => 'manage', 'action' => 'login'), 'admin_default', true) ?>';
        var baseUrl = '<?php echo $this->url(array(), 'default', true) ?>';
        (new Request.JSON({
            url: url,
            data: {
                format: 'json',
                id: id
            },
            onSuccess: function () {
                window.location.replace(baseUrl);
            }
        })).send();
    }
</script>
<div class="layout_page_moderatortoolbox-index-index">
    <div class="generic_layout_container layout_top">
        <div class="generic_layout_container layout_middle">
            <div class="generic_layout_container layout_moderatortoolbox_browse_menu">

                <div class="headline">
                    <h2><?php echo $this->translate('Moderation') ?></h2>
                    <div class="tabs">
                        <?php
                        // Render the menu
                        echo $this->navigation()
                                ->menu()
                                ->setContainer($this->navigation)
                                ->render();
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="generic_layout_container layout_main">
    <div class='generic_layout_container  layout_right'>
        <div class="generic_layot_container layout_moderatortoolbox_browse_search">
            <?php echo $this->form->render($this) ?>
        </div>
    </div>
    <div class="generic_layout_container layout_middle">
        <div class="generic_layout_container layout_core_content">
            <div class="browsemembers_results" id="browsemembers_results">
                <div class="membermoderation_results">
                    <h3>
                        <?php echo $this->translate(array('%s member found.', '%s members found.', $this->paginator->getTotalItemCount()), $this->locale()->toNumber($this->paginator->getTotalItemCount())) ?>
                    </h3>
                    <form id="multimodify_form" method="post">
                        <ul id="membermoderation_ul">
                            <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                                <li><input onclick="selectAll(this)" type='checkbox' class='checkbox'></li>
                            <?php endif ?>
                            <?php foreach ($this->paginator as $user): ?>


                                <?php
                                if (isset($comment->poster_id))
                                    $sender = Engine_Api::_()->user()->getUser($comment->poster_id);
                                $allow_message = false;

                                $permission = Engine_Api::_()->authorization()->getPermission($this->viewer->level_id, 'messages', 'create');
                                if (Authorization_Api_Core::LEVEL_DISALLOW === $permission) {

                                } else {
                                    $messageAuth = Engine_Api::_()->authorization()->getPermission($this->viewer->level_id, 'messages', 'auth');
                                    if ($messageAuth == 'none') {

                                    } else if ($messageAuth == 'friends') {
                                        if (isset($this->viewer->membership()->getRow($user)->active) &&
                                                $this->viewer->membership()->getRow($user)->active == 1) {
                                            $allow_message = true;
                                        }
                                    }
                                }
                                ?>


                                <li>
                                    <input style="margin-top: 20px;" <?php
                                    if ($user->level_id == 1 || $user->level_id == 2)
                                        echo 'disabled';
                                    ?> name='modify_<?php echo $user->getIdentity(); ?>' value=<?php echo $user->getIdentity(); ?> type='checkbox' class='checkbox' />
                                           <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon')) ?>
                                           <?php if (Engine_Api::_()->core()->getSubject()->getIdentity() != $user->user_id) : ?>
                                        <div class="membermoderation_results_links">
                                            <?php if ($allow_message) echo $this->htmlLink($this->url(array('action' => 'compose', 'to' => $user->user_id), 'messages_general', true), $this->translate('Send Message'), array('class' => 'buttonlink moderation_link')); ?>
                                            <br />
                                            <?php echo Engine_Api::_()->getItemTable('moderatortoolbox_note')->hasNote($user) ? $this->htmlLink(Engine_Api::_()->getItemTable('moderatortoolbox_note')->getNote($user)->getHref(), $this->translate('View Note'), array('class' => 'smoothbox viewnode')) : $this->htmlLink($this->url(array('uid' => $user->getIdentity()), 'moderatortoolbox_note', true), $this->translate('Add Note'), array('class' => 'smoothbox addnode')); ?>
                                            <br />
                                            <?php if ($this->viewer->level_id == 1 && $user->level_id != 1) : ?>
                                                <?php echo $this->htmlLink($this->url(array('module' => 'user', 'controller' => 'manage', 'action' => 'login', 'id' => $user->user_id), 'admin_default', true), $this->translate('Login as') . ' ' . $user->getTitle(), array('class' => 'user_enable buttonlink', 'onclick' => 'loginAsUser(' . $user->getIdentity() . '); return false;')) ?>
                                                <?php echo $this->htmlLink($this->url(array('action' => 'ban-period', 'id' => $user->user_id)), $this->translate('Ban till...'), array('class' => 'smoothbox user_disable buttonlink', 'onclick' => 'return false;')) ?>
                                            <?php endif ?>
                                        </div>
                                    <?php endif ?>
                                    <div class='membermoderation_results_info'>
                                        <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?> (<?php echo $user->email ?>)
                                        <div class="membermoderation_info">
                                            <?php
                                            unset($c_ipObj, $l_ipObj);
                                            echo '<b>' . $this->translate('Creation IP: ') . '</b>';
                                            if (trim($user->creation_ip)) {
                                                $c_ipObj = new Engine_IP($user->creation_ip);
                                            }
                                            if (trim($user->lastlogin_ip)) {
                                                $l_ipObj = new Engine_IP($user->lastlogin_ip);
                                            }
                                            echo (isset($c_ipObj) ? $c_ipObj->toString() : '-') . ' ';
                                            echo '<b>' . $this->translate('Last Login IP: ') . '</b>';
                                            echo (isset($l_ipObj) ? $l_ipObj->toString() : '-') . ' ';
                                            ?>
                                            <br />
                                            <?php
                                            echo '<b>' . $this->translate('Signup Date: ') . '</b>';
                                            echo $user->creation_date;
                                            ?>
                                            <br />
                                            <?php
                                            echo '<b>' . $this->translate('Status: ') . '</b>';
                                            echo ($user->enabled == 1) ? $this->translate('Enabled') : $this->translate('Disabled');
                                            ?>
                                            <br />
                                            <?php
                                            echo '<b>' . $this->translate('Username: ') . '</b>';
                                            echo $user->username . '&nbsp';
                                            if (Engine_Api::_()->moderatortoolbox()->getCountSentMessages($user)->count > 0 && ($user->level_id != 1 && $user->level_id != 2)) {
                                                echo '<br />' . $this->htmlLink($this->url(array('id' => $user->user_id), 'moderatortoolbox_messages', true), '<b>' . $this->translate('Messages Count: ') . '</b>' . Engine_Api::_()->moderatortoolbox()->getCountSentMessages($user)->count, array('class' => 'smoothbox'));
                                            } else {
                                                echo '<br /><b>' . $this->translate('Messages Count: ') . '</b>' . Engine_Api::_()->moderatortoolbox()->getCountSentMessages($user)->count;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                            <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                                <li><input onclick="selectAll(this)" type='checkbox' class='checkbox' /></li>
                            <?php endif ?>
                        </ul>

                        <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                            <div class='buttons'>
                                <button type='submit' name="submit_button" value="delete" onClick="multiModify('delete');

                                            return false;"><?php echo $this->translate("Delete Selected") ?></button>
                                <button type='submit' name="submit_button" value="approve" onClick="multiModify('approve');

                                            return false;"><?php echo $this->translate("Approve Selected") ?></button>
                                <button type='submit' name="submit_button" value="disable" onClick="multiModify('disable');

                                            return false;"><?php echo $this->translate("Disable Selected") ?></button>
                                <button type='submit' name="submit_button" value="enable" onClick="multiModify('enable');

                                            return false;"><?php echo $this->translate("Enable Selected") ?></button>
                            </div>
                        <?php endif ?>
                    </form>
                    <div class="paginator">
                        <?php
                        echo $this->paginationControl($this->paginator, null, null, array(
                            'query' => $this->formValues,
                                //'params' => $this->formValues,
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>