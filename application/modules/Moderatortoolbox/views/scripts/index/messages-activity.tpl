<?php
$localeObject = Zend_Registry::get('Locale');

$months = Zend_Locale::getTranslationList('months', $localeObject);
if ($months['default'] == NULL) {
    $months['default'] = "wide";
}
$months = $months['format'][$months['default']];

$days = Zend_Locale::getTranslationList('days', $localeObject);
if ($days['default'] == NULL) {
    $days['default'] = "wide";
}
$days = $days['format'][$days['default']];
?>

<?php
$this->headScript()
        ->appendFile($this->layout()->staticBaseUrl . 'externals/calendar/calendar.compat.js');
$this->headLink()
        ->appendStylesheet($this->layout()->staticBaseUrl . 'externals/calendar/styles.css');
?>

<?php $this->headScript()->captureStart(); ?>
en4.core.runonce.add(function(){
neCalendar=new Class({
Implements: [Calendar],

clicked: function(td, day, cal) {
cal.val = (this.value(cal) == day) ? null : new Date(cal.year, cal.month, day);
this.write(cal);
var value = this.format(cal.val, 'Y-m-d');
this.toggle(cal);
$('date_form').submit();
}
});
});
<?php $this->headScript()->captureEnd(); ?>

<script type="text/javascript">
    en4.core.runonce.add(function(){
        
        new neCalendar({ 'start-date': 'd/m/Y', 'end-date': 'd/m/Y' }, {
            classes: ['event_calendar'],
            pad: 0,
            direction: 0,
            months : <?php echo Zend_Json::encode(array_values($months)) ?>,
            days : <?php echo Zend_Json::encode(array_values($days)) ?>,
            day_suffixes: ['', '', '', ''],
            onHideStart: function()   { if (typeof cal_date_onHideStart    == 'function') cal_date_onHideStart(); },
            onHideComplete: function(){ if (typeof cal_date_onHideComplete == 'function') cal_date_onHideComplete(); },
            onShowStart: function()   { if (typeof cal_date_onShowStart    == 'function') cal_date_onShowStart(); },
            onShowComplete: function(){ if (typeof cal_date_onShowComplete == 'function') cal_date_onShowComplete(); }
        })
    });
</script>

<script type="text/javascript">
    
    function selectAll(item) {
        var i;
        var multimodify_form = $('multimodify_form');
        var inputs = multimodify_form.elements;
        for (i = 0; i < inputs.length; i++) {
            if (!inputs[i].disabled && inputs[i].type == 'checkbox') {
                inputs[i].checked = item.checked;
            }
        }
    }
    
    function multiModify(type) {
        var i;
        var ids = [];
        var multimodify_form = $('multimodify_form');
        var inputs = multimodify_form.elements;
        for (i = 1; i < inputs.length; i++) {
            if (!inputs[i].disabled && inputs[i].checked && inputs[i].value != 'on') {
                ids.include(inputs[i].value);
            }
        }
        ids_string = ids.join('|');
        Smoothbox.open('<?php echo $this->url(array(), 'moderatortoolbox_multimodify', true); ?>/key/'+type+'/ids/'+ids_string);
    }
</script>

<div class="headline">
    <h2><?php echo $this->translate('Moderation') ?></h2>
    <div class="tabs">
        <?php
        // Render the menu
        echo $this->navigation()
                ->menu()
                ->setContainer($this->navigation)
                ->render();
        ?>
    </div>
</div>


    <div class="generic_layout_container layout_main"> 
    <div class='generic_layout_container layout_right'>
    <div class="generic_layout_container layout_moderatortoolbox_browse_search">  
        <form method="post" action="<?php echo $this->url() ?>" enctype="application/x-www-form-urlencoded" id="date_form">

            <div class="browsemembers_criteria">

                <div id="date-wrapper">
                    <div class="form-label" id="date-label">
                        <label class="optional" for="start-date"><?php echo $this->translate('Start Date') ?></label>
                    </div>
                    <div class="form-element" id="date-element">
                        <div class="event_calendar_container" style="display:inline">
                            <?php echo $this->formHidden('start-date', $this->start_date, array_merge(array('class' => 'calendar', 'id' => 'start-date'), array())) ?>
                            <span class="calendar_output_span" id="calendar_output_span_start-date"><?php echo $this->start_date ?></span>
                        </div>
                    </div>
                </div>
                <div id="date-wrapper">
                    <div class="form-label" id="date-label">
                        <label class="optional" for="end-date"><?php echo $this->translate('End Date') ?></label>
                    </div>
                    <div class="form-element" id="date-element">
                        <div class="event_calendar_container" style="display:inline">
                            <?php echo $this->formHidden('end-date', $this->end_date, array_merge(array('class' => 'calendar', 'id' => 'end-date'), array())) ?>
                            <span class="calendar_output_span" id="calendar_output_span_end-date"><?php echo $this->end_date ?></span>
                        </div>
                    </div>
                </div>
                <div class="mcount-wrapper" id="mcount-wrapper">
                    <div class="form-label" id="mcount-label">
                        <label class="optional" for="mcount"><?php echo $this->translate('Messages (more than)') ?></label>
                    </div>
                    <div class="form-element" id="mcount-element">
                        <select id="mcount" name="mcount" onchange="this.getParent('form').submit();">
                            <?php for ($i = 0; $i <= 100; $i += 5) : ?>
                                <option label="<?php echo $i ?>" value="<?php echo $i ?>" <?php echo ($this->mcount == $i) ? 'selected="selected"' : '' ?>><?php echo $i ?></option>
                            <?php endfor ?>
                        </select>
                    </div>
                </div>
             </div>
            </form>
        </div>
    </div>
    <div class="generic_layout_container layout_middle">
    <div class="generic_layout_container layout_core_content">  
    <div class="membermoderation_results" id="browsemembers_results">
        <h3>
            <?php echo $this->translate(array('%s member found.', '%s members found.', $this->paginator->getTotalItemCount()), $this->locale()->toNumber($this->paginator->getTotalItemCount())) ?>
        </h3>
        <form id="multimodify_form" method="post">
            <ul id="membermoderation_ul">
                <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                    <li><input onclick="selectAll(this)" type='checkbox' class='checkbox'></li>
                <?php endif ?>
                <?php foreach ($this->paginator as $user): ?>
                    <li>
                        <input style="margin-top: 20px;" <?php if ($user->level_id == 1 || $user->level_id == 2)
                    echo 'disabled'; ?> name='modify_<?php echo $user->getIdentity(); ?>' value=<?php echo $user->getIdentity(); ?> type='checkbox' class='checkbox' />
                               <?php echo $this->htmlLink($user->getHref(), $this->itemPhoto($user, 'thumb.icon')) ?>
                               <?php if (Engine_Api::_()->core()->getSubject()->getIdentity() != $user->user_id) : ?>
                            <div class="membermoderation_results_links">
                                <?php echo $this->htmlLink($this->url(array('action' => 'compose', 'to' => $user->user_id), 'messages_general', true), $this->translate('Send Message'), array('class' => 'buttonlink moderation_link')); ?>
                                <br />
                                <?php echo Engine_Api::_()->getItemTable('moderatortoolbox_note')->hasNote($user) ? $this->htmlLink(Engine_Api::_()->getItemTable('moderatortoolbox_note')->getNote($user)->getHref(), $this->translate('View Note'), array('class' => 'smoothbox viewnode')) : $this->htmlLink($this->url(array('uid' => $user->getIdentity()), 'moderatortoolbox_note', true), $this->translate('Add Note'), array('class' => 'smoothbox addnode')); ?>
                            </div>
                        <?php endif ?>
                        <div class='membermoderation_results_info'>
                            <?php echo $this->htmlLink($user->getHref(), $user->getTitle()) ?> (<?php echo $user->email ?>)
                            <div class="membermoderation_info">
                                <?php
                                unset($c_ipObj, $l_ipObj);
                                echo '<b>' . $this->translate('Creation IP: ') . '</b>';
                                if (trim($user->creation_ip)) {
                                    $c_ipObj = new Engine_IP($user->creation_ip);
                                }
                                if (trim($user->lastlogin_ip)) {
                                    $l_ipObj = new Engine_IP($user->lastlogin_ip);
                                }
                                echo (isset($c_ipObj) ? $c_ipObj->toString() : '-') . ' ';
                                echo '<b>' . $this->translate('Last Login IP: ') . '</b>';
                                echo (isset($l_ipObj) ? $l_ipObj->toString() : '-') . ' ';
                                ?>
                                <br />
                                <?php
                                echo '<b>' . $this->translate('Signup Date: ') . '</b>';
                                echo $user->creation_date;
                                ?>
                                <br />
                                <?php
                                echo '<b>' . $this->translate('Username: ') . '</b>';
                                echo $user->username . '&nbsp';
                                if (Engine_Api::_()->moderatortoolbox()->getCountSentMessages($user)->count > 0 && ($user->level_id != 1 && $user->level_id != 2)) {
                                    echo '<br />' . $this->htmlLink($this->url(array('id' => $user->user_id), 'moderatortoolbox_messages', true), '<b>' . $this->translate('Messages Count: ') . '</b>' . Engine_Api::_()->moderatortoolbox()->getCountSentMessages($user)->count, array('class' => 'smoothbox'));
                                } else {
                                    echo '<br /><b>' . $this->translate('Messages Count: ') . '</b>' . Engine_Api::_()->moderatortoolbox()->getCountSentMessages($user)->count;
                                }
                                ?>
                            </div>                        
                        </div>
                    </li>
                <?php endforeach; ?>
                <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                    <li><input onclick="selectAll(this)" type='checkbox' class='checkbox' /></li>
                <?php endif ?>
            </ul>
            <?php if ($this->paginator->getTotalItemCount() > 0) : ?>
                <div class='buttons'>
                    <button type='submit' name="submit_button" value="delete" onClick="multiModify('delete');return false;"><?php echo $this->translate("Delete Selected") ?></button>
                    <button type='submit' name="submit_button" value="approve" onClick="multiModify('approve');return false;"><?php echo $this->translate("Approve Selected") ?></button>
                    <button type='submit' name="submit_button" value="disable" onClick="multiModify('disable');return false;"><?php echo $this->translate("Disable Selected") ?></button>
                    <button type='submit' name="submit_button" value="enable" onClick="multiModify('enable');return false;"><?php echo $this->translate("Enable Selected") ?></button>
                </div>
            <?php endif ?>
        </form>
        <div class="paginator">
            <?php
            echo $this->paginationControl($this->paginator, null, null, array(
                'query' => $this->formValues,
                    //'params' => $this->formValues,
            ));
            ?>
                </div>
            </div>
        </div>
    </div>
</div>