<?php if ($this->isAdmin != true) : ?>
    <div class="messages_list">
        <ul>
            <li>
                <div class="messages_list_from"><?php echo $this->translate('Recipients') ?></div>
                <div class="messages_list_info"><?php echo $this->translate('Message') ?></div>
            </li>
            <?php foreach ($this->messages as $conversation): ?>
                <?php
                $message = $conversation->getOutboxMessage($this->user);
                $recipients = $conversation->getRecipients();
                $resource = "";
                $sender = "";
                if ($conversation->hasResource() &&
                        ($resource = $conversation->getResource())) {
                    $sender = $resource;
                } else if ($conversation->recipients > 1) {
                    foreach ($conversation->getRecipients() as $recip) {
                        $user = Engine_Api::_()->user()->getUser($recip->getIdentity());
                        $recipient = $conversation->getRecipientInfo($user);
                        if (!$recipient->inbox_read) {
                            $sender = $user;
                            break;
                        }
                    }
                } else {
                    foreach ($conversation->getRecipients() as $tmpUser) {
                        if ($tmpUser->getIdentity() != $this->user->getIdentity()) {
                            $sender = $tmpUser;
                            break;
                        }
                    }
                }
                if ((!isset($sender) || !$sender)) {
                    if ($this->user->getIdentity() !== $conversation->user_id) {
                        $sender = Engine_Api::_()->user()->getUser($conversation->user_id);
                    } else {
                        $sender = $this->user;
                    }
                }

                $recipient = $conversation->getRecipientInfo($sender);
                ?>
                <li<?php if (!$recipient->inbox_read): ?> class='messages_list_new'<?php endif; ?> id="message_conversation_<?php echo $conversation->getIdentity() ?>">
                    <div class="messages_list_from">
                        <p class="messages_list_from_name">
                            <?php if (!empty($resource)): ?>
                                <?php echo $resource->toString() ?>
                            <?php elseif ($conversation->recipients == 1): ?>
                                <?php echo $this->htmlLink($sender->getTitle(), $sender->getTitle(), array('onclick' => 'parent.window.location = "'. $sender->getHref() . '"; return false')) ?>
                            <?php else: ?>
                                <?php foreach ($conversation->getRecipients() as $resip) : ?>
                                    <?php if ($resip != $sender) : ?>
                                        <?php echo $this->htmlLink($resip->getHref(), $resip->getTitle()) . '<br />' ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            <?php endif; ?>
                        </p>
                        <p class="messages_list_from_date">
                            <?php echo $this->timestamp($message->date) ?>
                        </p>
                    </div>
                    <div class="messages_list_info">
                        <p class="messages_list_info_title">
                            <?php
                            // ... scary
                            ( (isset($message) && '' != ($title = trim($message->getTitle()))) ||
                                    (isset($conversation) && '' != ($title = trim($conversation->getTitle()))) ||
                                    $title = '<em>' . $this->translate('(No Subject)') . '</em>' );
                            ?>
                            <?php echo $title ?>
                        </p>
                        <p class="messages_list_info_body">
                            <?php echo $this->string()->truncate(html_entity_decode($message->body)) ?>
                        </p>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php else : ?>
    <?php echo $this->translate('You don\'t have permissions to view this!'); ?>
<?php endif ?>
