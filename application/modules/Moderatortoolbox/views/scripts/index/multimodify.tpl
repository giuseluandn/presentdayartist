<?php if ($this->empty == true) : ?>
    <div>
        <h3>You must select at least one user.</h3>
        <br/>
        <p style="text-align: center">
            <a href='javascript:void(0);' onclick='javascript:parent.Smoothbox.close()'>
                <?php echo $this->translate("Ok") ?></a>
        </p>
    </div>
<?php else : ?>

    <form method="post" class="global_form_popup">
        <div>
            <h3>
                <?php if ($this->single == true) : ?>
                    <?php
                    switch ($this->key) :
                        case 'delete':
                            ?>
                            <?php echo $this->translate('Delete This User?') ?>
                            <?php break ?>
                        <?php case 'approve' : ?>
                            <?php echo $this->translate('Approve This User?') ?>
                            <?php break ?>
                        <?php case 'disable' : ?>
                            <?php echo $this->translate('Disable This User?') ?>
                            <?php break ?>
                        <?php case 'enable' : ?>
                            <?php echo $this->translate('Enable This User?') ?>
                            <?php break ?>
                    <?php endswitch; ?>
                <?php else : ?>
                    <?php
                    switch ($this->key) :
                        case 'delete':
                            ?>
                            <?php echo $this->translate(array('Delete %s User?', 'Delete %s Users?', $this->count), $this->locale()->toNumber($this->count)) ?>
                            <?php break ?>
                        <?php case 'approve' : ?>
                            <?php echo $this->translate(array('Approve %s User?', 'Approve %s Users?', $this->count), $this->locale()->toNumber($this->count)) ?>
                            <?php break ?>
                        <?php case 'disable' : ?>
                            <?php echo $this->translate(array('Disable %s User?', 'Disable %s Users?', $this->count), $this->locale()->toNumber($this->count)) ?>
                            <?php break ?>
                        <?php case 'enable' : ?>
                            <?php echo $this->translate(array('Enable %s User?', 'Enable %s Users?', $this->count), $this->locale()->toNumber($this->count)) ?>
                            <?php break ?>
                    <?php endswitch; ?>
                <?php endif; ?>
            </h3>
            <br />
            <p>
                <button type='submit'>
                    <?php
                    switch ($this->key) :
                        case 'delete':
                            ?>
                            <?php echo $this->translate('Delete') ?>
                            <?php break ?>
                        <?php case 'approve' : ?>
                            <?php echo $this->translate('Approve') ?>
                            <?php break ?>
                        <?php case 'disable' : ?>
                            <?php echo $this->translate('Disable') ?>
                            <?php break ?>
                        <?php case 'enable' : ?>
                            <?php echo $this->translate('Enable') ?>
                            <?php break ?>
                    <?php endswitch; ?>
                </button>
                <?php echo $this->translate("or") ?>
                <a href='javascript:void(0);' onclick='javascript:parent.Smoothbox.close()'>
                    <?php echo $this->translate("cancel") ?></a>
            </p>
        </div>
    </form>

    <?php if (@$this->closeSmoothbox): ?>
        <script type="text/javascript">
            TB_close();
        </script>
    <?php endif ?>
<?php endif ?>