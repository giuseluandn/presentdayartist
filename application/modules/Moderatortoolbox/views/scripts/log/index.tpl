<div class="headline">
    <h2><?php echo $this->translate('Moderation') ?></h2>
    <div class="tabs">
        <?php
        // Render the menu
        echo $this->navigation()
                ->menu()
                ->setContainer($this->navigation)
                ->render();
        ?>
    </div>
</div>
<div class="generic_layout_container layout_main"> 
<div class='generic_layout_container layout_right_custom'> 
<div class='generic_layout_container layou_moderatortoolbox_browse_search layout_right_custom'>
    <?php echo $this->form->render($this) ?>
</div>
</div>
<div class="generic_layout_container layout_middle">
<div class="generic_layout_container layou_moderatortoolbox_browse_search"> 
    <?php if (count($this->paginator) > 0): ?>
        <ul class='logs_browse'>
            <?php foreach ($this->paginator as $log): ?>
                <?php $params = unserialize($log->log) ?>
                <li>
                    <div class="logs_info">
                        <?php $moderator = Engine_Api::_()->user()->getUser($log->user_id); ?>
                        <?php if ($params['item_name'] == 'user') : ?>
                            <?php if ($params['action'] == 'delete') : ?>
                                <?php echo $this->translate('%1$s delete user %2$s', $this->htmlLink($moderator->getHref(), $moderator->getTitle()), $params['item_value']) ?>
                            <?php else : ?>
                                <?php $user = Engine_Api::_()->user()->getUser($params['item_value']); ?>
                                <?php echo $this->translate('%1$s ' . $params['action'] . ' user %2$s', $this->htmlLink($moderator->getHref(), $moderator->getTitle()), $this->htmlLink($user->getHref(), $user->getTitle())) ?>
                            <?php endif ?>
                        <?php elseif ($params['item_name'] == 'note') : ?>
                            <?php $user = Engine_Api::_()->user()->getUser($params['item_value']); ?>
                            <?php echo $this->translate('%1$s ' . $params['action'] . ' note of the %2$s', $this->htmlLink($moderator->getHref(), $moderator->getTitle()), $this->htmlLink($user->getHref(), $user->getTitle())) ?>
                        <?php endif ?>
                        <?php echo $this->translate('at') ?>
                        <?php echo $this->locale()->toDateTime(strtotime($log->log_time)); ?>
                    </div>
                </li>
            <?php endforeach; ?> 
            <?php else : ?>
         <h3><?php echo $this->translate('No comments found'); ?></h3>
        </ul>

        <?php if ($this->paginator->count() > 1): ?>
            <?php
            echo $this->paginationControl($this->paginator, null, null, array(
                'query' => $this->formValues,
            ));
            ?>
        <?php endif; ?>
    <?php endif; ?>
</div></div></div>