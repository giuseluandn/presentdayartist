<script type="text/javascript">
    en4.core.runonce.add(function () {
        $$('th.admin_table_short input[type=checkbox]').addEvent('click', function () {
            $$('input[type=checkbox]').set('checked', $(this).get('checked', false));
        });
    });

    var delectSelected = function () {
        var checkboxes = $$('input[type=checkbox]');
        var selecteditems = [];
        var selectedtypes = [];

        checkboxes.each(function (item, index) {
            var checked = $(item).get('checked');
            var value = $(item).get('value');
            var stype = $(item).get('stype');
            if (checked == true && value != 'on') {
                selecteditems.push(value);
                selectedtypes.push(stype);
            }
        });

        ('ids').value = selecteditems;
        ('types').value = selectedtypes;
        $('delete_selected').submit();
    }

</script>
<div id="global_content">
    <div class="headline">
        <h2><?php echo $this->translate('Abuse Reports') ?></h2>
        <div class="tabs">
            <?php
            // Render the menu
            echo $this->navigation()
                    ->menu()
                    ->setContainer($this->navigation)
                    ->render();
            ?>
        </div>
    </div>

    <div class="generic_layout_container layout_middle">
        <?php if ($this->paginator->getTotalItemCount() > 0): ?>
            <script type="text/javascript">
                var currentOrder = '<?php echo $this->filterValues['order'] ?>';
                var currentOrderDirection = '<?php echo $this->filterValues['direction'] ?>';
                var changeOrder = function (order, default_direction) {
                    // Just change direction
                    if (order == currentOrder) {
                        $('direction').value = (currentOrderDirection == 'ASC' ? 'DESC' : 'ASC');
                    } else {
                        $('order').value = order;
                        $('direction').value = default_direction;
                    }
                    $('filter_form').submit();
                }
            </script>

            <div class='admin_search'>
                <?php
                $this->formFilter->removeAttrib('class');
                echo $this->formFilter->render($this)
                ?>
            </div>

        <?php endif; ?>


        
        <div class="generic_layout_container layout_core_content">  
        <div class='admin_results' id="browsemembers_results">
            <h3>
                <?php $count = $this->paginator->getTotalItemCount() ?>
                <?php echo $this->translate(array("%s report found", "%s reports found", $count), $count) ?>
            </h3>
            <div>
                <?php
                echo $this->paginationControl($this->paginator, null, null, array(
                    'query' => $this->filterValues,
                    'pageAsQuery' => true,
                ));
                ?>
            </div>
        </div>

        <br />



        <?php if (count($this->paginator)): ?>
            <div class="amandabugfix">
                <table class='admin_table_mt'>
                    <thead class="table_headline">
                        <tr>
                            <th style="width: 1%;" class="admin_table_short"><input type='checkbox' class='checkbox'></th>
                            <th style="width: 1%;">
                                <a href="javascript:void(0);" onclick="javascript:changeOrder('report_id', 'ASC');">
                                    <?php echo $this->translate("ID") ?>
                                </a>
                            </th>
                            <th>
                                <a href="javascript:void(0);" onclick="javascript:changeOrder('description', 'ASC');">
                                    <?php echo $this->translate("Description") ?>
                                </a>
                            </th>
                            <th style="width: 1%;">
                                <?php echo $this->translate("Reporter") ?>
                            </th>
                            <th style="width: 1%;">
                                <a href="javascript:void(0);" onclick="javascript:changeOrder('creation_date', 'ASC');">
                                    <?php echo $this->translate("Date") ?>
                                </a>
                            </th>
                            <th style="width: 1%;">
                                <a href="javascript:void(0);" onclick="javascript:changeOrder('subject_type', 'ASC');">
                                    <?php echo $this->translate("Content Type") ?>
                                </a>
                            </th>
                            <th style="width: 1%;">
                                <a href="javascript:void(0);" onclick="javascript:changeOrder('category', 'ASC');">
                                    <?php echo $this->translate("Reasons") ?>
                                </a>
                            </th>
                            <th style="width: 1%;">
                                <?php echo $this->translate("Options") ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($this->paginator as $item): ?>
                            <tr>
                                <td><input type='checkbox' class='checkbox' value="<?php echo $item->report_id ?>"></td>
                                <td><?php echo $item->report_id ?></td>
                                <td style="white-space: normal;"><?php echo $item->description ?></td>
                                <td class="nowrap"><?php echo $this->htmlLink($this->item('user', $item->user_id)->getHref(), $this->item('user', $item->user_id)->getTitle(), array('target' => '_blank')) ?></td>
                                <td class="nowrap"><?php echo $item->creation_date ?></td>
                                <td class="nowrap"><?php echo $item->subject_type ?></td>
                                <td class="nowrap"><?php echo $item->category ?></td>
                                <td class="admin_table_options">
                                    <?php echo $this->htmlLink(array('action' => 'action', 'id' => $item->getIdentity(), 'reset' => false, 'format' => 'smoothbox'), $this->translate("take action"), array('class' => 'smoothbox')) ?>
                                    <span class="sep">|</span>
                                    <?php if (!empty($item->subject_type)): ?>
                                        <?php echo $this->htmlLink(array('action' => 'view', 'id' => $item->getIdentity(), 'reset' => false), $this->translate("view content"), array('target' => '_blank')) ?>
                                        <span class="sep">|</span>
                                    <?php endif; ?>
                                    <?php echo $this->htmlLink(array('action' => 'delete', 'id' => $item->getIdentity(), 'reset' => false), $this->translate("dismiss"), array('class' => 'smoothbox')) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <br/>

            <div class='buttons'>
                <button onclick="javascript:delectSelected();" type='submit'><?php echo $this->translate("Dismiss Selected") ?></button>
            </div>

            <form id='delete_selected' method='post' action='<?php echo $this->url(array('action' => 'deleteselected')) ?>'>
                <input type="hidden" id="ids" name="ids" value=""/>
            </form>

        <?php else: ?>

            <div class="tip">
                <span><?php echo $this->translate("There are currently no outstanding abuse reports.") ?></span>
            </div>

        <?php endif; ?>
  </div></div>
</div>