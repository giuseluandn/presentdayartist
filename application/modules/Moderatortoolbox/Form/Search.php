<?php

/**
 * Description of Search
 *
 * @author mortal
 */
class Moderatortoolbox_Form_Search extends Fields_Form_Search {

    public function init() {

        $custom_elements = array(
            'username',
            'displayname',
            'email',
            'level_id',
            'creation_ip',
            'lastlogin_ip',
            'approved',
            'verified',
            'enabled',
            'status_text',
            'per_page',
            'sort',
            'done',
            'reset',
        );

        parent::init();

        // Add custom elements
        $this->getUserNameElement();
        $this->getDisplayNameElement();
        $this->getEmailElement();
        $this->getLevelElement();
        $this->getMemberTypeElement();
        $this->getAdditionalOptionsElement();

        $this->loadDefaultDecorators();

        $this->getDecorator('HtmlTag')->setOption('class', 'browsemembers_criteria membermoderation_filters');
        $this->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'moderatortoolbox_home', true));

        foreach ($this->getElements() as $name => $el) {
            if (!in_array($name, $custom_elements))
                $this->removeElement($name);
        }
    }

    public function getUserNameElement() {
        $this->addElement('Text', 'username', array(
            'label' => 'UserName',
            'order' => -1000004,
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'filters' => array(
                'StringTrim',
            ),
        ));
        return $this->username;
    }

    public function getDisplayNameElement() {
        $this->addElement('Text', 'displayname', array(
            'label' => 'DisplayName',
            'order' => -1000003,
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'filters' => array(
                'StringTrim',
            ),
        ));
        return $this->displayname;
    }

    public function getEmailElement() {
        $this->addElement('Text', 'email', array(
            'label' => 'Email',
            'order' => -1000002,
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'filters' => array(
                'StringTrim',
            ),
        ));
        return $this->email;
    }

    public function getLevelElement() {
        $levelOptions = array('' => 'All Levels');
        foreach (Engine_Api::_()->getDbtable('levels', 'authorization')->fetchAll() as $level) {
            $levelOptions[$level->level_id] = $level->getTitle();
        }


        $this->addElement('Select', 'level_id', array(
            'label' => 'Member Level',
            'multiOptions' => $levelOptions,
            'order' => -1000001,
            'decorators' => array(
                'viewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
        ));
        return $this->level;
    }

    public function getMemberTypeElement() {
        $multiOptions = array('' => 'All Types');
        $profileTypeFields = Engine_Api::_()->fields()->getFieldsObjectsByAlias($this->_fieldType, 'profile_type');
        if (count($profileTypeFields) !== 1 || !isset($profileTypeFields['profile_type']))
            return;
        $profileTypeField = $profileTypeFields['profile_type'];

        $options = $profileTypeField->getOptions();

        if (count($options) <= 1) {
            if (count($options) == 1) {
                $this->_topLevelId = $profileTypeField->field_id;
                $this->_topLevelValue = $options[0]->option_id;
            }
            return;
        }

        foreach ($options as $option) {
            $multiOptions[$option->option_id] = $option->label;
        }

        $this->addElement('Select', 'profile_type', array(
            'label' => 'Member Type',
            'order' => -1000000,
            'class' =>
            'field_toggle' . ' ' .
            'parent_' . 0 . ' ' .
            'option_' . 0 . ' ' .
            'field_' . $profileTypeField->field_id . ' ',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'multiOptions' => $multiOptions,
        ));
        return $this->profile_type;
    }

    public function getAdditionalOptionsElement() {
        $this->addElement('Text', 'creation_ip', array(
            'label' => 'Creation Ip',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'filters' => array(
                'StringTrim',
            ),
        ));

        $this->addElement('Text', 'lastlogin_ip', array(
            'label' => 'Last Login Ip',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'filters' => array(
                'StringTrim',
            ),
        ));
        $approvedMultiOptions = array(
            '' => '',
            '0' => 'Not Approved',
            '1' => 'Approved'
        );
        $this->addElement('Select', 'approved', array(
            'label' => 'Approved',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'multioptions' => $approvedMultiOptions,
        ));

        $verifiedMultiOptions = array(
            '' => '',
            '0' => 'Not Verified',
            '1' => 'Verified',
        );
        $this->addElement('Select', 'verified', array(
            'label' => 'Verified',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'multioptions' => $verifiedMultiOptions,
        ));

        $enabledMultiOptions = array(
            '' => '',
            '0' => 'Not Enabled',
            '1' => 'Enabled',
        );
        $this->addElement('Select', 'enabled', array(
            'label' => 'Enabled',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'multioptions' => $enabledMultiOptions,
        ));

        $this->addElement('textarea', 'status_text', array(
            'label' => 'Status Text',
            'rows' => 2,
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
        ));

        $itemsPerPageMultiOptions = array(
            '100' => '100',
            '50' => '50',
            '10' => '10',
            '20' => '20',
            '30' => '30',
            'All' => 'All',
        );
        $this->addElement('Select', 'per_page', array(
            'label' => 'Show result',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'multioptions' => $itemsPerPageMultiOptions,
        ));
        $sortMultiOptions = array(
            'creation_date' => 'signup date',
            'lastlogin_ip' => 'last login ip',
            'creation_ip' => 'creation ip',
            'username' => 'username',
            'displayname' => 'display name',
        );
        $this->addElement('Select', 'sort', array(
            'label' => 'Order by',
            'decorators' => array(
                'ViewHelper',
                array('Label', array('tag' => 'span')),
                array('HtmlTag', array('tag' => 'li'))
            ),
            'multioptions' => $sortMultiOptions,
        ));
        $this->addElement('Button', 'done', array(
            'label' => 'Search',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array(
                'ViewHelper',
            //array('HtmlTag', array('tag' => 'li'))
            ),
        ));
        $this->addElement('Cancel', 'reset', array(
            'label' => 'Reset',
            'link' => true,
            'prependText' => ' or ',
            'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'moderatortoolbox_home', true),
            'decorators' => array(
                'ViewHelper'
            )
        ));
    }

}

?>