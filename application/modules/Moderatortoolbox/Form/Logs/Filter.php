<?php

class Moderatortoolbox_Form_Logs_Filter extends Engine_Form {

    public function init() {

        $this->clearDecorators()
                ->addDecorators(array(
                    'FormElements',
                    array('HtmlTag', array('tag' => 'dl')),
                    'Form',
                ))
                ->setMethod('get')
                ->setAttrib('class', 'filters')
                ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'moderatortoolbox_logs', true))
        ;

        $this->addElement('Text', 'moderator', array(
            'label' => 'Moderator\'s username:',
            'decorators' => array(
                'ViewHelper',
                array('HtmlTag', array('tag' => 'dd')),
                array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
            ),
        ));

        $this->addElement('Date', 'min_date', array(
            'label' => 'Start Date:',
            'decorators' => array(
                'ViewHelper',
                array('HtmlTag', array('tag' => 'dd')),
                array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
            ),
        ));

        $this->addElement('Date', 'max_date', array(
            'label' => 'End Date:',
            'decorators' => array(
                'ViewHelper',
                array('HtmlTag', array('tag' => 'dd')),
                array('Label', array('tag' => 'dt', 'placement' => 'PREPEND'))
            ),
        ));

        $itemsPerPageMultiOptions = array(
            '50' => '50',
            '10' => '10',
            '20' => '20',
            '30' => '30',
            '100' => '100',
        );
        $this->addElement('Select', 'per_page', array(
            'label' => 'Show result:',
            'decorators' => array(
                'ViewHelper',
                array('HtmlTag', array('tag' => 'dd')),
                array('Label', array('tag' => 'dt', 'placement' => 'PREPEND')),
            ),
            'multioptions' => $itemsPerPageMultiOptions,
        ));

        $this->addElement('Button', 'done', array(
            'label' => 'Filter',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array(
                'ViewHelper',
            ),
        ));
        $this->addElement('Cancel', 'reset', array(
            'label' => 'Reset',
            'link' => true,
            'prependText' => ' or ',
            'href' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'moderatortoolbox_logs', true),
            'decorators' => array(
                'ViewHelper'
            )
        ));
    }

}