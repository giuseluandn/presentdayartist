<?php

class Moderatortoolbox_Form_Note extends Engine_Form {

    public function init() {
        $this->setTitle('User Note')
                ->setAttrib('class', 'global_form_popup')
                ->setMethod('POST');
        ;
        
        $textarea = new Engine_Form_Element_Textarea('note');
        $textarea->setAttrib('rows', 3);
        $this->addElement($textarea);
        // Buttons
        $this->addElement('Button', 'submit', array(
            'label' => 'Save Note',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'cancel',
            'link' => true,
            'prependText' => ' or ',
            'href' => '',
            'onclick' => 'parent.Smoothbox.close();',
            'decorators' => array(
                'ViewHelper'
            )
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
        $button_group = $this->getDisplayGroup('buttons');
    }

}