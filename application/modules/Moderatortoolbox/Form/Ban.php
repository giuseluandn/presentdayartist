<?php

class Moderatortoolbox_Form_Ban extends Engine_Form {

    public function init() {
        $this->setTitle('Ban user for specific period')
                ->setAttrib('class', 'global_form_popup')
                ->setMethod('POST');
        ;
        
        $this->addElement('CalendarDateTime', 'unban_date', array(
            'label' => 'Ban User',
            'description' => 'Choose date when user would be unbanned',
            'value' => date('M d Y'),
        ));
        
        // Buttons
        $this->addElement('Button', 'submit', array(
            'label' => 'Ban User',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper')
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'cancel',
            'link' => true,
            'prependText' => ' or ',
            'href' => '',
            'onclick' => 'parent.Smoothbox.close();',
            'decorators' => array(
                'ViewHelper'
            )
        ));

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
        $button_group = $this->getDisplayGroup('buttons');
    }

}