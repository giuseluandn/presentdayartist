<?php

class ModeratorToolbox_Form_Admin_Settings_Level extends Authorization_Form_Admin_Level_Abstract {

    public function init() {
        parent::init();

        // My stuff
        $this
                ->setTitle('Member Level Settings')
                ->setDescription("MODERATORTOOLBOX_FORM_ADMIN_LEVEL_DESCRIPTION");

        // Element: view
        $this->addElement('Radio', 'moderate', array(
            'label' => 'Allow Moderate Users?',
            'description' => 'Do you want to let members of this level moderate users?',
            'multiOptions' => array(
                1 => 'Yes, allow moderation.',
                0 => 'No, do not allow to moderate users.',
            ),
        ));
    }
}