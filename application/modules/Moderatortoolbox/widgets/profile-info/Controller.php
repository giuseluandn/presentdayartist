<?php

class Moderatortoolbox_Widget_ProfileInfoController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        if (!Engine_Api::_()->getApi('core', 'authorization')->isAllowed('moderatortoolbox', null, 'moderate'))
            return $this->setNoRender(true);

        if (!Engine_Api::_()->core()->hasSubject())
            return $this->setNoRender(true);

        $subject = Engine_Api::_()->core()->getSubject();

        if ($subject->level_id == 1)
            return $this->setNoRender(true);

        $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer == $subject)
            return $this->setNoRender(true);

        $this->view->user = $user = $subject;

        if (isset($user->creation_ip)) {
            $sameRegisterIps = array();
            $usersTable = Engine_Api::_()->getDbTable('users', 'user');
            $select = $usersTable->select();
            $select->where('creation_ip = ?', $user->creation_ip)
                    ->limit(50);
            $registerIps = $usersTable->fetchAll($select);
            foreach ($registerIps as $ip) {
                if ($ip->getIdentity() != $subject->getIdentity())
                    $sameRegisterIps[] = '<a href="' . $ip->getHref() . '" target="_blank">' . $ip->username . '</a>';
            }
            $this->view->sameRegisterIps = $sameRegisterIps;
        }

        if (isset($user->lastlogin_ip) && !empty($user->lastlogin_ip)) {
            $sameLoginIps = array();
            $select = $usersTable->select();
            $select->where('lastlogin_ip = ?', $user->lastlogin_ip)
                    ->limit(50);
            $loginIps = $usersTable->fetchAll($select);
            foreach ($loginIps as $ip) {
                if ($ip->getIdentity() != $subject->getIdentity())
                    $sameLoginIps[] = '<a href="' . $ip->getHref() . '" target="_blank">' . $ip->username . '</a>';
            }
            $this->view->sameLoginIps = $sameLoginIps;
        }

        $hasNote = Engine_Api::_()->getItemTable('moderatortoolbox_note')->hasNote($user);
        if (true === $hasNote) {
            $this->view->note = $note = Engine_Api::_()->getItemTable('moderatortoolbox_note')->getNote($user);
        }
    }

}
