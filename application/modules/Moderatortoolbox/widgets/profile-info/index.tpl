<ul id="moderation_tab">
    <li>
        <span><?php echo $this->translate('Email') ?></span>
        <span><?php echo $this->user->email ?></span>
    </li>
    <li>
        <span><?php echo $this->translate('IP (Registration)') ?></span>
        <span>
            <?php
            if (trim($this->user->creation_ip)) {
                $c_ipObj = new Engine_IP($this->user->creation_ip);
            }
            echo (isset($c_ipObj) ? $c_ipObj->toString() : '-') . ' ';
            ?>
            <?php if (isset($this->sameRegisterIps) && count($this->sameRegisterIps) > 0) : ?>
                <br />
                <?php
                $urls = implode(', ', $this->sameRegisterIps);
                echo $urls;
                ?>
            <?php endif ?>
        </span>
    </li>
    <li>
        <span><?php echo $this->translate('IP (Last Login)') ?></span>
        <span>
            <?php
            if (trim($this->user->lastlogin_ip)) {
                $l_ipObj = new Engine_IP($this->user->lastlogin_ip);
            }
            echo (isset($l_ipObj) ? $l_ipObj->toString() : '-') . ' ';
            ?>
            <?php if (isset($this->sameLoginIps) && count($this->sameLoginIps) > 0) : ?>
                <br />
                <?php
                $urls = implode(', ', $this->sameLoginIps);
                echo $urls;
                ?>
            <?php endif ?>
        </span>
    </li>
    <li>
        <span><?php echo $this->translate('Note') ?></span>
        <span>
            <?php if (isset($this->note)) : ?>
                <?php echo $this->note->note ?>
                <br />
            <?php endif ?>
            <?php echo Engine_Api::_()->getItemTable('moderatortoolbox_note')->hasNote($this->user) ? $this->htmlLink(Engine_Api::_()->getItemTable('moderatortoolbox_note')->getNote($this->user)->getHref() . '/tab/moderatortoolbox.profile-info', $this->translate('Edit Note'), array('class' => 'smoothbox viewnode')) : $this->htmlLink($this->url(array('uid' => $this->user->getIdentity(), 'tab' => 'moderatortoolbox.profile-info'), 'moderatortoolbox_note', true), $this->translate('Add Note'), array('class' => 'smoothbox addnode')); ?>
        </span>
    </li>
</ul>