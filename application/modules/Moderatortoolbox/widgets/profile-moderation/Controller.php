<?php

class Moderatortoolbox_Widget_ProfileModerationController extends Engine_Content_Widget_Abstract {

    public function indexAction() {
        if (!Engine_Api::_()->getApi('core', 'authorization')->isAllowed('moderatortoolbox', null, 'moderate'))
            return $this->setNoRender(true);

        if (!Engine_Api::_()->core()->hasSubject())
            return $this->setNoRender(true);

        $subject = Engine_Api::_()->core()->getSubject();
        
        if ($subject->level_id == 1)
            return $this->setNoRender(true);
        
        $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
        if ($viewer == $subject)
            return $this->setNoRender(true);
        
        $this->view->user = $subject;
    }

}