<?php if ($this->viewer->isAdmin()) : ?>
    <script type="text/javascript">
        function loginAsUser() {
            if (!confirm('<?php echo $this->translate('Note that you will be logged out of your current account if you click ok.') ?>')) {
                return;
            }
            var url = '<?php echo $this->url(array('module' => 'user', 'controller' => 'manage', 'action' => 'login'), 'admin_default', true) ?>';
            var baseUrl = '<?php echo $this->url(array(), 'default', true) ?>';
            (new Request.JSON({
                url: url,
                data: {
                    format: 'json',
                    id: <?php echo $this->user->getIdentity() ?>
                },
                onSuccess: function () {
                    window.location.replace(baseUrl);
                }
            })).send();
        }
    </script>
<?php endif ?>
<ul class="profile_moderate">
    <li>
        <?php echo $this->htmlLink($this->url(array('key' => 'delete', 'ids' => $this->user->user_id, 'format' => 'smoothbox', 'single' => true), 'moderatortoolbox_multimodify', true), $this->translate('Delete this user'), array('class' => 'smoothbox user_delete buttonlink')) ?>
    </li>
    <?php if ($this->user->approved == 0): ?>
        <li>
            <?php echo $this->htmlLink($this->url(array('key' => 'approve', 'ids' => $this->user->user_id, 'format' => 'smoothbox', 'single' => true), 'moderatortoolbox_multimodify', true), $this->translate('Approve this user'), array('class' => 'smoothbox user_approve buttonlink')) ?>
        </li>
    <?php endif ?>
    <?php if ($this->user->enabled == 1) : ?>
        <li>
            <?php echo $this->htmlLink($this->url(array('key' => 'disable', 'ids' => $this->user->user_id, 'format' => 'smoothbox', 'single' => true), 'moderatortoolbox_multimodify', true), $this->translate('Disable this user'), array('class' => 'smoothbox user_disable buttonlink')) ?>
        </li>
    <?php endif ?>
    <?php if ($this->user->enabled == 0) : ?>
        <li>
            <?php echo $this->htmlLink($this->url(array('key' => 'enable', 'ids' => $this->user->user_id, 'format' => 'smoothbox', 'single' => true), 'moderatortoolbox_multimodify', true), $this->translate('Enable this user'), array('class' => 'smoothbox user_enable buttonlink')) ?>
        </li>
    <?php endif ?>
    <?php if ($this->viewer->isAdmin()) : ?>
        <li>
            <?php echo $this->htmlLink($this->url(array('module' => 'user', 'controller' => 'manage', 'action' => 'login', 'id' => $this->user->user_id), 'admin_default', true), $this->translate('Login as') . ' ' . $this->user->getTitle(), array('class' => 'user_enable buttonlink', 'onclick' => 'loginAsUser(); return false;')) ?>
        </li>
    <?php endif ?>
    <?php if ($this->viewer->isAdmin()) : ?>
        <li>
            <?php echo $this->htmlLink($this->url(array("module" => "moderatortoolbox", "controller" => "index", 'action' => 'ban-period', 'id' => $this->user->user_id), 'default', true), $this->translate('Ban till..'), array('class' => 'smoothbox user_disable buttonlink', 'onclick' => 'return false;')) ?>
        </li>
    <?php endif ?>
</ul>