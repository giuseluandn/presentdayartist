<?php

class Moderatortoolbox_Model_DbTable_Notes extends Engine_Db_Table {

    protected $_rowClass = 'Moderatortoolbox_Model_Note';

    public function hasNote(User_Model_User $user) {
        $select = $this->select()
                ->where('user_id = ?', $user->getIdentity());

        $note = $this->fetchRow($select);

        if ($note) {
            return true;
        }
        return false;
    }
    
    public function getNote(User_Model_User $user) {
        $select = $this->select();
        $select->where('user_id = ?', $user->getIdentity());

        return $this->fetchRow($select);
    }

}