<?php

class Moderatortoolbox_Model_DbTable_Logs extends Engine_Db_Table {

    protected $_rowClass = 'Moderatortoolbox_Model_Log';

    public function getLogPaginator($params = array()) {
        return Zend_Paginator::factory($this->getLogSelect($params));
    }

    public function getLogSelect($params = array()) {
        $table = Engine_Api::_()->getItemTable('moderatortoolbox_log');
        $select = $table->select();

        if (isset($params['user']) && $params['user'] instanceof User_Model_User) {
            $select->where('user_id = ?', $params['user']->getIdentity());
        }
        
        if (isset($params['min_date']) && isset($params['max_date'])) {
            $select->where(new Zend_Db_Expr('`log_time` BETWEEN \'' . $params['min_date'] . '\' AND \'' . $params['max_date'] . '\''));
        } elseif (isset($params['min_date']) && !isset($params['max_date'])) {
            $select->where('log_time >= ?', $params['min_date']);
        } elseif (!isset($params['min_date']) && isset($params['max_date'])) {
            $select->where('log_time <= ?', $params['max_date']);
        }
        
        $select->order('log_id DESC');
        
        return $select;
    }

}