<?php

class Moderatortoolbox_Model_Note extends Core_Model_Item_Abstract {

    protected $_searchTriggers = false;

    public function getHref() {
        $params = array(
            'route' => 'moderatortoolbox_note',
            'reset' => true,
            'id'    => $this->getIdentity(),
        );
        $route = $params['route'];
        $reset = $params['reset'];
        unset($params['route']);
        unset($params['reset']);
        return Zend_Controller_Front::getInstance()->getRouter()
                        ->assemble($params, $route, $reset);
    }

}
