<?php

class Moderatortoolbox_CommentsController extends Core_Controller_Action_Standard {

    public function init() {
        // only show to member_level if authorized
        if (!$this->_helper->requireAuth()->setAuthParams('moderatortoolbox', null, 'moderate')->isValid())
            return;
    }

    public function indexAction() {

        if (Engine_Api::_()->user()->getViewer()){
            $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
            Engine_Api::_()->core()->setSubject($viewer);
        }

        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                   ->getNavigation('moderatortoolbox_main');
        
        
        $this->view->start_date = $start_date = $this->_getParam('start-date', '');
        $this->view->end_date = $end_date = $this->_getParam('end-date', '');
        $this->view->username = $username = $this->_getParam('uname', '');
        $this->view->email = $email = $this->_getParam('email', '');
        

        if ($end_date == '' || $start_date == '') {
            $oldTz = date_default_timezone_get();
            date_default_timezone_set($viewer->timezone);
            $this->view->date = $date = date('d/m/Y');
            date_default_timezone_set($oldTz);

            $year = substr($date, 6, 4);
            $month = substr($date, 3, 2);
            $day = substr($date, 0, 2);

            if ($start_date == '')
                $this->view->start_date = $start_date = date("d/m/Y", mktime(0, 0, 0, $month, $day - 3, $year));

            if ($end_date == '')
                $this->view->end_date = $end_date = $date;
        } else {
            if (!checkdate(substr($start_date, 3, 2), substr($start_date, 0, 2), substr($start_date, 6, 4)) || !checkdate(substr($end_date, 3, 2), substr($end_date, 0, 2), substr($end_date, 6, 4)))
                return;
        }

        $start_date_arr['year'] = substr($start_date, 6, 4);
        $start_date_arr['month'] = substr($start_date, 3, 2);
        $start_date_arr['day'] = substr($start_date, 0, 2);
        $end_date_arr['year'] = substr($end_date, 6, 4);
        $end_date_arr['month'] = substr($end_date, 3, 2);
        $end_date_arr['day'] = substr($end_date, 0, 2);
        
        
        if(!empty($start_date)) $values['start-date'] = $start_date;
        if(!empty($end_date)) $values['end-date'] = $end_date;
        if(!empty($username)) $values['uname'] = $username;
        if(!empty($email)) $values['email'] = $email;
        $this->view->values = $values;

        
        $comTable = Engine_Api::_()->getDbtable('comments', 'activity');
        $union = $comTable->select();
        
        $table = Engine_Api::_()->getDbtable('comments', 'activity');
        $cName = $table->info('name');
        $table2 = Engine_Api::_()->getDbtable('comments', 'core');
        $ccName = $table2->info('name');
        $uName = Engine_Api::_()->getDbtable('users', 'user')->info('name');
        
        $sql1 = $table->select();
            $sql1->setIntegrityCheck(false); 
            $sql1->from($cName,array("comment_id","resource_id",new Zend_Db_Expr('"activity_action" AS resource_type'),"poster_id","body","creation_date"));
            $sql1->joinLeft($uName, "`{$uName}`.`user_id` = `{$cName}`.`poster_id`", array("user_id","username","email"));
            if(!empty($username)){
                $sql1->where("{$uName}.username LIKE ?", '%' . $username . '%');
            }
            if(!empty($email)){
                $sql1->where("{$uName}.email LIKE ?", '%' . $email . '%');
            }
            $sql1->where(new Zend_Db_Expr('(`' . $cName . '`.`creation_date` BETWEEN \'' . $start_date_arr['year'] . '-' . $start_date_arr['month'] . '-' . $start_date_arr['day'] . ' 00:00:00\' AND \'' . $end_date_arr['year'] . '-' . $end_date_arr['month'] . '-' . $end_date_arr['day'] . ' 23:59:59\')'));
        $sql2 = $table2->select();
            $sql2->setIntegrityCheck(false); 
            $sql2->from($ccName,array("comment_id","resource_id","resource_type","poster_id","body","creation_date"));
            $sql2->joinLeft($uName, "`{$uName}`.`user_id` = `{$ccName}`.`poster_id`", array("user_id","username","email"));
            if(!empty($username)){
                $sql2->where("{$uName}.username LIKE ?", '%' . $username . '%');
            }
            if(!empty($email)){
                $sql2->where("{$uName}.email LIKE ?", '%' . $email . '%');
            }
            $sql2->where(new Zend_Db_Expr('(`' . $ccName . '`.`creation_date` BETWEEN \'' . $start_date_arr['year'] . '-' . $start_date_arr['month'] . '-' . $start_date_arr['day'] . ' 00:00:00\' AND \'' . $end_date_arr['year'] . '-' . $end_date_arr['month'] . '-' . $end_date_arr['day'] . ' 23:59:59\')'));
            
        $union->union(array($sql1,$sql2));
        $union->order("creation_date DESC");
        
        $this->view->page = $page = $this->_getParam('page', 1);
        $this->view->paginator = $paginator = Zend_Paginator::factory($union);
        $paginator->setItemCountPerPage(30);
        $paginator->setCurrentPageNumber($page);

    }
    
    public function deleteCommentAction(){
        
        if (!Engine_Api::_()->user()->getViewer())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        
        $resources   = $this->_getParam('res', '');
        $action      = $this->_getParam('act', '');
        $user_id     = (int)$this->_getParam('uid', 0);
        $comment_ids = $this->_getParam('cid', '');
        $action_ids  = $this->_getParam('rid', '');
        
        if( $action == 'disable' ){
            
            $user = Engine_Api::_()->getItem('user',$user_id);
            if ($user->getIdentity() > 0 && !$user->isAdmin()) {
                $user->enabled = 0;
                $user->approved = 0;
                $user->verified = 0;
                $user->save();

                $logTable = Engine_Api::_()->getItemTable('log');
                $log = array('action' => 'disable', 'item_name' => 'user', 'item_value' => $user_id);
                $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
                $db = $logTable->getAdapter();
                $db->beginTransaction();
                try {
                    
                    $logTable->insert($data);

                    $db->commit();
                } catch (Exception $e) {
                    $db->rollBack();
                    throw $e;
                }
            }
            
        }
        
        $comment_ids = explode('|',$comment_ids);
        $action_ids  = explode('|',$action_ids);
        $resources   = explode('|',$resources);
        
        $db = Engine_Api::_()->getDbTable('comments','core')->getAdapter();
        
        $i = 0;
        
        for( $i; $i < count($comment_ids); $i++ ){
            $item = Engine_Api::_()->getItem($resources[$i], $action_ids[$i]);
            $db->beginTransaction();
            try
            {
              $item->comments()->removeComment($comment_ids[$i]);
              $db->commit();
            }
            catch( Exception $e )
            {
              $db->rollBack();
              throw $e;
            }
        }
        
        return true;
        
    }
    
    public function updateCommentAction() {
        
        $text        = $this->_getParam('text', '');
        $resource    = $this->_getParam('resource', '');
        $comment_ids = (int)$this->_getParam('cid', '');
        $action_ids  = (int)$this->_getParam('rid', '');
        
        if($resource == 'activity_action'){
            $table = Engine_Api::_()->getDbTable('comments', 'activity');
        } else {
            $table = Engine_Api::_()->getDbTable('comments', 'core');
        }
        $db = $table->getAdapter();
        $db->beginTransaction();
        try {
            $table->update(array('body' => $text), array("comment_id = ?" => $comment_ids));
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
        
        
    }

}