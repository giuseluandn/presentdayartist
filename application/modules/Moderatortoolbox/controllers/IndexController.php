<?php

class Moderatortoolbox_IndexController extends Core_Controller_Action_Standard {

    public function init() {
        // only show to member_level if authorized
        if (!$this->_helper->requireAuth()->setAuthParams('moderatortoolbox', null, 'moderate')->isValid())
            return;
    }

    public function indexAction() {
        if (Engine_Api::_()->user()->getViewer())
            Engine_Api::_()->core()->setSubject(Engine_Api::_()->user()->getViewer());

        $this->view->viewer = Engine_Api::_()->user()->getViewer();

        $this->view->form = $form = new Moderatortoolbox_Form_Search(array('type' => 'user'));
        $values = array();
        if ($form->isValid($this->_getAllParams())) {
            $values = $form->getValues();
        }

        foreach ($values as $key => $value) {
            if (null === $value || '' == $value) {
                unset($values[$key]);
            }
        }

        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('moderatortoolbox_main');

        $this->view->formValues = $values;

        $users = Engine_Api::_()->getDbTable('users', 'user');
        $select = $users->select();

        if (!empty($values['username'])) {
            $select->where('username LIKE ?', '%' . $values['username'] . '%');
        }

        if (!empty($values['displayname'])) {
            $select->where('displayname LIKE ?', '%' . $values['displayname'] . '%');
        }

        if (!empty($values['email'])) {
            $select->where('email LIKE ?', '%' . $values['email'] . '%');
        }

        if (!empty($values['level_id'])) {
            $select->where('level_id = ?', $values['level_id']);
        }

        if (!empty($values['creation_ip'])) {
            $values['creation_ip'] = new Engine_IP($values['creation_ip']);
            $values['creation_ip'] = $values['creation_ip']->toBinary();

            $select->where('creation_ip = ?', $values['creation_ip']);
        }

        if (!empty($values['lastlogin_ip'])) {
            $values['lastlogin_ip'] = new Engine_IP($values['lastlogin_ip']);
            $values['lastlogin_ip'] = $values['lastlogin_ip']->toBinary();

            $select->where('lastlogin_ip = ?', $values['lastlogin_ip']);
        }

        if (isset($values['enabled']) && $values['enabled'] != '') {
            $select->where('enabled = ?', $values['enabled']);
        }

        if (isset($values['verified']) && $values['verified'] != '') {
            $select->where('verified = ?', $values['verified']);
        }

        if (isset($values['approved']) && $values['approved'] != '') {
            $select->where('approved = ?', $values['approved']);
        }

        if (isset($values['sort']) && $values['sort'] != '') {
            switch ($values['sort']) {
                case 'lastlogin_ip':
                case 'creation_ip':
                case 'username':
                case 'displayname':
                    $select->order($values['sort'] . ' ASC');
                    break;
                case 'creation_date':
                default:
                    $select->order('creation_date DESC');
                    break;
            }
        } else {
            $select->order('creation_date DESC');
        }

        if (isset($values['status_text']) && $values['status_text'] != '') {
            $select->where('status LIKE ?', '%' . $values['status_text'] . '%');
        }

        if (isset($values['profile_type'])) {
            $profiles = Engine_Api::_()->fields()->getMatchingItemIds('user', 1, $values['profile_type']);
            if (!isset($users_ids)) {
                $users_ids = $profiles;
            } else {
                $users_ids = array_merge($users_ids, $profiles);
            }
        }

        if (isset($users_ids) && count($users_ids) > 0) {
            $users_ids = array_unique($users_ids);
            $select->where('user_id IN (?)', $users_ids);
        }

        if (isset($values['per_page']) && $values['per_page'] == 'All') {
            $values['per_page'] = count($users->fetchAll($select));
        }

        $this->view->page = $page = $this->_getParam('page', 1);
        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(isset($values['per_page']) ? $values['per_page'] : 50);
        $paginator->setCurrentPageNumber($page);
    }

    public function multimodifyAction() {
        if (!Engine_Api::_()->user()->getViewer())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();

        $this->view->key = $key = $this->_getParam('key', '');
        $ids = $this->_getParam('ids');
        $this->view->single = $single = $this->_getParam('single');
        if (!$ids) {
            $this->view->empty = true;
            return;
        }

        $ids_array = explode('|', $ids);
        $this->view->count = count($ids_array);

        if (count($ids_array) > 0) {
            if ($this->getRequest()->isPost()) {
                switch ($key) {
                    case 'delete' :
                        foreach ($ids_array as $id) {
                            $user = Engine_Api::_()->getItem('user', (int) $id);
                            if (count($user->toArray()) > 0 && ($user->level_id != 1 || $user->level_id != 2)) {
                                $logTable = Engine_Api::_()->getItemTable('moderatortoolbox_log');
                                $log = array('action' => 'delete', 'item_name' => 'user', 'item_value' => $user->username);
                                $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
                                $db = $logTable->getAdapter();
                                $db->beginTransaction();
                                try {
                                    $user->delete();
                                    $logTable->insert($data);

                                    $db->commit();
                                } catch (Exception $e) {
                                    $db->rollBack();
                                    throw $e;
                                }
                            }
                        }
                        $this->_forward('success', 'utility', 'core', array(
                            'smoothboxClose' => 10,
                            'parentRefresh' => 10,
                            'messages' => array('')
                        ));
                        break;
                    case 'approve' :
                        foreach ($ids_array as $id) {
                            $user = Engine_Api::_()->getItem('user', (int) $id);
                            if (count($user->toArray()) == 0) {
                                $this->_forward('success', 'utility', 'core', array(
                                    'smoothboxClose' => 10,
                                    'messages' => array('')
                                ));
                                return;
                            }

                            $old_status = $user->enabled;
                            $user->enabled = 1;
                            $user->approved = 1;

                            $logTable = Engine_Api::_()->getItemTable('moderatortoolbox_log');
                            $log = array('action' => 'approve', 'item_name' => 'user', 'item_value' => $id);
                            $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
                            $db = $logTable->getAdapter();
                            $db->beginTransaction();
                            try {
                                $user->save();
                                $logTable->insert($data);

                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollBack();
                                throw $e;
                            }

                            // Send a notification that the account was not approved previously
                            if ($old_status == 0) {
                                Engine_Api::_()->getApi('mail', 'core')->sendSystem($user, 'user_account_approved', array(
                                    'host' => $_SERVER['HTTP_HOST'],
                                    'email' => $user->email,
                                    'date' => time(),
                                    'recipient_title' => $user->getTitle(),
                                    'recipient_link' => $user->getHref(),
                                    'recipient_photo' => $user->getPhotoUrl('thumb.icon'),
                                    'object_link' => 'http://'
                                    . $_SERVER['HTTP_HOST']
                                    . Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'user_login', true),
                                ));
                            }
                        }
                        $this->_forward('success', 'utility', 'core', array(
                            'smoothboxClose' => 10,
                            'parentRefresh' => 10,
                            'messages' => array('')
                        ));
                        break;
                    case 'disable' :
                        foreach ($ids_array as $id) {
                            $user = Engine_Api::_()->getItem('user', (int) $id);
                            if ($user->level_id != 1 || $user->level_id != 2) {
                                if (count($user->toArray()) == 0) {
                                    $this->_forward('success', 'utility', 'core', array(
                                        'smoothboxClose' => 10,
                                        'messages' => array('')
                                    ));
                                    return;
                                }
                                $user->enabled = 0;
                                $user->approved = 0;

                                $logTable = Engine_Api::_()->getItemTable('moderatortoolbox_log');
                                $log = array('action' => 'disable', 'item_name' => 'user', 'item_value' => $id);
                                $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
                                $db = $logTable->getAdapter();
                                $db->beginTransaction();
                                try {
                                    $user->save();
                                    $logTable->insert($data);
                                    $db->commit();
                                } catch (Exception $e) {
                                    $db->rollBack();
                                    throw $e;
                                }
                            }
                        }
                        $this->_forward('success', 'utility', 'core', array(
                            'smoothboxClose' => 10,
                            'parentRefresh' => 10,
                            'messages' => array('')
                        ));
                        break;
                    case 'enable' :
                        foreach ($ids_array as $id) {
                            $user = Engine_Api::_()->getItem('user', (int) $id);
                            if (count($user->toArray()) == 0) {
                                $this->_forward('success', 'utility', 'core', array(
                                    'smoothboxClose' => 10,
                                    'messages' => array('')
                                ));
                                return;
                            }
                            $user->enabled = 1;
                            $user->approved = 1;

                            $logTable = Engine_Api::_()->getItemTable('moderatortoolbox_log');
                            $log = array('action' => 'enable', 'item_name' => 'user', 'item_value' => $id);
                            $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
                            $db = $logTable->getAdapter();
                            $db->beginTransaction();
                            try {
                                $user->save();
                                $logTable->insert($data);

                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollBack();
                                throw $e;
                            }
                        }
                        $this->_forward('success', 'utility', 'core', array(
                            'smoothboxClose' => 10,
                            'parentRefresh' => 10,
                            'messages' => array('')
                        ));
                        break;
                }
            }
        }
    }

    public function messagesAction() {
        $user_id = (int) $this->_getParam('id');
        $this->view->user = $user = Engine_Api::_()->user()->getUser($user_id);
        $this->view->isAdmin = false;
        if ($user->level_id == 1 || $user->level_id == 2) {
            $this->view->isAdmin = true;
        } else {
            $messagesSelect = Engine_Api::_()->getItemTable('messages_conversation')->getOutboxSelect($user);
            $messagesSelect->limit(5);

            $messagesTable = Engine_Api::_()->getDbTable('conversations', 'messages');
            $this->view->messages = $messages = $messagesTable->fetchAll($messagesSelect);
        }
    }

    public function noteAction() {
        if (!Engine_Api::_()->user()->getViewer())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        $logTable = Engine_Api::_()->getItemTable('moderatortoolbox_log');

        $id = (int) $this->_getParam('id', 0);
        $uid = (int) $this->_getParam('uid', 0);
        $tab = $this->_getParam('tab');

        $this->view->form = $form = new Moderatortoolbox_Form_Note();
        if ($id > 0) {
            $note = Engine_Api::_()->getItem('moderatortoolbox_note', $id);
            if ($note) {
                $form->populate($note->toArray());
                $user_id = $note->user_id;
            }
        }

        // If not post or form not valid, return
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        // Process
        $values = $form->getValues();
        if ($id > 0) {
            if ($values['note'] != '') {
                $log = array('action' => 'change', 'item_name' => 'note', 'item_value' => $note->user_id);
                $note->note = $values['note'];
                $note->save();
            } else {
                $log = array('action' => 'delete', 'item_name' => 'note', 'item_value' => $note->user_id);
                $noteTable = Engine_Api::_()->getItemTable('moderatortoolbox_note');
                $db = $noteTable->getAdapter();
                $db->beginTransaction();
                try {
                    $note->delete();
                    $db->commit();
                } catch (Exeption $e) {
                    $db->rollBack();
                    throw $e;
                }
            }

            $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
            $db = $logTable->getAdapter();
            $db->beginTransaction();
            try {
                $logTable->insert($data);

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }

            $this->view->message = Zend_Registry::get('Zend_Translate')->_('Note has been changed.');
            if ($tab == 'moderatortoolbox.profile-info') {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRedirect' => Engine_Api::_()->user()->getUser($user_id)->getHref() . '/tab/moderatortoolbox.profile-info',
                            'messages' => Array($this->view->message)
                ));
            } else {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRefresh' => true,
                            'messages' => Array($this->view->message)
                ));
            }
        } elseif ($id == 0 && $uid > 0) {
            $noteTable = Engine_Api::_()->getItemTable('moderatortoolbox_note');
            $db = $noteTable->getAdapter();
            $db->beginTransaction();
            try {
                $noteTable->insert(array('user_id' => $uid, 'note' => $values['note']));
                $db->commit();
            } catch (Exeption $e) {
                $db->rollBack();
                throw $e;
            }

            $log = array('action' => 'create', 'item_name' => 'note', 'item_value' => $uid);
            $data = array('user_id' => $viewer->getIdentity(), 'log' => serialize($log));
            $db = $logTable->getAdapter();
            $db->beginTransaction();
            try {
                $logTable->insert($data);

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                throw $e;
            }

            $this->view->message = Zend_Registry::get('Zend_Translate')->_('Note has been added.');
            if ($tab == 'moderatortoolbox.profile-info') {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRedirect' => Engine_Api::_()->user()->getUser($uid)->getHref() . '/tab/moderatortoolbox.profile-info',
                            'messages' => Array($this->view->message)
                ));
            } else {
                return $this->_forward('success', 'utility', 'core', array(
                            'parentRefresh' => true,
                            'messages' => Array($this->view->message)
                ));
            }
        }
    }

    public function messagesActivityAction() {

        if (Engine_Api::_()->user()->getViewer())
            Engine_Api::_()->core()->setSubject(Engine_Api::_()->user()->getViewer());

        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('moderatortoolbox_main');

        $this->view->start_date = $start_date = $this->_getParam('start-date', '');
        $this->view->end_date = $end_date = $this->_getParam('end-date', '');
        $this->view->mcount = $mcount = (int) $this->_getParam('mcount', 0);

        if ($end_date == '' || $start_date == '') {
            $viewer = Engine_Api::_()->user()->getViewer();
            $oldTz = date_default_timezone_get();
            date_default_timezone_set($viewer->timezone);
            $this->view->date = $date = date('d/m/Y');
            date_default_timezone_set($oldTz);

            $year = substr($date, 6, 4);
            $month = substr($date, 3, 2);
            $day = substr($date, 0, 2);

            if ($start_date == '')
                $this->view->start_date = $start_date = date("d/m/Y", mktime(0, 0, 0, $month, $day - 3, $year));

            if ($end_date == '')
                $this->view->end_date = $end_date = $date;
        } else {
            if (!checkdate(substr($start_date, 3, 2), substr($start_date, 0, 2), substr($start_date, 6, 4)) || !checkdate(substr($end_date, 3, 2), substr($end_date, 0, 2), substr($end_date, 6, 4)))
                return;
        }

        $start_date_arr['year'] = substr($start_date, 6, 4);
        $start_date_arr['month'] = substr($start_date, 3, 2);
        $start_date_arr['day'] = substr($start_date, 0, 2);
        $end_date_arr['year'] = substr($end_date, 6, 4);
        $end_date_arr['month'] = substr($end_date, 3, 2);
        $end_date_arr['day'] = substr($end_date, 0, 2);

        $usersTable = Engine_Api::_()->getDbTable('users', 'user');
        $uName = $usersTable->info('name');

        $cName = Engine_Api::_()->getDbtable('conversations', 'messages')->info('name');
        $mName = Engine_Api::_()->getDbtable('messages', 'messages')->info('name');
        $rName = Engine_Api::_()->getDbtable('recipients', 'messages')->info('name');

        $select = $usersTable->select()
                ->setIntegrityCheck(false)
                ->from($uName, array('*'))
                ->joinLeft($rName, "`{$rName}`.`user_id` = `{$uName}`.`user_id`", null)
                ->joinLeft($mName, "`{$mName}`.`conversation_id` = `{$rName}`.`conversation_id`", array("count(`{$mName}`.`user_id`) as cnt"))
                ->joinLeft($cName, "`{$rName}`.`conversation_id` = `{$cName}`.`conversation_id`", null)
                ->where($rName . '.outbox_message_id = ' . $mName . '.message_id')
                ->where($rName . '.outbox_deleted = ?', 0)
                ->where(new Zend_Db_Expr('(`' . $mName . '`.`date` BETWEEN \'' . $start_date_arr['year'] . '-' . $start_date_arr['month'] . '-' . $start_date_arr['day'] . ' 00:00:00\' AND \'' . $end_date_arr['year'] . '-' . $end_date_arr['month'] . '-' . $end_date_arr['day'] . ' 23:59:59\')'))
                ->having('cnt > ?', $mcount)
                ->group($rName . '.user_id')
                ->order('count(' . $mName . '.user_id) DESC');

        $this->view->page = $page = $this->_getParam('page', 1);
        $this->view->paginator = $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(50);
        $paginator->setCurrentPageNumber($page);
    }

    public function banPeriodAction() {

        $viewer = Engine_Api::_()->user()->getViewer();

        $id = $this->_getParam('id', NULL);
        $user = Engine_Api::_()->getItem('user', (int) $id);

        $this->view->form = $form = new Moderatortoolbox_Form_Ban();

        $table = Engine_Api::_()->getDbTable('ban', 'moderatortoolbox');

        $select = $table->select();
        $select->from($table->info('name'), array('*'));
        $select->where('user_id = ?', $id);
        $result = $table->fetchAll($select);

        if (count($result) > 0) {
            $result = $result->toArray();
            $DateTime = DateTime::createFromFormat('Y-m-d', $result[0]['date']);
            $date = $DateTime->format('m/d/Y');
        } else {
            $date = date('m/d/Y', strtotime('+1 day'));
        }

        $form->populate(array('unban_date' => array('date' => $date, 'hour' => 0, 'minute' => 0)));


        if (!$this->getRequest()->isPost()) {
            return;
        }

        $bandate = $this->_getParam('unban_date');

        $DateTime = DateTime::createFromFormat('m/d/Y', $bandate['date']);
        $end_date = $DateTime->format('Y-m-d');

        if ($end_date > date('Y-m-d')) {

            $db = $table->getAdapter();
            $db->beginTransaction;

            try {

                Engine_Api::_()->getApi('mail', 'core')->sendSystem($user, 'moderatortoolbox_ban', array(
                    'host' => $_SERVER['HTTP_HOST'],
                    'email' => $user->email,
                    'ban_date' => $bandate['date']
                ));

                $user->approved = 0;
                $user->verified = 0;
                $user->enabled = 0;
                $user->save();

                if (count($result) == 0) {
                    $table->insert(array(
                        'user_id' => $id,
                        'date' => $end_date,
                    ));
                } else {
                    $table->update(array(
                        'date' => $end_date,
                            ), array('user_id = ?' => $id));
                }

                Zend_Registry::get('Zend_Log')->log('User with id: ' . $viewer->getIdentity() . ' has banned user with id: ' . $id, Zend_Log::INFO);

                $db->commit();
            } catch (Exception $ex) {
                $db->rollBack();
                throw($ex);
            }

            return $this->_forward('success', 'utility', 'core', array(
                        'parentRefresh' => true,
                        'messages' => Array('User banned successfully')
            ));
        } else {


            Engine_Api::_()->getApi('mail', 'core')->sendSystem($user, 'moderatortoolbox_unban', array(
                'host' => $_SERVER['HTTP_HOST'],
                'email' => $user->email
            ));

            $user->approved = 1;
            $user->verified = 1;
            $user->enabled = 1;
            $user->save();


            $db = $table->getAdapter();
            $db->beginTransaction;

            try {

                $table->delete(array('user_id = ?' => $id));

                Zend_Registry::get('Zend_Log')->log('User with id: ' . $viewer->getIdentity() . ' has approved user with id: ' . $id, Zend_Log::INFO);

                $db->commit();
            } catch (Exception $ex) {
                $db->rollBack();
                throw($ex);
            }

            return $this->_forward('success', 'utility', 'core', array(
                        'parentRefresh' => true,
                        'messages' => Array('User approved successfully')
            ));
        }
    }

}
