<?php

class Moderatortoolbox_LogController extends Core_Controller_Action_Standard {

    public function init() {
        // only show to member_level if authorized
        if (!$this->_helper->requireAuth()->setAuthParams('moderatortoolbox', null, 'view_logs')->isValid())
            return;
    }

    public function indexAction() {

        $viewer = Engine_Api::_()->user()->getViewer();
        Engine_Api::_()->core()->setSubject($viewer);

        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('moderatortoolbox_main');

        $this->view->form = $form = new Moderatortoolbox_Form_Logs_Filter();

        $now = strtotime('now');
        $min_date = date('Y-m-d', $now - 604800);
        $max_date = date('Y-m-d', $now);


        if ($form->isValid($this->getRequest()->getParams())) {
            $values = $form->getValues();
        }


        if (isset($values)) {
            foreach ($values as $key => $value) {
                if (null === $value || '' == $value) {
                    unset($values[$key]);
                }
            }
            $this->view->formValues = $values;
        }

        if (isset($values['moderator'])) {
            $user = Engine_Api::_()->user()->getUser($values['moderator']);
            $filterValues['user'] = $user;
        }

        if (isset($values['min_date'])) {
            $filterValues['min_date'] = $values['min_date'] . ' 00:00:00';
        } else {
            $filterValues['min_date'] = $min_date . ' 00:00:00';
            $values['min_date'] = $min_date;
        }

        if (isset($values['max_date'])) {
            $filterValues['max_date'] = $values['max_date'] . ' 23:59:59';
        } else {
            $filterValues['max_date'] = $max_date . ' 23:59:59';
            $values['max_date'] = $max_date;
        }
        
        if (isset($values['per_page'])) {
            $per_page = $values['per_page'];
        } else {
            $per_page = 50;
        }

        $form->populate($values);
        //var_dump($values);

        $this->view->paginator = $paginator = Engine_Api::_()->getItemTable('moderatortoolbox_log')
                ->getLogPaginator($filterValues);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $paginator->setItemCountPerPage($per_page);
    }

}