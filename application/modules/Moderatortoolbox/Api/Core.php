<?php

class Moderatortoolbox_Api_Core extends Core_Api_Abstract {

    public function getCountSentMessages(Core_Model_Item_Abstract $user) {
        $identity = $user->getIdentity();

        $rName = Engine_Api::_()->getDbtable('recipients', 'messages')->info('name');
        $cName = Engine_Api::_()->getDbtable('conversations', 'messages')->info('name');
        $mName = Engine_Api::_()->getDbtable('messages', 'messages')->info('name');
        $table = Engine_Api::_()->getDbtable('messages', 'messages');

        $select = Engine_Api::_()->getDbtable('recipients', 'messages')->select()
                ->setIntegrityCheck(false)
                ->from($rName, array('COUNT(*) as count'))
                ->joinLeft($mName, $rName . '.conversation_id = ' . $mName . '.conversation_id', array())
                ->joinLeft($cName, $rName . '.conversation_id = ' . $cName . '.conversation_id', array())
                ->where($rName . '.user_id = ?', $identity)
                ->where($rName . '.outbox_message_id = ' . $mName . '.message_id')
                ->where($rName . '.outbox_deleted = ?', 0);
        return $table->fetchRow($select);
    }

}