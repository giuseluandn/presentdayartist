-- --------------------------------------------------------

--
-- Dumping data for table `engine4_authorization_modules`
--

INSERT IGNORE INTO `engine4_core_modules` (`name`, `title`, `description`, `version`, `enabled`, `type`) VALUES  ('moderatortoolbox', 'Moderator Toolbox', '', '4.2.2p1', 1, 'extra') ;

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_authorization_permissions`
--

-- ADMIN
-- moderate
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'moderatortoolbox' as `type`,
    'moderate' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN ('admin');
-- Others
-- moderate
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'moderatortoolbox' as `type`,
    'moderate' as `name`,
    0 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` NOT IN ('admin');

-- ADMIN
-- log view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'moderatortoolbox' as `type`,
    'view_logs' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN ('admin');
-- Others
-- log view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'moderatortoolbox' as `type`,
    'view_logs' as `name`,
    0 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` NOT IN ('admin');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_menus`
--

INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`) VALUES
('moderatortoolbox_main', 'standard', 'Moderation Main Navigation Menu');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_authorization_menuitems`
--

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('core_admin_main_plugins_moderatortoolbox', 'moderatortoolbox', 'Moderator Toolbox', '', '{"route":"admin_default","module":"moderatortoolbox","controller":"level"}', 'core_admin_main_plugins', '', 999),
('core_mini_moderatortoolbox', 'moderatortoolbox', 'Moderation', 'Moderatortoolbox_Plugin_Menus', '', 'core_mini', '', 999),
('moderate_users', 'moderatortoolbox', 'Users', '', '{"route":"moderatortoolbox_home"}', 'moderatortoolbox_main', '', 1),
('moderate_activity_messages', 'moderatortoolbox', 'Messages Activity', '', '{"route":"moderatortoolbox_activity_messages","module":"moderatortoolbox","controller":"index","action":"messages-activity"}', 'moderatortoolbox_main', '', 2),
('moderate_reports', 'moderatortoolbox', 'Reports', '', '{"route":"moderatortoolbox_reports"}', 'moderatortoolbox_main', '', 3),
('moderate_logs', 'moderatortoolbox', 'Log', 'Moderatortoolbox_Plugin_Menus', '', 'moderatortoolbox_main', '', 4),
('moderate_comments', 'moderatortoolbox', 'Comments', '', '{"route":"moderatortoolbox_comments"}', 'moderatortoolbox_main', '', 999);
-- --------------------------------------------------------

--
-- Creating table `engine4_moderatortoolbox_notes`
--

DROP TABLE IF EXISTS `engine4_moderatortoolbox_notes`;
CREATE TABLE  `engine4_moderatortoolbox_notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Creating table `engine4_moderatortoolbox_logs`
--

DROP TABLE IF EXISTS `engine4_moderatortoolbox_logs`;
CREATE TABLE  `engine4_moderatortoolbox_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `log` text NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Creating table `engine4_moderatortoolbox_ban`
--

DROP TABLE IF EXISTS `engine4_moderatortoolbox_ban`;
CREATE TABLE  `engine4_moderatortoolbox_ban` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `engine4_core_tasks` (`title`, `module`, `plugin`, `timeout`) VALUES
('Unban Users', 'moderatortoolbox', 'Moderatortoolbox_Plugin_Task_Ban', 43200);