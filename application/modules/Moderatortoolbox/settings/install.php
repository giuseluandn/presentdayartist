<?php

class Moderatortoolbox_Installer extends Engine_Package_Installer_Module {

    function onInstall() {

        $db = $this->getDb();
        $select = new Zend_Db_Select($db);

        //page_id
        $select
                ->from('engine4_core_pages')
                ->where('name = ?', 'user_profile_index')
                ->limit(1);
        $page_id = $select->query()->fetchObject()->page_id;
        //check
        $select = new Zend_Db_Select($db);
        $select
                ->from('engine4_core_content')
                ->where('page_id = ?', $page_id)
                ->where('type = ?', 'widget')
                ->where('name = ?', 'moderatortoolbox.profile-moderation')
        ;
        $info = $select->query()->fetch();

        if (empty($info)) {

            // container_id (will always be there)
            $select = new Zend_Db_Select($db);
            $select
                    ->from('engine4_core_content')
                    ->where('page_id = ?', $page_id)
                    ->where('type = ?', 'container')
                    ->where('name = ?', 'left')
                    ->limit(1);
            $container_id = $select->query()->fetchObject()->content_id;

            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'widget',
                'name' => 'moderatortoolbox.profile-moderation',
                'parent_content_id' => $container_id,
                'order' => 6,
                'params' => '{"title":"Moderation"}',
            ));
        }

        unset($info, $container_id);
        //check
        $select = new Zend_Db_Select($db);
        $select
                ->from('engine4_core_content')
                ->where('page_id = ?', $page_id)
                ->where('type = ?', 'widget')
                ->where('name = ?', 'moderatortoolbox.profile-info')
        ;
        $info = $select->query()->fetch();
        
        if (empty($info)) {
            // container_id (will always be there)
            $select = new Zend_Db_Select($db);
            $select
                    ->from('engine4_core_content')
                    ->where('page_id = ?', $page_id)
                    ->where('type = ?', 'container')
                    ->limit(1);
            $container_id = $select->query()->fetchObject()->content_id;

            // middle_id (will always be there)
            $select = new Zend_Db_Select($db);
            $select
                    ->from('engine4_core_content')
                    ->where('parent_content_id = ?', $container_id)
                    ->where('type = ?', 'container')
                    ->where('name = ?', 'middle')
                    ->limit(1);
            $middle_id = $select->query()->fetchObject()->content_id;

            // tab_id (tab container) may not always be there
            $select
                    ->reset('where')
                    ->where('type = ?', 'widget')
                    ->where('name = ?', 'core.container-tabs')
                    ->where('page_id = ?', $page_id)
                    ->limit(1);
            $tab_id = $select->query()->fetchObject();
            if ($tab_id && @$tab_id->content_id) {
                $tab_id = $tab_id->content_id;
            } else {
                $tab_id = null;
            }

            // tab on profile
            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'widget',
                'name' => 'moderatortoolbox.profile-info',
                'parent_content_id' => ($tab_id ? $tab_id : $middle_id),
                'order' => 999,
                'params' => '{"title":"Moderation"}',
            ));
        }
        parent::onInstall();
    }

}

?>