UPDATE `engine4_core_menuitems` SET `order` = 3 WHERE `name` = 'moderate_reports';
UPDATE `engine4_core_menuitems` SET `order` = 4 WHERE `name` = 'moderate_logs';

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('moderate_activity_messages', 'moderatortoolbox', 'Messages Activity', '', '{"route":"moderatortoolbox_activity_messages","module":"moderatortoolbox","controller":"index","action":"messages-activity"}', 'moderatortoolbox_main', '', 2);