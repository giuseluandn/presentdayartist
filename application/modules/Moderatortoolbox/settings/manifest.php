<?php

return array(
    'package' =>
    array(
        'type' => 'module',
        'name' => 'moderatortoolbox',
        'version' => '4.8.9p2',
        'path' => 'application/modules/Moderatortoolbox',
        'title' => 'Moderator Toolbox',
        'description' => 'Moderator Toolbox Plugin',
        'author' => 'WebHive Team',
        'meta' => array(
            'title' => 'Moderation',
            'description' => 'Moderator Toolbox Plugin',
            'author' => 'WebHive Team',
        ),
        'callback' =>
        array(
            'path' => 'application/modules/Moderatortoolbox/settings/install.php',
            'class' => 'Moderatortoolbox_Installer',
        ),
        'actions' =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'directories' =>
        array(
            0 => 'application/modules/Moderatortoolbox',
        ),
        'files' =>
        array(
            0 => 'application/languages/en/moderatortoolbox.csv',
        ),
    ),
    //ITEMS
    'items' => array(
        'moderatortoolbox_note',
        'moderatortoolbox_log',
    ),
    //ROUTES
    'routes' =>
    array(
        // Public
        'moderatortoolbox_home' => array(
            'route' => 'moderate/users/:action/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'index',
                'action' => 'index'
            ),
            'reqs' => array(
                'action' => '(index|ban-period)'
            )
        ),
        'moderatortoolbox_multimodify' => array(
            'route' => 'moderate/users/multimodify/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'index',
                'action' => 'multimodify',
            ),
        ),
        'moderatortoolbox_messages' => array(
            'route' => 'moderate/users/messages/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'index',
                'action' => 'messages',
            ),
        ),
        'moderatortoolbox_logs' => array(
            'route' => 'moderate/logs/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'log',
                'action' => 'index',
            ),
        ),
        'moderatortoolbox_activity_messages' => array(
            'route' => 'moderate/activity/messages/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'index',
                'action' => 'messages-activity',
            ),
        ),
        'moderatortoolbox_comments' => array(
            'route' => 'moderate/comments/:action/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'comments',
                'action' => 'index',
            ),
            'reqs' => array(
                'action' => '(delete-comment|update-comment|index)'
            )
        ),
        'moderatortoolbox_reports' => array(
            'route' => 'moderate/reports/:action/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'report',
                'action' => 'index',
            ),
            'reqs' => array(
                'action' => '\D+',
            )
        ),
        'moderatortoolbox_note' => array(
            'route' => 'moderate/users/note/*',
            'defaults' => array(
                'module' => 'moderatortoolbox',
                'controller' => 'index',
                'action' => 'note',
            ),
            'reqs' => array(
                'id' => '\d+',
                'uid' => '\d+'
            ),
        ),
    ),
);
?>
