DROP TABLE IF EXISTS `engine4_moderatortoolbox_ban`;
CREATE TABLE  `engine4_moderatortoolbox_ban` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `engine4_core_tasks` (`title`, `module`, `plugin`, `timeout`) VALUES
('Unban Users', 'moderatortoolbox', 'Moderatortoolbox_Plugin_Task_Ban', 43200);