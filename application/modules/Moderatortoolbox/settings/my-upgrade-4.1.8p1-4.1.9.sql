-- --------------------------------------------------------

--
-- Creating table `engine4_moderatortoolbox_notes`
--

DROP TABLE IF EXISTS `engine4_moderatortoolbox_notes`;
CREATE TABLE  `engine4_moderatortoolbox_notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Creating table `engine4_moderatortoolbox_logs`
--

DROP TABLE IF EXISTS `engine4_moderatortoolbox_logs`;
CREATE TABLE  `engine4_moderatortoolbox_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `log` text NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_authorization_permissions`
--

-- ADMIN
-- log view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'moderatortoolbox' as `type`,
    'view_logs' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN ('admin');
-- Others
-- log view
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'moderatortoolbox' as `type`,
    'view_logs' as `name`,
    0 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` NOT IN ('admin');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_menus`
--

INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`) VALUES
('moderatortoolbox_main', 'standard', 'Moderation Main Navigation Menu');

-- --------------------------------------------------------

--
-- Dumping data for table `engine4_core_menuitems`
--

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('moderate_users', 'moderatortoolbox', 'Users', '', '{"route":"moderatortoolbox_home"}', 'moderatortoolbox_main', '', 1),
('moderate_reports', 'moderatortoolbox', 'Reports', '', '{"route":"moderatortoolbox_reports"}', 'moderatortoolbox_main', '', 2),
('moderate_logs', 'moderatortoolbox', 'Log', 'Moderatortoolbox_Plugin_Menus', '', 'moderatortoolbox_main', '', 3);