<?php

return array(
    array(
        'title' => 'Profile Moderation links',
        'description' => 'Displays Moderate Buttons.',
        'category' => 'Moderator Toollbox',
        'type' => 'widget',
        'name' => 'moderatortoolbox.profile-moderation',
        'defaultParams' => array(
            'title' => 'Moderation',
        ),
        'requirements' => array(
            'subject' => 'user',
        ),
    ),
    array(
        'title' => 'Profile Moderation Info',
        'description' => 'Displays Members registered or signed up from the same IP.',
        'category' => 'Moderator Toollbox',
        'type' => 'widget',
        'name' => 'moderatortoolbox.profile-info',
        'defaultParams' => array(
            'title' => 'Moderation',
        ),
        'requirements' => array(
            'subject' => 'user',
        ),
    ),
        )
?>