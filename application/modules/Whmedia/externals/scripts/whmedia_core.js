var whpageAction = function(page) {
    $('page').value = page;
    $('filter_form').submit();
}

var whmedia = new Class({
    Implements: [Options],
    options: {
        lang: null,
        module: 'whmedia',
        controller: 'index'
    },
    initialize: function(a) {
        this.setOptions(a);
    },
    run_JSON: function(action, data, SuccessFunction) {
        new Request.JSON({
            'url': this.getURL(action),
            'data': $extend(data, {'isajax': true,
                'format': 'json'}),
            'onSuccess': function(responseObject) {
                if ($type(responseObject) != "object") {
                    alert('ERROR occurred. Please try againe.');
                    return false;
                }
                if (!responseObject.status || responseObject.status != true) {
                    if (responseObject.reload == true)
                        window.location.reload(true);
                    if (responseObject.error && $type(responseObject.error) == 'string') {
                        alert(responseObject.error);
                    }
                    return false;
                }
                if (responseObject.status == true) {
                    delete responseObject.status;
                    if (typeOf(SuccessFunction) == 'function')
                        SuccessFunction(responseObject);
                    return false;
                }
            }.bind(this)
        }).send();
    },
    getURL: function(action) {
        return en4.core.baseUrl + this.options.module + '/' + this.options.controller + '/' + action;
    },
    version_compare: function(v1, v2, operator) {
        var i = 0,
                x = 0,
                compare = 0,
                vm = {
                    'dev': -6,
                    'alpha': -5,
                    'a': -5,
                    'beta': -4,
                    'b': -4,
                    'RC': -3,
                    'rc': -3,
                    '#': -2,
                    'p': 1,
                    'pl': 1
                },
        prepVersion = function(v) {
            v = ('' + v).replace(/[_\-+]/g, '.');
            v = v.replace(/([^.\d]+)/g, '.$1.').replace(/\.{2,}/g, '.');
            return (!v.length ? [-8] : v.split('.'));
        },
                numVersion = function(v) {
                    return !v ? 0 : (isNaN(v) ? vm[v] || -7 : parseInt(v, 10));
                };
        v1 = prepVersion(v1);
        v2 = prepVersion(v2);
        x = Math.max(v1.length, v2.length);
        for (i = 0; i < x; i++) {
            if (v1[i] == v2[i]) {
                continue;
            }
            v1[i] = numVersion(v1[i]);
            v2[i] = numVersion(v2[i]);
            if (v1[i] < v2[i]) {
                compare = -1;
                break;
            } else if (v1[i] > v2[i]) {
                compare = 1;
                break;
            }
        }
        if (!operator) {
            return compare;
        }

        switch (operator) {
            case '>':
            case 'gt':
                return (compare > 0);
            case '>=':
            case 'ge':
                return (compare >= 0);
            case '<=':
            case 'le':
                return (compare <= 0);
            case '==':
            case '=':
            case 'eq':
                return (compare === 0);
            case '<>':
            case '!=':
            case 'ne':
                return (compare !== 0);
            case '':
            case '<':
            case 'lt':
                return (compare < 0);
            default:
                return null;
        }
    },
    mceExec: function(command, ui, value) {
        if (this.version_compare(en4.version, '4.7.0', '>=')) {
            if (command === 'mceAddControl') {
                command = 'mceAddEditor';
            }

            if (command === 'mceRemoveControl') {
                command = 'mceRemoveEditor';
            }
        }
        return tinyMCE.execCommand(command, ui, value);
    }
});
/**
 * Search
 */
whmedia.search = new Class({
    Extends: whmedia,
    form: null,
    text: null,
    initialize: function(options) {
        this.parent(options);
        this.form = new Element('form', {
            id: 'filter_form',
            action: this.getURL('index'),
            method: 'post',
            target: '_parent'
        });
        this.text = new Element('input', {type: 'text'}).inject(this.form);
    },
    tagAction: function(tag) {
        this.text.set('name', 'tags').set('value', tag);
        this.send();
    },
    categoryAction: function(category_id) {
        this.text.set('name', 'category').set('value', category_id);
        this.send();
    },
    send: function() {
        document.body.appendChild(this.form);
        this.form.submit();
    }
});

/**
 * Project
 */
whmedia.project = new Class({
    Extends: whmedia,
    options: {
        project_id: null,
        controller: 'project'
    },
    initialize: function(options) {
        this.parent(options);
    },
    edit_media_title: function(media_id) {
        var title = $('mediatitle_' + media_id).get('value');
        new Request.JSON({
            'url': this.getURL('editmediatitle'),
            'data': {
                'media_id': media_id,
                'project_id': this.options.project_id,
                'mediatitle': title,
                'isajax': true
            },
            'onSuccess': function(responseJSON) {
                if (responseJSON.status == true) {
                    $('span_save_result').set('text', title);
                    $('save_result').setStyle('display');
                    return false;
                }
                else {
                    alert(responseJSON.error);
                }
            }.bind(this)
        }).send();
    },
    set_cover: function(media_id) {
        new Request.JSON({
            'url': this.getURL('setcover'),
            'data': {
                'media_id': media_id,
                'project_id': this.options.project_id,
                'isajax': true
            },
            'onSuccess': function(responseJSON) {
                if (responseJSON.status == true) {
                    var cover = $('project_cover');
                    if (cover != null) {
                        var make = cover.getParent('li.media_sortable');
                        make.getElement('li.cover_it').erase('style');
                        cover.dispose();
                    }
                    var media = $('media_li_' + media_id);
                    new Element('div', {
                        text: 'Cover',
                        id: 'project_cover'
                    }).inject(media);
                    media.getElement('li.cover_it').setStyle('display', 'none');
                    return false;
                }
                else {
                    alert(responseJSON.error);
                }
            }
        }).send();
    },
    reorder: function(e) {
        var steps = e.parentNode.childNodes;
        var ordering = {};
        var i = 1;
        for (var step in steps)
        {
            var child_id = steps[step].id;
            if ((child_id != undefined) && (child_id.substr(0, 9) == 'media_li_'))
            {
                ordering[child_id] = i;
                i++;
            }
        }
        new Request.JSON({
            'url': this.getURL('order'),
            'data': {
                'order_data': ordering,
                'project_id': this.options.project_id,
                'isajax': true
            },
            onSuccess: function(responseJSON) {
                if (responseJSON.status == true) {
                    return true;
                }
                else {
                    alert(responseJSON.error);
                }
            }
        }).send();
    },
    videoURL_preview: function(input) {
        var url = input.get('value').replace(/(^\s+)|(\s+$)/g, "");
        if (url == '')
            return;
        input.set('value', '');
        Smoothbox.open(null, {mode: 'Request',
            url: en4.core.baseUrl + this.options.module + '/project/' + this.options.project_id + '/videourlpreview',
            requestMethod: 'post',
            requestData: {
                url: url
            },
            onLoad: function() {
                var th_img = $('wh_video_thumb');
                if (th_img != null) {
                    Asset.image(th_img.get('osrc'), {
                        id: 'wh_video_thumb',
                        title: 'Thumb Loading',
                        alt: 'Thumb Loading',
                        onLoad: function(img) {
                            $('wh_thumb_loading').dispose();
                            img.replaces(th_img);
                            Smoothbox.instance.doAutoResize($('global_content_simple'));
                        }
                    });
                }
            }
        });
    },
    add_video_service: function(type, code) {
        $('buttons_video').setStyle('display', 'none');
        $('saving_video').setStyle('display', 'block');
        var bind = this;
        new Request.JSON({
            'url': this.getURL('videourladd'),
            'data': {
                'type': type,
                'code': code,
                'title': $('video_title').get('value'),
                'project_id': this.options.project_id,
                'isajax': true
            },
            'onSuccess': function(responseObject) {
                if ($type(responseObject) != "object") {
                    alert('ERROR occurred. Please try againe.');
                    return;
                }
                if (!responseObject.status || responseObject.status != true) {
                    if (responseObject.error && $type(responseObject.error) == 'string') {
                        new Element('span', {'style': 'color:red;',
                            text: 'Error: ' + responseObject.error
                        }).replaces('saving_video');
                    }
                    return;
                }
                Smoothbox.close();
                uploadCount += 1;
                var count_max_files = $('count_max_files');
                if (count_max_files != null)
                    count_max_files.set('text', count_max_files.get('text') - 1);
                var vs = new Element('div', {id: 'vs_' + responseObject.media_id});
                var img_type_src = '';
                if (type == 'youtube')
                    img_type_src = 'youtube_icon.png';
                if (type == 'vimeo')
                    img_type_src = 'vimeo_icon.png';
                new Element('img', {alt: 'Site Thumb',
                    src: en4.core.baseUrl + 'application/modules/Whmedia/externals/images/' + img_type_src
                }).inject(vs);
                new Element('span', {text: responseObject.title}).inject(vs);
                var del_link = new Element('a', {text: 'delete',
                    href: 'javascript:void(0);'});
                del_link.addEvent('click', function(e) {
                    this.fileRemove(responseObject.media_id)
                }.bind(bind));
                del_link.inject(vs);
                vs.inject($('site_video_list'));
            }
        }).send();
    },
    fileRemove: function(file_id) {
        uploadCount -= 1;

        new Request.JSON({
            'url': this.getURL('removemedia'),
            'data': {
                'media_id': file_id,
                'project_id': this.options.project_id,
                'isajax': true
            },
            'onSuccess': function(responseObject) {
                if ($type(responseObject) != "object") {
                    alert('ERROR occurred. Please try againe.');
                    return;
                }
                if (!responseObject.status || responseObject.status != true) {
                    if (responseObject.error && $type(responseObject.error) == 'string') {
                        new Element('span', {'style': 'color:red;',
                            text: 'Error: ' + responseObject.error
                        }).inject($('vs_' + file_id));
                    }
                    return;
                }

                var count_max_files = $('count_max_files');
                if (count_max_files != null)
                    count_max_files.set('text', parseInt(count_max_files.get('text')) + 1);
                $('vs_' + file_id).destroy();

            }
        }).send();
    },
    delSelected: function() {
        var selected = $('step_list').getElements("input.del_check_box:checked");
        if (selected.length > 0) {
            var media_ids = new Array();
            selected.each(function(item, index) {
                media_ids.push(item.get('value'));

            });
            Smoothbox.open(null, {mode: 'Request',
                url: en4.core.baseUrl + this.options.module + '/project/' + this.options.project_id + '/delselectedmedia',
                requestMethod: 'post',
                requestData: {
                    media_ids: JSON.encode(media_ids)
                }
            });
        }
        return false;
    }

});

/**
 * Media
 */
whmedia.project.media = new Class({
    Extends: whmedia.project,
    options: {
        type: 'whmedia_media',
        module: 'whmedia',
        controller: 'likes'
    },
    tooltips: null,
    initialize: function(options) {
        this.parent(options);
        this.tooltips = new Tips($$('.' + this.options.type + '_likes'), {
            fixed: true,
            className: this.options.type + '_likes_tips',
            offset: {
                'x': 38,
                'y': -10
            }
        });
        // Add hover event to get likes
        $$('.' + this.options.type + '_likes').addEvent('mouseover', function(event) {
            this.addhover($(event.target))
        }.bind(this));
    },
    addhover: function(el) {
        if (!el.retrieve('tip-loaded', false)) {
            el.store('tip-loaded', true);
            el.store('tip:title', this.options.lang.loading);
            el.store('tip:text', '');
            var id = el.get('id').match(/\d+/)[0];
            // Load the likes
            var url = this.getURL('get-likes');
            var req = new Request.JSON({
                url: url,
                data: {
                    format: 'json',
                    type: this.options.type,
                    id: id

                },
                onComplete: function(responseJSON) {
                    el.store('tip:title', responseJSON.body);
                    el.store('tip:text', '');
                    this.tooltips.elementEnter('mouseover', el); // Force it to update the text
                }.bind(this)
            });
            req.send();
        }
    },
    like: function(media_id) {
        en4.core.request.send(new Request.JSON({
            url: this.getURL('like'),
            data: {
                format: 'json',
                type: this.options.type,
                id: media_id
            }
        }), {
            'element': $('media_like_' + media_id)
        });
    },
    unlike: function(media_id) {
        en4.core.request.send(new Request.JSON({
            url: this.getURL('unlike'),
            data: {
                format: 'json',
                type: this.options.type,
                id: media_id
            }
        }), {
            'element': $('media_like_' + media_id)
        });
    }
});

whmedia.cover_media = new Class({
    Extends: whmedia,
    options: {
        controller: 'video',
        video_id: null
    },
    slider: null,
    initialize: function(options) {
        this.parent(options);
        this.loading_image = new Asset.image('application/modules/Core/externals/images/loading.gif', {id: 'wh_loader'});
    },
    getURL: function(action) {
        return en4.core.baseUrl + this.options.module + '/' + this.options.controller + '/' + this.options.video_id + '/' + action;
    },
    get_frame: function(time) {
        var loading_image = this.loading_image;
        var frame_img = window.$('frame_img');
        var slide_container_block = window.$('slide_container_block');
        frame_img.empty();
        loading_image.inject(frame_img);
        slide_container_block.setStyle('display', 'none');
        window.$('button_get_frame').setStyle('display', 'none');
        this.run_JSON('get-frame', {'time': time}, function(response) {
            var bind = this;
            new Asset.image(response.src, {
                onload: function(j) {
                    frame_img.empty();
                    this.erase('height');
                    this.inject(frame_img);
                    slide_container_block.setStyle('display', 'block');
                    window.$('set_cover_slider').setStyle('display', 'block').removeEvents().addEvent('click', function(e) {
                        bind.set_cover(response.file_id)
                    }.bind(this))
                }
            });
        }.bind(this));
    },
    get_time_frame: function() {
        var hour = parseInt(window.$('selected_hour').get('value'));
        var minute = parseInt(window.$('selected_minute').get('value'));
        if (minute > 59) {
            minute = 59;
            window.$('selected_minute').set('value', 59);
        }
        var second = parseInt(window.$('selected_second').get('value'));
        if (second > 59) {
            second = 59;
            window.$('selected_second').set('value', 59);
        }
        var pos = 3600 * hour + 60 * minute + second;
        this.get_frame(pos);
        this.slider.setMin(pos);
    },
    set_cover: function(id) {
        var covers = window.$('all_covers');
        covers.empty();
        this.loading_image.inject(covers);
        this.run_JSON('set-cover', {'id': id}, function(response) {
            parent.wh_project.updateMediaBlock(this.options.video_id);
            new Element('div', {'class': 'global_form_popup_message',
                'text': parent.en4.core.language.translate("Video cover saved.")}).replaces(covers);
            parent.Smoothbox.instance.doAutoResize($('global_content_simple'));
            setTimeout(function()
            {
                parent.Smoothbox.close();
            }, 1000);
        }.bind(this));
    }
});

function timeFormat(input) {
    input = parseInt(input);
    if (input < 10) {
        return '0' + input;
    }
    else
        return input;
}

function ignoreDrag(e) {
    if (e && e.stopPropagation)
        e.stopPropagation()
    else
        event.cancelBubble = true
    return false;
}

function projects_viewmore(identity, addition_data, isMobile) {
    if (identity == null) {
        var projects_viewmore = $('projects_viewmore');
        var projects_loading = $('projects_loading');
        var media_browse = $('media-browse');
        current_projects_page = current_projects_page + 1;
        var fun_current_projects_page = current_projects_page;
        var fun_max_projects_page = max_projects_page;
        var url = window.location.href;
        var formObjects = $('filter_form').toQueryString().parseQueryString();
        var data = $extend(formObjects, {format: 'html', page: fun_current_projects_page});
    }
    else {
        var projects_viewmore = $('projects_viewmore_' + identity);
        var projects_loading = $('projects_loading_' + identity);
        var media_browse = $('media-browse_' + identity);
        window['current_projects_page_' + identity] = window['current_projects_page_' + identity] + 1;
        var fun_max_projects_page = window['max_projects_page_' + identity];
        var fun_current_projects_page = window['current_projects_page_' + identity];
        var url = en4.core.baseUrl + 'widget/index/content_id/' + identity;
        var data = {format: 'html', page: fun_current_projects_page};
    }
    if (addition_data != null && typeof addition_data == 'object') {
        data = $extend(data, addition_data);
    }
    projects_viewmore.setStyle('display', 'none');
    projects_loading.setStyle('display');



    new Request.HTML({
        url: url,
        evalScripts: false,
        data: data,
        onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript) {
            var new_elements = Elements.from(responseHTML);
            try {
                if (isMobile) {
                    new_elements.each(function(item) {
                        item.getElement("div.m_proj_settings_mobile").addEvent('click', function(i) {
                            i.target.toggleClass('active');
                        });
                    });
                }
            }
            catch (e) {
            }
            var imgs = new Array();
            new_elements.each(function(item) {
                imgs.push(item.getElement('img').get('src'));
            });
            new Asset.images(imgs, {
                'onComplete': function() {
                    new_elements.inject(media_browse);
                    Smoothbox.bind(this.pc_div);
                    if (fun_current_projects_page < fun_max_projects_page) {
                        projects_viewmore.setStyle('display');
                    }
                    projects_loading.setStyle('display', 'none');
                    media_browse.masonry({
                        singleMode: true,
                        itemSelector: '.media-browse-box'
                    });
                }
            });


        }
    }).send();
}

function open_smooth(url) {
    parent.Smoothbox.instance.hideWindow();
    parent.Smoothbox.instance.position();
    parent.Smoothbox.instance.showOverlay();
    parent.Smoothbox.instance.showLoading();
    window.location.href = url;
}
var wh_leafing_instance = false;
var current_keyboard = false;

var wh_leafing = new Class({
    Implements: [Options],
    options: {
        prev: null,
        next: null
    },
    initialize: function(a) {
        //<a onclick="javascript:parent.Smoothbox.close();" href="javascript:void(0);" class="media-close-btn" ><?php echo $this->translate("Close"); ?></a>
        new Element('a', {href: 'javascript:void(0);',
            'class': 'media-close-btn'})
                .addEvent('click', function() {
                    parent.Smoothbox.close();
                })
                .inject($('TB_iframeContent'), 'before');
        this.setOptions(a);
        if (wh_leafing_instance == false) {
            wh_leafing_instance = new Keyboard({active: true});
        }
        if (current_keyboard != false) {
            wh_leafing_instance.drop(current_keyboard);
            current_keyboard = false;
        }
        current_keyboard = new Keyboard({active: true});
        current_keyboard.addShortcut('prev', {
            'keys': 'left',
            'propagate': false,
            'handler': function(e) {
                this.prev(e)
            }.bind(this)
        });
        current_keyboard.addShortcut('next_space', {
            'keys': 'space',
            'propagate': false,
            'handler': function(e) {
                this.next(e)
            }.bind(this)
        });
        current_keyboard.addShortcut('next_right', {
            'keys': 'right',
            'propagate': false,
            'handler': function(e) {
                this.next(e)
            }.bind(this)
        });
        wh_leafing_instance.manage(current_keyboard);

    },
    prev: function(e) {
        if (typeOf(this.options.prev) == 'element' && this.options.prev.getPosition().x > 0) {
            this.open_smooth(this.options.prev.get('href'));
            e.stop();
        }
        else {
            this.checkKey();
        }
    },
    next: function(e) {
        if (typeOf(this.options.next) == 'element' && this.options.next.getPosition().x > 0) {
            this.open_smooth(this.options.next.get('href'));
            e.stop();
        }
        else {
            this.checkKey();
        }
    },
    checkKey: function() {
        if ((typeOf(this.options.prev) != 'element' || this.options.prev.getPosition().x == 0) && (typeOf(this.options.next) != 'element' || this.options.next.getPosition().x == 0)) {
            wh_leafing_instance.drop(current_keyboard);
        }
    },
    open_smooth: function(url) {
        Smoothbox.instance.hideWindow();
        Smoothbox.instance.position();
        Smoothbox.instance.showOverlay();
        Smoothbox.instance.showLoading();
        $('TB_iframeContent').set('src', url);
    }
});

/*
 * Edit layout
 */
try {
    var WhFancyUpload2_File = new Class({
        Extends: FancyUpload2.File,
        onRemove: function() {
            var parent = this.element.getParent('ul');
            this.element.dispose();
            if (parent.getElements('li').length <= 0) {
                parent.setStyle('display', 'none');
            }
        }
    });
}
catch (e) {
}

whmedia.edit_layout = new Class({
    Extends: whmedia,
    options: {
        controller: 'project',
        project_id: null,
        max_files: 0,
        count_files: 0,
        uploaderTemplate: '',
        is_published: 0,
        language: 'en'
    },
    sort: null,
    window_scroll: null,
    tinyMCE_actived: [],
    uploader: null,
    FancyUpload: null,
    initialize: function(options) {
        this.parent(options);
        this.checkOrder();
        this.init_sort();
        tinyMCE.dom.Event.domLoaded = true;
        if (this.version_compare(en4.version, '4.7.0', '<')) {
            tinyMCE.settings = {theme: "advanced",
                theme_advanced_buttons1: "undo,redo,cleanup,removeformat,pasteword,|,link,unlink,|,formatselect",
                theme_advanced_buttons2: "fontselect,fontsizeselect,bold,italic,underline,strikethrough,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote",
                theme_advanced_buttons3: "",
                plugins: "paste, media, inlinepopups",
                theme_advanced_toolbar_align: "left",
                theme_advanced_toolbar_location: "top",
                element_format: "html",
                height: "200px",
                convert_urls: false,
                media_strict: false,
                directionality: "ltr",
                mode: "specific_textareas",
                editor_selector: "mceEditor",
                language: this.options.language,
                extended_valid_elements: 'a[href|rel=nofollow]'
            };
        } else {
            tinyMCE.settings = {theme: "modern",
                toolbar1: "undo,redo,removeformat,pastetext,|,link,unlink,|,formatselect,fontselect,fontsizeselect",
                toolbar2: "bold,italic,underline,strikethrough,forecolor,backcolor,|,alignleft,aligncenter,alignright,alignjustify,|,bullist,numlist,|,outdent,indent,blockquote",
                toolbar3: "",
                plugins: "paste, media, code, link, textcolor",
                element_format: "html",
                height: "200px",
                convert_urls: false,
                media_strict: false,
                directionality: "ltr",
                mode: "specific_textareas",
                editor_selector: "mceEditor",
                language: this.options.language,
                extended_valid_elements: 'a[href|rel=nofollow]',
                menubar: false,
                statusbar: false
            };
        }
        this.updateCountFiles();

        this.window_scroll = new Fx.Scroll(window);
    },
    init_sort: function() {
        var bind = this;
        this.sort = new Fx.Sort($$('#media_container .media_div'), {mode: 'vertical',
            transition: Fx.Transitions.Back.easeInOut,
            duration: 1000,
            onComplete: function() {
                this.rearrangeDOM();
                bind.checkOrder();

                bind.run_JSON('order', {
                    'project_id': bind.options.project_id,
                    'order': bind.getOrder()
                });
                if (bind.tinyMCE_actived.length > 0) {
                    while (bind.tinyMCE_actived.length > 0) {
                        bind.mceExec('mceAddControl', false, bind.tinyMCE_actived.pop());
                    }
                }
            }
        });
    },
    edit_media_title: function(media_id) {
        var media_div = $('whmedia_' + media_id);
        var div_title = media_div.getElement('div.div_title');
        media_div.getElement('.wh_edit_media_controls').setStyle('display', 'none');
        var text = div_title.get('text').trim();
        div_title.empty();
        new Element('textarea', {
            rows: 2,
            text: text
        }).inject(div_title);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Save")}).addEvent('click', function() {
            media_div.getElement('.wh_edit_media_controls').setStyle('display', '');
            this.save_media_title(media_id);
        }.bind(this)
                ).inject(div_title);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Cancel")}).addEvent('click', function() {
            media_div.getElement('.wh_edit_media_controls').setStyle('display', '');
            this.show_media_title(media_id);
        }.bind(this)
                ).inject(div_title);
    },
    save_media_title: function(media_id) {
        var div_title = $('whmedia_' + media_id).getElement('div.div_title');
        var title = div_title.getElement('textarea').get('value').trim();
        div_title.getElements('button').destroy();
        new Element('span', {
            text: en4.core.language.translate("Saving...")
        }).inject(div_title);
        new Request.JSON({
            'url': this.getURL('editmediatitle'),
            'data': {
                'media_id': media_id,
                'project_id': this.options.project_id,
                'mediatitle': title,
                'isajax': true
            },
            'onSuccess': function(responseJSON) {
                if (responseJSON.status == true) {
                    div_title.empty().set('text', title);
                    return false;
                }
                else {
                    alert(responseJSON.error);
                }
            }.bind(this)
        }).send();
    },
    show_media_title: function(media_id) {
        var div_title = $('whmedia_' + media_id).getElement('div.div_title');
        var text = div_title.getElement('textarea').get('text').trim();
        div_title.empty();
        div_title.set('text', text);
    },
    confirm_delete: function(media_id, is_text) {
        Smoothbox.open('', {mode: "whmediaConfirm",
            delete_media: function(smooth) {
                this.run_JSON('delmedia',
                        {'media_id': media_id,
                            'project_id': this.options.project_id
                        },
                function(response) {
                    $('whmedia_' + media_id).dispose();
                    if (is_text == false) {
                        this.options.count_files--;
                        this.updateCountFiles();
                    }
                    this.init_sort();
                    this.checkOrder();
                    smooth.content.empty();
                    new Element('p', {text: en4.core.language.translate('Media deleted.')}).inject(smooth.content);
                    smooth.hideLoading();
                    smooth.showWindow();
                    smooth.onLoad();
                    setTimeout(function() {
                        smooth.close();
                    }, 1000);
                }.bind(this));
            }.bind(this)});
    },
    updateCountFiles: function() {
        $('medias_count').set('text', this.options.count_files);
        if (this.options.max_files > 0) {
            var remains_count = this.options.max_files - this.options.count_files;
            if (remains_count < 0) {
                remains_count = 0;
            }
            $('max_files').set('text', remains_count);
            $$("a.add_media", "a.embed_media").each(function(link) {
                link.setStyle('display', (remains_count > 0) ? '' : 'none');
            });

            if (remains_count == 0) {
                $('not_more_add').setStyle('display', '');
            }
            else {
                $('not_more_add').setStyle('display', 'none');
            }
        }
    },
    order: function(media_id, order) {
        if (tinyMCE.editors.length > 0) {
            while (tinyMCE.editors.length > 0) {
                if (tinyMCE.editors[0].isHidden() === false) {
                    this.tinyMCE_actived.push(tinyMCE.editors[0].id);
                    this.mceExec('mceRemoveControl', false, tinyMCE.editors[0].id);
                }

            }
        }
        var media_div = $('whmedia_' + media_id);
        var elements = $$('div.media_div');
        if (order == 'top') {
            var element_index = elements.indexOf(media_div);
            var order_array = [element_index];
            for (var key = 0; key < elements.length; key++) {
                if (key != element_index) {
                    order_array.include(key);
                }
            }
            this.sort.sort(order_array);
        }
        else if (order == 'bottom') {
            var element_index = elements.indexOf(media_div);
            var order_array = [];
            for (var key = 0; key < elements.length; key++) {
                if (key != element_index) {
                    order_array.include(key);
                }
            }
            order_array.include(element_index);
            this.sort.sort(order_array);
        }
        else if (order == 'up') {
            var one;
            for (var key = 0; key < elements.length; key++) {
                if (elements[key].get('id') == 'whmedia_' + media_id) {
                    break;
                }
                else
                    one = key;
            }
            this.sort.swap(key, one);
        }
        else if (order == 'down') {
            for (var key = 0; key < elements.length; key++) {
                if (elements[key].get('id') == 'whmedia_' + media_id) {
                    break;
                }
            }
            this.sort.swap(key + 1, key);
        }
        else
            return false;


    },
    checkOrder: function() {
        var elements = $$('div.media_div');
        var length = elements.length - 1;
        elements.each(function(item, index) {
            var links_order = item.getElement('div div.links_order');
            if (links_order) {
                links_order.getElements('a').each(function(link_item) {
                    if (link_item.hasClass('link_top')) {
                        if (index == 0) {
                            link_item.setStyle('display', 'none');
                            return;
                        }
                    }
                    if (link_item.hasClass('link_up')) {
                        if (index == 0) {
                            link_item.setStyle('display', 'none');
                            return;
                        }
                    }
                    if (link_item.hasClass('link_down')) {
                        if (index >= length) {
                            link_item.setStyle('display', 'none');
                            return;
                        }
                    }
                    if (link_item.hasClass('link_bottom')) {
                        if (index >= length) {
                            link_item.setStyle('display', 'none');
                            return;
                        }
                    }
                    link_item.setStyle('display', '');

                });
            }
        });
    },
    blockCancel: function(block) {
        block.dispose();
        this.init_sort();
        this.checkOrder();
    },
    addTextBlock: function(block_id) {
        var gen_block_id = Math.floor(Math.random() * (block_id + 1) * 1000) + block_id;

        var text_block_div = new Element('div', {
            id: 'whmedia_' + gen_block_id,
            'class': "media_div media_div_par new_media"
        });
        new Element('textarea', {
            id: 'textblock_text_' + gen_block_id,
            rows: 3,
            cols: 100
        }).inject(text_block_div);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Save")}).addEvent('click', function() {
            this.saveTextBlock(gen_block_id);
        }.bind(this)
                ).inject(text_block_div);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Cancel")}).addEvent('click', function() {
            this.mceExec('mceRemoveControl', false, 'textblock_text_' + gen_block_id);
            this.blockCancel(text_block_div);
        }.bind(this)
                ).inject(text_block_div);
        text_block_div.inject('whmedia_' + block_id, 'after');
        this.init_sort();
        this.checkOrder();

        this.mceExec('mceAddControl', false, 'textblock_text_' + gen_block_id);

    },
    getOrder: function(block_id) {
        var order_out = new Array();
        var medias = $$('div.media_div');
        if (medias.length > 0) {
            while (medias.length > 0) {
                var tmp = medias.shift().get('id');
                if (typeOf(block_id) == 'number' && tmp == 'whmedia_' + block_id) {
                    order_out.push('current');
                }
                else {
                    order_out.push(tmp);
                }

            }
        }
        return order_out;
    },
    saveTextBlock: function(block_id) {
        var order_out = this.getOrder(block_id);
        this.run_JSON('add-text',
                {'project_id': this.options.project_id,
                    'order': order_out,
                    'body': tinyMCE.get('textblock_text_' + block_id).getContent()},
        function(response) {
            Elements.from(response.html).replaces('whmedia_' + block_id);
            this.init_sort();
            this.checkOrder();
        }.bind(this));

    },
    doEditTextBlock: function(block_id) {
        var content = tinyMCE.get('textblock_text_' + block_id).getContent();
        this.run_JSON('edit-text',
                {'project_id': this.options.project_id,
                    'media_id': block_id,
                    'body': content},
        function(response) {
            this.mceExec('mceRemoveControl', false, 'textblock_text_' + block_id);
            var media_content = $('whmedia_' + block_id).getElement('div.media_content');
            media_content.empty();
            media_content.set('html', content);
        }.bind(this));

    },
    editTextBlock: function(block_id) {
        var media_div = $('whmedia_' + block_id);
        media_div.getElement('div').addClass('edit-media-text');
        var media_content = media_div.getElement('div.media_content');
        var html = media_content.get('html');
        media_content.empty();
        new Element('textarea', {
            id: 'textblock_text_' + block_id,
            rows: 3,
            cols: 100,
            html: html
        }).inject(media_content);
        media_div.getElement('.wh_edit_media_controls').setStyle('display', 'none');
        new Element('button', {type: "button",
            text: en4.core.language.translate("Save")}).addEvent('click', function() {
            media_div.getElement('div').removeClass('edit-media-text');
            media_div.getElement('.wh_edit_media_controls').setStyle('display', '');
            this.doEditTextBlock(block_id);
        }.bind(this)
                ).inject(media_content);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Cancel")}).addEvent('click', function() {
            media_div.getElement('div').removeClass('edit-media-text');
            media_div.getElement('.wh_edit_media_controls').setStyle('display', '');
            this.mceExec('mceRemoveControl', false, 'textblock_text_' + block_id);
            media_content.empty();
            media_content.set('html', html);
        }.bind(this)
                ).inject(media_content);

        this.mceExec('mceAddControl', false, 'textblock_text_' + block_id);
    },
    isCanAddMedia: function() {
        if (this.options.max_files == 0)
            return true;
        if ((this.options.max_files - this.options.count_files) > 0)
            return true;
        return false;
    },
    addVideoServices: function(block_id) {
        if (!this.isCanAddMedia())
            return;
        var gen_block_id = Math.floor(Math.random() * (block_id + 1) * 1000) + block_id;

        var block_div = new Element('div', {
            id: 'whmedia_' + gen_block_id,
            'class': "media_div media_div_par new_media"
        });
        new Element('div', {'text': en4.core.language.translate("You can add video link from next resources:")}).inject(block_div);
        var ul_list = new Element('ul').inject(block_div);
        new Element('li', {'text': 'Youtube'}).inject(ul_list);
        new Element('li', {'text': 'Vimeo'}).inject(ul_list);

        new Element('input', {'type': 'text'}).inject(block_div);

        new Element('button', {type: "button",
            text: en4.core.language.translate("Get Video")}).addEvent('click', function() {
            this.getVideoServices(gen_block_id);
        }.bind(this)
                ).inject(block_div);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Cancel")}).addEvent('click', function() {
            this.blockCancel(block_div);
        }.bind(this)
                ).inject(block_div);
        block_div.inject('whmedia_' + block_id, 'after');
        this.init_sort();
        this.checkOrder();

    },
    getVideoServices: function(block_id) {
        if (!this.isCanAddMedia()) {
            $('whmedia_' + block_id).dispose();
            return;
        }
        var input_url = $('whmedia_' + block_id).getElement('input');
        var url = input_url.get('value').replace(/(^\s+)|(\s+$)/g, "");
        if (url == '')
            return;
        input_url.set('value', '');

        Smoothbox.open(null, {mode: 'Request',
            url: en4.core.baseUrl + this.options.module + '/project/' + this.options.project_id + '/videourlpreview',
            requestMethod: 'post',
            requestData: {
                url: url,
                block_id: block_id
            },
            onLoad: function() {
                var th_img = $('wh_video_thumb');
                if (th_img != null) {
                    Asset.image(th_img.get('osrc'), {
                        id: 'wh_video_thumb',
                        title: 'Thumb Loading',
                        alt: 'Thumb Loading',
                        onLoad: function(img) {
                            $('wh_thumb_loading').dispose();
                            img.replaces(th_img);
                            Smoothbox.instance.doAutoResize($('global_content_simple'));
                        }
                    });
                }
            }
        });
    },
    saveVideoServices: function(type, code, block_id) {
        $('buttons_video').setStyle('display', 'none');
        $('saving_video').setStyle('display', 'block');

        var order_out = this.getOrder(block_id);

        new Request.JSON({
            'url': this.getURL('videourladd'),
            'data': {
                'type': type,
                'code': code,
                'order': order_out,
                'title': $('video_title').get('value'),
                'project_id': this.options.project_id,
                'isajax': true
            },
            'onSuccess': function(responseObject) {
                if ($type(responseObject) != "object") {
                    alert('ERROR occurred. Please try againe.');
                    return;
                }
                if (!responseObject.status || responseObject.status != true) {
                    if (responseObject.error && $type(responseObject.error) == 'string') {
                        new Element('span', {'style': 'color:red;',
                            text: 'Error: ' + responseObject.error
                        }).replaces('saving_video');
                    }
                    return;
                }
                Smoothbox.close();
                this.options.count_files++;

                Elements.from(responseObject.html).replaces('whmedia_' + block_id);
                this.init_sort();
                this.checkOrder();
                this.updateCountFiles();
            }.bind(this)
        }).send();
    },
    hideUploader: function() {
        if (this.uploader != null) {
            this.FancyUpload = null;
            this.uploader.getElement("#demo-list").getElements('li').dispose();
            this.uploader.getElement("#demo-list").setStyle('display', 'none');
            this.uploader.dispose();
            this.uploader = null;
        }
    },
    addMedia: function(block_id) {
        if (!this.isCanAddMedia())
            return;
        this.hideUploader();
        var gen_block_id = Math.floor(Math.random() * (block_id + 1) * 1000) + block_id;

        var self = this;

        var block_div = new Element('div', {
            id: 'whmedia_' + gen_block_id,
            'class': "media_div media_div_par new_media"
        });
        this.options.uploaderTemplate.inject(block_div);
        this.init_sort();
        this.checkOrder();
        new Element('button', {type: "button",
            text: en4.core.language.translate("Close")}).addEvent('click', function() {
            this.blockCancel(block_div);
        }.bind(this)
                ).inject(block_div);
        block_div.inject('whmedia_' + block_id, 'after');
        var order_out = this.getOrder(gen_block_id);

        new Form.Upload('fallback', {
            onComplete: function(response) {
                var json = new Hash(JSON.decode(response, true) || {});
                if (json.get('status') == '1') {
                    this.options.count_files++;
                    Elements.from(json.html).inject(block_div, 'before');
                    //file.onRemove();
                    self.init_sort();
                    self.checkOrder();
                    self.updateCountFiles();
                    self.window_scroll.stop().toElement(block_div);

                    // show the html code element and populate with uploaded image html
                } else {
                    //file.element.addClass('file-failed');
                    //file.info.set('html', '<span>An error occurred:</span> ' + (json.get('error') ? (json.get('error')) : response));
                }
            }
        });
        this.uploader = block_div;
    },
    publishToggle: function() {
        this.options.is_published = (this.options.is_published) ? 0 : 1;
        this.run_JSON('publish', {
            'project_id': this.options.project_id,
            'is_published': this.options.is_published
        });
        $('button_publish').set('text', en4.core.language.translate(this.options.is_published ? 'Unpublish' : 'Publish'));
    },
    set_cover: function(media_id) {
        this.run_JSON('setcover',
                {
                    'media_id': media_id,
                    'project_id': this.options.project_id
                },
        function(responseJSON) {
            var el_cover = $('media_container').getElement('div.project-cover');
            if (el_cover != null) {
                el_cover.removeClass('project-cover');
                el_cover.getElement('span.project-cover-caption').setStyle('display', 'none');
            }
            var curr_cover = $('whmedia_' + media_id).getElement('div.media_content');
            curr_cover.addClass('project-cover');
            curr_cover.getElement('span.project-cover-caption').setStyle('display', '');
            $$('a.set_as_cover').setStyle('display', '');
            $('whmedia_' + media_id).getElement('a.set_as_cover').setStyle('display', 'none')
        });
    },
    updateMediaBlock: function(block_id) {
        this.run_JSON('get-media-content',
                {
                    'project_id': this.options.project_id,
                    'media_id': block_id
                },
        function(response) {
            var elements = Elements.from(response.html);
            elements.replaces('whmedia_' + block_id);
            this.init_sort();
            this.checkOrder();
            elements.getElements('.smoothbox').each(function(item) {
                Smoothbox.bind(item);
            })

            var el = Elements.from(response.html, false).getElement('script');
            eval(el[0].get('text'));
        }.bind(this));

    }
});

Smoothbox.Modal.whmediaConfirm = new Class({
    Extends: Smoothbox.Modal,
    element: false,
    load: function()
    {
        if (this.content)
        {
            return;
        }

        this.parent();

        this.content = new Element('div', {
            id: 'TB_ajaxContent'
        });
        this.content.inject(this.window);

        new Element('h3', {text: en4.core.language.translate('Delete?')}).inject(this.content);
        new Element('p', {text: en4.core.language.translate('Are you sure that you want to delete it? It will not be recoverable after being deleted.')}).inject(this.content);
        var buttons = new Element('div', {'class': 'confirm_buttons'});
        new Element('button', {type: "button",
            text: en4.core.language.translate("Delete")}).addEvent('click', function() {
            this.showLoading();
            this.hideWindow();
            this.options['delete_media'](this);
        }.bind(this)
                ).inject(buttons);
        new Element('button', {type: "button",
            text: en4.core.language.translate("Cancel")}).addEvent('click', function() {
            this.close();
        }.bind(this)
                ).inject(buttons);
        buttons.inject(this.content)

        this.hideLoading();
        this.showWindow();
        this.onLoad();
    },
    setOptions: function(options)
    {
        this.element = $(options.element);
        this.parent(options);
    }

});


// Avoiding MooTools.lang dependency
(function() {
    var phrases = {
        'progressOverall': 'Overall Progress ({total})',
        'currentTitle': 'File Progress',
        'currentFile': 'Uploading "{name}"',
        'currentProgress': 'Upload: {bytesLoaded} with {rate}, {timeRemaining} remaining.',
        'fileName': '{name}',
        'remove': 'Remove',
        'removeTitle': 'Click to remove this entry.',
        'fileError': 'Upload failed',
        'validationErrors': {
            'duplicate': '{name} already added.',
            'sizeLimitMin': '{name} ({size}) is too small, the minimal file size is {fileSizeMin}.',
            'sizeLimitMax': '{name} ({size}) is too big, the maximal file size is {fileSizeMax}.',
            'fileListMax': '{name} file can not be uploaded. It is over the limit.',
            'fileListSizeMax': '{name} ({size}) is too big, overall filesize of {fileListSizeMax} exceeded.'
        },
        'errors': {
            'httpStatus': 'Server returned HTTP-Status <code>#{code}</code>',
            'securityError': 'Security error occurred ({text})',
            'ioError': 'Error caused a send or load operation to fail ({text})'
        }
    };

    // en4 hack
    if (('en4' in window) && $type(en4) && $type(en4.core.language)) {
        $H(phrases).each(function(value, key) {
            if ($type(value) == 'string') {
                phrases[key] = en4.core.language.translate(value);
            } else if ($type(value) == 'object') {
                $H(value).each(function(pvalue, pkey) {
                    if ($type(value) == 'string') {
                        phrases[key][pkey] = en4.core.language.translate(pvalue);
                    }
                });
            }
        });
    }

    if (MooTools.lang) {
        MooTools.lang.set('en-US', 'FancyUpload', phrases);
    } else {
        MooTools.lang = {
            data: {
                'FancyUpload': phrases
            },
            get: function(from, key) {
                return this.data[from][key];
            },
            set: function(locale, from, data) {
                data[from] = data;
            }
        };
    }
})();