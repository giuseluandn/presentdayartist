<?php

class Whmedia_IndexController extends Core_Controller_Action_Standard {

    public function init() {
        parent::init();
        // Render
        $this->_helper->content->setEnabled();
    }

    public function indexAction() {
        if (!$this->_helper->requireAuth()->setAuthParams('whmedia_project', null, 'view')->isValid())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->whmedia_title = Zend_Registry::get('Zend_Translate')->_('Media');
        $this->view->form = $form = Whmedia_Form_Search::getInstance();

        // Populate form
        $this->view->categories = $categories = Engine_Api::_()->whmedia()->getCategories();
        // Process form
        $form->isValid($this->getRequest()->getPost());
        $values = $form->getValues();
        if ($user_whmedia_id = $this->_getParam('user_id', false)) {
            $values['user_id'] = $user_whmedia_id;
            $this->view->owner = Engine_Api::_()->getItem('user', $user_whmedia_id);
        }
        // Do the show thingy
        if (@$values['show'] == 2) {

            $table = Engine_Api::_()->getItemTable('user');
            $select = $viewer->membership()->getMembersSelect('user_id');
            $friends = $table->fetchAll($select);
            // Get stuff
            $ids = array();
            foreach ($friends as $friend) {
                $ids[] = $friend->user_id;
            }
            //unset($values['show']);
            $values['users'] = $ids;
        }
        $this->view->assign($values);
        $values['is_published'] = true;
        $this->view->paginator = $paginator = Engine_Api::_()->whmedia()->getWhmediaPaginator($values);
        $items_per_page = Engine_Api::_()->getApi('settings', 'core')->getSetting('media_per_page', 15);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage($items_per_page);
        $this->view->can_create = $this->_helper->requireAuth()->setAuthParams('whmedia_project', null, 'create')->checkRequire();
    }

    public function viewAction() {
        $project = Engine_Api::_()->getItem('whmedia_project', $this->_getParam('project_id'));
        if (!Engine_Api::_()->core()->hasSubject('whmedia_project') and $project instanceof Whmedia_Model_Project) {
            Engine_Api::_()->core()->setSubject($project);
        }
        if (!$this->_helper->requireSubject()->isValid())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!$project->is_published) {
            if (!$project->authorization()->isAllowed($viewer, 'edit') || !$project->authorization()->isAllowed($viewer, 'delete')) {
                Engine_Api::_()->core()->clearSubject();
                $this->_helper->content->setEnabled(false);
                return $this->_helper->Message("Project isn't published.", false, false)->setError();
            }
        }
        if (!Engine_Api::_()->whmedia()->isAdmin($viewer)) {
            if (!$this->_helper->requireAuth()->setAuthParams($project, $viewer, 'view')->isValid()) {
                Engine_Api::_()->core()->clearSubject();
                $this->_helper->content->setEnabled(false);
                return;
            }
        }
        $project->project_views++;
        $project->save();

        $this->view->project = $project;
        $this->view->categories = Engine_Api::_()->whmedia()->getCategories();
        $this->view->img_width = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('image_width', '600') + 90;
        $this->view->isOwner = $isOwner = $project->isOwner($viewer);
        $addition_media_filtr = array();
        if (!$isOwner and ! Engine_Api::_()->whmedia()->isAdmin($viewer)) {
            $addition_media_filtr[] = 'encode <= 1';
        }
        $this->view->medias = $project->getMedias($addition_media_filtr);
        $this->view->allow_d_orig = Engine_Api::_()->authorization()->context->isAllowed($project, 'everyone', 'allow_d_orig');
        $this->view->isMobile = Engine_Api::_()->whcore()->isMobile();
    }

    public function manageAction() {
        if (!$this->_helper->requireUser()->isValid())
            return;
        if (!$can_create = $this->_helper->requireAuth()->setAuthParams('whmedia_project', null, 'create')->checkRequire())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        $this->view->whmedia_title = Zend_Registry::get('Zend_Translate')->_('Media');
        $this->view->form = $form = Whmedia_Form_Search::getInstance();
        $form->removeElement('show');
        $multi = $form->orderby->getMultiOptions();
        unset($multi['project_user']);
        $form->orderby->setMultiOptions($multi);
        // Process form
        $values = array();
        if ($form->isValid($this->getRequest()->getPost()))
            $values = $form->getValues();
        $values['user_id'] = $viewer->getIdentity();
        $this->view->paginator = $paginator = Engine_Api::_()->whmedia()->getWhmediaPaginator($values);
        $items_per_page = Engine_Api::_()->getApi('settings', 'core')->getSetting('media_per_page', 15);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage($items_per_page);
        $this->view->can_create = $can_create;
        $this->view->isApple = Engine_Api::_()->whcore()->isApple();
        $this->view->isMobile = Engine_Api::_()->whcore()->isMobile();
    }

    public function showMediaAction() {
        if (!$this->_helper->requireAuth()->setAuthParams('whmedia_project', null, 'view')->isValid())
            return;
        $viewer = Engine_Api::_()->user()->getViewer();
        $media = (int) $this->_getParam('media', false);
        if (empty($media)) {
            return $this->_helper->Message('Media is empty.', false, false)->setError();
        }
        $this->view->media = $media = Engine_Api::_()->getItem('whmedia_media', $media);
        if (empty($media)) {
            return $this->_helper->Message('Media is empty.', false, false)->setError();
        }
        $this->view->project = $project = $media->getProject();
        if (!$this->_helper->requireAuth()->setAuthParams($project, $viewer, 'view')->isValid())
            return;
        $this->_helper->content->setEnabled(false);
        $project_medias = $project->getMedias(array('is_text = 0'));
        $project_medias_count = $project_medias->count(array('is_text = 0'));
        $this->view->previous = false;
        $this->view->next = false;
        $this->view->hot_keys_enable = Engine_Api::_()->getApi('settings', 'core')->getSetting('arrow_sliding', 1);
        if ($project_medias_count > 1) {
            foreach ($project_medias as $project_media) {
                if ($project_media->getIdentity() == $media->getIdentity()) {
                    $key = $project_medias->key();
                    if ($key > 0) {
                        $this->view->previous = array('route' => 'whmedia_default', 'action' => 'show-media', 'media' => $project_medias->getRow($key - 1)->getIdentity(), 'format' => 'smoothbox');
                    }
                    if ($key < ($project_medias_count - 1)) {
                        $this->view->next = array('route' => 'whmedia_default', 'action' => 'show-media', 'media' => $project_medias->getRow($key + 1)->getIdentity(), 'format' => 'smoothbox');
                    }
                    break;
                }
            }
        }
    }

}
