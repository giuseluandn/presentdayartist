<?php

class Whmedia_Form_Search extends Engine_Form
{
  protected static $_instance;

  public static function getInstance() {
    if (self::$_instance===null) {
        self::$_instance = new self;
    }
    return self::$_instance;
  }

  public function init()
  {
    $this
      ->setAttribs(array(
        'id' => 'filter_form',
        'class' => 'global_form_box',
      ))
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()))
      ;

    $this->addElement('Text', 'search', array(
      'label' => Zend_Registry::get('Zend_Translate')->_("Search Projects"),
      'onchange' => 'this.form.submit();',
    ));

    $this->addElement('Text', 'tags', array(
      'label' => Zend_Registry::get('Zend_Translate')->_("Keywords"),
      'autocomplete' => 'off',
      'allowEmpty' => true,
      'filters' => array('StringTrim'),
      'onKeyPress' => "return submitenter(this,event)",
      'onchange' => 'var forms = function(){$("filter_form").submit();}; setTimeout(forms, 500);'
    ));
    $this->tags->addDecorator('viewScript', array('viewScript' => 'application/modules/Whmedia/views/scripts/_Tags.tpl',
                                                  'placement'  => 'prepend'
                                                 ));
    $this->addElement('Select', 'orderby', array(
      'label' => Zend_Registry::get('Zend_Translate')->_('Browse By'),
      'multiOptions' => array(
        'creation_date' => Zend_Registry::get('Zend_Translate')->_('Most Recent'),
        'project_views' => Zend_Registry::get('Zend_Translate')->_("Most Viewed"),
        'count_likes' => Zend_Registry::get('Zend_Translate')->_("Most Appreciated"),
        'title' => Zend_Registry::get('Zend_Translate')->_('whTitle'),
        'project_user' => Zend_Registry::get('Zend_Translate')->_("by Author")
      ),
      'onchange' => 'this.form.submit();',
    ));

    $this->addElement('Select', 'bytime', array(
      'label' => Zend_Registry::get('Zend_Translate')->_("By Time"),
      'multiOptions' => array(
        0 => Zend_Registry::get('Zend_Translate')->_("All Time"),
        'today' => Zend_Registry::get('Zend_Translate')->_("Today"),
        'week' => Zend_Registry::get('Zend_Translate')->_("This Week"),
        'month' => Zend_Registry::get('Zend_Translate')->_("This Month")
      ),
      'onchange' => 'this.form.submit();',
    ));
    if (Engine_Api::_()->user()->getViewer()->getIdentity()) {
        $this->addElement('Select', 'show', array(
          'label' => Zend_Registry::get('Zend_Translate')->_('Show'),
          'multiOptions' => array(
            '1' => Zend_Registry::get('Zend_Translate')->_("Everyone's Projects"),
            '2' => Zend_Registry::get('Zend_Translate')->_("Only My Friends' Projects"),
          ),
          'onchange' => 'this.form.submit();',
        ));
    }
    $this->addElement('Select', 'category', array(
      'label' => Zend_Registry::get('Zend_Translate')->_('Category'),
      'multiOptions' => array(
        '0' => Zend_Registry::get('Zend_Translate')->_("All Categories"),
      ),
      'onchange' => 'this.form.submit();',
    ));
    $categories = Engine_Api::_()->whmedia()->getCategories();
    foreach( $categories as $category )
    {
      $this->category->addMultiOption($category->category_id, $category->category_name);
    }
    $this->addElement('Hidden', 'page', array(
      'order' => 1
    ));

  }
}
