<?php

class Whmedia_Form_Upload extends Engine_Form
{
  public function init()
  {
    $this
      ->setTitle(Zend_Registry::get('Zend_Translate')->_("Upload a media to project"))
      //->setDescription(Zend_Registry::get('Zend_Translate')->_("Share your media."))
      ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array('controller' => 'project',
                                                                                    'action' => 'upload'), 'whmedia_default', true))
      ->setAttrib('enctype', 'multipart/form-data')
      ->setAttrib('id', 'form-upload');

        $this->addElement('FancyUpload', 'file');
        $this->file->clearDecorators()
                   ->addDecorator('FormFancyUpload')
                   ->addDecorator('viewScript', array(
                                                      'viewScript' => '_FancyUpload.tpl',
                                                      'placement'  => ''
                                                      ));
        $subject = Engine_Api::_()->core()->getSubject();
        $max_files = (($MediasCount = Engine_Api::_()->authorization()->getPermission(Engine_Api::_()->user()->getViewer()->level_id, 'whmedia_project', 'medias_count')) > 0) ? $MediasCount - $subject->getMediasCount()
                                                                       : false;
        
        $file_types = Zend_Json::decode(Engine_Api::_()->authorization()->getPermission(Engine_Api::_()->user()->getViewer()->level_id, 'whmedia_project', 'file_type'));
        $type_name = '';
        $exts = '';
        $types_array = array();
        if (in_array('image', $file_types)) {
            $type_name .= 'images,';
            $exts .= '*.jpg; *.jpeg; *.gif; *.png; ';
            $types_array[] = 'Images (*.jpg; *.jpeg; *.gif; *.png)';
        }
        if (in_array('video', $file_types) && Engine_Api::_()->getApi('settings', 'core')->getSetting('video_format') != 'none') {
            $type_name .= 'videos,';
            $exts .= '*.mpeg; *.mp4; *.mkv; *.mpg; *.mpe; *.qt; *.mov; *.avi; ';
            $types_array[] = 'Videos (*.mpeg; *.mp4; *.mkv; *.mpg; *.mpe; *.qt; *.mov; *.avi)';
        }
        if (in_array('audio', $file_types)) {
            $type_name .= 'audios,';
            $exts .= '*.mp3; ';
            $types_array[] = 'Audios (*.mp3)';
        }
        if (in_array('pdf', $file_types)) {
            $type_name .= 'pdf,';
            $exts .= '*.pdf; ';
            $types_array[] = 'Portable Document Format (*.pdf)';
        }
        if (in_array('ppt', $file_types)) {
            $type_name .= 'ppt,';
            $exts .= '*.ppt; *.pptx; ';
            $types_array[] = 'PowerPoint (*.ppt; *.pptx)';
        }
        $type_name = rtrim($type_name, ',');
        $max_up = @ini_get('upload_max_filesize');
        $max_post = @ini_get('post_max_size');
        if ( ( (int) $max_up) < ( (int) $max_post ) ) {
            $fileSizeMax = $max_up;
        }
        else {
            $fileSizeMax = $max_post;
        }
        $fileSizeMax = $this->return_bytes($fileSizeMax);
        
        $this->file->getDecorator('viewScript')->setOption('data', array('max_files' => $max_files,
                                                                         'file_types' => "'$type_name': '$exts'",
                                                                         'extradata' => array('project_id' => $subject->project_id,
                                                                                              'isajax' => true),
                                                                         'fileSizeMax' => $fileSizeMax,
                                                                         'file_types_array' => $types_array ));

  }
  
  protected function return_bytes($val) {
    $val = trim($val);
    $last = strtolower(substr($val, -1));

    if($last == 'g')
        $val = $val*1024*1024*1024;
    if($last == 'm')
        $val = $val*1024*1024;
    if($last == 'k')
        $val = $val*1024;

    return $val;
  }
}
