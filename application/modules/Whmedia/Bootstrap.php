<?php

class Whmedia_Bootstrap extends Engine_Application_Bootstrap_Abstract {

    public function __construct($application) {
        parent::__construct($application);
        $this->initViewHelperPath();
    }

    protected function _initRouter() {

        $router = Zend_Controller_Front::getInstance()->getRouter();
        defined('WHMEDIA_URL_WORLD') || define('WHMEDIA_URL_WORLD', Engine_Api::_()->getApi('settings', 'core')->getSetting('url_main_world', 'whmedia'));
        $userConfig = array('create_whproject' => array(
                'route' => WHMEDIA_URL_WORLD . '/create',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'project',
                    'action' => 'create'
                )
            ),
            'add_whmedia' => array(
                'route' => WHMEDIA_URL_WORLD . '/addmedia/:project_id',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'project',
                    'action' => 'index'
                ),
                'reqs' => array(
                    'project_id' => '\d+',
                ),
            ),
            'user_projects' => array(
                'route' => WHMEDIA_URL_WORLD . '/:user_id',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'index',
                    'action' => 'index',
                ),
                'reqs' => array(
                    'user_id' => '\d+'
                )
            ),
            'whmedia_admin_manage_level' => array(
                'route' => 'admin/whmedia/level/:level_id',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'admin-level',
                    'action' => 'index',
                    'level_id' => 1
                )
            ),
            'whmedia_project' => array(
                'route' => WHMEDIA_URL_WORLD . '/project/:project_id/:action/*',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'project',
                    'action' => 'index',
                ),
                'reqs' => array(
                    'action' => '(index|edit|delmedia|delproject|videourlpreview|delselectedmedia|add-text|edit-text|publish|get-media-content)',
                    'project_id' => '\d+'
                )
            ),
            'whmedia_default' => array(
                'route' => WHMEDIA_URL_WORLD . '/:controller/:action/*',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'index',
                    'action' => 'index',
                ),
                'reqs' => array(
                    'controller' => '\D+',
                    'action' => '\D+',
                )
            ),
            'whmedia_project_view' => array(
                'route' => WHMEDIA_URL_WORLD . '/view/:project_id/:slug',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'index',
                    'action' => 'view',
                    'slug' => ''
                ),
                'reqs' => array(
                    'project_id' => '\d+'
                )
            ),
            'whmedia_video_edit_cover' => array('route' => WHMEDIA_URL_WORLD . '/video/:video_id/:action/*',
                'defaults' => array(
                    'module' => 'whmedia',
                    'controller' => 'video',
                    'action' => 'index'
                ),
                'reqs' => array(
                    'action' => '(index|get-frame|set-cover)',
                    'video_id' => '\d+'
                )
            ),
        );
        $router->addConfig(new Zend_Config($userConfig));

        return $router;
    }

    protected function _initHelper() {
        $priority = Zend_Controller_Action_HelperBroker::getStack()->getNextFreeHigherPriority(-1);
        Zend_Controller_Action_HelperBroker::getStack()->offsetSet($priority, new Whmedia_Controller_Action_Helper_Message());
    }

}