<h3><?php echo $this->translate("Author") ?></h3>
<div class="media_details">
	
    <div class="media_owner_details">
        <?php echo $this->htmlLink($this->subject()->getOwner()->getHref(), $this->itemPhoto($this->subject()->getOwner(), 'thumb.icon')) ?>
        <div class="media_owner_name"><?php echo $this->subject()->getOwner()->toString() ?>
            <p><span class="media_browse_projects"><?php echo $this->htmlLink(array('route' => 'user_projects', 'user_id' => $this->subject()->getOwner()->getIdentity(), 'reset' => true), $this->translate("View All Projects") . '&raquo;') ?></span></p>
        </div>
    </div>

    <div class="media_info">
        <p>
            <?php
            $tmp_categories = $this->categories->getRowMatching('category_id', $this->subject()->category_id);
            if (is_object($tmp_categories)):
                ?>
                <?php echo $this->translate("Category: "); ?>
                <a href="javascript:void(0)" onclick="javascript:wh_search_project.categoryAction(<?php echo $this->subject()->category_id ?>)"><?php echo $tmp_categories->category_name ?></a>
            <?php endif; ?>
        </p>

            <p><?php echo $this->translate("Created:") . ' ' . $this->timestamp($this->subject()->creation_date) ?></p>
            <p><?php echo $this->translate("Views: %d", $this->subject()->project_views) ?></p>
            <p><?php echo $this->translate("Comments: %s", $this->htmlLink(array('HASH' => 'comments', 'reset' => false), $this->subject()->comments()->getCommentCount())) ?></p>
            <p><?php echo $this->translate("Likes: %d", $this->subject()->likes()->getLikeCount()) ?></p>

    </div>
    <?php
    $tags = $this->subject()->gettags();
    if ($tags !== null and count($tags)):
        ?>
        <div class="whmedia_browse_info_tag">
            <div class="whmedia_browse_info_tag_title"><?php echo $this->translate('Tags: '); ?></div>
            <?php foreach ($tags as $tag): ?>
                <a href='javascript:void(0);' onclick='javascript:wh_search_project.tagAction("<?php echo $tag->getTag()->text; ?>");'><span class="fp_tag">&bull;</span><?php echo $tag->getTag()->text ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

</div>