<?php

class Whmedia_Widget_ProjectDetailsController extends Engine_Content_Widget_Abstract {

    public function indexAction() {

        if (!Engine_Api::_()->core()->hasSubject('whmedia_project')) 
            return $this->setNoRender();
        
        $subject = Engine_Api::_()->core()->getSubject();
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!$subject->authorization()->isAllowed($viewer, 'view'))
            return $this->setNoRender();
        
        $owner = $subject->getOwner();
        if (!($owner instanceof User_Model_User))
            return $this->setNoRender();

        $this->view->categories = Engine_Api::_()->whmedia()->getCategories();
    }

}