<?php
class Whmedia_Widget_PortfolioController extends Engine_Content_Widget_Abstract
{
	public function indexAction()
	{
		// Don't render this if not authorized
		$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
		if( !Engine_Api::_()->core()->hasSubject() ) {
			return $this->setNoRender();
		}

		// Get subject and check auth
		$this->view->subject = $subject = Engine_Api::_()->core()->getSubject('user');
		if( !$subject->authorization()->isAllowed($viewer, 'view') ) {
			return $this->setNoRender();
		}

		$view  =  Zend_Registry::get('Zend_View');
		$staticUrl = $view -> layout() -> staticBaseUrl;
		$view -> headLink() -> appendStylesheet($staticUrl . 'application/modules/Whmedia/externals/styles/custom.css');
		$view -> headLink() -> appendStylesheet($staticUrl . 'application/modules/Whmedia/externals/styles/bootstrap.min.css');

		$folder = Engine_Api::_()->pdaportfolio()->getMyFolder($subject->getIdentity());

		$this->view->path = str_replace(DIRECTORY_SEPARATOR, '/', $folder -> path);

		$this->view->items = $items = Engine_Api::_()->pdaportfolio()->getFilesInFolder($folder);
		//$this->view->items = $paginator = Engine_Api::_()->whmedia()->getWhmediaPaginator(array('user_id' => $subject->getIdentity(), 'is_portfolio' => 1));

		// Hide if nothing to show
		if( count($items) <= 0 ) {
			return $this->setNoRender();
		}
	}

}
