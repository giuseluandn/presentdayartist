<div class="block_photo layout_ynresponsivepurity_blkcont">
	<ul class="ynresponsivepurity_albumgrid clearfix">
	<?php foreach($this -> items as $item):?>
		<li>
			<div>
				<a class="img_albthumb" title="<?php echo $item -> getTitle()?>" href="<?php echo $item -> getHref()?>">
					<span style="background-image:url('<?php echo $item -> getPdaPath($this->path)?>')"></span>
				</a>
				<span style="display: none;" class="text_albname">
					<em><?php echo $item;?></em>
				</span>
			</div>
		</li>
	<?php endforeach;?>
	</ul>
</div>