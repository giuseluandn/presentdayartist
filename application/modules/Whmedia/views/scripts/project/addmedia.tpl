<?php $project = Engine_Api::_()->core()->getSubject() ?>
<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whmedia/externals/scripts/whmedia_core.js') ?>
<?php
    $tmp_url_world = WHMEDIA_URL_WORLD;
    $script = <<<EOF
        en4.core.runonce.add(function() {
            wh_project = new whmedia.project({
                                          project_id:{$project->project_id},
                                          module:'{$tmp_url_world}'
                                      });
        });
EOF;
    $this->headScript()->appendScript($script, $type = 'text/javascript', $attrs = array());
?>
<div class="headline">


    <h2>
      <?php echo $this->pageTitle ?>
    </h2>
    
    <?php if( count($this->navigation) ): ?>
      <div class='tabs'>
        <?php
        $tmp = $this->navigation()->menu()->setContainer($this->navigation);
        echo $tmp->render() ?>
      </div>
    <?php endif; ?>
    </div>

<div class="uploadphotosbl">

    <strong>"<?php echo Engine_Api::_()->core()->getSubject()->getTitle() ?>"</strong>
    
    <?php if (isset($this->error)):?>
    <p><?php echo $this->error ?></p>
    <?php else:?>
    <?php echo $this->form->render($this) ?>
    <?php echo $this->htmlLink(array('route' => 'whmedia_project', 'action' => 'index', 'project_id' => $project->project_id), $this->translate('Proceed'), array('class' => 'icon_next buttonlink_right',
                                                                                                                                                                  'id' => 'proceed_link',
                                                                                                                                                                  'style' => 'display:none;float:left;')); ?>
    <?php endif;?>
</div>
    
<div class="uploadyoutube">    
    <h3><?php echo $this->translate('Add Video from video services')?></h3>
   <br /> 
    <div class="tip">
      <span><?php echo $this->translate("You can add video from youtube or vimeo.")?></span>
    </div>

    <input type="text" id="site_url" onkeypress="if ( event.keyCode == 13 ) wh_project.videoURL_preview(this);"/>
    <button onclick="javascript:wh_project.videoURL_preview($('site_url'));return false;"><?php echo $this->translate("wh Add Video")?></button>
    <div id="site_video_list">
    
    </div>
    
</div>