<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whmedia/externals/scripts/whmedia_core.js') ?>

<?php if (isset($this->owner)): ?>
    <h2>
        <?php echo $this->translate('%1$s\'s Projects', $this->htmlLink($this->owner->getHref(), $this->owner->getTitle())) ?>
    </h2>
<?php endif; ?>


<?php
if ($this->paginator->getTotalItemCount() > 0):
    $thumb_block_height = Whmedia_Model_Media::getThumbDimension('height') + 70;
    ?>
    <ul class="media_browse">
        <?php foreach ($this->paginator as $whmedia): ?>
            <li style="height:<?php echo $thumb_block_height ?>px">
                <div class="media_thumb_wrapper"><?php echo $this->htmlLink($whmedia->getHref(), "<img alt='Project Thumb' src=\"{$whmedia->getPhotoUrl()}\" />"); ?></div>
                <div style="clear:both;"></div>
                <div class="media_browse_title">
                    <?php echo $this->htmlLink($whmedia->getHref(), $this->whtruncate($whmedia->getTitle())); ?>
                </div>

                <div class="media_author"><?php echo $this->translate('by %s', $whmedia->getOwner()->toString()) ?></div>


                <span class="media_files">
                    <?php
                    $MediasCount = $whmedia->count_media;
                    echo $this->translate(array("%d file", "%d files", $MediasCount), $MediasCount);
                    ?>
                </span> |
                <span class="media_views"><?php echo $this->translate('whViews: %d', $whmedia->project_views) ?></span> | <span class="media_likes"><?php echo $whmedia->likes()->getLikeCount(); ?></span>


            </li>
        <?php endforeach; ?>
    </ul>
    <?php if ($this->paginator->count() >= 1): ?>
        <div>
            <?php echo $this->paginationControl($this->paginator, null, 'pagination/whmediapagination.tpl'); ?>
        </div>
    <?php endif; ?>

<?php elseif ($this->category || $this->show == 2 || $this->search): ?>
    <div class="tip">
        <span>
            <?php echo $this->translate('Nobody has posted a project in this criteria yet.'); ?>
        </span>
    </div>
<?php else: ?>
    <div class="tip">
        <span>
            <?php echo $this->translate('Nobody has written a projects yet.'); ?>
            <?php if ($this->can_create): // @todo check if user is allowed to create a poll ?>
                <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="' . $this->url(array('controller' => 'project', 'action' => 'create'), 'whmedia_default', true) . '">', '</a>'); ?>
            <?php endif; ?>
        </span>
    </div>
<?php endif; ?>