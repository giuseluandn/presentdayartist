<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whmedia/externals/scripts/whmedia_core.js') ?>
<?php
$tmp_url_world = WHMEDIA_URL_WORLD;
$script = <<<EOF
            en4.core.runonce.add(function() {
                new Tips($$('.Tips'));
                wh_search_project = new whmedia.search();
                wh_media = new whmedia.project.media({project_id:{$this->project->getIdentity()},
                                                      module:'{$tmp_url_world}',
                                                      lang : {loading: '{$this->translate('Loading...')}'}
                                                     });
                                                     
            });
EOF;
if ($this->isMobile) {
    $script .= <<<EOF
                en4.core.runonce.add(function() {                                                    
                    $$("div.m_proj_settings_mobile").addEvent('click', function(i) {
                        i.target.toggleClass('active');
                    });
                });
EOF;
}
$this->headScript()->appendScript($script, $type = 'text/javascript', $attrs = array());
?>

<?php if ($this->project->authorization()->isAllowed($this->viewer(), 'edit')) : ?>
    <div class="pulldown <?php echo ($this->isMobile) ? 'm_proj_settings_mobile' : 'm_proj_settings' ?>">
        <div class="pulldown_contents_wrapper">
            <div class="pulldown_contents">
                <ul>
                    <li>
                        <?php echo $this->htmlLink(array('route' => 'whmedia_project', 'action' => 'index', 'project_id' => $this->project->getIdentity(), 'reset' => true), $this->translate("Manage Project"), array('class' => 'icon_manage_media')) ?>
                    </li>
                    <li>
                        <?php echo $this->htmlLink(array('route' => 'whmedia_project', 'action' => 'edit', 'project_id' => $this->project->getIdentity(), 'reset' => true), $this->translate("Edit Details"), array('class' => 'editcover')) ?>
                    </li>
                    <li>
                        <?php echo $this->htmlLink(array('route' => 'add_whmedia', 'project_id' => $this->project->getIdentity(), 'reset' => true), $this->translate("Add Media"), array('class' => 'icon_whmedia_new')) ?>
                    </li>
                    <?php if ($this->project->authorization()->isAllowed($this->viewer(), 'delete')) : ?>
                        <li>
                            <?php echo $this->htmlLink(array('route' => 'whmedia_project', 'action' => 'delproject', 'project_id' => $this->project->getIdentity(), 'forward' => 'index'), $this->translate("Delete Project"), array('class' => 'smoothbox icon_delete_media')) ?>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>

<h2>
    <?php echo $this->project->getTitle() ?>
</h2>

<?php $medias = $this->medias; ?>
<?php if (($medias_count = $medias->count())) : ?>
    <?php //$descFlag = false ?>
    <?php foreach ($medias as $this->media): ?>

        <div class="mediaslides" style="width: <?php echo (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('image_width', '600') + 90 ?>px;">

            <div class="media_file" id="whmedia_<?php echo $this->media->media_id; ?>">
                <?php
                //if ($this->media->is_text && $descFlag == false) {
                //    $this->headMeta()->appendProperty('og:description', $this->media->getDescription());
                //    $descFlag = true;
                //}
                ?>
                <div style="width: <?php echo $this->img_width ?>px;">
                    <?php
                    if ($this->media->getMediaType() == 'image')
                        echo $this->htmlLink(array('route' => 'whmedia_default', 'action' => 'show-media', 'media' => $this->media->getIdentity()), $this->media->Embedded(), array('class' => 'smoothbox'));
                    else
                        echo $this->media->Embedded();
                    ?>
                    <div class="media_desc" style="width: <?php echo (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('image_width', '600') - 90 ?>px;">
                        <p><?php echo nl2br($this->media->getTitle()); ?></p>
                        <div class="wh_media_button_wrapper">
                            <?php if ($this->allow_d_orig and $this->media->issetOriginal()): ?>
                                <div class="wh_media_download_button">
                                    <?php echo $this->htmlLink(array('route' => 'whmedia_default', 'controller' => 'share', 'action' => 'download', 'media_id' => $this->media->getIdentity()), $this->translate(""), array('class' => 'buttonlink Tips wh_media_download_btn', 'rel' => $this->translate("Download original file"))) ?>
                                </div>    
                            <?php endif ?>    

                            <?php if ($this->isOwner and !in_array($this->media->getMediaType(), array('youtube', 'vimeo')) and $this->media->getMediaType() != 'text'): ?>
                                <div class="wh_media_share_button">
                                    <?php
                                    echo $this->htmlLink(array('route' => 'whmedia_default', 'controller' => 'share', 'action' => 'get-code', 'media_id' => $this->media->getIdentity()), $this->translate("whmedia_Share"), array('class' => 'buttonlink smoothbox Tips',
                                        'rel' => $this->translate("whmedia_Share")))
                                    ?>
                                </div>
                            <?php endif ?>
                            <?php if ($this->media->getMediaType() != 'text'): ?>
                                <div id="media_like_<?php echo $this->media->getIdentity() ?>" class="likeunlike">
                                    <?php echo $this->render('likes/_like.tpl') ?>
                                </div>
                            <?php endif ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    <?php endforeach; ?>

    <?php
    //if ($descFlag == false) {
    //    $this->headMeta()->appendProperty('og:description', $this->translate('Posted by %s', Engine_Api::_()->user()->getUser($this->project->user_id)->getTitle()));
    //}
    ?>

<?php else : ?>


    <div class="tip">
        <span>
            <?php echo $this->translate('No media was found.'); ?>
        </span>
    </div>
<?php endif; ?>	