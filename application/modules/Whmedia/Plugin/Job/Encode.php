<?php

class Whmedia_Plugin_Job_Encode extends Core_Plugin_Job_Abstract {

    protected $_ffmpeg_path;
    protected $_video;
    protected $_settings;
    protected $_log;
    protected $_tmpDir;

    public function __construct(Zend_Db_Table_Row_Abstract $job, $jobType = null) {
        parent::__construct($job, $jobType);
        $this->_settings = Engine_Api::_()->getApi('settings', 'core');
        $this->_ffmpeg_path = $this->_settings->whvideo_ffmpeg_path;

        $log = new Zend_Log();
        $log->addWriter(new Zend_Log_Writer_Stream(APPLICATION_PATH . '/temporary/log/whmedia.log'));
        $this->_log = $log;

        // Check we can execute
        if (!function_exists('shell_exec')) {
            throw new Engine_Exception('Unable to execute shell commands using shell_exec(); the function is disabled.');
        }

        // Check the video temporary directory
        $this->_tmpDir = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary' . DIRECTORY_SEPARATOR . 'whmedia';
        if (!is_dir($this->_tmpDir)) {
            if (!mkdir($this->_tmpDir, 0777, true)) {
                throw new Engine_Exception('Media temporary directory did not exist and could not be created.');
            }
        }
        if (!is_writable($this->_tmpDir)) {
            throw new Engine_Exception('Media temporary directory is not writable.');
        }
    }

    protected function _execute() {
        // Get job and params
        $job = $this->getJob();

        // No video id?
        if (!($media_id = $this->getParam('media_id'))) {
            $this->_setState('failed', 'No video identity provided.');
            $this->_setWasIdle();
            return;
        }

        // Get video object
        $video = Engine_Api::_()->getItem('whmedia_media', $media_id);
        if (!$video || !($video instanceof Whmedia_Model_Media)) {
            $this->_setState('failed', 'Video is missing.');
            $this->_setWasIdle();
            return;
        }
        $this->_video = $video;
        if (!($this->_video instanceof Whmedia_Model_Media)) {
            throw new Engine_Exception('Argument was not a valid media.');
        }
        // Check video encode
        if (1 != $video->encode) {
            $this->_setState('failed', 'Video has already been encoded, or has already failed encoding.');
            $this->_setWasIdle();
            return;
        }

        // Process
        try {
            $this->_process();
            $this->_setIsComplete(true);
        } catch (Exception $e) {
            $this->_setState('failed', 'Exception: ' . $e->getMessage());
        }
    }

    private function getFfmpeg() {
        // Make sure FFMPEG path is set
        $ffmpeg_path = $this->_ffmpeg_path;
        if (!$ffmpeg_path)
            throw new Engine_Exception('Ffmpeg not configured');

        // Make sure FFMPEG can be run
        if (!@file_exists($ffmpeg_path) || !@is_executable($ffmpeg_path)) {
            exec($ffmpeg_path . ' -version', $output, $return);
            if ($return > 0)
                throw new Engine_Exception('Ffmpeg found, but is not executable');
        }
        return $ffmpeg_path;
    }

    private function getFlvtool2() {
        // Make sure flvtool2 path is set
        $flvtool2_path = $this->_settings->whvideo_flvtool2_path;
        if (!$flvtool2_path)
            return false;

        // Make sure flvtool2 can be run
        if (!@file_exists($flvtool2_path) || !@is_executable($flvtool2_path)) {
            exec($flvtool2_path, $output, $return);
            if ($return > 0)
                return false;
        }
        return $flvtool2_path;
    }

    private function getFaad() {
        $faad_path = $this->_settings->whvideo_flvtool2_faad;
        if (!$faad_path)
            return false;

        // Make sure faad can be run
        if (!@file_exists($faad_path) || !@is_executable($faad_path)) {
            exec($faad_path . ' -h', $output, $return);
            if ($return != 1) {
                return false;
            }
        }
        return $faad_path;
    }

    private function getStorageObject() {
        // Pull video from storage system for encoding
        $storageObject = Engine_Api::_()->getItemTable('storage_file')->fetchRow(array('parent_type = ?' => 'whmedia_media',
            'parent_id = ?'   => $this->_video->media_id,
            'type is null'));
        if (!$storageObject) {
            $this->_video->encode = 9;
            $this->_video->save();
            throw new Engine_Exception('Video storage file was missing');
        }

        $originalPath = $storageObject->temporary();
        if (!file_exists($originalPath)) {
            $this->_video->encode = 10;
            $this->_video->save();
            throw new Engine_Exception('Could not pull to temporary file');
        }

        return $storageObject;
    }

    private function getMediaInfo() {
        return 'User: ' . $this->_video->getOwner()->getTitle() . PHP_EOL .
                'Project: ' . $this->_video->getProject()->getTitle() . PHP_EOL .
                'Media ID: ' . $this->_video->getIdentity() . PHP_EOL;
    }

    private function encodeAudio($storageObject) {
        $originalPath = $storageObject->temporary();
        $outputPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_converted.ogg';

        $project = $this->_video->getParent();
        // Prepare information
        $owner = $project->getOwner();

        $audioCommand = $ffmpeg_path . ' '
                . '-i ' . escapeshellarg($originalPath) . ' '
                . ' -acodec libvorbis -f ogg' . ' '
                . '-y ' . escapeshellarg($outputPath) . ' '
                . '2>&1'
        ;

        // Prepare output header
        $output = PHP_EOL;
        $output .= $getMediaInfo();
        $output .= $originalPath . PHP_EOL;
        $output .= $outputPath . PHP_EOL;

        // Execute video encode command
        $Output = $output .
                $audioCommand . PHP_EOL .
                shell_exec($audioCommand);

        // Log
        if ($this->_log) {
            $this->_log->log($Output, Zend_Log::INFO);
        }

        // Check for failure
        $success = true;

        // Unsupported format
        if (preg_match('/Unknown format/i', $Output) ||
                preg_match('/Unsupported codec/i', $Output) ||
                preg_match('/patch welcome/i', $Output) ||
                preg_match('/Audio encoding failed/i', $Output) ||
                !is_file($outputPath) ||
                filesize($outputPath) <= 0) {
            $success = false;
            $this->_video->encode = 3;
        }
        if (!$success) {
            $exceptionMessage = '';
            $translate = Zend_Registry::get('Zend_Translate');
            $db = $this->_video->getTable()->getAdapter();
            $db->beginTransaction();
            try {
                $this->_video->save();
                $exceptionMessage = 'Audio format is not supported by FFMPEG.';
                $notificationMessage = $translate->translate(sprintf(
                                'Audio conversion failed. Audio format is not supported by FFMPEG. Please try %1$sagain%2$s.', '', ''
                        ), $language);
                Engine_Api::_()->getDbtable('notifications', 'activity')
                        ->addNotification($owner, $owner, $project, 'whmedia_processed_failed', array(
                            'message'      => $notificationMessage,
                            'message_link' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index', 'project_id' => $project->project_id), 'whmedia_project', true),
                ));

                $db->commit();
            } catch (Exception $e) {
                $videoOutput .= PHP_EOL . $e->__toString() . PHP_EOL;
                if ($log) {
                    $log->write($outMediaInfo . $e->__toString(), Zend_Log::ERR);
                }
                $db->rollBack();
            }
            // Write to additional log in dev
            if (APPLICATION_ENV == 'development') {
                file_put_contents($this->_tmpDir . '/' . $this->_video->media_id . '.txt', $videoOutput);
            }

            throw new Engine_Exception($exceptionMessage);
        } else {
            $params = array(
                'parent_id'   => $this->_video->getIdentity(),
                'parent_type' => $this->_video->getType(),
                'user_id'     => $owner->getIdentity()
            );

            $db = $this->_video->getTable()->getAdapter();
            $db->beginTransaction();
            try {
                $secondFileRow = Engine_Api::_()->storage()->create($outputPath, array_merge($params, array('type'           => 'audio.html5',
                    'parent_file_id' => $storageObject->file_id)));
                $secondFileRow->setFromArray(array('mime_major' => 'audio',
                            'mime_minor' => 'ogg'))
                        ->save();
                $db->commit();
                unlink($secondPath);
                unlink($originalPath);
            } catch (Exception $e) {
                $db->rollBack();

                // delete the files from temp dir
                unlink($secondPath);
                unlink($originalPath);

                $this->_video->encode = 7;
                $this->_video->save();

                // notify the owner
                $translate = Zend_Registry::get('Zend_Translate');
                $notificationMessage = '';
                $language = (!empty($owner->language) && $owner->language != 'auto' ? $owner->language : null );
                if ($this->_video->encode == 7) {
                    $notificationMessage = $translate->translate(sprintf(
                                    'Audio conversion failed. You may be over the site upload limit.  Try %1$suploading%2$s a smaller file, or delete some files to free up space.', '', ''
                            ), $language);
                }
                Engine_Api::_()->getDbtable('notifications', 'activity')
                        ->addNotification($owner, $owner, $project, 'whmedia_processed_failed', array(
                            'message'      => $notificationMessage,
                            'message_link' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index', 'project_id' => $project->project_id), 'whmedia_project', true),
                ));

                throw $e; // throw
            }
            $this->_video->encode = 0;
            $this->_video->save();
        }
    }

    private function encodeFlv($storageObject) {
        $project = $this->_video->getParent();
        $owner = $project->getOwner();
        $thumbPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_vthumb.jpg';
        $flvPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_converted.flv';
        if ($this->_settings->getSetting('hd_video_format', 0))
            $hdFlvPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_hdconverted.flv';
        $faad_path = $this->getFaad();
        $ffmpeg_path = $this->getFfmpeg();
        $originalPath = $storageObject->temporary();
        $flvtool2_path = $this->getFlvtool2();

        if ($faad_path !== false) {
            try {
                $file_audio_info = Engine_Api::_()->whmedia()->getAudioInfo($originalPath);
                $channels = (int) $file_audio_info['channels'];
                $rate = (int) $file_audio_info['rate'];
                if ($channels > 2 and $rate > 45000) {
                    /*
                     *  ffmpeg -i ./28.mkv -acodec copy ./source.6.aac
                      faad -d -o source.2.aac source.6.aac
                      ffmpeg -y -i ./28.mkv -i source.2.aac -map 0:0 -map 1:0 -vcodec copy -acodec copy output.avi
                     */
                    $audioFAADPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_faad.' . $file_audio_info['codec'];
                    $audioFAADPath2 = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_faad2.aac';
                    $tmp_originalPath = $originalPath . '.avi';
                    //Get audio channel
                    $acodecCommand = $ffmpeg_path . ' '
                            . '-i ' . escapeshellarg($originalPath) . ' '
                            . ' -acodec copy ' . escapeshellarg($audioFAADPath) . ' '
                            . '2>&1'
                    ;
                    // Prepare output header
                    $output = PHP_EOL;
                    $output .= $this->getMediaInfo();
                    $output .= $originalPath . PHP_EOL;
                    $output .= $audioFAADPath . PHP_EOL;
                    $output .= 'Get audio channel for FAAD' . PHP_EOL;

                    // Execute video encode command
                    $acodecOutput = $output . $acodecCommand . PHP_EOL .
                            shell_exec($acodecCommand);

                    // Log
                    if ($this->_log) {
                        $this->_log->log($acodecOutput, Zend_Log::INFO);
                    }
                    if ($file_audio_info['codec'] != 'aac' and file_exists($audioFAADPath)) {
                        $tmp_audioFAADPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_faad.aac';
                        // convert to acc audio
                        $acodecCommand = $ffmpeg_path . ' '
                                . '-i ' . escapeshellarg($audioFAADPath) . ' '
                                . ' -acodec libfaac ' . escapeshellarg($tmp_audioFAADPath) . ' '
                                . '2>&1'
                        ;
                        // Prepare output header
                        $output = PHP_EOL;
                        $output .= $this->getMediaInfo();
                        $output .= $originalPath . PHP_EOL;
                        $output .= $audioFAADPath . PHP_EOL;
                        $output .= 'convert to acc audio' . PHP_EOL;

                        // Execute video encode command
                        $acodecOutput = $output . $acodecCommand . PHP_EOL .
                                shell_exec($acodecCommand);

                        // Log
                        if ($this->_log) {
                            $this->_log->log($acodecOutput, Zend_Log::INFO);
                        }
                        if (file_exists($tmp_audioFAADPath)) {
                            $audioFAADPath = $tmp_audioFAADPath;
                        }
                    }
                    if (file_exists($audioFAADPath)) {
                        // downmix audio
                        $acodecCommand = $faad_path . ' '
                                . ' -d -o ' . escapeshellarg($audioFAADPath2) . ' ' . escapeshellarg($audioFAADPath) . ' '
                                . '2>&1'
                        ;
                        // Prepare output header
                        $output = PHP_EOL;
                        $output .= $this->getMediaInfo();
                        $output .= $originalPath . PHP_EOL;
                        $output .= $audioFAADPath . PHP_EOL;
                        $output .= 'FAAD work' . PHP_EOL;

                        // Execute video encode command
                        $acodecOutput = $output . $acodecCommand . PHP_EOL .
                                shell_exec($acodecCommand);

                        // Log
                        if ($this->_log) {
                            $this->_log->log($acodecOutput, Zend_Log::INFO);
                        }
                    }
                    if (file_exists($audioFAADPath2)) {
                        //Mix audio with video
                        $acodecCommand = $ffmpeg_path . ' '
                                . ' -y -i ' . escapeshellarg($originalPath) . ' '
                                . ' -i ' . escapeshellarg($audioFAADPath2) . ' -map 0:0 -map 1:0 -vcodec copy -acodec copy '
                                . escapeshellarg($tmp_originalPath) . ' '
                                . '2>&1'
                        ;
                        // Prepare output header
                        $output = PHP_EOL;
                        $output .= $this->getMediaInfo();
                        $output .= $originalPath . PHP_EOL;
                        $output .= $audioFAADPath . PHP_EOL;
                        $output .= 'Mix audio with video' . PHP_EOL;

                        // Execute video encode command
                        $acodecOutput = $output . $acodecCommand . PHP_EOL .
                                shell_exec($acodecCommand);

                        // Log
                        if ($this->_log) {
                            $this->_log->log($acodecOutput, Zend_Log::INFO);
                        }
                    }
                    if (file_exists($tmp_originalPath)) {
                        $originalPath = $tmp_originalPath;
                    }
                }
            } catch (Exception $e) {
                
            }
            file_exists($audioFAADPath) && unlink($audioFAADPath);
            file_exists($audioFAADPath2) && unlink($audioFAADPath2);
            file_exists($tmp_audioFAADPath) && unlink($tmp_audioFAADPath);
        }

        $video_width = $this->_settings->getSetting('video_width', '320');
        $video_height = $this->_settings->getSetting('video_height', '240');
        $videoDimension = Engine_Api::_()->whmedia()->getVideoDimension($originalPath);
        $VideoEncodeDimension = $this->_getVideoEncodeDimension($video_width, $video_height, $videoDimension['width'], $videoDimension['height']);
        $videoCommand = $ffmpeg_path . ' '
                . '-i ' . escapeshellarg($originalPath) . ' '
                . '-ab 64k' . ' '
                . '-ar 44100' . ' '
                . '-qscale 5' . ' '
                . '-vcodec flv' . ' '
                . '-f flv' . ' '
                . '-r 25' . ' '
                . "-s {$VideoEncodeDimension['width']}x{$VideoEncodeDimension['height']}" . ' '
                . '-v 2' . ' '
                . '-y ' . escapeshellarg($flvPath) . ' '
                . '2>&1'
        ;

        // Prepare output header
        $output = PHP_EOL;
        $output .= $this->getMediaInfo();
        $output .= $originalPath . PHP_EOL;
        $output .= $flvPath . PHP_EOL;
        $output .= $thumbPath . PHP_EOL;

        // Execute video encode command
        $videoOutput = $output .
                $videoCommand . PHP_EOL .
                shell_exec($videoCommand);

        // Log
        if ($this->_log) {
            $this->_log->log($videoOutput, Zend_Log::INFO);
        }

        // Check for failure
        $success = true;

        // Unsupported format
        if (preg_match('/Unknown format/i', $videoOutput) ||
                preg_match('/Unsupported codec/i', $videoOutput) ||
                preg_match('/patch welcome/i', $videoOutput) ||
                preg_match('/Audio encoding failed/i', $videoOutput) ||
                !is_file($flvPath) ||
                filesize($flvPath) <= 0) {
            $success = false;
            $this->_video->encode = 4;
        }

        // This is for audio files
        else if (preg_match('/video:0kB/i', $videoOutput)) {
            $success = false;
            $this->_video->encode = 5;
        }

        // Failure
        if (!$success) {

            $exceptionMessage = '';

            $db = $this->_video->getTable()->getAdapter();
            $db->beginTransaction();
            try {
                $this->_video->save();


                // notify the owner
                $translate = Zend_Registry::get('Zend_Translate');
                $language = (!empty($owner->language) && $owner->language != 'auto' ? $owner->language : null );
                $notificationMessage = '';

                if ($this->_video->encode == 4) {
                    $exceptionMessage = 'Video format is not supported by FFMPEG.';
                    $notificationMessage = $translate->translate(sprintf(
                                    'Video conversion failed. Video format is not supported by FFMPEG. Please try %1$sagain%2$s.', '', ''
                            ), $language);
                } else if ($this->_video->encode == 5) {
                    $exceptionMessage = 'Incorrect video format.';
                    $notificationMessage = $translate->translate(sprintf(
                                    'Video conversion failed. Incorrect video format. Please try %1$sagain%2$s.', '', ''
                            ), $language);
                } else {
                    $exceptionMessage = 'Unknown encoding error.';
                    $notificationMessage = 'Encoding error.';
                }

                Engine_Api::_()->getDbtable('notifications', 'activity')
                        ->addNotification($owner, $owner, $project, 'whmedia_processed_failed', array(
                            'message'      => $notificationMessage,
                            'message_link' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index', 'project_id' => $project->project_id), 'whmedia_project', true),
                ));

                $db->commit();
            } catch (Exception $e) {
                $videoOutput .= PHP_EOL . $e->__toString() . PHP_EOL;
                if ($this->_log) {
                    $this->_log->write($this->getMediaInfo() . $e->__toString(), Zend_Log::ERR);
                }
                $db->rollBack();
            }

            // Write to additional log in dev
            if (APPLICATION_ENV == 'development') {
                file_put_contents($this->_tmpDir . '/' . $this->_video->media_id . '.flv.txt', $videoOutput);
            }

            throw new Engine_Exception($exceptionMessage);
        }

        // Success
        else {
            if (!empty($flvtool2_path)) {
                $videoCommand = $flvtool2_path . ' -UP ' . escapeshellarg($flvPath) . ' 2>&1';


                // Execute video encode command
                $videoOutput_flvtool = $videoCommand . PHP_EOL .
                        shell_exec($videoCommand);

                // Log
                if ($this->_log) {
                    $this->_log->log(PHP_EOL . $this->getMediaInfo() . $videoOutput_flvtool, Zend_Log::INFO);
                }
            }

            // Save video and thumbnail to storage system
            $params = array(
                'parent_id'   => $this->_video->getIdentity(),
                'parent_type' => $this->_video->getType(),
                'user_id'     => $owner->getIdentity()
            );

            $db = $this->_video->getTable()->getAdapter();
            $db->beginTransaction();
            $tmpParams = array_merge($params, array('name'       => $storageObject->name,
                'mime_major' => $storageObject->mime_major,
                'mime_minor' => strtolower($storageObject->mime_minor)));
            try {
                $storageObject->store($flvPath);
                $storageObject->setFromArray($tmpParams);
                $storageObject->save();
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();

                // delete the files from temp dir
                unlink($originalPath);
                unlink($flvPath);

                $this->_video->encode = 7;
                $this->_video->save();

                // notify the owner
                $translate = Zend_Registry::get('Zend_Translate');
                $notificationMessage = '';
                $language = (!empty($owner->language) && $owner->language != 'auto' ? $owner->language : null );
                if ($this->_video->encode == 7) {
                    $notificationMessage = $translate->translate(sprintf(
                                    'Video conversion failed. You may be over the site upload limit.  Try %1$suploading%2$s a smaller file, or delete some files to free up space.', '', ''
                            ), $language);
                }
                Engine_Api::_()->getDbtable('notifications', 'activity')
                        ->addNotification($owner, $owner, $project, 'whmedia_processed_failed', array(
                            'message'      => $notificationMessage,
                            'message_link' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array('action' => 'index', 'project_id' => $project->project_id), 'whmedia_project', true),
                ));

                throw $e; // throw
            }
            $this->_video->size = json_encode($videoDimension);
            $this->_video->encode = 0;
            $this->_video->save();

            // delete the files from temp dir
            //unlink($originalPath);
            unlink($flvPath);
        }
    }

    private function encodeMp4($storageObject) {
        $project = $this->_video->getParent();
        $owner = $project->getOwner();
        $mp4Path = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_converted.mp4';
        if ($this->_settings->getSetting('hd_video_format', 0))
            $hdMp4Path = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_hdconverted.mp4';
        $ffmpeg_path = $this->getFfmpeg();
        $originalPath = $storageObject->temporary();


        $video_width = $this->_settings->getSetting('video_width', '320');
        $video_height = $this->_settings->getSetting('video_height', '240');
        $videoDimension = Engine_Api::_()->whmedia()->getVideoDimension($originalPath);
        $VideoEncodeDimension = $this->_getVideoEncodeDimension($video_width, $video_height, $videoDimension['width'], $videoDimension['height']);


        $videoCommand = $ffmpeg_path . ' '
                . '-i ' . escapeshellarg($originalPath)
                . ' -acodec aac -strict experimental -ac 2 -ab 128k '
                . " -s {$VideoEncodeDimension['width']}x{$VideoEncodeDimension['height']} "
                . ' -ac 2 -vcodec libx264 -crf 22 -threads 0 -f mp4 '
                . escapeshellarg($mp4Path) . ' 2>&1';
        // Execute video encode command
        $videoOutput_mp4 = $videoCommand . PHP_EOL .
                shell_exec($videoCommand);

        // Log
        if ($this->_log) {
            $this->_log->log(PHP_EOL . $this->getMediaInfo() . $videoOutput_mp4, Zend_Log::INFO);
        }

        if (APPLICATION_ENV == 'development') {
            file_put_contents($this->_tmpDir . '/' . $this->_video->media_id . '.mp4.txt', $videoOutput_mp4);
        }

        $params = array(
            'parent_id'      => $this->_video->getIdentity(),
            'parent_type'    => $this->_video->getType(),
            'user_id'        => $owner->getIdentity(),
            'type'           => 'video.html5',
            'parent_file_id' => $storageObject->file_id,
            'mime_major'     => 'video',
            'mime_minor'     => 'mp4'
        );

        $secondFileRow = Engine_Api::_()->storage()->create($mp4Path, $params);
        $secondFileRow->setFromArray(array(
                    'mime_major' => 'video',
                    'mime_minor' => 'mp4')
                )
                ->save();
        unlink($mp4Path);
        $this->_video->encode = 0;
        $this->_video->save();
    }

    private function generateThumbnail($storageObject) {

        $thumbPath = $this->_tmpDir . DIRECTORY_SEPARATOR . $this->_video->getIdentity() . '_vthumb.jpg';
        $ffmpeg_path = $this->getFfmpeg();
        $originalPath = $storageObject->temporary();
        $project = $this->_video->getParent();
        $owner = $project->getOwner();
        try {
            $duration_command = $ffmpeg_path . ' '
                    . '-i ' . escapeshellarg($originalPath) . ' 2>&1 '
                    . '| grep "Duration" '
                    . "| cut -d ' ' -f 4 | sed s/,// | sed 's@\..*@@g' | awk '{ split($1, A, \":\"); split(A[3], B, \".\"); print 3600*A[1] + 60*A[2] + B[1] }'";

            $duration = shell_exec($duration_command);

            // Log duration
            if ($this->_log) {
                $this->_log->log(PHP_EOL . $this->getMediaInfo() . 'Duration: ' . $duration, Zend_Log::INFO);
            }
            $this->_video->duration = $duration;
            $this->_video->save();
            // Fetch where to take the thumbnail
            $thumb_splice = $duration / 2;

            // Thumbnail proccess command
            $thumbCommand = $ffmpeg_path . ' '
                    . '-i ' . escapeshellarg($originalPath) . ' '
                    . '-an -r 1' . ' '
                    . '-ss ' . $thumb_splice . ' '
                    . '-t 00:00:01 -v 2' . ' '
                    . '-y ' . escapeshellarg($thumbPath) . ' '
                    . '2>&1'
            ;

            $output = PHP_EOL;
            $output .= $this->getMediaInfo();
            $output .= $originalPath . PHP_EOL;
            $output .= $thumbPath . PHP_EOL;

            // Process thumbnail
            $thumbOutput = $output .
                    $thumbCommand . PHP_EOL .
                    shell_exec($thumbCommand);

            // Log thumb output
            if ($this->_log) {
                $this->_log->log(PHP_EOL . $this->getMediaInfo() . $thumbOutput, Zend_Log::INFO);
            }

            // Check output message for success
            $thumbSuccess = true;
            if (preg_match('/video:0kB/i', $thumbOutput)) {
                $thumbSuccess = false;
            }
            if (!file_exists($thumbPath)) {
                $thumbSuccess = false;
            }
            $image_width = $this->_settings->getSetting('image_width', '600');
            $image_height = $this->_settings->getSetting('image_height', '900');
            // Resize thumbnail
            if ($thumbSuccess) {
                $image = Engine_Image::factory(array('quality' => 100));
                $image->open($thumbPath)
                        ->resize($image_width, $image_height)
                        ->write($thumbPath)
                        ->destroy();
            } else {
                $this->_log->log(PHP_EOL . $this->getMediaInfo() . PHP_EOL . 'Thumb Error. Thumb not created.', Zend_Log::INFO);
            }

            if ($thumbSuccess) {
                $params = array(
                    'parent_id'      => $this->_video->getIdentity(),
                    'parent_type'    => $this->_video->getType(),
                    'user_id'        => $owner->getIdentity(),
                    'name'           => $storageObject->name,
                    'mime_major'     => $storageObject->mime_major,
                    'mime_minor'     => strtolower($storageObject->mime_minor),
                    'type'           => 'thumb.etalon',
                    'parent_file_id' => $storageObject->file_id
                );
                Engine_Api::_()->storage()->create($thumbPath, $params);
            }
        } catch (Exception $e) {

            if ($thumbSuccess) {
                unlink($thumbPath);
            }

            throw $e; // throw
        }

        unlink($thumbPath);
    }

    protected function _process() {
        $settings = Engine_Api::_()->getApi('settings', 'core');
        try {
            $ffmpeg_path = $this->getFfmpeg();
            $flvtool2_path = $this->getFlvtool2();
            $faad_path = $this->getFaad();
        } catch (Exception $e) {
            $this->_video->encode = 8;
            $this->_video->save();
            throw $e;
        }
        // Update to encoding encode
        $this->_video->encode = 2;
        $this->_video->save();

        // Pull video from storage system for encoding
        $storageObject = $this->getStorageObject();

        if ($storageObject->mime_major == 'audio')
            $this->encodeAudio($storageObject);
        else {
            $this->generateThumbnail($storageObject);

            if ($this->_settings->getSetting('video_format', false) == 'both' || $this->_settings->getSetting('video_format', false) == 'flv')
                $this->encodeFlv($storageObject);
            if ($this->_settings->getSetting('video_format', false) == 'both' || $this->_settings->getSetting('video_format', false) == 'mp4')
                $this->encodeMp4($storageObject);

            $originalPath = $storageObject->temporary();
            unlink($originalPath);
        }
    }

    protected function _getVideoEncodeDimension($maxWidth, $maxHeight, $originalWidth, $originalHeight) {
        if ($originalHeight <= $maxHeight) {
            return array('width'  => $originalWidth,
                'height' => $originalHeight);
        } else {
            $outWidth = (int) (($maxHeight / $originalHeight) * $originalWidth);
            if (($outWidth % 2))
                $outWidth++;
            return array('width'  => $outWidth,
                'height' => $maxHeight);
        }
    }

    protected function _setState($state, $message = null, $doSave = true) {
        if (empty($this->_video)) {
            $message = 'Media ID: ' . $this->getParam('media_id', '-') . PHP_EOL . $message;
        } else {
            $message = 'User: ' . $this->_video->getOwner()->getTitle() . PHP_EOL .
                    'Project: ' . $this->_video->getProject()->getTitle() . PHP_EOL .
                    'Media ID: ' . $this->_video->getIdentity() . PHP_EOL .
                    $message . PHP_EOL .
                    'There was a problem with ffmpeg video processing. Check log file for details "temporary/log/whmedia.log"';
        }
        parent::_setState($state, $message, $doSave);
        return $this;
    }

}
