<?php

class Whmedia_Plugin_Menus {

    public function canCreateProjects() {
        // Must be logged in
        $viewer = Engine_Api::_()->user()->getViewer();
        if (!$viewer || !$viewer->getIdentity()) {
            return false;
        }

        // Must be able to create media projects
        if (!Engine_Api::_()->authorization()->isAllowed('whmedia_project', $viewer, 'create')) {
            return false;
        }

        return true;
    }

    public function canViewProjects() {
        $viewer = Engine_Api::_()->user()->getViewer();

        // Must be able to view media projects
        if (!Engine_Api::_()->authorization()->isAllowed('whmedia_project', $viewer, 'view')) {
            return false;
        }

        return true;
    }

    public function onMenuInitialize_WhmediaMainProject($row) {
        if (!Engine_Api::_()->core()->hasSubject('whmedia_project')) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $project = Engine_Api::_()->core()->getSubject('whmedia_project');

        if (!$project->isOwner($viewer)) {
            return false;
        }

        // Modify params
        $params = $row->params;
        $params['params']['project_id'] = $project->getIdentity();
        return $params;
    }

    public function onMenuInitialize_WhmediaManageprojectEdit($row) {
        if (!Engine_Api::_()->core()->hasSubject('whmedia_project')) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $project = Engine_Api::_()->core()->getSubject('whmedia_project');

        if (!$project->authorization()->isAllowed($viewer, 'edit'))
            return false;

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $params = $row->params;
        $params['params']['project_id'] = $project->getIdentity();
        return $params;
    }

    public function onMenuInitialize_WhmediaManageprojectDetails($row) {
        if (!Engine_Api::_()->core()->hasSubject('whmedia_project')) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $project = Engine_Api::_()->core()->getSubject('whmedia_project');

        if (!$project->authorization()->isAllowed($viewer, 'edit'))
            return false;

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $params = $row->params;
        $params['params']['project_id'] = $project->getIdentity();
        return $params;
    }

    public function onMenuInitialize_WhmediaManageprojectView() {
        if (!Engine_Api::_()->core()->hasSubject('whmedia_project')) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $project = Engine_Api::_()->core()->getSubject('whmedia_project');

        if (!$project->authorization()->isAllowed($viewer, 'view'))
            return false;

        $params['uri'] = $project->getHref();
        return $params;
    }

    public function onMenuInitialize_WhmediaManageprojectDel($row) {
        if (!Engine_Api::_()->core()->hasSubject('whmedia_project')) {
            return false;
        }

        $viewer = Engine_Api::_()->user()->getViewer();
        $project = Engine_Api::_()->core()->getSubject('whmedia_project');

        if (!$project->authorization()->isAllowed($viewer, 'edit'))
            return false;

        $request = Zend_Controller_Front::getInstance()->getRequest();

        $params = $row->params;
        $params['params']['project_id'] = $project->getIdentity();
        return $params;
    }

}