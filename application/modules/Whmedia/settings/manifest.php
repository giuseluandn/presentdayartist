<?php

return array(
    'package' =>
    array(
        'type' => 'module',
        'name' => 'whmedia',
        'version' => '4.8.8',
        'path' => 'application/modules/Whmedia',
        'title' => 'Media Plugin',
        'description' => 'Plugin for designers, photographers, artists and creatives that allows to upload images, videos, music and other files (download for other files).',
        'author' => 'WebHive Team',
        'meta' =>
        array(
            'title' => 'Media Plugin',
            'description' => 'Plugin for designers, photographers, artists and creatives that allows to upload images, videos, music and other files (download for other files).',
            'author' => 'WebHive Team',
        ),
        'callback' =>
        array(
            'path' => 'application/modules/Whmedia/settings/install.php',
            'class' => 'Whmedia_Installer',
        ),
        'actions' =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'dependencies' => array(
            array(
                'type' => 'module',
                'name' => 'core',
                'minVersion' => '4.2.2',
            ),
            array(
                'type' => 'module',
                'name' => 'whcore',
                'minVersion' => '4.7.0p2',
            ),
        ),
        'directories' =>
        array(
            0 => 'application/modules/Whmedia',
        ),
        'files' =>
        array(
            0 => 'application/languages/en/whmedia.csv',
        ),
    ),
    // Items ---------------------------------------------------------------------
    'items' => array(
        'whmedia_project',
        'whmedia_media'
    )
);
?>