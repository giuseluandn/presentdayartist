INSERT IGNORE INTO `engine4_core_menuitems`(`name`,`module`,`label`,`plugin`,`params`,`menu`,`submenu`,`enabled`,`custom`,`order`)
				    VALUES ('core_admin_main_plugins_whmedia','whmedia','Media Plugin','','{\"route\":\"admin_default\",\"module\":\"whmedia\",\"controller\":\"settings\"}','core_admin_main_plugins','',1,0,999);

INSERT IGNORE INTO `engine4_core_menuitems`(`name`,`module`,`label`,`plugin`,`params`,`menu`,`submenu`,`enabled`,`custom`,`order`)
				    VALUES ('whmedia_admin_main_manage','whmedia','View Media','','{\"route\":\"admin_default\",\"module\":\"whmedia\",\"controller\":\"manage\"}','whmedia_admin_main','',1,0,1);

INSERT IGNORE INTO `engine4_core_menuitems`(`name`,`module`,`label`,`plugin`,`params`,`menu`,`submenu`,`enabled`,`custom`,`order`)
				    VALUES ('whmedia_admin_main_settings','whmedia','Global Settings','','{\"route\":\"admin_default\",\"module\":\"whmedia\",\"controller\":\"settings\"}','whmedia_admin_main','',1,0,2);

INSERT IGNORE INTO `engine4_core_menuitems`(`name`,`module`,`label`,`plugin`,`params`,`menu`,`submenu`,`enabled`,`custom`,`order`)
				    VALUES ('whmedia_admin_main_level','whmedia','Member Level Settings','','{\"route\":\"admin_default\",\"module\":\"whmedia\",\"controller\":\"level\"}','whmedia_admin_main','',1,0,3);

INSERT IGNORE INTO `engine4_core_menuitems`(`name`,`module`,`label`,`plugin`,`params`,`menu`,`submenu`,`enabled`,`custom`,`order`)
				    VALUES ('whmedia_admin_main_categories','whmedia','Categories','','{\"route\":\"admin_default\",\"module\":\"whmedia\",\"controller\":\"settings\", \"action\":\"categories\"}','whmedia_admin_main','',1,0,4),
                                           ('core_main_whmedia','whmedia','Media','','{\"route\":\"whmedia_default\"}','core_main','',1,0,999),
                                           ('mobi_browse_whmedia','whmedia','Media','','{\"route\":\"whmedia_default\"}','mobi_browse','',1,0,999);

INSERT IGNORE into `engine4_activity_notificationtypes`(`type`,`module`,`body`,`is_request`,`handler`,`default`)
                                                values ('whmedia_processed_failed','whmedia','Your media file has failed to process in {item:$object:$label}.',0,'',1);

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'auth_comment' as `name`,
    5 as `value`,
    '[\"everyone\",\"registered\",\"owner_network\",\"owner_member_member\",\"owner_member\",\"owner\"]' as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'auth_view' as `name`,
    5 as `value`,
    '[\"everyone\",\"registered\",\"owner_network\",\"owner_member_member\",\"owner_member\",\"owner\"]' as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'comment' as `name`,
    1 as `value`,
    null as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'create' as `name`,
    1 as `value`,
    null as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'file_type' as `name`,
    5 as `value`,
    '[\"image\",\"video\",\"audio\"]' as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'medias_count' as `name`,
    3 as `value`,
    '100' as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'save_original' as `name`,
    1 as `value`,
    null as `params`
  FROM `engine4_authorization_levels` WHERE `type` != 'public';

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'view' as `name`,
    1 as `value`,
    null as `params`
  FROM `engine4_authorization_levels`;

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'delete' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
  INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'edit' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');

  INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'delete' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'edit' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');

insert IGNORE into `engine4_core_jobtypes`(`title`,`type`,`module`,`plugin`,`form`,`enabled`,`priority`,`multi`)
                                   values ('WhMedia Encode','whmedia_encode','whmedia','Whmedia_Plugin_Job_Encode',NULL,1,75,2);

insert IGNORE into `engine4_core_mailtemplates`(`type`,`module`,`vars`)
                                        values ('notify_whmedia_processed_failed','whmedia','[host],[email],[recipient_title],[recipient_link],[recipient_photo],[sender_title],[sender_link],[sender_photo],[object_title],[object_link],[object_photo],[object_description]');

CREATE TABLE `engine4_whmedia_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(128) NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '999',
  PRIMARY KEY (`category_id`),
  KEY `order` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

insert IGNORE into `engine4_whmedia_categories`(`category_name`,`order`) values ('Default Category',1);

CREATE TABLE `engine4_whmedia_medias` (
  `media_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `encode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `invisible` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `duration` int(10) unsigned DEFAULT NULL,
  `size` varchar(128) DEFAULT NULL,
  `is_text` tinyint(1) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `media_id` (`media_id`),
  KEY `project_id` (`project_id`),
  KEY `order` (`order`),
  KEY `encode` (`encode`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `engine4_whmedia_projects` (
  `project_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `project_views` int(11) NOT NULL DEFAULT '0',
  `owner_type` varchar(64) CHARACTER SET utf8 NOT NULL,
  `search` int(1) NOT NULL DEFAULT '1',
  `cover_file_id` int(11) unsigned DEFAULT NULL,
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0',
  `is_published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`project_id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `cover_file_id` (`cover_file_id`),
  KEY `project_views` (`project_views`),
  CONSTRAINT `FK_engine4_whmedia_projects_cover` FOREIGN KEY (`cover_file_id`) REFERENCES `engine4_whmedia_medias` (`media_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_engine4_whmedia_projects_user` FOREIGN KEY (`user_id`) REFERENCES `engine4_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `engine4_whmedia_medias` ADD CONSTRAINT `FK_engine4_whmedia_medias` FOREIGN KEY (`project_id`) REFERENCES `engine4_whmedia_projects` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`, `body`, `enabled`, `displayable`, `attachable`, `commentable`, `shareable`, `is_generated`)
                                           VALUES ('whmedia_media_new', 'whmedia', '{item:$subject} added new media files to the project {item:$object}:', 1, 5, 1, 3, 1, 1);

INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`, `body`, `enabled`, `displayable`, `attachable`, `commentable`, `shareable`, `is_generated`)
                                           VALUES ('whmedia_project_publish', 'whmedia', '{item:$subject} published a new project {item:$object}:', 1, 5, 1, 3, 1, 1);

INSERT IGNORE INTO `engine4_core_menus` (`name`, `type`, `title`) 
                                 VALUES ('whmedia_main', 'standard', 'Media Plugin Main Navigation Menu');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`)
                                     VALUES ('whmedia_main_browse', 'whmedia', 'Browse All', 'Whmedia_Plugin_Menus::canViewProjects', '{"route":"whmedia_default"}', 'whmedia_main', '', 1),
                                            ('whmedia_main_manage', 'whmedia', 'My Projects', 'Whmedia_Plugin_Menus::canCreateProjects', '{"route":"whmedia_default","action":"manage"}', 'whmedia_main', '', 2),
                                            ('whmedia_main_create', 'whmedia', 'Create a Project', 'Whmedia_Plugin_Menus::canCreateProjects', '{"route":"whmedia_default","controller":"project","action":"create"}', 'whmedia_main', '', 3),
                                            ('whmedia_main_project', 'whmedia', 'Manage Project', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project"}', 'whmedia_main', '', 4),
                                            ('whmedia_manageproject_edit', 'whmedia', 'Edit Project', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project","controller":"project","action":"index","project_id":"0"}', 'whmedia_manageproject', '', 1),
                                            ('whmedia_manageproject_details', 'whmedia', 'Details', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project","controller":"project","action":"edit","project_id":"0"}', 'whmedia_manageproject', '', 2),
                                            ('whmedia_manageproject_view', 'whmedia', 'View Project', 'Whmedia_Plugin_Menus', '', 'whmedia_manageproject', '', 3),
                                            ('whmedia_manageproject_del', 'whmedia', 'Delete Project', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project","controller":"project","action":"delproject","project_id":"0"}', 'whmedia_manageproject', '', 4);


INSERT IGNORE INTO `engine4_core_tasks` (`title`, `module`, `plugin`, `timeout`)
                                 VALUES ('Cleanup temporary files', 'whmedia', 'Whmedia_Plugin_Task_CleanupTemporary', 36000);


INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES
                                                             ('media.per.page', 15),
                                                             ('image.height', 900),
                                                             ('image.width', 600),
                                                             ('pdf.width', 200),
                                                             ('pdf.height', 794),
                                                             ('thumb_width', 200),
                                                             ('thumb_height', 156),
                                                             ('url.main.world', 'whmedia'),
                                                             ('arrow.sliding', 1),
                                                             ('mime_info_method', 1),
                                                             ('video.height', 240),
                                                             ('video.width', 320),
                                                             ('video.format', 'none');