-- admin

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'delete' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
  INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'edit' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
 
  -- user

  INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'delete' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'whmedia_project' as `type`,
    'edit' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `enabled`, `custom`, `order`) VALUES 
                                    ('whmedia_manageproject_edit', 'whmedia', 'Edit Project', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project","controller":"project","action":"index","project_id":"0"}', 'whmedia_manageproject', 1, 0, 1),
                                    ('whmedia_manageproject_details', 'whmedia', 'Details', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project","controller":"project","action":"edit","project_id":"0"}', 'whmedia_manageproject', 1, 0, 2),
                                    ('whmedia_manageproject_view', 'whmedia', 'View Project', 'Whmedia_Plugin_Menus', '', 'whmedia_manageproject', 1, 0, 3),
                                    ('whmedia_manageproject_del', 'whmedia', 'Delete Project', 'Whmedia_Plugin_Menus', '{"route":"whmedia_project","controller":"project","action":"delproject","project_id":"0"}', 'whmedia_manageproject', 1, 0, 3);

