<?php if( $this->paginator->getTotalItemCount() > 0 ):
        $setting_width = Engine_Api::_()->getApi('settings', 'core')->getSetting('thumb_width', '100');
?>
    <?php foreach( $this->paginator as $whmedia ):?>
        <div class="media-browse-box" style="width:<?php echo $setting_width ?>px;">
            <?php if (!$whmedia->is_published): ?>
                <div class="media-proj-draft"><?php echo $this->translate('Draft')?></div>
            <?php endif; ?>
            <div class="pulldown <?php echo ($this->isMobile) ? 'm_proj_settings_mobile' : 'm_proj_settings' ?>">
                <div class="pulldown_contents_wrapper">
                    <div class="pulldown_contents">
                            <ul>
                            <li><?php if (!$this->isApple)
                                        echo $this->htmlLink(array('route' => 'whmedia_project', 'project_id' => $whmedia->getIdentity()), $this->translate('Manage Project'), array('class' => 'manage' ))?>
                            </li>
                            <li>
                                <?php echo $this->htmlLink(array('route' => 'whmedia_project', 'action' => 'delproject', 'project_id' => $whmedia->getIdentity()), $this->translate("Delete Project"), array('class' => 'smoothbox buttonlink icon_delete_media')) ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php echo $this->htmlLink($whmedia->getHref(), $this->htmlImage($whmedia->getPhotoUrl(null, false, false),
                                                                             array('alt' => $this->translate('Project Thumb'))), array('class' => 'media-browse-img')); ?>
            <?php echo $this->htmlLink($whmedia->getHref(), $this->whtruncate($whmedia->getTitle()), array('class' => 'media-browse-title')); ?>
            <div class="media-info">
                <span class="media-files">
                    <?php $MediasCount = $whmedia->count_media;
                          echo $this->translate(array("%d file", "%d files",$MediasCount), $MediasCount);
                    ?>
                </span>
                |
                <span class="media-views">
                    <?php echo $this->translate('whViews: %d', $whmedia->project_views)?>
                </span>
                |
                <span class="media-likes"><?php echo $whmedia->likes()->getLikeCount(); ?></span>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif;?>