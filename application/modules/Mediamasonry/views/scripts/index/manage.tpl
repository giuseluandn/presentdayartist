<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whmedia/externals/scripts/whmedia_core.js')
                         ->appendFile($this->baseUrl() . '/application/modules/Mediamasonry/externals/scripts/mooMasonry.js')?>
<?php
    $setting_width = Engine_Api::_()->getApi('settings', 'core')->getSetting('thumb_width', '100');
    $script = <<<EOF
            window.addEvent('load', function(){
                $('media-browse').masonry({
			singleMode: true,
			itemSelector: '.media-browse-box'
		});

            });
            var max_projects_page = {$this->paginator->count()};
            var current_projects_page = 1;
EOF;
    $this->headScript()->appendScript($script, $type = 'text/javascript', $attrs = array());
?>
<?php
    if ($this->isMobile) {
        $script .= <<<EOF
                en4.core.runonce.add(function() {                                                    
                    $$("div.m_proj_settings_mobile").addEvent('click', function(i) {
                        i.target.toggleClass('active');
                    });
                });
EOF;
    }
    $this->headScript()->appendScript($script, $type = 'text/javascript', $attrs = array());
?>
<?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <div id="media-browse">
        <?php foreach( $this->paginator as $whmedia ):?>
            <div class="media-browse-box" style="width:<?php echo $setting_width ?>px;">
                <?php if (!$whmedia->is_published): ?>
                    <div class="media-proj-draft"><?php echo $this->translate('Draft')?></div>
                <?php endif; ?>
                <div class="pulldown <?php echo ($this->isMobile) ? 'm_proj_settings_mobile' : 'm_proj_settings' ?>">
                    <div class="pulldown_contents_wrapper">
                        <div class="pulldown_contents">
                                <ul>
                                <li><?php if (!$this->isApple)
                                            echo $this->htmlLink(array('route' => 'whmedia_project', 'project_id' => $whmedia->getIdentity()), $this->translate('Manage Project'), array('class' => 'manage' ))?>
                                </li>
                                <li>
                                    <?php echo $this->htmlLink(array('route' => 'whmedia_project', 'action' => 'delproject', 'project_id' => $whmedia->getIdentity()), $this->translate("Delete Project"), array('class' => 'smoothbox buttonlink icon_delete_media')) ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php echo $this->htmlLink($whmedia->getHref(), $this->htmlImage($whmedia->getPhotoUrl(null, false, false),
                                                                                 array('alt' => $this->translate('Project Thumb'))), array('class' => 'media-browse-img')); ?>
                <?php echo $this->htmlLink($whmedia->getHref(), $this->whtruncate($whmedia->getTitle()), array('class' => 'media-browse-title')); ?>
                <div class="media-info">
                    <span class="media-files">
                        <?php $MediasCount = $whmedia->count_media;
                              echo $this->translate(array("%d file", "%d files",$MediasCount), $MediasCount);
                        ?>
                    </span>
                    |
                    <span class="media-views">
                        <?php echo $this->translate('whViews: %d', $whmedia->project_views)?>
                    </span>
                    |
                    <span class="media-likes"><?php echo $whmedia->likes()->getLikeCount(); ?></span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if( $this->paginator->count() > 1): ?>
        <div class="projects_viewmore" id="projects_viewmore">
          <?php echo $this->htmlLink('javascript:void(0);', $this->translate('View More'), array(
                                                                                                'id' => 'projects_viewmore_link',
                                                                                                'class' => 'buttonlink icon_viewmore',
                                                                                                'onclick' => 'javascript:projects_viewmore(null, null, '. (int)$this->isMobile . ');'
                                                                                                )) ?>
        </div>

        <div class="projects_viewmore" id="projects_loading" style="display: none;">
          <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left;margin-right: 5px;' />
          <?php echo $this->translate("Loading ...") ?>
        </div>
    <?php endif; ?>

<?php elseif( $this->category || $this->show == 2 || $this->search  ):?>
    <div class="tip">
      <span>
        <?php echo $this->translate('Projects were not posted in this criterias yet.');?>
      </span>
    </div>
  <?php else:?>
    <div class="tip">
      <span>
        <?php echo $this->translate('You have not create projects yet.'); ?>
        <?php if ($this->can_create): ?>
          <?php echo $this->translate('You can %1$spost%2$s a new one!', '<a href="'.$this->url(array('controller' => 'project', 'action' => 'create'), 'whmedia_default', true).'">', '</a>'); ?>
        <?php endif; ?>
      </span>
    </div>
  <?php endif; ?>