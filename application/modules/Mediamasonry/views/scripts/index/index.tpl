<?php $this->headScript()->appendFile($this->baseUrl() . '/application/modules/Whmedia/externals/scripts/whmedia_core.js')
                         ->appendFile($this->baseUrl() . '/application/modules/Mediamasonry/externals/scripts/mooMasonry.js')?>
<?php
    $setting_width = Engine_Api::_()->getApi('settings', 'core')->getSetting('thumb_width', '100');
    $script = <<<EOF
            window.addEvent('load', function(){
                $('media-browse').addEvent('masoned', function (){\$('media-browse').setStyle('opacity', 1);}).masonry({
			singleMode: true,
			itemSelector: '.media-browse-box'                       
		});

            });
            var max_projects_page = {$this->paginator->count()};
            var current_projects_page = 1;
EOF;
    $this->headScript()->appendScript($script, $type = 'text/javascript', $attrs = array());
?>
  
<?php if (isset($this->owner)): ?>
 <h2>
    <?php echo $this->translate('%1$s\'s Projects', $this->htmlLink($this->owner->getHref(), $this->owner->getTitle()))?>
  </h2>
<?php endif; ?>

  <?php if( $this->paginator->getTotalItemCount() > 0 ): ?>
    <div id="media-browse" style="opacity:0;">
        <?php foreach( $this->paginator as $whmedia ):?>
            <div class="media-browse-box" style="width:<?php echo $setting_width ?>px;">
                <?php echo $this->htmlLink($whmedia->getHref(), $this->htmlImage($whmedia->getPhotoUrl(null, false, false),
                                                                                 array('alt' => $this->translate('Project Thumb'))), array('class' => 'media-browse-img')); ?>
                <?php echo $this->htmlLink($whmedia->getHref(), $this->whtruncate($whmedia->getTitle()), array('class' => 'media-browse-title')); ?> 
                <p class="media-author">
                    <?php echo $this->translate('by %s', $whmedia->getOwner()->toString()) ?>
                </p>   
                <div class="media-info">
                    <span class="media-files">
                        <?php $MediasCount = $whmedia->count_media;
                              echo $this->translate(array("%d file", "%d files",$MediasCount), $MediasCount);
                        ?>
                    </span>
                    |
                    <span class="media-views">
                        <?php echo $this->translate('whViews: %d', $whmedia->project_views)?>
                    </span>
                    |
                    <span class="media-likes"><?php echo $whmedia->likes()->getLikeCount(); ?></span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
  <?php if( $this->paginator->count() > 1): ?>
    <div class="projects_viewmore" id="projects_viewmore">
      <?php echo $this->htmlLink('javascript:void(0);', $this->translate('View More'), array(
                                                                                            'id' => 'projects_viewmore_link',
                                                                                            'class' => 'buttonlink icon_viewmore',
                                                                                            'onclick' => 'javascript:projects_viewmore();'
                                                                                            )) ?>
    </div>

    <div class="projects_viewmore" id="projects_loading" style="display: none;">
      <img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left;margin-right: 5px;' />
      <?php echo $this->translate("Loading ...") ?>
    </div>
  <?php endif; ?>

<?php elseif( $this->category || $this->show == 2 || $this->search  ):?>
    <div class="tip">
      <span>
        <?php echo $this->translate('Nobody has posted a project in this criteria yet.');?>
      </span>
    </div>
  <?php else:?>
    <div class="tip">
      <span>
        <?php echo $this->translate('Nobody has written a projects yet.'); ?>
        <?php if ($this->can_create): // @todo check if user is allowed to create a poll ?>
          <?php echo $this->translate('Be the first to %1$swrite%2$s one!', '<a href="'.$this->url(array('controller' => 'project', 'action' => 'create'), 'whmedia_default', true).'">', '</a>'); ?>
        <?php endif; ?>
      </span>
    </div>
  <?php endif; ?>