<?php if( $this->paginator->getTotalItemCount() > 0 ):
        $setting_width = Engine_Api::_()->getApi('settings', 'core')->getSetting('thumb_width', '100');
?>
    <?php foreach( $this->paginator as $whmedia ):?>
        <div class="media-browse-box" style="width:<?php echo $setting_width ?>px;">
            <?php echo $this->htmlLink($whmedia->getHref(), $this->htmlImage($whmedia->getPhotoUrl(null, false, false),
                                                                             array('alt' => $this->translate('Project Thumb'))), array('class' => 'media-browse-img')); ?>
            <?php echo $this->htmlLink($whmedia->getHref(), $this->whtruncate($whmedia->getTitle()), array('class' => 'media-browse-title')); ?>
            <p class="media-author">
                <?php echo $this->translate('by %s', $whmedia->getOwner()->toString()) ?>
            </p>
            <div class="media-info">
                <span class="media-files">
                    <?php $MediasCount = $whmedia->count_media;
                          echo $this->translate(array("%d file", "%d files",$MediasCount), $MediasCount);
                    ?>
                </span>
                |
                <span class="media-views">
                    <?php echo $this->translate('whViews: %d', $whmedia->project_views)?>
                </span>
                |
                <span class="media-likes"><?php echo $whmedia->likes()->getLikeCount(); ?></span>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif;?>