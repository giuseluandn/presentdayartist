<?php
Engine_Loader::autoload('application_modules_Whmedia_controllers_IndexController');
class Mediamasonry_IndexController extends Whmedia_IndexController {

    public function  postDispatch() {
        parent::postDispatch();
        $this->view->form->removeElement('page');
        $this->view->form->removeElement('orderby');
        if ($this->_helper->contextSwitch->getCurrentContext()  == 'html') {
            $this->_helper->content->setEnabled(false);
            $this->_helper->viewRenderer($this->getRequest()->getActionName() . '-ajax');
        }
    }
}
