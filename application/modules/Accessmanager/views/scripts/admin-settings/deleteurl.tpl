<form method="post" class="global_form_popup">
  <div>
    <h3><?php echo $this->translate("Delete URL?") ?></h3>
    <p>
      <?php echo $this->translate("Are you sure that you want to delete this Url? It will not be recoverable after being deleted.") ?>
    </p>
    <br />
    <p>
      <input type="hidden" name="confirm" value="<?php echo $this->id?>"/>
      <button type='submit'><?php echo $this->translate("Delete") ?></button>
      <?php echo Zend_Registry::get('Zend_Translate')->_(' or ') ?>
      <a href='javascript:void(0)' onclick="parent.Smoothbox.close()">
      <?php echo $this->translate("cancel") ?></a>
    </p>
  </div>
</form>

