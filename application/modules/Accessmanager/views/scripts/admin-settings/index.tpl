<h2>Access Manager</h2>
<?php if (count($this->navigation)): ?>
    <div class='tabs'>
        <?php
        // Render the menu
        //->setUlClass()
        echo $this->navigation()->menu()->setContainer($this->navigation)->render()
        ?>
    </div>
<?php endif; ?>

<?php
$this->form->setTitle('Access Manager Settings');
?>
<div class='settings'>
    <?php echo $this->form->render($this); ?>
</div>