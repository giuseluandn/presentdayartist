<script type="text/javascript">
    function toggleExceptions() {
        var exclude_all = $('exclude_all').checked;

        if (exclude_all) {
            $('exceptions').disabled = false;
        } else {
            $('exceptions').disabled = true;
        }
    }

    en4.core.runonce.add(function() {
        toggleExceptions();
        $('exclude_all-wrapper').hide();
        $('exceptions-wrapper').hide();

        var url = '<?php echo $this->url(array('module' => 'accessmanager', 'controller' => 'settings', 'action' => 'getroute'), 'admin_default', true) ?>';
        en4.core.request.send(new Request.HTML({
            url: url,
            data: {
                format: 'html',
                url: $('url').value
            },
            onComplete: function(a, b, c) {
                if (c == '<span>' + $('url').value + '</span>') {
                    $('exceptions').disabled = true;
                    $('exceptions').value = '';

                    $('exclude_all-wrapper').hide();
                    $('exceptions-wrapper').hide();

                    $('exclude_all').checked = false;
                } else {
                    $('exclude_all-wrapper').show();
                    $('exceptions-wrapper').show();
                    parent.window.Smoothbox.instance.doAutoResize();
                }
            }
        }), {
            'element': $('example_link')
        });
    });
</script>
<?php echo $this->form ?>