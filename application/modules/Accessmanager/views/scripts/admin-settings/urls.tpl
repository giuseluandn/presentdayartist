<h2>Access Manager</h2>
<?php if (count($this->navigation)): ?>
    <div class='tabs'>
        <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
    </div>
<?php endif; ?>
<p>
    <?php echo $this->translate('ACESSMANAGER_MANAGE_URLS_DESC') ?>
</p>
<a class="buttonlink admin_access_manager_addquestion smoothbox" href="<?php echo $this->url(array('action' => 'addurl')) ?>"><?php echo $this->translate('Add URL') ?></a><br/>

<?php if (count($this->urls) > 0) : ?>
<table id="access_manage_urls" class="admin_table">
    <thead>
        <th><?php echo $this->translate('URL') ?></th>
        <th><?php echo $this->translate('Options') ?></th>
    </thead>
    <tbody>
        <?php foreach ($this->urls as $url) : ?>
            <tr>
                <td><?php echo $url->url ?></td>
                <td>
                    <?php echo $this->htmlLink($this->url(array('action' => 'editurl', 'id' => $url->getIdentity())), $this->translate('edit'), array('class' => 'smoothbox')); ?>
                    &nbsp;|&nbsp;
                    <?php echo $this->htmlLink($this->url(array('action' => 'deleteurl', 'id' => $url->getIdentity())), $this->translate('delete'), array('class' => 'smoothbox')); ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>    
</table>
<?php endif; ?>