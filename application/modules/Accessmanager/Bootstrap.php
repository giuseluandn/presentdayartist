<?php

class Accessmanager_Bootstrap extends Engine_Application_Bootstrap_Abstract {

    protected function _initPlugins() {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Accessmanager_Plugin_Core());
    }
}