<?php

class Accessmanager_Plugin_Core extends Zend_Controller_Plugin_Abstract {

    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        $frontController = Zend_Controller_Front::getInstance();
        $route = $frontController->getRouter()->getCurrentRouteName();
        $pathinfo = $request->getPathInfo();
        $currUrl = $url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getRequestUri();

        $settings = Engine_Api::_()->getApi('settings', 'core');
        if (!$settings->getSetting('accessmanager_enable', 0))
            return;

        $path_ok = array('admin', 'login', 'utility/tasks', 'signup', 'user/auth/forgot', 'auth/reset/code', 'payment/subscription', 'help', '/');
        foreach ($path_ok as $value_path) {
            if (strpos($pathinfo, $value_path))
                return;
        }

        $url = 'http' . (_ENGINE_SSL ? 's' : '') . '://' . $request->getHttpHost() . $request->getRequestUri();
        $urlsTable = Engine_Api::_()->getDbTable('urls', 'accessmanager');
        $select = $urlsTable->select();
        $select->where('url = ?', $url);
        $url = $urlsTable->fetchRow($select);

        $viewer = Engine_Api::_()->user()->getViewer();

        if ($url) {
            if ($url->allowedToView($viewer))
                return;
            $response = $frontController->getResponse();
            if (!$viewer->getIdentity())
                return $request->setActionName('requireauth')
                                ->setControllerName('error')
                                ->setModuleName('core');
            else
                return $response->setRedirect(Zend_Registry::get('StaticBaseUrl') . 'no-access');
        } else {
            $select = $urlsTable->select();
            $select->where('route = ?', $route);

            foreach ($urlsTable->fetchAll($select) as $url) {
                if (!$url->exclude_all)
                    continue;

                if (count($url->exceptions) > 0) {

                    foreach ($url->exceptions as $exception) {
                        if ($currUrl == $exception)
                            return;
                    }
                }

                if ($url->route == $route && $url->route != 'default') {
                    if ($url->allowedToView($viewer))
                        return;

                    $response = $frontController->getResponse();
                    if (!$viewer->getIdentity())
                        return $request->setActionName('requireauth')
                                        ->setControllerName('error')
                                        ->setModuleName('core');
                    else
                        return $response->setRedirect(Zend_Registry::get('StaticBaseUrl') . 'no-access');
                }
            }
        }
    }

}