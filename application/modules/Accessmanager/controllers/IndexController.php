<?php

class Accessmanager_IndexController extends Core_Controller_Action_Standard {

    public function indexAction() {
        $this->_helper->content
                ->setNoRender()
                ->setEnabled();
    }

}
