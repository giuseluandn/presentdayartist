<?php

class Accessmanager_AdminSettingsController extends Core_Controller_Action_Admin {

    public function indexAction() {
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('accessmanager_admin_main', array(), 'accessmanager_admin_main_settings');

        $this->view->form = $form = new Accessmanager_Form_Admin_Settings_General();

        $settings = Engine_Api::_()->getApi('settings', 'core');
        $form->populate(array('accessmanager_enable' => $settings->getSetting('accessmanager_enable', 0)));

        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $values = $form->getValues();

        $settings->accessmanager_enable = $values['accessmanager_enable'];

        $form->addNotice('Your changes have been saved.');
    }

    public function urlsAction() {
        $this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
                ->getNavigation('accessmanager_admin_main', array(), 'accessmanager_admin_main_urls');

        $this->view->urls = Engine_Api::_()->getDbTable('urls', 'accessmanager')->fetchAll();
    }

    public function addurlAction() {
        $this->view->form = $form = new Accessmanager_Form_Admin_Settings_Url();
        $form->setTitle('Add URL');

        $form->url->addValidator('Db_NoRecordExists', true, array('table' => 'engine4_accessmanager_urls', 'field' => 'url'));

        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $values = $form->getValues();

        $request = new Zend_Controller_Request_Http($values['url']);

        $frontController = Zend_Controller_Front::getInstance();

        $router = $frontController->getRouter();

        $router->route($request);
        $params = $request->getParams();
        $params = array_diff($params, $values);
        unset($params['rewrite'], $params['submit'], $params['format'], $params['levels']);

        $values['params'] = $params;
        $values['route'] = $router->getCurrentRouteName();
        if ($values['levels'])
            $values['levels'] = Zend_Json_Encoder::encode(str_replace(', ', '', $values['levels']));

        if ($values['exceptions'])
            $values['exceptions'] = explode(',', str_replace(', ', ',', $values['exceptions']));

        $urlsTable = Engine_Api::_()->getDbTable('urls', 'accessmanager');
        $row = $urlsTable->createRow();
        $row->setFromArray($values);
        $row->save();

        return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'smoothboxClose' => true,
                    'messages' => array($this->view->translate('Url has been successfully added.'))
        ));
    }

    public function editurlAction() {
        $this->view->form = $form = new Accessmanager_Form_Admin_Settings_Url();
        $form->submit->setLabel($this->view->translate('Save Changes'));
        $form->setTitle('Edit Url');
        $form->url->setAttrib('readOnly', true);
        $form->removeElement('checkUrl');
        $form->submit->setAttrib('disabled', null);

        $row = Engine_Api::_()->getItem('accessmanager_url', $this->_getParam('id', false));

        if (!$row)
            return $this->_forward('success', 'utility', 'core', array(
                        'parentRefresh' => true,
                        'smoothboxClose' => true,
            ));

        $form->populate($row->toArray());
        if ($row->exceptions)
            $form->exceptions->setValue(implode(',', $row->exceptions));

        $levels = $form->getElement('levels');
        if ($levels && !empty($row->levels)) {
            $levels->setValue(Zend_Json_Decoder::decode($row->levels));
        } else if ($levels) {
            $levels->setValue(array_keys($levels->getMultiOptions()));
        }

        if (!$this->getRequest()->isPost()) {
            return;
        }
        if (!$form->isValid($this->getRequest()->getPost())) {
            return;
        }

        $values = $form->getValues();

        if ($values['levels'])
            $values['levels'] = Zend_Json_Encoder::encode($values['levels']);

        $request = new Zend_Controller_Request_Http($values['url']);

        $frontController = Zend_Controller_Front::getInstance();

        $router = $frontController->getRouter();

        $router->route($request);
        $params = $request->getParams();
        $params = array_diff($params, $values);
        unset($params['rewrite'], $params['submit'], $params['format'], $params['levels']);

        $values['params'] = $params;
        $values['route'] = $router->getCurrentRouteName();
        if ($values['exceptions']) {
            foreach ($urls = explode(',', $values['exceptions']) as $key => $url) {
                if (!Zend_Uri::check($url)) {
                    $form->addError($this->view->translate('%s is not a valid URL.', $url));
                    return;
                }

                $request = new Zend_Controller_Request_Http($url);
                $frontController = Zend_Controller_Front::getInstance();
                $router = $frontController->getRouter();
                $router->route($request);

                if ($row->route != $router->getCurrentRouteName()) {
                    $form->addError($this->view->translate('%s is not a part of this section.', $url));
                    return;
                }
            }
        }

        $values['exceptions'] = $urls;

        $row->setFromArray($values);
        $row->save();

        return $this->_forward('success', 'utility', 'core', array(
                    'parentRefresh' => true,
                    'smoothboxClose' => true,
                    'messages' => array($this->view->translate('Your changes have been saved.'))
        ));
    }

    public function deleteurlAction() {
        $url = Engine_Api::_()->getItem('accessmanager_url', $this->_getParam('id', false));
        $this->view->id = $url->getIdentity();

        if ($this->getRequest()->isPost() && $url) {
            $url->delete();

            return $this->_forward('success', 'utility', 'core', array(
                        'parentRefresh' => true,
                        'smoothboxClose' => true,
                        'messages' => array($this->view->translate('Url has been deleted.')),
            ));
        }
    }

    public function getrouteAction() {
        $url = $this->_getParam('url', false);

        if ($url) {
            try {
                $request = new Zend_Controller_Request_Http($url);
                $frontController = Zend_Controller_Front::getInstance();
                $router = $frontController->getRouter();

                $router->route($request);
                $params = $request->getParams();

                if ($router->getCurrentRoute() instanceof Zend_Controller_Router_Route)
                    $variables = $router->getCurrentRoute()->getVariables();
                else {
                    $this->view->url = $params['url'];
                    return;
                }

                foreach ($params as $key => $value) {
                    if (in_array($key, $variables))
                        $params['url'] = str_replace('/' . $value, '/XX', $params['url']);
                }
                
                $this->view->url = $params['url'];
            } catch (Exception $e) {
                //Silent
            }
        }
    }

}