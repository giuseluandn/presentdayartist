<?php

class Accessmanager_Form_Admin_Settings_General extends Engine_Form {

    public function init() {

        $this->addElement('Radio', 'accessmanager_enable', array(
            'label' => 'Enable Access Manager',
            'description' => 'Do you want to enable access manager?',
            'multiOptions' => array(
                1 => 'Yes, enable access manager.',
                0 => 'No.'
            )
        ));

        $this->addElement('Button', 'submit', array(
            'label' => 'Save Changes',
            'type' => 'submit',
            'ignore' => true
        ));
    }

}