<?php

class Accessmanager_Form_Admin_Settings_Url extends Engine_Form {

    public function init() {
        $this->setAttrib('class', 'global_form_popup');

        $this->addElement('Text', 'url', array(
            'label' => 'URL',
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                new Accessmanager_Validate_Url(),
            )
        ));

        $this->addElement('dummy', 'checkUrl', array(
            'content' => '<button onclick="checkUrl(); return false;">' . Zend_Registry::get('Zend_Translate')->_('Check Url') . '</button>'
        ));

        $this->addElement('checkbox', 'exclude_all', array(
            'label' => 'Block the whole section',
            'onclick' => 'toggleExceptions()',
        ));

        $this->addElement('Textarea', 'exceptions', array(
            'label' => 'Exceptions',
                //'description' => '<small>For example, add any URL of a section you want to allow users to view: http://yourwebsite.com/</small><br />Comma separated list'
        ));
        $description = Zend_Registry::get('Zend_Translate')
                ->_('<small>For example, add any URL of a section you want to allow users to view: ' . '<span id="example_link">http://%s</span></small><br />Comma separated list');
        $description = sprintf($description, $_SERVER['HTTP_HOST']
                . Zend_Controller_Front::getInstance()->getRouter()
                        ->assemble(array(), 'default'));
        $this->exceptions->getDecorator('Description')->setEscape(false);
        $this->exceptions->setDescription($description);

        $levels = Engine_Api::_()->getDbtable('levels', 'authorization')->fetchAll();
        foreach ($levels as $level) {
            $levels_prepared[$level->getIdentity()] = $level->getTitle();
        }
        reset($levels_prepared);
        //removeing super admins
        unset($levels_prepared[1]);

        $this->addElement('MultiCheckbox', 'levels', array(
            'label' => 'Viewing Permissions',
            'multiOptions' => $levels_prepared,
        ));

        $this->addElement('Button', 'submit', array(
            'label' => 'Create Rule',
            'type' => 'submit',
            'ignore' => true,
            'decorators' => array('ViewHelper'),
            'disabled' => 'disabled'
        ));

        $this->addElement('Cancel', 'cancel', array(
            'label' => 'cancel',
            'link' => true,
            'prependText' => ' or ',
            'href' => '',
            'onclick' => 'parent.Smoothbox.close();',
            'decorators' => array('ViewHelper')
        ));
        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
    }

}