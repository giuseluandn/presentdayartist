INSERT IGNORE INTO `engine4_core_settings` (`name`, `value`) VALUES ('accessmanager.enable', 1);

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, label, plugin, params, menu, submenu, enabled, custom, `order`)
                                   VALUES ('core_admin_main_plugins_accessmanager', 'accessmanager', 'Access Manager', '', '{"route":"admin_default","module":"accessmanager","controller":"settings"}', 'core_admin_main_plugins', '', 1, 0, 999),
                                          ('accessmanager_admin_main_settings', 'accessmanager', 'General Settings', '', '{"route":"admin_default","module":"accessmanager","controller":"settings"}', 'accessmanager_admin_main', '', 1, 0, 999),
                                          ('accessmanager_admin_main_urls', 'accessmanager', 'Manage URLs', '', '{"route":"admin_default","module":"accessmanager","controller":"settings", "action":"urls"}', 'accessmanager_admin_main', '', 1, 0, 999);

CREATE  TABLE `engine4_accessmanager_urls` (
  `url_id` INT NOT NULL AUTO_INCREMENT ,
  `url` VARCHAR(255) NOT NULL ,
  `route` VARCHAR(255) NOT NULL,
  `params` TEXT NOT NULL,
  `levels` TEXT NOT NULL,
  `exclude_all` TINYINT(1) NOT NULL DEFAULT 0,
  `exceptions` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`url_id`) ,
  UNIQUE INDEX `url_UNIQUE` (`url` ASC) )
ENGINE = InnoDB;