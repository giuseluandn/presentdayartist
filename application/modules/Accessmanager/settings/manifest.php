<?php

return array(
    'package' =>
    array(
        'type' => 'module',
        'name' => 'accessmanager',
        'version' => '4.7.0p1',
        'path' => 'application/modules/Accessmanager',
        'title' => 'Access Manager',
        'description' => '',
        'author' => 'WebHive Team',
        'callback' =>
        array(
            'path' => 'application/modules/Accessmanager/settings/install.php',
            'class' => 'Acсessmanager_Installer',
        ),
        'actions' =>
        array(
            0 => 'install',
            1 => 'upgrade',
            2 => 'refresh',
            3 => 'enable',
            4 => 'disable',
        ),
        'directories' =>
        array(
            0 => 'application/modules/Accessmanager',
        ),
        'files' =>
        array(
            0 => 'application/languages/en/accessmanager.csv',
        ),
    ),
    'items' => array(
        'accessmanager_url',
    ),
    'routes' => array(
        'noaccess' => array(
            'route' => 'no-access',
            'defaults' => array(
                'module' => 'accessmanager',
                'controller' => 'index',
                'action' => 'index'
            )
        ),
    ),
);
?>