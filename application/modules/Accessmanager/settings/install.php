<?php

class Acсessmanager_Installer extends Engine_Package_Installer_Module {

    function onInstall() {
        $this->_addPage();
        parent::onInstall();
    }

    protected function _addPage() {
        $db = $this->getDb();

        // Check if it's already been placed
        $select = new Zend_Db_Select($db);
        $hasPage = $select
                ->from('engine4_core_pages', new Zend_Db_Expr('TRUE'))
                ->where('name = ?', 'accessmanager_index_index')
                ->limit(1)
                ->query()
                ->fetchColumn()
        ;

        // Add it
        if (empty($hasPage)) {

            $db->insert('engine4_core_pages', array(
                'name' => 'accessmanager_index_index',
                'displayname' => 'Access Manager Error Page',
                'title' => 'Access Manager Error Page',
                'description' => 'This is the error page for authorized users.',
                'custom' => 0,
            ));
            $page_id = $db->lastInsertId('engine4_core_pages');

            // containers
            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'container',
                'name' => 'main',
                'parent_content_id' => null,
                'order' => 1,
                'params' => '',
            ));
            $container_id = $db->lastInsertId('engine4_core_content');

            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'container',
                'name' => 'middle',
                'parent_content_id' => $container_id,
                'order' => 3,
                'params' => '',
            ));
            $middle_id = $db->lastInsertId('engine4_core_content');

            // middle column
            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'widget',
                'name' => 'core.html-block',
                'parent_content_id' => $middle_id,
                'order' => 2,
                'params' => '{"max":"6"}',
            ));
            //HTML Widget
            $db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'widget',
                'name' => 'core.html-block',
                'parent_content_id' => $middle_id,
                'order' => 1,
                'params' => Zend_Json_Encoder::encode(array(
                    'title' => '',
                    'data' => '<div class="tip"><span>Your account level does not have access to this page.</span></div>',
                    'name' => 'core.html-block',
                    'nomobile' => 0,
                )),
            ));
        }
    }

}