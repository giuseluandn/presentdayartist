<?php

class Accessmanager_Model_Url extends Core_Model_Item_Abstract {

    protected $_searchTriggers = false;

    public function getHref() {

        $route = $this->route;
        $reset = true;
        return Zend_Controller_Front::getInstance()->getRouter()
                        ->assemble($this->params, $route, $reset);
    }

    public function allowedToView($viewer) {
        // Check if empty
        if (empty($this->levels))
            return false;

        // Check if not array
        $allowedLevels = Zend_Json::decode($this->levels);
        if (!is_array($allowedLevels))
            return false;

        // set up current $viewer's level_id
        if (!empty($viewer->level_id))
            $level_id = $viewer->level_id;
        else
            $level_id = Engine_Api::_()->getDbtable('levels', 'authorization')->getPublicLevel()->level_id;
        
        if ($level_id == 1)
            return true;

        // Check if allowed
        if (in_array($level_id, $allowedLevels))
            return true;
        else
            return false;
    }

}