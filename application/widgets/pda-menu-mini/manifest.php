<?php
return array(
  'package' => array(
    'type' => 'widget',
    'name' => 'pda-menu-mini',
    'version' => '4.01',
    'revision' => '',
    'path' => 'application/widgets/pda-menu-mini',
    'repository' => '',
    'title' => 'PresentDayArtist Menu Mini',
    'description' => 'Shows the menu mini in the header.',
    'author' => 'meomimi061084@gmail.com',
    'directories' => array(
      'application/widgets/pda-menu-mini',
    ),
  ),

  // Backwards compatibility
  'type' => 'widget',
  'name' => 'pda-menu-mini',
  'version' => '4.01',
  'revision' => '',
  'title' => 'PresentDayArtist Menu Mini',
  'description' => 'Shows the menu mini in the header.',
  'author' => 'meomimi061084@gmail.com',
  'category' => 'Widgets',
  'description' => 'Shows your site-wide main logo or title.  Images are uploaded via the <a href="admin/files" target="_parent">File Media Manager</a>.',
  'adminForm' => 'Core_Form_Admin_Widget_Logo',
  'requirements' => array(
    'header-footer',
  ),
) ?>