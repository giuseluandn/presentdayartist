<?php
/**
 * SocialEngine
 *
 * @category   Application_Theme
 * @package    Present Day Artist
 * @copyright  
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl $
 */
?>

<div class="pda_menu_mini_container">
	<?php if( $this->viewer->getIdentity()) :?>
	<div class="menuLogo loggedIn">
	<?php //$title = Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->translate('_SITE_TITLE')) ?>
	<?php //$route = $this->viewer()->getIdentity()
				 //? array('route'=>'user_general', 'action'=>'home')
				 //: array('route'=>'default'); ?>
	<?php //echo $this->htmlLink($route, "<img class='logo' src='" . $this->baseUrl('application/themes/clean/images/logo.png') . "'>")?>


	<?php

    $module_name = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
    $action_name = Zend_Controller_Front::getInstance()->getRequest()->getActionName();

	// Logo
	$title = Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->translate('_SITE_TITLE'));
	$logo  = $this->logo;
	$route = $this->viewer()->getIdentity()
				 ? array('route'=>'user_general', 'action'=>'home')
				 : array('route'=>'default');

	echo ($logo)
		 ? $this->htmlLink($route, $this->htmlImage($logo, array('alt'=>$title)))
		 : $this->htmlLink($route, $title);
	
	?>	

	</div>	
	<div id='core_menu_mini_menu'>
	  <?php
		// Reverse the navigation order (they're floating right)
		$count = count($this->navigation);
		foreach( $this->navigation->getPages() as $item ) $item->setOrder(--$count);
	  ?>
	  <ul class="minimenu_left"> 
		<?php if($this->search_check):?>
		  <li id="global_search_form_container" class="minimenu_e">
			<form id="global_search_form" action="<?php echo $this->url(array('controller' => 'search'), 'default', true) ?>" method="get">
			  <input type='text' class='text suggested' name='query' id='global_search_field' size='20' maxlength='100' alt='<?php echo $this->translate('Search') ?>' />
			</form>
		  </li>
		<?php endif;?>
		<li class="custom_menu_main minimenu_e">
            <ul id="navigation">
                <?php $s = 0;
                $total = 0;
                $more = array();
                foreach ($this->navigation as $item): ?>
                    <?php
                    $label = $item->getLabel();
                    $class = $item->getClass();
                    if ($s < $this->menucount) {
                        // We have to this manually
                        if ((strstr(strtolower($label), $module_name) != "") || ($module_name == "core" && $label == "Home") || ($module_name == "user" && $label == "Home" && $action_name == "home") || ($module_name == "user" && $label == "Members" && $action_name == "browse")) {
                            ?>

                            <li class="active">
                                <a href="<?php echo $item->getHref(); ?>" class="<?php echo $class; ?>"><span><?php echo $this->translate($item->getLabel()); ?></span></a>
                            </li>
                            <?php
                            $total += 1;
                            $s += 1;
                        } else {
                            ?>
                            <li>
                                <a href="<?php echo $item->getHref(); ?>" class="<?php echo $class; ?>"><span><?php echo $this->translate($item->getLabel()); ?></span></a>
                            </li>
                            <?php
                            $s += 1;
                        }
                    } else {
                        if (strstr(strtolower($label), $module_name) != "") {
                            $more[$s] = "<li class='active'><a href='" . $item->getHref() . "' class='" . $class . "'><span>" . $this->translate($item->getLabel()) . "</span></a></li>";
                            $s += 1;
                        } else {
                            $more[$s] = "<li><a href='" . $item->getHref() . "' class='" . $class . "'><span>" . $this->translate($item->getLabel()) . "</span></a></li>";
                            $s += 1;
                        }
                    }
                    ?>
                    <?php
                endforeach;

                if ($s > $this->menucount) {
                    ?>
                    <li class="submenu_explore">
                        <a href="javascript:void(0);">
						  <span>
							  <?php
							  if (empty($this->menuname)) {
								  echo $this->translate('Explore');
							  } else {
								  echo $this->translate($this->menuname);
							  }
							  ?>
							  <span class="submenu_explore_plus">+</span>
						  </span>
                        </a>
                        <ul>
                            <?php
                            foreach ($more as $more_item) {
                                echo $more_item;
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>
		</li>
		<div class="clear"></div>
	  </ul>
	  <ul class="minimenu_right">
		<li class="ava_profile">
			<?php echo $this->htmlLink($this->viewer()->getHref(), $this->itemPhoto($this->viewer(), 'thumb.profile')) ?>	
		</li>
		
		<li id='core_menu_mini_menu_update' class="icon_type_pulldown menuUpdate_pulldown">
		  <span onclick="toggleUpdatesPulldown(event, this, '4');" style="display: inline-block;" class="updates_pulldown">
			<a href="javascript:void(0);" id="updates_toggle" <?php if( $this->notificationCount ):?> class="new_updates"<?php endif;?>>
				<span class="pulldown_toggle_icon updates_toggle_icon"></span>
				<span id="updates_span"><?php echo $this->notificationCount; ?></span>
			</a>			
			<div class="pulldown_contents_wrapper">
			  <div class="pulldown_contents">
				<h3><?php echo $this->translate("My Notifications") ?><a href="javascript:void(0);" class="toggle_closer">×</a></h3>
				<ul class="notifications_menu" id="notifications_menu">
				  <div class="notifications_loading" id="notifications_loading">
					<img src='<?php echo $this->layout()->staticBaseUrl ?>application/modules/Core/externals/images/loading.gif' style='float:left; margin-right: 5px;' />
					<?php echo $this->translate("Loading ...") ?>
				  </div>
				</ul>
			  </div>
			  <div class="pulldown_options">
				<?php echo $this->htmlLink(array('route' => 'default', 'module' => 'activity', 'controller' => 'notifications'),
				   $this->translate('View All Updates'),
				   array('id' => 'notifications_viewall_link')) ?>				 
				<?php echo $this->htmlLink('javascript:void(0);', $this->translate('Mark All Read'), array(
				  'id' => 'notifications_markread_link',
				)) ?>
				<div class="clear"></div>
			  </div>
			</div>
		  </span>
		</li>
		
		<li id='menusettings_pulldown' class="icon_type_pulldown menusettings_pulldown">		
		  <span onclick="toggleSettingsPulldown(event, this, '4');" style="display: inline-block;" class="settings_pulldown">			
			<a href="javascript:void(0);" id="settings_toggle">
                <span class="pulldown_toggle_icon settings_toggle_icon"></span>
				<span><?php echo $this->translate("Account") ?></span>
            </a> 
			<div class="pulldown_contents_wrapper">
			  <div class="pulldown_contents">
				<div class="pulldown_contents">
					<h3><?php echo $this->translate('My Account') ?><a href="javascript:void(0);" class="toggle_closer">×</a></h3>
					<ul>
						<?php foreach( $this->navigation_mini as $item ): ?>
						  <li><?php echo $this->htmlLink($item->getHref(), $this->translate($item->getLabel()), array_filter(array(
							'class' => ( !empty($item->class) ? $item->class : null ),
							'alt' => ( !empty($item->alt) ? $item->alt : null ),
							'target' => ( !empty($item->target) ? $item->target : null ),
						  ))) ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			  </div>
			</div>
		  </span>
		</li>
		<div class="clear"></div>
	  </ul>
	  <div class="clear"></div>
	</div>
	<?php else: ?>
	<div class="menuLogo logo_no_login">
	<?php //$title = Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->translate('_SITE_TITLE')) ?>
	<?php //$route = $this->viewer()->getIdentity()
				 //? array('route'=>'user_general', 'action'=>'home')
				 //: array('route'=>'default'); ?>
	<?php //echo $this->htmlLink($route, "<img class='logo' src='" . $this->baseUrl('application/themes/clean/images/logo.png') . "'>")?>


	<?php
	// Logo
	$title = Engine_Api::_()->getApi('settings', 'core')->getSetting('core_general_site_title', $this->translate('_SITE_TITLE'));
	$logo  = $this->logo;
	$route = $this->viewer()->getIdentity()
				 ? array('route'=>'user_general', 'action'=>'home')
				 : array('route'=>'default');

	echo ($logo)
		 ? $this->htmlLink($route, $this->htmlImage($logo, array('alt'=>$title)))
		 : $this->htmlLink($route, $title);
	
	?>	

	</div>	
	<!--
	<a href="/signup" class="header_signup_btn"> <?php //echo $this->translate("Sign Up"); ?></a>
	-->
	<div class="minilogin">
		<div class="form_mini_login">
			<!-- CORE - USER FORM LOGIN  -->
			<?php if( !$this->noForm ): ?>
				<?php echo $this->form->setAttrib('class', 'global_form_box')->render($this) ?>
				<?php if( !empty($this->fbUrl) ): ?>
					<script type="text/javascript">
					  var openFbLogin = function() {
						Smoothbox.open('<?php echo $this->fbUrl ?>');
					  }
					  var redirectPostFbLogin = function() {
						window.location.href = window.location;
						Smoothbox.close();
					  }
					</script>
					<?php // <button class="user_facebook_connect" onclick="openFbLogin();"></button> ?>
				<?php endif; ?>			
			<?php endif; ?> 
			<!-- END CORE - USER FORM LOGIN  -->
		</div>
	</div>
	<?php endif; ?>
	<div class="clear"></div>
</div>

<script type='text/javascript'>
  var toggleSettingsPulldown = function(event, element, user_id) {
    if( element.className=='settings_pulldown' ) {
      element.className= 'settings_pulldown_active';
      showNotifications();
    } else {
      element.className='settings_pulldown';
    }
  }


  var notificationUpdater;

  en4.core.runonce.add(function(){
    if($('global_search_field')){
      new OverText($('global_search_field'), {
        poll: true,
        pollInterval: 500,
        positionOptions: {
          position: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          edge: ( en4.orientation == 'rtl' ? 'upperRight' : 'upperLeft' ),
          offset: {
            x: ( en4.orientation == 'rtl' ? -4 : 4 ),
            y: 2
          }
        }
      });
    }

    if($('notifications_markread_link')){
      $('notifications_markread_link').addEvent('click', function() {
        //$('notifications_markread').setStyle('display', 'none');
        en4.activity.hideNotifications('<?php echo $this->string()->escapeJavascript($this->translate("0 Updates"));?>');
      });
    }

    <?php if ($this->updateSettings && $this->viewer->getIdentity()): ?>
    notificationUpdater = new NotificationUpdateHandler({
              'delay' : <?php echo $this->updateSettings;?>
            });
    notificationUpdater.start();
    window._notificationUpdater = notificationUpdater;
    <?php endif;?>
  });


  var toggleUpdatesPulldown = function(event, element, user_id) {
    if( element.className=='updates_pulldown' ) {
      element.className= 'updates_pulldown_active';
      showNotifications();
    } else {
      element.className='updates_pulldown';
    }
  }

  var showNotifications = function() {
    en4.activity.updateNotifications();
    new Request.HTML({
      'url' : en4.core.baseUrl + 'activity/notifications/pulldown',
      'data' : {
        'format' : 'html',
        'page' : 1
      },
      'onComplete' : function(responseTree, responseElements, responseHTML, responseJavaScript) {
        if( responseHTML ) {
          // hide loading icon
          if($('notifications_loading')) $('notifications_loading').setStyle('display', 'none');

          $('notifications_menu').innerHTML = responseHTML;
          $('notifications_menu').addEvent('click', function(event){
            event.stop(); //Prevents the browser from following the link.

            var current_link = event.target;
            var notification_li = $(current_link).getParent('li');

            // if this is true, then the user clicked on the li element itself
            if( notification_li.id == 'core_menu_mini_menu_update' ) {
              notification_li = current_link;
            }

            var forward_link;
            if( current_link.get('href') ) {
              forward_link = current_link.get('href');
            } else{
              forward_link = $(current_link).getElements('a:last-child').get('href');
            }

            if( notification_li.get('class') == 'notifications_unread' ){
              notification_li.removeClass('notifications_unread');
              en4.core.request.send(new Request.JSON({
                url : en4.core.baseUrl + 'activity/notifications/markread',
                data : {
                  format     : 'json',
                  'actionid' : notification_li.get('value')
                },
                onSuccess : function() {
                  window.location = forward_link;
                }
              }));
            } else {
              window.location = forward_link;
            }
          });
        } else {
          $('notifications_loading').innerHTML = '<?php echo $this->string()->escapeJavascript($this->translate("You have no new updates."));?>';
        }
      }
    }).send();
  };

  /*
  function focusSearch() {
    if(document.getElementById('global_search_field').value == 'Search') {
      document.getElementById('global_search_field').value = '';
      document.getElementById('global_search_field').className = 'text';
    }
  }
  function blurSearch() {
    if(document.getElementById('global_search_field').value == '') {
      document.getElementById('global_search_field').value = 'Search';
      document.getElementById('global_search_field').className = 'text suggested';
    }
  }
  */
</script>