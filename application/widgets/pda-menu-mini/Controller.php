<?php
/**
 * SocialEngine
 */

/** 
 * @copyright
 */
class Widget_PdaMenuMiniController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
	//Logo
	$this->view->logo = $this->_getParam('logo');  
	
	//if(Engine_Api::_()->user()->getViewer()->getIdentity()){
		//return $this->setNoRender();
	//}
	
	//LOGIN FORM
    // Display form
    $form = $this->view->form = new User_Form_Login(array(
      'mode' => 'column',
    ));;
    $form->setTitle(null)->setDescription(null);
    //$form->removeElement('forgot');

    // Facebook login
    if( 'none' == Engine_Api::_()->getApi('settings', 'core')->core_facebook_enable ) {
      $form->removeElement('facebook');
    }
    
    // Check for recaptcha - it's too fat
    $this->view->noForm = false;
    if( ($captcha = $form->getElement('captcha')) instanceof Zend_Form_Element_Captcha && 
        $captcha->getCaptcha() instanceof Zend_Captcha_ReCaptcha ) {
      $this->view->noForm = true;
      $form->removeElement('email');
      $form->removeElement('password');
      $form->removeElement('captcha');
      $form->removeElement('submit');
      $form->removeElement('remember');
//      $form->removeElement('facebook');
//      $form->removeElement('twitter');
      $form->removeDisplayGroup('buttons');
    }	
	
  
	// MENU MINI
	$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();

    $require_check = Engine_Api::_()->getApi('settings', 'core')->core_general_search;
    if(!$require_check){
      if( $viewer->getIdentity()){
        $this->view->search_check = true;
      }
      else{
        $this->view->search_check = false;
      }
    }
    else $this->view->search_check = true;

    $this->view->navigation = $navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('core_mini');

    if( $viewer->getIdentity() )
    {
      $this->view->notificationCount = Engine_Api::_()->getDbtable('notifications', 'activity')->hasNotifications($viewer);
    }

    $request = Zend_Controller_Front::getInstance()->getRequest();
    $this->view->notificationOnly = $request->getParam('notificationOnly', false);
    $this->view->updateSettings = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.notificationupdate');
	
	
	// MENU MAIN
    $this->view->navigation = $navigation = Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation('core_main');

    $this->view->menucount = 5;
    $this->view->menuname = 'Features';
    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    $require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.browse', 1);
    if(!$require_check && !$viewer->getIdentity()){
      $navigation->removePage($navigation->findOneBy('route','user_general'));
    }

    $this->view->navigation_mini = $navigation = Engine_Api::_()
        ->getApi('menus', 'core')
        ->getNavigation('core_mini');
  }
  public function getCacheKey()
  {
    return ;
    $viewer = Engine_Api::_()->user()->getViewer();
    $viewer_id = $viewer->getIdentity();
    if (!$viewer_id) {
      return parent::getCacheKey();
    }
    $translate = Zend_Registry::get('Zend_Translate');
    return $viewer_id . $translate->getLocale() . sprintf('%d', $viewer->photo_id);
  }  
}